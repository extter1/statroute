ALTER TABLE subscription ADD COLUMN createdat TIMESTAMPTZ DEFAULT NOW();
UPDATE subscription set createdat = created_at;
ALTER TABLE subscription DROP COLUMN IF EXISTS created_at;