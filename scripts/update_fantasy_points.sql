DO $$ 
<<first_block>>
DECLARE 
	PassingYards_Factor numeric := 0.25;
	RushingYards_Factor numeric := 0.10;
	ReceivingYards_Factor numeric := 0.20;
	PassingTouchdowns_Factor numeric := 6.0;
	RushingTouchdowns_Factor numeric := 6.0;
	ReceivingTouchdowns_Factor numeric := 6.0;
	Receptions_Factor numeric := 1.0;
	twopointconversionX_Factor numeric := 2.0;
	passingInterceptions_Factor numeric := -2.0;
	Fumbleslost_Factor numeric := -2.0;
	Fumblereturntouchdowns_Factor numeric := 6.0;
	Kickreturnyards_Factor numeric := 0.15;
	PuntReturnYards_Factor numeric := 0.15;
	puntreturntouchdowns_Factor numeric := 6.0;
	KickReturnTouchdowns_Factor numeric := 6.0;
	Extrapointsmade_Factor numeric := 1.0;
	fieldgoalsmade50plus_Factor numeric := 5.0;
	fieldgoalsmade40to49_Factor numeric := 4.0;
	fieldgoalsmade0to19_Factor numeric := 3.0;
	fieldgoalsmade20to29_Factor numeric := 3.0;
	fieldgoalsmade30to39_Factor numeric := 3.0;
	
	resultVal BIGINT;
  begin
	
  update nflplayer set fantasypoints = ( (PassingYards/(PassingYards_Factor))+(RushingYards/(RushingYards_Factor))+(ReceivingYards/(ReceivingYards_Factor))+(PassingTouchdowns*PassingTouchdowns_Factor)+(RushingTouchdowns*RushingTouchdowns_Factor)+(ReceivingTouchdowns*ReceivingTouchdowns_Factor)+(Receptions*Receptions_Factor)+(twopointconversionpasses*twopointconversionX_Factor)+(twopointconversionruns*twopointconversionX_Factor)+(twopointconversionreceptions*twopointconversionX_Factor)+(twopointconversionreturns*twopointconversionX_Factor)+(passingInterceptions*passingInterceptions_Factor)+(Fumbleslost*Fumbleslost_Factor)+(Fumblereturntouchdowns*Fumblereturntouchdowns_Factor)+(Kickreturnyards/(Kickreturnyards_Factor))+(PuntReturnYards/(PuntReturnYards_Factor))+(puntreturntouchdowns*puntreturntouchdowns_Factor)+(KickReturnTouchdowns*KickReturnTouchdowns_Factor)+(Extrapointsmade*Extrapointsmade_Factor)+(fieldgoalsmade50plus*fieldgoalsmade50plus_Factor)+(fieldgoalsmade40to49*fieldgoalsmade40to49_Factor)+(fieldgoalsmade0to19*fieldgoalsmade0to19_Factor)+(fieldgoalsmade20to29*fieldgoalsmade20to29_Factor)+(fieldgoalsmade30to39*fieldgoalsmade30to39_Factor) );
  
  commit;
  
END first_block $$;
