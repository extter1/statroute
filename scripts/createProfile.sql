CREATE TABLE public.profilemaster(
	profileid integer PRIMARY KEY,
	paramName character varying(40),
	description character varying(150),
	defaultValue real DEFAULT 0
);

CREATE TABLE public.leaguemaster (
	leagueid integer PRIMARY KEY,
	name character varying(50),
	description character varying(150)
);

CREATE TABLE public.profileassociation (
	associationid integer PRIMARY KEY,
	userid integer REFERENCES srusers(userid),	
	profileid integer REFERENCES profilemaster(profileid),
	leagueid integer REFERENCES leaguemaster(leagueid) DEFAULT 1,
	assnValue real DEFAULT 0		
);

CREATE SEQUENCE public.leaguemaster_leagueid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.profilemaster_profileid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE ONLY public.profilemaster ALTER COLUMN profileid SET DEFAULT nextval('public.profilemaster_profileid_seq'::regclass);

ALTER TABLE ONLY public.leaguemaster ALTER COLUMN leagueid SET DEFAULT nextval('public.leaguemaster_leagueid_seq'::regclass);

CREATE SEQUENCE public.profileassociation_associationid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE ONLY public.profileassociation ALTER COLUMN associationid SET DEFAULT nextval('public.profileassociation_associationid_seq'::regclass);

insert into profilemaster(paramname,description,defaultvalue) values('rush yards','Rush Yards',10);
insert into profilemaster(paramname,description,defaultvalue) values('receiving yards','Receiving Yards',20);
insert into profilemaster(paramname,description,defaultvalue) values('pass yards','Pass Yards',25);
insert into profilemaster(paramname,description,defaultvalue) values('passing touchdowns','Passing Touchdowns',6);
insert into profilemaster(paramname,description,defaultvalue) values('receptions','Receptions',0.5);
insert into profilemaster(paramname,description,defaultvalue) values('qb Interceptions','QB Interceptions',-2);

insert into leaguemaster(name,description) values('Default','Default league');
