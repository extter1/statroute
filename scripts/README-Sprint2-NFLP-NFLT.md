Players 2017
==========
	
	DROP ffplayers2017		
	Run sprint2ffplayer2017.sql	
	Load data into ffplayers2017 (Java Program) 

	Run sprint2PlayerAlter-ADD.sql	-	
	Load data into nflplayer, nflprz (Java Program) - 
	UPDATE nflplayer SET ffp2017 = true FROM ffplayers2017 WHERE ffplayers2017.playerid = nflplayer.playerid; 
	UPDATE nflprz SET ffp2017 = true FROM ffplayers2017 WHERE ffplayers2017.playerid = nflprz.playerid; 
	update nflplayer set currentteam = team where season='2017';	
	update nflprz set currentteam = team where season='2017';		
	Run sprint2-nflplayer-Alter-DROP.sql			
	Run sprint2-nflprz-Alter-DROP.sql				
	 \i ./sprint2PlayerMatView-REFRESH.sql 			
	 
Team 2017
========
	Load data to nflteam,nflteamrz (Java Program)	
	Run sprint2TeamAlter-ADD.sql		
	Run sprint2TeamAlter-DROP.sql		
	\i ./sprint2TeamMatView-REFRESH.sql	