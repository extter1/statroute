import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
/*
 * !!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!
 * 
 * Quick dirty program to read excel and update nflteam nflteamrz.
 * This is not a reusable program. Created only for the dataloading and this 
 * is tightly coupled with the excel document structure shared by SR
 *
 * Author: Mahesh S.R
 * 
 * */


public class StatExcelReader {
    //public static final String SAMPLE_XLS_FILE_PATH = "./sample-xls-file.xls";
    public static final String SAMPLE_XLSX_FILE_PATH = "/home/maheshsr/Documents/Extter/Sprint2-Apr-2018/SPRINT2_nflteamPOI.xlsx";
    
    //public static final String teamInsertSQL = "INSERT INTO nflteam (gamedate,seasontype,season,week,team,opponent,homeoraway,score,opponentscore,totalscore,stadium,grassorturf,temperature,windspeed,score1q,score2q,score3q,score4q,scoreovt,timeofpossessionminutes,timeofpossessionseconds,timeofpossession,firstdowns,firstdownsbyrushing,firstdownsbypassing,firstdownsbypenalty,offensiveplays,offensiveyards,offensiveyardsperplay,touchdowns,rushingattempts,rushingyards,rushingyardsperattempt,rushingtouchdowns,passingattempts,passingcompletions,passingyards,passingtouchdowns,passinginterceptions,passingyardsperattempt,passingyardspercompletion,completionpercentage,passerrating,thirddownattempts,thirddownconversions,thirddownpercentage,fourthdownattempts,fourthdownconversions,fourthdownpercentage,redzoneattempts,redzoneconversions,goaltogoattempts,goaltogoconversions,returnyards,penalties,penaltyyards,fumbles,fumbleslost,timessacked,timessackedyards,quarterbackhits,tacklesforloss,safeties,punts,puntyards,puntaverage,giveaways,takeaways,turnoverdifferential,opponentscore1q,opponentscore2q,opponentscore3q,opponentscore4q,opponentscoreovt,opponenttimeofpossessionminutes,opponenttimeofpossessionseconds,opponenttimeofpossession,opponentfirstdowns,opponentfirstdownsbyrushing,opponentfirstdownsbypassing,opponentfirstdownsbypenalty,opponentoffensiveplays,opponentoffensiveyards,opponentoffensiveyardsperplay,opponenttouchdowns,opponentrushingattempts,opponentrushingyards,opponentrushingyardsperattempt,opponentrushingtouchdowns,opponentpassingattempts,opponentpassingcompletions,opponentpassingyards,opponentpassingtouchdowns,opponentpassinginterceptions,opponentpassingyardsperattempt,opponentpassingyardspercompletion,opponentcompletionpercentage,opponentpasserrating,opponentthirddownattempts,opponentthirddownconversions,opponentthirddownpercentage,opponentfourthdownattempts,opponentfourthdownconversions,opponentfourthdownpercentage,opponentredzoneattempts,opponentredzoneconversions,opponentgoaltogoattempts,opponentgoaltogoconversions,opponentreturnyards,opponentpenalties,opponentpenaltyyards,opponentfumbles,opponentfumbleslost,opponenttimessacked,opponenttimessackedyards,opponentquarterbackhits,opponenttacklesforloss,opponentsafeties,opponentpunts,opponentpuntyards,opponentpuntaverage,opponentgiveaways,opponenttakeaways,opponentturnoverdifferential,redzonepercentage,goaltogopercentage,opponentredzonepercentage,opponentgoaltogopercentage,puntshadblocked,puntnetaverage,extrapointkickingattempts,extrapointkickingconversions,extrapointshadblocked,extrapointpassingattempts,extrapointpassingconversions,extrapointrushingattempts,extrapointrushingconversions,fieldgoalattempts,fieldgoalsmade,fieldgoalshadblocked,puntreturns,puntreturnyards,kickreturns,kickreturnyards,interceptionreturns,interceptionreturnyards,opponentpuntshadblocked,opponentpuntnetaverage,opponentextrapointkickingattempts,opponentextrapointkickingconversions,opponentextrapointshadblocked,opponentextrapointpassingattempts,opponentextrapointpassingconversions,opponentextrapointrushingattempts,opponentextrapointrushingconversions,opponentfieldgoalattempts,opponentfieldgoalsmade,opponentfieldgoalshadblocked,opponentpuntreturns,opponentpuntreturnyards,opponentkickreturns,opponentkickreturnyards,opponentinterceptionreturns,opponentinterceptionreturnyards,solotackles,assistedtackles,sacks,sackyards,passesdefended,fumblesforced,fumblesrecovered,fumblereturnyards,fumblereturntouchdowns,interceptionreturntouchdowns,blockedkicks,puntreturntouchdowns,puntreturnlong,kickreturntouchdowns,kickreturnlong,blockedkickreturnyards,blockedkickreturntouchdowns,fieldgoalreturnyards,fieldgoalreturntouchdowns,puntnetyards,opponentsolotackles,opponentassistedtackles,opponentsacks,opponentsackyards,opponentpassesdefended,opponentfumblesforced,opponentfumblesrecovered,opponentfumblereturnyards,opponentfumblereturntouchdowns,opponentinterceptionreturntouchdowns,opponentblockedkicks,opponentpuntreturntouchdowns,opponentpuntreturnlong,opponentkickreturntouchdowns,opponentkickreturnlong,opponentblockedkickreturnyards,opponentblockedkickreturntouchdowns,opponentfieldgoalreturnyards,opponentfieldgoalreturntouchdowns,opponentpuntnetyards,teamname,gamedayofweek,passingdropbacks,opponentpassingdropbacks,twopointconversionreturns,opponenttwopointconversionreturns,teamid,opponentid,dayofgame,datetime,globalopponentid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    public static final String teamInsertSQL = "INSERT INTO nflteamrz (gamedate,seasontype,season,week,team,opponent,homeoraway,score,opponentscore,totalscore,stadium,grassorturf,temperature,windspeed,score1q,score2q,score3q,score4q,scoreovt,timeofpossessionminutes,timeofpossessionseconds,timeofpossession,firstdowns,firstdownsbyrushing,firstdownsbypassing,firstdownsbypenalty,offensiveplays,offensiveyards,offensiveyardsperplay,touchdowns,rushingattempts,rushingyards,rushingyardsperattempt,rushingtouchdowns,passingattempts,passingcompletions,passingyards,passingtouchdowns,passinginterceptions,passingyardsperattempt,passingyardspercompletion,completionpercentage,passerrating,thirddownattempts,thirddownconversions,thirddownpercentage,fourthdownattempts,fourthdownconversions,fourthdownpercentage,redzoneattempts,redzoneconversions,goaltogoattempts,goaltogoconversions,returnyards,penalties,penaltyyards,fumbles,fumbleslost,timessacked,timessackedyards,quarterbackhits,tacklesforloss,safeties,punts,puntyards,puntaverage,giveaways,takeaways,turnoverdifferential,opponentscore1q,opponentscore2q,opponentscore3q,opponentscore4q,opponentscoreovt,opponenttimeofpossessionminutes,opponenttimeofpossessionseconds,opponenttimeofpossession,opponentfirstdowns,opponentfirstdownsbyrushing,opponentfirstdownsbypassing,opponentfirstdownsbypenalty,opponentoffensiveplays,opponentoffensiveyards,opponentoffensiveyardsperplay,opponenttouchdowns,opponentrushingattempts,opponentrushingyards,opponentrushingyardsperattempt,opponentrushingtouchdowns,opponentpassingattempts,opponentpassingcompletions,opponentpassingyards,opponentpassingtouchdowns,opponentpassinginterceptions,opponentpassingyardsperattempt,opponentpassingyardspercompletion,opponentcompletionpercentage,opponentpasserrating,opponentthirddownattempts,opponentthirddownconversions,opponentthirddownpercentage,opponentfourthdownattempts,opponentfourthdownconversions,opponentfourthdownpercentage,opponentredzoneattempts,opponentredzoneconversions,opponentgoaltogoattempts,opponentgoaltogoconversions,opponentreturnyards,opponentpenalties,opponentpenaltyyards,opponentfumbles,opponentfumbleslost,opponenttimessacked,opponenttimessackedyards,opponentquarterbackhits,opponenttacklesforloss,opponentsafeties,opponentpunts,opponentpuntyards,opponentpuntaverage,opponentgiveaways,opponenttakeaways,opponentturnoverdifferential,redzonepercentage,goaltogopercentage,opponentredzonepercentage,opponentgoaltogopercentage,puntshadblocked,puntnetaverage,extrapointkickingattempts,extrapointkickingconversions,extrapointshadblocked,extrapointpassingattempts,extrapointpassingconversions,extrapointrushingattempts,extrapointrushingconversions,fieldgoalattempts,fieldgoalsmade,fieldgoalshadblocked,puntreturns,puntreturnyards,kickreturns,kickreturnyards,interceptionreturns,interceptionreturnyards,opponentpuntshadblocked,opponentpuntnetaverage,opponentextrapointkickingattempts,opponentextrapointkickingconversions,opponentextrapointshadblocked,opponentextrapointpassingattempts,opponentextrapointpassingconversions,opponentextrapointrushingattempts,opponentextrapointrushingconversions,opponentfieldgoalattempts,opponentfieldgoalsmade,opponentfieldgoalshadblocked,opponentpuntreturns,opponentpuntreturnyards,opponentkickreturns,opponentkickreturnyards,opponentinterceptionreturns,opponentinterceptionreturnyards,solotackles,assistedtackles,sacks,sackyards,passesdefended,fumblesforced,fumblesrecovered,fumblereturnyards,fumblereturntouchdowns,interceptionreturntouchdowns,blockedkicks,puntreturntouchdowns,puntreturnlong,kickreturntouchdowns,kickreturnlong,blockedkickreturnyards,blockedkickreturntouchdowns,fieldgoalreturnyards,fieldgoalreturntouchdowns,puntnetyards,opponentsolotackles,opponentassistedtackles,opponentsacks,opponentsackyards,opponentpassesdefended,opponentfumblesforced,opponentfumblesrecovered,opponentfumblereturnyards,opponentfumblereturntouchdowns,opponentinterceptionreturntouchdowns,opponentblockedkicks,opponentpuntreturntouchdowns,opponentpuntreturnlong,opponentkickreturntouchdowns,opponentkickreturnlong,opponentblockedkickreturnyards,opponentblockedkickreturntouchdowns,opponentfieldgoalreturnyards,opponentfieldgoalreturntouchdowns,opponentpuntnetyards,teamname,gamedayofweek,passingdropbacks,opponentpassingdropbacks,twopointconversionreturns,opponenttwopointconversionreturns,teamid,opponentid,dayofgame,datetime,globalopponentid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    // DEV
    /*
    public static final String USER = "statroutedev";
    public static final String PASSWORD = "statroutedev1";
    public static final String CONNECTIONSTRING = "jdbc:postgresql://statroutedev.cbklqhcjzvuk.us-west-2.rds.amazonaws.com/statroutedev";
    */
    // QA
    public static final String USER = "statrouteqa";
    public static final String PASSWORD = "statrouteqa";
    public static final String CONNECTIONSTRING = "jdbc:postgresql://statrouteqa.cbklqhcjzvuk.us-west-2.rds.amazonaws.com/statrouteqa";

    
    
    public static void main(String[] args) throws IOException, InvalidFormatException {

        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File(SAMPLE_XLSX_FILE_PATH));

        long start = System.currentTimeMillis();
        insertData(workbook);
        System.out.println(System.currentTimeMillis() - start);
        // Closing the workbook
        workbook.close();
    }
    
    public java.util.HashMap<String,String> generateNFLTMapping(Workbook workbook){
    	// Getting the Sheet at index zero
        Sheet sheetNFLTMapping = workbook.getSheet("NFLTMapping");

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        // 1. You can obtain a rowIterator and columnIterator and iterate over them
        System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
        Iterator<Row> rowIterator = sheetNFLTMapping.rowIterator();
        
        java.util.HashMap<String,String> mappings = new java.util.HashMap<String,String>();
        
        while (rowIterator.hasNext()) {
        	
            Row row = rowIterator.next();
            
            Cell excelColNameCell = row.getCell(0);
            Cell tableColNameCell = row.getCell(1);
            
            String excelColName = dataFormatter.formatCellValue(excelColNameCell);
            String tableColName = dataFormatter.formatCellValue(tableColNameCell);
            if(excelColName != null && !excelColName.isEmpty()) {
            	mappings.put(excelColName.toLowerCase(), tableColName);
            }
            //System.out.println(i+" -->"+excelColName);
        }
        System.out.println("Mappings Size: "+mappings.size());    	
        return mappings;
    }
    
    public static void insertData(Workbook workbook) {
    	
    
	    java.sql.Connection con = null;
	    java.sql.PreparedStatement stmt = null;
	    
	    System.out.println("Getting IMport sheet");
	    Sheet sheetImport = workbook.getSheet("IMPORT ME nflteam");
	   	 DataFormatter dataFormatter = new DataFormatter();
	   	 
	   	 Iterator<Row> rowIterator = sheetImport.rowIterator();
	   	 
	   	 //java.text.SimpleDateFormat dateFormat  = new java.text.SimpleDateFormat("dd/MM/yy");
	 	DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("d/MM/yy");
	 	
	   	 java.util.Properties props = new java.util.Properties();
	   	 props.setProperty("user", USER);
	   	 props.setProperty("password", PASSWORD);
	   	 props.setProperty("stringtype", "unspecified");
		    System.out.println("Getting Connection");

	   	 try {
		   	 Class.forName("org.postgresql.Driver");
		   	 con = DriverManager.getConnection(CONNECTIONSTRING,props);
		   	System.out.println("Got Connection");
		   	 stmt = con.prepareStatement(teamInsertSQL);
		   	
		   	 int i = 0;
		   	 int count = 0;
		   	 while (rowIterator.hasNext()) {
		        	
		            Row row = rowIterator.next();
		            
		            if(i == 0) {
		           	 	i++;
		            	continue;
		           	 
		            }else {
		            	count++;
		            	 Iterator<Cell> cellIterator = row.cellIterator();
		            	 int index = 1;
		                 while (cellIterator.hasNext()){
		                     Cell cell = cellIterator.next();
		                     String cellValue = dataFormatter.formatCellValue(cell);
		                     
		                     if(index == 1 || index == 213 || index == 214   ) {
		                    	 String[] arr = cellValue.split(" ");
		                    	 cellValue = arr[0];
		                    	 LocalDate localDate = LocalDate.parse(cellValue, dateFormat);
		                    	 
		                    	 stmt.setObject(index, localDate);
		                     }else {
		                    	 stmt.setString(index, cellValue);
		                     }
		                     index++;
		                 }
		                
		                 // TODO: Make it batch update
		                 int updResult = stmt.executeUpdate();
		                 System.out.println(count+" Updated Result: "+updResult);
		                 
		            }
		            
		     }
	   	 }catch(Exception e) {
	   		 e.printStackTrace();
	   	 }finally {
	   		 
	   		 try {
				stmt.close();
				con.close();
				System.out.println("CLosed resources");
			} catch (Exception e) {
				e.printStackTrace();
			}
	   		 
	   	 }
   	
   }    
    public static void insertDataGenSQL(Workbook workbook,java.util.HashMap<String,String> mappings) {
    	
    	
    	 Sheet sheetImport = workbook.getSheet("IMPORT ME nflteam");
    	 DataFormatter dataFormatter = new DataFormatter();
    	 
    	 Iterator<Row> rowIterator = sheetImport.rowIterator();
    	 
    	 StringBuffer prepSQL = new StringBuffer();
    	 prepSQL.append("INSERT INTO nflteam (");
    	 
    	 StringBuffer valuesPart = new StringBuffer();
    	 valuesPart.append(" VALUES (");
/*    	 
    	 INSERT INTO table(column1, column2, …)
    	 VALUES
    	  (value1, value2, …);
 */   	 int i = 0;
    	 while (rowIterator.hasNext()) {
         	
             Row row = rowIterator.next();
             
             if(i == 0) {
            	 
            	 Iterator<Cell> cellIterator = row.cellIterator();

                 while (cellIterator.hasNext()) {
                     Cell cell = cellIterator.next();
                     String cellValue = dataFormatter.formatCellValue(cell);
                     prepSQL.append(mappings.get(cellValue.toLowerCase()));
                     prepSQL.append(",");
                     
                     valuesPart.append("?,");
                     
                 }
                 valuesPart.deleteCharAt(valuesPart.length()-1);
                 valuesPart.append(")");
                 
                 prepSQL.deleteCharAt(prepSQL.length()-1);
                 prepSQL.append(")");
                 
                 prepSQL.append(valuesPart.toString());
                 
                 System.out.println(prepSQL.toString());
                 break;
            	 
             }else {
            	 
             }
             i++;
             //System.out.println(i+" -->"+excelColName);
         }    	
    	
    }
}
