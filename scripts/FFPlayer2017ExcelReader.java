import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;

import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.Types;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Iterator;
/*
 * !!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!
 * 
 * Quick dirty program to read excel and update nflteam nflteamrz.
 * This is not a reusable program. Created only for the dataloading and this 
 * is tightly coupled with the excel document structure shared by SR
 *
 * Author: Mahesh S.R
 * 
 * */


public class FFPlayer2017ExcelReader {
    //public static final String SAMPLE_XLS_FILE_PATH = "./sample-xls-file.xls";
    public static final String SAMPLE_XLSX_FILE_PATH = "/home/maheshsr/Documents/Extter/Sprint2-Apr-2018/ffplayers2017.xlsx";
    
    public static final String ffPlayer017InsertSQL = "INSERT INTO public.ffplayers2017 (playerid,team,pnumber,firstname,lastname,playerposition,status,height,weight,birthdate,college,experience,fantasyposition,active,positioncategory,name,age,experiencestring,birthdatestring,photourl,byeweek,upcominggameopponent,upcominggameweek,shortname,depthpositioncategory,depthposition,depthorder,depthdisplayorder,currentteam,collegedraftteam,collegedraftyear,collegedraftround,collegedraftpick,isundraftedfreeagent,heightfeet,heightinches,upcomingopponentrank,upcomingopponentpositionrank,currentstatus,fantasyalarmplayerid,sportradarplayerid,rotoworldplayerid,rotowireplayerid,statsplayerid,sportsdirectplayerid,xmlteamplayerid,fanduelplayerid,draftkingsplayerid,yahooplayerid,injurystatus,injurybodypart,injurystartdate,injurynotes,fanduelname,draftkingsname,yahooname,injurypractice,injurypracticedescription,declaredinactive,upcomingfanduelsalary,upcomingdraftkingssalary,upcomingyahoosalary,teamid,fantasydraftplayerid,fantasydraftname) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    // DEV
    /*
    public static final String USER = "statroutedev";
    public static final String PASSWORD = "statroutedev1";
    public static final String CONNECTIONSTRING = "jdbc:postgresql://statroutedev.cbklqhcjzvuk.us-west-2.rds.amazonaws.com/statroutedev";
    */
    // QA
    
    public static final String USER = "statrouteqa";
    public static final String PASSWORD = "statrouteqa";
    public static final String CONNECTIONSTRING = "jdbc:postgresql://statrouteqa.cbklqhcjzvuk.us-west-2.rds.amazonaws.com/statrouteqa";
     
    
    
    public static void main(String[] args) throws IOException, InvalidFormatException {

        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File(SAMPLE_XLSX_FILE_PATH));

        long start = System.currentTimeMillis();
        insertData(workbook);
        System.out.println(System.currentTimeMillis() - start);
        // Closing the workbook
        workbook.close();
    }
    
    public static void mains(String[] args) {
    	DateTimeFormatter f = new DateTimeFormatterBuilder()
    			  .appendPattern("d/MM/")
    			  .appendValueReduced(ChronoField.YEAR, 4, 4, 1900)
    			  .toFormatter();
    			LocalDate date = LocalDate.parse("29/01/94",f);
    			System.out.println(date);
    }
    public static void insertData(Workbook workbook) {
    	
    
	    java.sql.Connection con = null;
	    java.sql.PreparedStatement stmt = null;
	    
	    System.out.println("Getting Import sheet");
	    Sheet sheetImport = workbook.getSheet("Sheet1");
	   	 DataFormatter dataFormatter = new DataFormatter();
	   	 
	   	 Iterator<Row> rowIterator = sheetImport.rowIterator();
	   	 
	   	 //java.text.SimpleDateFormat dateFormat  = new java.text.SimpleDateFormat("dd/MM/yy");
	 	//DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("d/MM/yy");
	   	DateTimeFormatter dateFormat = new DateTimeFormatterBuilder().appendPattern("d/MM/")
	   		  .appendValueReduced(ChronoField.YEAR, 2, 2, 1900)
	   		  .toFormatter();
	   	DateTimeFormatter dateFormatNew = DateTimeFormatter.ofPattern("d/MM/yy");
	   	
	   	 java.util.Properties props = new java.util.Properties();
	   	 props.setProperty("user", USER);
	   	 props.setProperty("password", PASSWORD);
	   	 props.setProperty("stringtype", "unspecified");
		    System.out.println("Getting Connection");

	   	 try {
		   	 Class.forName("org.postgresql.Driver");
		   	 con = DriverManager.getConnection(CONNECTIONSTRING,props);
		   	System.out.println("Got Connection");
		   	 stmt = con.prepareStatement(ffPlayer017InsertSQL);
		   	
		   	 int i = 0;
		   	 int count = 0;
		   	 while (rowIterator.hasNext()) {
		        	
		            Row row = rowIterator.next();
		            
		            if(i == 0) {
		           	 	i++;
		            	continue;
		           	 
		            }else {
		            	count++;
		            	int index = 1;
		            	/*
		            	 Iterator<Cell> cellIterator = row.cellIterator();
		            	 int index = 1;
		                 while (cellIterator.hasNext()){
		                     Cell cell = cellIterator.next();
		                     String cellValue = dataFormatter.formatCellValue(cell);
		                     System.out.println(index+":"+cellValue);
		                     if(index == 10) {
		                    	 String[] arr = cellValue.split(" ");
		                    	 cellValue = arr[0];
		                    	 LocalDate localDate = LocalDate.parse(cellValue, dateFormat);
		                    	 
		                    	 stmt.setObject(index, localDate);
		                     }else {
		                    	 stmt.setString(index, cellValue);
		                     }
		                     index++;
		                 }
		                
		                 // TODO: Make it batch update
		                 int updResult = stmt.executeUpdate();
		                 System.out.println(count+" Updated Result: "+updResult);
		                 break;
		                 */
		            	for(int x=0; x<65; x++) {
		            		
		            		Cell cell = row.getCell(x,MissingCellPolicy.CREATE_NULL_AS_BLANK);
		            		
		                     String cellValue = dataFormatter.formatCellValue(cell);
		                     //if(x==0)System.out.println(cellValue);
		                     //System.out.println(index+":"+cellValue);
		                     if(index == 10) {
		                    	 String[] arr = cellValue.split(" ");
		                    	 cellValue = arr[0];
		                    	 
		                    	 if(cellValue == null || cellValue.isEmpty()) {
		                    		 stmt.setNull(index, Types.NULL);
		                    	 }else {
		                    		 LocalDate localDate = LocalDate.parse(cellValue, dateFormat);		                    	 
		                    		 stmt.setObject(index, localDate);
		                    	 }
		                    	 
		                     }else if(index == 52) {
		                    	 
		                    	 String[] arr = cellValue.split(" ");
		                    	 cellValue = arr[0];
		                    	 
		                    	 if(cellValue == null || cellValue.isEmpty()) {
		                    		 stmt.setNull(index, Types.NULL);
		                    	 }else {
		   
		                    		 LocalDate localDate = LocalDate.parse(cellValue, dateFormatNew);		                    	 
		                    		 stmt.setObject(index, localDate);
		                    	 }
		                    	 
		                     }else if (index == 14 || index == 34){
		                    	 String boolValue = "false";
		                    	 if(cellValue.startsWith("TRUE"))
		                    		 boolValue = "true";
		                    	 stmt.setObject(index, boolValue);
		                     }else {
		                    	 if(cell.getCellType()==Cell.CELL_TYPE_BLANK) {
		                    		 stmt.setNull(index, Types.NULL);
		                    	 }else {
		                    		 stmt.setString(index, cellValue);
		                    	 }
		                     }
		                     
		                     // 14 boolean 34 boolean
		                     
		                     index++;		            		
		            	}
		            	
		            	 int updResult = stmt.executeUpdate();
		                System.out.println(count+" Updated Result: "+updResult);
		            	// System.out.println(count);
		            	//System.out.println("###############");
		            	//if(count == 2)
		            	//break;
		            }
		            
		     }
	   	 }catch(Exception e) {
	   		 e.printStackTrace();
	   	 }finally {
	   		 
	   		 try {
				stmt.close();
				con.close();
				System.out.println("CLosed resources");
			} catch (Exception e) {
				e.printStackTrace();
			}
	   		 
	   	 }
   	
   }    
    
}
