import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.Types;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Iterator;
/*
 * !!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!
 * 
 * Quick dirty program to read excel and update nflteam nflteamrz.
 * This is not a reusable program. Created only for the dataloading and this 
 * is tightly coupled with the excel document structure shared by SR
 *
 * Author: Mahesh S.R
 * 
 * */


public class NFLPlayerCSVReader {
    //public static final String SAMPLE_XLS_FILE_PATH = "./sample-xls-file.xls";
    //public static final String CSV_FILE_PATH = "/home/maheshsr/Documents/Extter/Sprint2-Apr-2018/nflplayercsv.csv";
    
    
    /*
    public static final String nflPlayerInsertSQL = "INSERT INTO public.nflplayer "
    		+ "(playerid,seasontype,season,gamedate,week,team,opponent,homeoraway,pnumber,playername,positioncategory,activated,played,started,"
    		+ "passingattempts,passingcompletions,passingyards,passingtouchdowns,passinginterceptions,passinglong,passingsacks,passingsackyards,"
    		+ "rushingattempts,rushingyards,rushingtouchdowns,rushinglong,receivingtargets,receptions,receivingyards,receivingtouchdowns,"
    		+ "receivinglong,fumbles,fumbleslost,puntreturns,puntreturnyards,puntreturntouchdowns,puntreturnlong,kickreturns,kickreturnyards,"
    		+ "kickreturntouchdowns,kickreturnlong,solotackles,assistedtackles,tacklesforloss,sacks,sackyards,passesdefended,fumblesforced,"
    		+ "fumblesrecovered,fumblereturnyards,fumblereturntouchdowns,interceptions,interceptionreturnyards,interceptionreturntouchdowns,"
    		+ "blockedkicks,fieldgoalsattempted,fieldgoalsmade,fieldgoalslongestmade,extrapointsmade,twopointconversionpasses,twopointconversionruns,"
    		+ "twopointconversionreceptions,tackles,offensivetouchdowns,defensivetouchdowns,specialteamstouchdowns,touchdowns,extrapointsattempted,"
    		+ "blockedkickreturntouchdowns,fieldgoalreturntouchdowns,safeties,fieldgoalshadblocked,extrapointshadblocked,blockedkickreturnyards,"
    		+ "fieldgoalreturnyards,stadium,temperature,windspeed,twopointconversionreturns,fieldgoalsmade0to19,fieldgoalsmade20to29,"
    		+ "fieldgoalsmade30to39,fieldgoalsmade40to49,fieldgoalsmade50plus,teamid,opponentid,grassorturf,playerposition,"
    		+ "passingcompletionpercentage,passingyardsperattempt,passingyardspercompletion,rushingyardsperattempt,receivingyardsperreception,"
    		+ "puntreturnyardsperattempt,kickreturnyardsperattempt,receptionpercentage,receivingyardspertarget,fieldgoalpercentage,shortname,"
    		+ "datetime,yyear,mmonth,dday,hhour,mminute,playerticklessname,ffp2017) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
    		+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
    		+ "?,?,?,?,?,?,?,?,?,?,?,?)"; 
    		
    		*/
	public static final String CSV_FILE_PATH = "/home/maheshsr/Documents/Extter/Sprint2-Apr-2018/nflplayer2017v2.csv";
	
    public static final String nflPlayerInsertSQL = "INSERT INTO public.nflprz "
    		+ "(playerid,seasontype,season,gamedate,week,team,opponent,homeoraway,pnumber,playername,playerposition,"
    		+ "positioncategory,activated,played,started,passingattempts,passingcompletions,passingyards,passingtouchdowns,passinginterceptions,"
    		+ "passinglong,passingsacks,passingsackyards,rushingattempts,rushingyards,rushingtouchdowns,rushinglong,receivingtargets,receptions,"
    		+ "receivingyards,receivingtouchdowns,receivinglong,fumbles,fumbleslost,puntreturns,puntreturnyards,puntreturntouchdowns,puntreturnlong,"
    		+ "kickreturns,kickreturnyards,kickreturntouchdowns,kickreturnlong,solotackles,assistedtackles,tacklesforloss,sacks,passesdefended,"
    		+ "fumblesforced,fumblesrecovered,fumblereturnyards,fumblereturntouchdowns,interceptions,interceptionreturnyards,"
    		+ "interceptionreturntouchdowns,blockedkicks,punts,puntyards,puntaverage,fieldgoalsattempted,fieldgoalsmade,fieldgoalslongestmade,"
    		+ "extrapointsmade,twopointconversionpasses,twopointconversionruns,twopointconversionreceptions,tackles,offensivetouchdowns,"
    		+ "defensivetouchdowns,specialteamstouchdowns,touchdowns,extrapointsattempted,blockedkickreturntouchdowns,fieldgoalreturntouchdowns,"
    		+ "safeties,fieldgoalshadblocked,puntshadblocked,extrapointshadblocked,blockedkickreturnyards,fieldgoalreturnyards,puntlong,shortname,"
    		+ "playingsurface,stadium,temperature,windspeed,offensiveteamsnaps,defensiveteamsnaps,specialteamsteamsnaps,twopointconversionreturns,"
    		+ "fieldgoalsmade0to19,fieldgoalsmade20to29,fieldgoalsmade30to39,fieldgoalsmade40to49,fieldgoalsmade50plus,fantasyposition,teamid,"
    		+ "opponentid,datetime,passingcompletionpercentage,passingyardsperattempt,passingyardspercompletion,rushingyardsperattempt,"
    		+ "receivingyardsperreception,puntreturnyardsperattempt,kickreturnyardsperattempt,receptionpercentage,receivingyardspertarget,"
    		+ "fieldgoalpercentage,playergameid,globalopponentid,scoreid,yyear,mmonth,dday,hhour,mminute,playerticklessname,ffp2017) \n" + 
    		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
    		+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    // DEV
    /*
    public static final String USER = "statroutedev";
    public static final String PASSWORD = "statroutedev1";
    public static final String CONNECTIONSTRING = "jdbc:postgresql://statroutedev.cbklqhcjzvuk.us-west-2.rds.amazonaws.com/statroutedev";
    */
    // QA
    
    public static final String USER = "statrouteqa";
    public static final String PASSWORD = "statrouteqa";
    public static final String CONNECTIONSTRING = "jdbc:postgresql://statrouteqa.cbklqhcjzvuk.us-west-2.rds.amazonaws.com/statrouteqa";
     
    
    public static void main(String[] args){

    	long start = System.currentTimeMillis();
    	 File file 			= new File(CSV_FILE_PATH);    	 
    	 BufferedReader br 	= null;
    	 String record 		= null;
    	 int lineNo 		= 0;
    	
    	 java.sql.Connection con 			= null;
 	     java.sql.PreparedStatement stmt 	= null;
 	    
    	 ////////////////// NOT NULL FIELDS
 
    	 String yyear = null;
    	 String mmonth = null;
    	 String dday = null;
    	 String hhour = null;
    	 String mminute = null;
    	 String playerticklessname = null;
    	 //String ffp2017 = null;
    	 ///////////////////////////////////
    	 
    	 DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("d/MM/yy");
    	 
    	 java.util.Properties props = new java.util.Properties();
	   	 props.setProperty("user", USER);
	   	 props.setProperty("password", PASSWORD);
	   	 props.setProperty("stringtype", "unspecified");
    	 
	   	 try {
	   	
		     br 	= new BufferedReader(new FileReader(file));
		   	 Class.forName("org.postgresql.Driver");
		   	 con = DriverManager.getConnection(CONNECTIONSTRING,props);
		   	 System.out.println("Got Connection PlayerRZ");
		   	 stmt = con.prepareStatement(nflPlayerInsertSQL);
	    	 
		   	 
		   	 
	    	 while ((record = br.readLine()) != null) {
	    		 
	    		 
	    		 if(lineNo == 0) {
	    			 lineNo++;
	    			 continue;
	    		 }
	    		 String[] fields = record.split(",");
	    		 for(int i=0;i<fields.length;i++) {
	    			 
	    			 int index = i +1;
	    			 // 4 and 100 Date
	    			//boolean - 12,13,14 Y/N
	    			 // 10 is playername
	    			 
	    			 if(index == 10) {
	    				 playerticklessname = fields[i];
	    			 }
	    			 
	    			 if(index == 4 || index == 98) { //gamedate, datetime
	    				 
	    				 String[] arr 		= fields[i].split(" ");
	                	 String dateValue 	= arr[0];
	                	 
	                	 String[] hhmm = arr[1].split(":");
	                	 
	                	 if(dateValue == null || dateValue.isEmpty()) {
	                		 stmt.setNull(index, Types.NULL);
	                	 }else {
	                		 LocalDate localDate = LocalDate.parse(dateValue, dateFormat);		                    	 
	                		 stmt.setObject(index, localDate);
	                		 
	                    	 yyear = ""+localDate.getYear();
	                    	 mmonth = ""+localDate.getMonthValue();
	                    	 dday = ""+localDate.getDayOfMonth();
	                    	 hhour = hhmm[0];
	                    	 mminute = hhmm[1];     		 
	                		 
	                	 }
	    			 }else if(index == 13 ||index == 14 ||index == 15  ) {
		         			String boolValue = "false";
		         			if(fields[i].equals("Y"))
		                 		 boolValue = "true";
		                 	 stmt.setObject(index, boolValue); 
	    			 }else {
	    				 if(fields[i] == null || fields[i].isEmpty()) {
	                		 stmt.setNull(index, Types.NULL);
	                	 }else {
	                		 stmt.setString(index, fields[i]);
	                	 }
	    			 }
	    			 
	    		 }
	    		 //// Set the NOT NULL Params
	    		 //yyear,mmonth,dday,hhour,mminute,playerticklessname,ffp2017
	    		 stmt.setString(112, yyear);
	    		 stmt.setString(113, mmonth);
	    		 stmt.setString(114, dday);
	    		 stmt.setString(115, hhour);
	    		 stmt.setString(116, mminute);
	    		 stmt.setString(117, playerticklessname);
	    		 stmt.setString(118, "false"); //ffp2017
	    		 ////////////////////////////
	    		 stmt.addBatch();
	    		 
	    		 
	    		 if(lineNo % 10 == 0) {
	    			int[] arr = stmt.executeBatch();
	    			System.out.println(lineNo+" Updated Result: "+arr.length);
	    		 }	             
	             lineNo++;
	             
	             
	             
	    	 }
	    	 int[] arr = stmt.executeBatch();
 			System.out.println(lineNo+" Updated Result: "+arr.length);
	    	
 			System.out.println("Took (ms): "+(System.currentTimeMillis()-start));
 			
	   	 }catch(Exception e) {
	   		 e.printStackTrace();
	   	 }finally {
	   		 
	   		 try {
				stmt.close();
				con.close();
				System.out.println("Closed DB resources");
			} catch (Exception e) {
				e.printStackTrace();
			}
	   		 
	   		 try {
	   			br.close();
				System.out.println("Closed File");
			} catch (Exception e) {
				e.printStackTrace();
			}
	   		
	   	 }
    }
   
}
