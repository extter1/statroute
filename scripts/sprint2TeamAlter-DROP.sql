ALTER TABLE nflteam 
DROP COLUMN IF EXISTS QuarterbackHitsDifferential,
DROP COLUMN IF EXISTS TacklesForLossDifferential,
DROP COLUMN IF EXISTS QuarterbackSacksDifferential,
DROP COLUMN IF EXISTS TacklesForLossPercentage,
DROP COLUMN IF EXISTS QuarterbackHitsPercentage,
DROP COLUMN IF EXISTS TimesSackedPercentage,
DROP COLUMN IF EXISTS OpponentQuarterbackHitsDifferential,
DROP COLUMN IF EXISTS OpponentTacklesForLossDifferential,
DROP COLUMN IF EXISTS OpponentQuarterbackSacksDifferential,
DROP COLUMN IF EXISTS OpponentTacklesForLossPercentage,
DROP COLUMN IF EXISTS OpponentQuarterbackHitsPercentage,
DROP COLUMN IF EXISTS OpponentTimesSackedPercentage,
DROP COLUMN IF EXISTS Kickoffs,
DROP COLUMN IF EXISTS KickoffsInEndZone,
DROP COLUMN IF EXISTS KickoffTouchbacks,
DROP COLUMN IF EXISTS OpponentKickoffs,
DROP COLUMN IF EXISTS OpponentKickoffsInEndZone,
DROP COLUMN IF EXISTS OpponentKickoffTouchbacks,
DROP COLUMN IF EXISTS humidity,
DROP COLUMN IF EXISTS overunder,
DROP COLUMN IF EXISTS pointspread;

ALTER TABLE nflteamrz 
DROP COLUMN IF EXISTS QuarterbackHitsDifferential,
DROP COLUMN IF EXISTS TacklesForLossDifferential,
DROP COLUMN IF EXISTS QuarterbackSacksDifferential,
DROP COLUMN IF EXISTS TacklesForLossPercentage,
DROP COLUMN IF EXISTS QuarterbackHitsPercentage,
DROP COLUMN IF EXISTS TimesSackedPercentage,
DROP COLUMN IF EXISTS OpponentQuarterbackHitsDifferential,
DROP COLUMN IF EXISTS OpponentTacklesForLossDifferential,
DROP COLUMN IF EXISTS OpponentQuarterbackSacksDifferential,
DROP COLUMN IF EXISTS OpponentTacklesForLossPercentage,
DROP COLUMN IF EXISTS OpponentQuarterbackHitsPercentage,
DROP COLUMN IF EXISTS OpponentTimesSackedPercentage,
DROP COLUMN IF EXISTS Kickoffs,
DROP COLUMN IF EXISTS KickoffsInEndZone,
DROP COLUMN IF EXISTS KickoffTouchbacks,
DROP COLUMN IF EXISTS OpponentKickoffs,
DROP COLUMN IF EXISTS OpponentKickoffsInEndZone,
DROP COLUMN IF EXISTS OpponentKickoffTouchbacks,
DROP COLUMN IF EXISTS humidity,
DROP COLUMN IF EXISTS overunder,
DROP COLUMN IF EXISTS pointspread;


















