CREATE TABLE public.ffplayers2017(
playerid bigint,
team	character(3),
pnumber character(3),
firstname character varying(25),
lastname character varying(25),
playerposition character varying(10),
status character varying(50),
height	character varying(10),
weight real DEFAULT 0,
birthdate	date,
college character varying(50),
experience smallint,
fantasyposition character varying(10),
active boolean,
positioncategory character varying(10),
name character varying(50),
age smallint,	
experiencestring character varying(50),
birthdatestring	character varying(50),
photourl character varying(150),
byeweek	smallint,
upcominggameopponent	character varying(10),
upcominggameweek	smallint,
shortname character varying(40),
depthpositioncategory character varying(10),
depthposition	character varying(10),
depthorder smallint,
depthdisplayorder	smallint,
currentteam character varying(10),
collegedraftteam	character varying(10),
collegedraftyear	smallint,
collegedraftround	smallint,
collegedraftpick	smallint,
isundraftedfreeagent boolean,
heightfeet	smallint,
heightinches	smallint,
upcomingopponentrank	smallint,
upcomingopponentpositionrank	smallint,
currentstatus character varying(20),
fantasyalarmplayerid	bigint,
sportradarplayerid	character varying(50),
rotoworldplayerid	bigint,
rotowireplayerid bigint,
statsplayerid	bigint,
sportsdirectplayerid	bigint,
xmlteamplayerid	bigint,
fanduelplayerid	bigint,
draftkingsplayerid	bigint,
yahooplayerid bigint,
injurystatus	character varying(20),
injurybodypart	character varying(20),
injurystartdate	date,
injurynotes	character varying(250),
fanduelname character varying(50),
draftkingsname	character varying(50),
yahooname	character varying(50),
injurypractice	character varying(20),
injurypracticedescription character varying(150),
declaredinactive	smallint,
upcomingfanduelsalary real DEFAULT 0,
upcomingdraftkingssalary	real DEFAULT 0,
upcomingyahoosalary	real DEFAULT 0,
teamid smallint,
fantasydraftplayerid	bigint,
fantasydraftname	character varying(50)
);