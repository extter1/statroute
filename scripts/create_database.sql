--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.8

-- Started on 2018-03-15 19:48:51

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12393)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3313 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 3 (class 3079 OID 24655)
-- Name: chkpass; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS chkpass WITH SCHEMA public;


--
-- TOC entry 3314 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION chkpass; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION chkpass IS 'data type for auto-encrypted passwords';


--
-- TOC entry 2 (class 3079 OID 24665)
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- TOC entry 3315 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- TOC entry 688 (class 1247 OID 16454)
-- Name: fieldt; Type: TYPE; Schema: public; Owner: admin
--

CREATE TYPE public.fieldt AS ENUM (
    'Grass',
    'Turf'
);


ALTER TYPE public.fieldt OWNER TO admin;

--
-- TOC entry 682 (class 1247 OID 16442)
-- Name: haway; Type: TYPE; Schema: public; Owner: admin
--

CREATE TYPE public.haway AS ENUM (
    'HOME',
    'AWAY'
);


ALTER TYPE public.haway OWNER TO admin;

--
-- TOC entry 679 (class 1247 OID 16428)
-- Name: ppositions; Type: TYPE; Schema: public; Owner: admin
--

CREATE TYPE public.ppositions AS ENUM (
    'Quarterback',
    'Running Back',
    'Wide Receiver',
    'Tight End',
    'Kicker',
    'NONE'
);


ALTER TYPE public.ppositions OWNER TO admin;

--
-- TOC entry 594 (class 1247 OID 16406)
-- Name: primes; Type: TYPE; Schema: public; Owner: admin
--

CREATE TYPE public.primes AS ENUM (
    'Primetime',
    'Not'
);


ALTER TYPE public.primes OWNER TO admin;

--
-- TOC entry 694 (class 1247 OID 16468)
-- Name: tempt; Type: TYPE; Schema: public; Owner: admin
--

CREATE TYPE public.tempt AS ENUM (
    'Very Cold (<20)',
    'Cold (21-40)',
    'Fair (41-64)',
    'Warm (65-80)',
    'Hot (81-99)',
    'Very Hot (99+)'
);


ALTER TYPE public.tempt OWNER TO admin;

--
-- TOC entry 685 (class 1247 OID 16448)
-- Name: venuet; Type: TYPE; Schema: public; Owner: admin
--

CREATE TYPE public.venuet AS ENUM (
    'Indoor',
    'Outdoor'
);


ALTER TYPE public.venuet OWNER TO admin;

--
-- TOC entry 711 (class 1247 OID 20455)
-- Name: weathert; Type: TYPE; Schema: public; Owner: admin
--

CREATE TYPE public.weathert AS ENUM (
    'Clear',
    'Overcast',
    'Wet/Rain',
    'Heavy Rain',
    'Snow',
    'Heavy Snow',
    'NONE'
);


ALTER TYPE public.weathert OWNER TO admin;

--
-- TOC entry 597 (class 1247 OID 16412)
-- Name: weekday; Type: TYPE; Schema: public; Owner: admin
--

CREATE TYPE public.weekday AS ENUM (
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
);


ALTER TYPE public.weekday OWNER TO admin;

--
-- TOC entry 591 (class 1247 OID 16394)
-- Name: weekst; Type: TYPE; Schema: public; Owner: admin
--

CREATE TYPE public.weekst AS ENUM (
    'Weeks 1-6',
    'Weeks 7-13',
    'Weeks 14-16',
    'NFL Playoffs',
    'NONE'
);


ALTER TYPE public.weekst OWNER TO admin;

--
-- TOC entry 691 (class 1247 OID 16460)
-- Name: windt; Type: TYPE; Schema: public; Owner: admin
--

CREATE TYPE public.windt AS ENUM (
    'Calm (below 5 mph)',
    'Windy (5-10 mph)',
    'Very Windy (10+ mph)'
);


ALTER TYPE public.windt OWNER TO admin;

--
-- TOC entry 259 (class 1255 OID 16385)
-- Name: compose_session_replication_role(text); Type: FUNCTION; Schema: public; Owner: focker
--

CREATE FUNCTION public.compose_session_replication_role(role text) RETURNS text
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
        DECLARE
                curr_val text := 'unset';
        BEGIN
                EXECUTE 'SET session_replication_role = ' || quote_literal(role);
                EXECUTE 'SHOW session_replication_role' INTO curr_val;
                RETURN curr_val;
        END
$$;


ALTER FUNCTION public.compose_session_replication_role(role text) OWNER TO focker;

--
-- TOC entry 267 (class 1255 OID 16386)
-- Name: kill_all_connections(); Type: FUNCTION; Schema: public; Owner: focker
--

CREATE FUNCTION public.kill_all_connections() RETURNS text
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
        BEGIN
                EXECUTE 'SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = current_database() AND pid <> pg_backend_pid()';
                RETURN 'ok';
        END
$$;


ALTER FUNCTION public.kill_all_connections() OWNER TO focker;

--
-- TOC entry 268 (class 1255 OID 16387)
-- Name: pg_kill_connection(integer); Type: FUNCTION; Schema: public; Owner: focker
--

CREATE FUNCTION public.pg_kill_connection(integer) RETURNS boolean
    LANGUAGE sql SECURITY DEFINER
    AS $_$select pg_terminate_backend($1);$_$;


ALTER FUNCTION public.pg_kill_connection(integer) OWNER TO focker;

--
-- TOC entry 284 (class 1255 OID 16388)
-- Name: upgrade_postgis_23x(); Type: FUNCTION; Schema: public; Owner: focker
--

CREATE FUNCTION public.upgrade_postgis_23x() RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO public, pg_temp
    AS $$
                    DECLARE ver TEXT;
                    BEGIN
                            SELECT version INTO ver FROM pg_available_extension_versions WHERE name = 'postgis' AND version LIKE '2.3%';
                            EXECUTE 'ALTER EXTENSION postgis UPDATE TO ' || quote_literal(ver);
                    END;
                    $$;


ALTER FUNCTION public.upgrade_postgis_23x() OWNER TO focker;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 214 (class 1259 OID 26956)
-- Name: allplayerlookup; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.allplayerlookup (
    season character(4),
    playername character varying(50),
    teamkey character(3),
    innflp boolean,
    shartname character varying(25)
);


ALTER TABLE public.allplayerlookup OWNER TO admin;

--
-- TOC entry 190 (class 1259 OID 24649)
-- Name: depthchart; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.depthchart (
    positionact character(6) NOT NULL,
    team character(3) NOT NULL,
    activeplayer boolean NOT NULL,
    weekgame integer NOT NULL,
    playername character varying(50) NOT NULL,
    injuryweek character varying(10),
    offenseweek boolean,
    offensivelineweek boolean,
    formationweek character(20),
    defcoordweek character varying(50),
    offcoordweek character varying(50),
    hcweek character varying(50),
    vsteamweek character(3),
    vsdefcoordinatorweek character varying(50),
    vsoffcoordinatorweek character varying(50),
    vshcweek character varying(50),
    weatherkey character(16),
    stat1 real,
    stat2 real,
    stat3 real,
    stat4 real,
    stat5 real,
    stat6 real,
    stat7 real,
    tpposition character(4),
    playerid bigint,
    secondarycolor character varying(10),
    primarycolor character varying(10),
    grouprl character(1),
    depthteam character(1)
);


ALTER TABLE public.depthchart OWNER TO admin;

--
-- TOC entry 224 (class 1259 OID 30389)
-- Name: fffteam; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.fffteam (
    dayofweek public.weekday,
    hhour character(2),
    mminute character(2),
    team character(3),
    gamedateteam character(11),
    homeoraway public.haway,
    opponent character(3),
    headcoach character varying(50),
    offensivecoordinator character varying(50),
    defensivecoordinator character varying(50),
    zipofstadium character(8),
    grassorturf public.fieldt,
    nightgame boolean,
    primtstart boolean,
    temp real,
    temprange public.tempt,
    windspeed real,
    windc character(1),
    windrange public.windt,
    conditions public.weathert,
    wcondc character(1),
    stadium character varying(100),
    indooroutdoor public.venuet,
    tempc character(1)
);


ALTER TABLE public.fffteam OWNER TO admin;

--
-- TOC entry 189 (class 1259 OID 24606)
-- Name: ffplayers2017; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.ffplayers2017 (
    player character varying(40),
    existsnflp boolean DEFAULT false,
    shortname character varying(40),
    currentteam character(3),
    nickname character varying(50),
    playerid bigint
);


ALTER TABLE public.ffplayers2017 OWNER TO admin;

--
-- TOC entry 219 (class 1259 OID 27267)
-- Name: forgotten; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.forgotten (
    userid integer,
    hash character varying(128),
    valid_unitl date,
    created_at date,
    deleted_at date
);


ALTER TABLE public.forgotten OWNER TO admin;

--
-- TOC entry 223 (class 1259 OID 30386)
-- Name: fvs; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.fvs (
    gamedateteam character(11),
    vscoach character varying(50),
    vsdefensivecoordinator character varying(50),
    vsoffensivecoordinator character varying(50)
);


ALTER TABLE public.fvs OWNER TO admin;

--
-- TOC entry 229 (class 1259 OID 175035)
-- Name: newnew; Type: TABLE; Schema: public; Owner: cblanton
--

CREATE TABLE public.newnew (
    playerid bigint,
    team character(3),
    ffp2017 boolean,
    playername character varying(50),
    playerposition character varying(10)
);


ALTER TABLE public.newnew OWNER TO cblanton;

--
-- TOC entry 187 (class 1259 OID 18779)
-- Name: nflplayer; Type: TABLE; Schema: public; Owner: cblanton
--

CREATE TABLE public.nflplayer (
    playerid bigint NOT NULL,
    seasontype smallint DEFAULT 0,
    season character(4),
    gamedate date,
    mmonth integer NOT NULL,
    dday integer NOT NULL,
    yyear integer NOT NULL,
    hhour integer DEFAULT 0 NOT NULL,
    mminute integer DEFAULT 0,
    week smallint DEFAULT 0,
    newweeks public.weekst,
    primetime character(15),
    dayofweek public.weekday,
    team character(3) NOT NULL,
    opponent character(3),
    homeoraway public.haway,
    pnumber character(3),
    playername character varying(50),
    playerposition character varying(10),
    nflpposition public.ppositions,
    positioncategory character(4),
    target integer DEFAULT 0,
    teamcity character varying(20),
    teamname character varying(20),
    conference character(3),
    division character(7),
    teamkey2 character(3),
    fullteamname character varying(50),
    playerticklessname character varying(50) NOT NULL,
    playerfirstname character varying(25),
    playerlastname character varying(25),
    playernickname character varying(50),
    activated boolean,
    played boolean,
    started boolean,
    passingattempts real DEFAULT 0,
    passingcompletions real DEFAULT 0,
    passingyards real DEFAULT 0,
    passingcompletionpercentage real DEFAULT 0,
    passingyardsperattempt real DEFAULT 0,
    passingyardspercompletion real DEFAULT 0,
    passingtouchdowns real DEFAULT 0,
    passinginterceptions real DEFAULT 0,
    passingrating real DEFAULT 0,
    passinglong real DEFAULT 0,
    passingsacks real DEFAULT 0,
    passingsackyards real DEFAULT 0,
    rushingattempts real DEFAULT 0,
    rushingyards real DEFAULT 0,
    rushingyardsperattempt real DEFAULT 0,
    rushingtouchdowns real DEFAULT 0,
    rushinglong real DEFAULT 0,
    receivingtargets real DEFAULT 0,
    receptions real DEFAULT 0,
    receivingyards real DEFAULT 0,
    receivingyardsperreception real DEFAULT 0,
    receivingtouchdowns real DEFAULT 0,
    receivinglong real DEFAULT 0,
    fumbles real DEFAULT 0,
    fumbleslost real DEFAULT 0,
    puntreturns real DEFAULT 0,
    puntreturnyards real DEFAULT 0,
    puntreturnyardsperattempt real DEFAULT 0,
    puntreturntouchdowns real DEFAULT 0,
    puntreturnlong real DEFAULT 0,
    kickreturns real DEFAULT 0,
    kickreturnyards real DEFAULT 0,
    kickreturnyardsperattempt real DEFAULT 0,
    kickreturntouchdowns real DEFAULT 0,
    kickreturnlong real DEFAULT 0,
    solotackles real DEFAULT 0,
    assistedtackles real DEFAULT 0,
    tacklesforloss real DEFAULT 0,
    sacks real DEFAULT 0,
    sackyards real DEFAULT 0,
    quarterbackhits real DEFAULT 0,
    passesdefended real DEFAULT 0,
    fumblesforced real DEFAULT 0,
    fumblesrecovered real DEFAULT 0,
    fumblereturnyards real DEFAULT 0,
    fumblereturntouchdowns real DEFAULT 0,
    interceptions real DEFAULT 0,
    interceptionreturnyards real DEFAULT 0,
    interceptionreturntouchdowns real DEFAULT 0,
    blockedkicks real DEFAULT 0,
    specialteamssolotackles real DEFAULT 0,
    specialteamsassistedtackles real DEFAULT 0,
    miscsolotackles real DEFAULT 0,
    miscassistedtackles real DEFAULT 0,
    punts real DEFAULT 0,
    puntyards real DEFAULT 0,
    puntaverage real DEFAULT 0,
    fieldgoalsattempted real DEFAULT 0,
    fieldgoalsmade real DEFAULT 0,
    fieldgoalslongestmade real DEFAULT 0,
    extrapointsmade real DEFAULT 0,
    twopointconversionpasses real DEFAULT 0,
    twopointconversionruns real DEFAULT 0,
    twopointconversionreceptions real DEFAULT 0,
    fantasypoints real DEFAULT 0,
    receptionpercentage real DEFAULT 0,
    receivingyardspertarget real DEFAULT 0,
    tackles real DEFAULT 0,
    offensivetouchdowns real DEFAULT 0,
    defensivetouchdowns real DEFAULT 0,
    specialteamstouchdowns real DEFAULT 0,
    touchdowns real DEFAULT 0,
    passingyards25d real DEFAULT 0,
    passingyards30 real DEFAULT 0,
    passingyards20 real DEFAULT 0,
    rushingyards10d real DEFAULT 0,
    rushingyards15 real DEFAULT 0,
    rushingyards20 real DEFAULT 0,
    reeptions5d real DEFAULT 0,
    reeptions1 real DEFAULT 0,
    reeptions0 real DEFAULT 0,
    receivingyards20d real DEFAULT 0,
    receivingyards15 real DEFAULT 0,
    receivingyards10 real DEFAULT 0,
    passinginterceptions2d real DEFAULT 0,
    passinginterceptions3 real DEFAULT 0,
    passinginterceptions1 real DEFAULT 0,
    touchdowns6d real DEFAULT 0,
    touchdowns5 real DEFAULT 0,
    touchdowns4 real DEFAULT 0,
    fieldgoalpercentage real DEFAULT 0,
    fumblesownrecoveries real DEFAULT 0,
    fumblesoutofbounds real DEFAULT 0,
    kickreturnfaircatches real DEFAULT 0,
    puntreturnfaircatches real DEFAULT 0,
    punttouchbacks real DEFAULT 0,
    puntinside20 real DEFAULT 0,
    puntnetaverage real DEFAULT 0,
    extrapointsattempted real DEFAULT 0,
    blockedkickreturntouchdowns real DEFAULT 0,
    fieldgoalreturntouchdowns real DEFAULT 0,
    safeties real DEFAULT 0,
    fieldgoalshadblocked real DEFAULT 0,
    puntshadblocked real DEFAULT 0,
    extrapointshadblocked real DEFAULT 0,
    puntlong real DEFAULT 0,
    blockedkickreturnyards real DEFAULT 0,
    fieldgoalreturnyards real DEFAULT 0,
    puntnetyards real DEFAULT 0,
    specialteamsfumblesforced real DEFAULT 0,
    specialteamsfumblesrecovered real DEFAULT 0,
    miscfumblesforced real DEFAULT 0,
    miscfumblesrecovered real DEFAULT 0,
    shortname character varying(40),
    playingsurface character varying(15),
    isgameover boolean,
    safetiesallowed smallint DEFAULT 0,
    stadium character varying(100),
    indooroutdoor public.venuet,
    grassorturf public.fieldt,
    codetemp character(1),
    codewind character(1),
    codewcond character(1),
    temprange public.tempt,
    wind public.windt,
    temperature real DEFAULT 0,
    humidity real DEFAULT 0,
    windspeed real DEFAULT 0,
    offensivesnapsplayed real DEFAULT 0,
    defensivesnapsplayed real DEFAULT 0,
    specialteamssnapsplayed real DEFAULT 0,
    offensiveteamsnaps real DEFAULT 0,
    defensiveteamsnaps real DEFAULT 0,
    specialteamsteamsnaps real DEFAULT 0,
    twopointconversionreturns real DEFAULT 0,
    fieldgoalsmade0to19 real DEFAULT 0,
    fieldgoalsmade20to29 real DEFAULT 0,
    fieldgoalsmade30to39 real DEFAULT 0,
    fieldgoalsmade40to49 real DEFAULT 0,
    fieldgoalsmade50plus real DEFAULT 0,
    injurystatus character varying(50),
    injurybodypart character varying(50),
    injurystartdate date,
    injurynotes text,
    opponentrank character varying(10),
    opponentpositionrank character varying(10),
    injurypractice character varying(50),
    injurypracticedescription character varying(50),
    declaredinactive boolean,
    teamid integer DEFAULT 0,
    opponentid integer DEFAULT 0,
    dayofgame date,
    datetime date,
    globalopponentid integer DEFAULT 0,
    passingattempts1q real,
    passingattempts2q real,
    passingattempts3q real,
    passingattempts4q real,
    passingattempts1h real,
    passingattempts2h real,
    passingattemptsovt real,
    passingyards1q real,
    passingyards2q real,
    passingyards3q real,
    passingyards4q real,
    passingyards1h real,
    passingyards2h real,
    passingyardsovt real,
    passingcompletionpercentage1q real,
    passingcompletionpercentage2q real,
    passingcompletionpercentage3q real,
    passingcompletionpercentage4q real,
    passingcompletionpercentage1h real,
    passingcompletionpercentage2h real,
    passingcompletionpercentageovt real,
    passingyardsperattempt1q real,
    passingyardsperattempt2q real,
    passingyardsperattempt3q real,
    passingyardsperattempt4q real,
    passingyardsperattempt1h real,
    passingyardsperattempt2h real,
    passingyardsperattemptovt real,
    passingyardspercompletion1q real,
    passingyardspercompletion2q real,
    passingyardspercompletion3q real,
    passingyardspercompletion4q real,
    passingyardspercompletion1h real,
    passingyardspercompletion2h real,
    passingyardspercompletionovt real,
    passingtouchdowns1q real,
    passingtouchdowns2q real,
    passingtouchdowns3q real,
    passingtouchdowns4q real,
    passingtouchdowns1h real,
    passingtouchdowns2h real,
    passingtouchdownsovt real,
    passinginterceptions1q real,
    passinginterceptions2q real,
    passinginterceptions3q real,
    passinginterceptions4q real,
    passinginterceptions1h real,
    passinginterceptions2h real,
    passinginterceptionsovt real,
    passingrating1q real,
    passingrating2q real,
    passingrating3q real,
    passingrating4q real,
    passingrating1h real,
    passingrating2h real,
    passingratingovt real,
    passinglong1q real,
    passinglong2q real,
    passinglong3q real,
    passinglong4q real,
    passinglong1h real,
    passinglong2h real,
    passinglongovt real,
    passingsacks1q real,
    passingsacks2q real,
    passingsacks3q real,
    passingsacks4q real,
    passingsacks1h real,
    passingsacks2h real,
    passingsacksovt real,
    passingsackyards1q real,
    passingsackyards2q real,
    passingsackyards3q real,
    passingsackyards4q real,
    passingsackyards1h real,
    passingsackyards2h real,
    passingsackyardsovt real,
    rushingattempts1q real,
    rushingattempts2q real,
    rushingattempts3q real,
    rushingattempts4q real,
    rushingattempts1h real,
    rushingattempts2h real,
    rushingattemptsovt real,
    rushingyards1q real,
    rushingyards2q real,
    rushingyards3q real,
    rushingyards4q real,
    rushingyards1h real,
    rushingyards2h real,
    rushingyardsovt real,
    rushingyardsperattempt1q real,
    rushingyardsperattempt2q real,
    rushingyardsperattempt3q real,
    rushingyardsperattempt4q real,
    rushingyardsperattempt1h real,
    rushingyardsperattempt2h real,
    rushingyardsperattemptovt real,
    rushingtouchdowns1q real,
    rushingtouchdowns2q real,
    rushingtouchdowns3q real,
    rushingtouchdowns4q real,
    rushingtouchdowns1h real,
    rushingtouchdowns2h real,
    rushingtouchdownsovt real,
    rushinglong1q real,
    rushinglong2q real,
    rushinglong3q real,
    rushinglong4q real,
    rushinglong1h real,
    rushinglong2h real,
    rushinglongovt real,
    receivingtargets1q real,
    receivingtargets2q real,
    receivingtargets3q real,
    receivingtargets4q real,
    receivingtargets1h real,
    receivingtargets2h real,
    receivingtargetsovt real,
    receptions1q real,
    receptions2q real,
    receptions3q real,
    receptions4q real,
    receptions1h real,
    receptions2h real,
    receptionsovt real,
    receivingyards1q real,
    receivingyards2q real,
    receivingyards3q real,
    receivingyards4q real,
    receivingyards1h real,
    receivingyards2h real,
    receivingyardsovt real,
    receivingyardsperreception1q real,
    receivingyardsperreception2q real,
    receivingyardsperreception3q real,
    receivingyardsperreception4q real,
    receivingyardsperreception1h real,
    receivingyardsperreception2h real,
    receivingyardsperreceptionovt real,
    receivingtouchdowns1q real,
    receivingtouchdowns2q real,
    receivingtouchdowns3q real,
    receivingtouchdowns4q real,
    receivingtouchdowns1h real,
    receivingtouchdowns2h real,
    receivingtouchdownsovt real,
    receivinglong1q real,
    receivinglong2q real,
    receivinglong3q real,
    receivinglong4q real,
    receivinglong1h real,
    receivinglong2h real,
    receivinglongovt real,
    fumbles1q real,
    fumbles2q real,
    fumbles3q real,
    fumbles4q real,
    fumbles1h real,
    fumbles2h real,
    fumblesovt real,
    fumbleslost1q real,
    fumbleslost2q real,
    fumbleslost3q real,
    fumbleslost4q real,
    fumbleslost1h real,
    fumbleslost2h real,
    fumbleslostovt real,
    puntreturns1q real,
    puntreturns2q real,
    puntreturns3q real,
    puntreturns4q real,
    puntreturns1h real,
    puntreturns2h real,
    puntreturnsovt real,
    puntreturnyards1q real,
    puntreturnyards2q real,
    puntreturnyards3q real,
    puntreturnyards4q real,
    puntreturnyards1h real,
    puntreturnyards2h real,
    puntreturnyardsovt real,
    puntreturnyardsperattempt1q real,
    puntreturnyardsperattempt2q real,
    puntreturnyardsperattempt3q real,
    puntreturnyardsperattempt4q real,
    puntreturnyardsperattempt1h real,
    puntreturnyardsperattempt2h real,
    puntreturnyardsperattemptovt real,
    puntreturntouchdowns1q real,
    puntreturntouchdowns2q real,
    puntreturntouchdowns3q real,
    puntreturntouchdowns4q real,
    puntreturntouchdowns1h real,
    puntreturntouchdowns2h real,
    puntreturntouchdownsovt real,
    puntreturnlong1q real,
    puntreturnlong2q real,
    puntreturnlong3q real,
    puntreturnlong4q real,
    puntreturnlong1h real,
    puntreturnlong2h real,
    puntreturnlongovt real,
    kickreturns1q real,
    kickreturns2q real,
    kickreturns3q real,
    kickreturns4q real,
    kickreturns1h real,
    kickreturns2h real,
    kickreturnsovt real,
    kickreturnyards1q real,
    kickreturnyards2q real,
    kickreturnyards3q real,
    kickreturnyards4q real,
    kickreturnyards1h real,
    kickreturnyards2h real,
    kickreturnyardsovt real,
    kickreturnyardsperattempt1q real,
    kickreturnyardsperattempt2q real,
    kickreturnyardsperattempt3q real,
    kickreturnyardsperattempt4q real,
    kickreturnyardsperattempt1h real,
    kickreturnyardsperattempt2h real,
    kickreturnyardsperattemptovt real,
    kickreturntouchdowns1q real,
    kickreturntouchdowns2q real,
    kickreturntouchdowns3q real,
    kickreturntouchdowns4q real,
    kickreturntouchdowns1h real,
    kickreturntouchdowns2h real,
    kickreturntouchdownsovt real,
    kickreturnlong1q real,
    kickreturnlong2q real,
    kickreturnlong3q real,
    kickreturnlong4q real,
    kickreturnlong1h real,
    kickreturnlong2h real,
    kickreturnlongovt real,
    solotackles1q real,
    solotackles2q real,
    solotackles3q real,
    solotackles4q real,
    solotackles1h real,
    solotackles2h real,
    solotacklesovt real,
    assistedtackles1q real,
    assistedtackles2q real,
    assistedtackles3q real,
    assistedtackles4q real,
    assistedtackles1h real,
    assistedtackles2h real,
    assistedtacklesovt real,
    tacklesforloss1q real,
    tacklesforloss2q real,
    tacklesforloss3q real,
    tacklesforloss4q real,
    tacklesforloss1h real,
    tacklesforloss2h real,
    tacklesforlossovt real,
    sacks1q real,
    sacks2q real,
    sacks3q real,
    sacks4q real,
    sacks1h real,
    sacks2h real,
    sacksovt real,
    sackyards1q real,
    sackyards2q real,
    sackyards3q real,
    sackyards4q real,
    sackyards1h real,
    sackyards2h real,
    sackyardsovt real,
    quarterbackhits1q real,
    quarterbackhits2q real,
    quarterbackhits3q real,
    quarterbackhits4q real,
    quarterbackhits1h real,
    quarterbackhits2h real,
    quarterbackhitsovt real,
    passesdefended1q real,
    passesdefended2q real,
    passesdefended3q real,
    passesdefended4q real,
    passesdefended1h real,
    passesdefended2h real,
    passesdefendedovt real,
    fumblesforced1q real,
    fumblesforced2q real,
    fumblesforced3q real,
    fumblesforced4q real,
    fumblesforced1h real,
    fumblesforced2h real,
    fumblesforcedovt real,
    fumblesrecovered1q real,
    fumblesrecovered2q real,
    fumblesrecovered3q real,
    fumblesrecovered4q real,
    fumblesrecovered1h real,
    fumblesrecovered2h real,
    fumblesrecoveredovt real,
    fumblereturnyards1q real,
    fumblereturnyards2q real,
    fumblereturnyards3q real,
    fumblereturnyards4q real,
    fumblereturnyards1h real,
    fumblereturnyards2h real,
    fumblereturnyardsovt real,
    fumblereturntouchdowns1q real,
    fumblereturntouchdowns2q real,
    fumblereturntouchdowns3q real,
    fumblereturntouchdowns4q real,
    fumblereturntouchdowns1h real,
    fumblereturntouchdowns2h real,
    fumblereturntouchdownsovt real,
    interceptions1q real,
    interceptions2q real,
    interceptions3q real,
    interceptions4q real,
    interceptions1h real,
    interceptions2h real,
    interceptionsovt real,
    interceptionreturnyards1q real,
    interceptionreturnyards2q real,
    interceptionreturnyards3q real,
    interceptionreturnyards4q real,
    interceptionreturnyards1h real,
    interceptionreturnyards2h real,
    interceptionreturnyardsovt real,
    interceptionreturntouchdowns1q real,
    interceptionreturntouchdowns2q real,
    interceptionreturntouchdowns3q real,
    interceptionreturntouchdowns4q real,
    interceptionreturntouchdowns1h real,
    interceptionreturntouchdowns2h real,
    interceptionreturntouchdownsovt real,
    blockedkicks1q real,
    blockedkicks2q real,
    blockedkicks3q real,
    blockedkicks4q real,
    blockedkicks1h real,
    blockedkicks2h real,
    blockedkicksovt real DEFAULT 0,
    specialteamssolotackles1q real DEFAULT 0,
    specialteamssolotackles2q real DEFAULT 0,
    specialteamssolotackles3q real DEFAULT 0,
    specialteamssolotackles4q real DEFAULT 0,
    specialteamssolotackles1h real DEFAULT 0,
    specialteamssolotackles2h real DEFAULT 0,
    specialteamssolotacklesovt real DEFAULT 0,
    specialteamsassistedtackles1q real DEFAULT 0,
    specialteamsassistedtackles2q real DEFAULT 0,
    specialteamsassistedtackles3q real DEFAULT 0,
    specialteamsassistedtackles4q real DEFAULT 0,
    specialteamsassistedtackles1h real DEFAULT 0,
    specialteamsassistedtackles2h real DEFAULT 0,
    specialteamsassistedtacklesovt real DEFAULT 0,
    miscsolotackles1q real DEFAULT 0,
    miscsolotackles2q real DEFAULT 0,
    miscsolotackles3q real DEFAULT 0,
    miscsolotackles4q real DEFAULT 0,
    miscsolotackles1h real DEFAULT 0,
    miscsolotackles2h real DEFAULT 0,
    miscsolotacklesovt real DEFAULT 0,
    miscassistedtackles1q real DEFAULT 0,
    miscassistedtackles2q real DEFAULT 0,
    miscassistedtackles3q real DEFAULT 0,
    miscassistedtackles4q real DEFAULT 0,
    miscassistedtackles1h real DEFAULT 0,
    miscassistedtackles2h real DEFAULT 0,
    miscassistedtacklesovt real DEFAULT 0,
    punts1q real DEFAULT 0,
    punts2q real DEFAULT 0,
    punts3q real DEFAULT 0,
    punts4q real DEFAULT 0,
    punts1h real DEFAULT 0,
    punts2h real DEFAULT 0,
    puntsovt real DEFAULT 0,
    puntyards1q real DEFAULT 0,
    puntyards2q real DEFAULT 0,
    puntyards3q real DEFAULT 0,
    puntyards4q real DEFAULT 0,
    puntyards1h real DEFAULT 0,
    puntyards2h real DEFAULT 0,
    puntyardsovt real DEFAULT 0,
    puntaverage1q real DEFAULT 0,
    puntaverage2q real DEFAULT 0,
    puntaverage3q real DEFAULT 0,
    puntaverage4q real DEFAULT 0,
    puntaverage1h real DEFAULT 0,
    puntaverage2h real DEFAULT 0,
    puntaverageovt real DEFAULT 0,
    fieldgoalsattempted1q real DEFAULT 0,
    fieldgoalsattempted2q real DEFAULT 0,
    fieldgoalsattempted3q real DEFAULT 0,
    fieldgoalsattempted4q real DEFAULT 0,
    fieldgoalsattempted1h real DEFAULT 0,
    fieldgoalsattempted2h real DEFAULT 0,
    fieldgoalsattemptedovt real DEFAULT 0,
    fieldgoalsmade1q real DEFAULT 0,
    fieldgoalsmade2q real DEFAULT 0,
    fieldgoalsmade3q real DEFAULT 0,
    fieldgoalsmade4q real DEFAULT 0,
    fieldgoalsmade1h real DEFAULT 0,
    fieldgoalsmade2h real DEFAULT 0,
    fieldgoalsmadeovt real DEFAULT 0,
    fieldgoalslongestmade1q real DEFAULT 0,
    fieldgoalslongestmade2q real DEFAULT 0,
    fieldgoalslongestmade3q real DEFAULT 0,
    fieldgoalslongestmade4q real DEFAULT 0,
    fieldgoalslongestmade1h real DEFAULT 0,
    fieldgoalslongestmade2h real DEFAULT 0,
    fieldgoalslongestmadeovt real DEFAULT 0,
    extrapointsmade1q real DEFAULT 0,
    extrapointsmade2q real DEFAULT 0,
    extrapointsmade3q real DEFAULT 0,
    extrapointsmade4q real DEFAULT 0,
    extrapointsmade1h real DEFAULT 0,
    extrapointsmade2h real DEFAULT 0,
    extrapointsmadeovt real DEFAULT 0,
    twopointconversionpasses1q real DEFAULT 0,
    twopointconversionpasses2q real DEFAULT 0,
    twopointconversionpasses3q real DEFAULT 0,
    twopointconversionpasses4q real DEFAULT 0,
    twopointconversionpasses1h real DEFAULT 0,
    twopointconversionpasses2h real DEFAULT 0,
    twopointconversionpassesovt real DEFAULT 0,
    twopointconversionruns1q real DEFAULT 0,
    twopointconversionruns2q real DEFAULT 0,
    twopointconversionruns3q real DEFAULT 0,
    twopointconversionruns4q real DEFAULT 0,
    twopointconversionruns1h real DEFAULT 0,
    twopointconversionruns2h real DEFAULT 0,
    twopointconversionrunsovt real DEFAULT 0,
    twopointconversionreceptions1q real DEFAULT 0,
    twopointconversionreceptions2q real DEFAULT 0,
    twopointconversionreceptions3q real DEFAULT 0,
    twopointconversionreceptions4q real DEFAULT 0,
    twopointconversionreceptions1h real DEFAULT 0,
    twopointconversionreceptions2h real DEFAULT 0,
    twopointconversionreceptionsovt real DEFAULT 0,
    fantasypoints1q real DEFAULT 0,
    fantasypoints2q real DEFAULT 0,
    fantasypoints3q real DEFAULT 0,
    fantasypoints4q real DEFAULT 0,
    fantasypoints1h real DEFAULT 0,
    fantasypoints2h real DEFAULT 0,
    fantasypointsovt real DEFAULT 0,
    receptionpercentage1q real DEFAULT 0,
    receptionpercentage2q real DEFAULT 0,
    receptionpercentage3q real DEFAULT 0,
    receptionpercentage4q real DEFAULT 0,
    receptionpercentage1h real DEFAULT 0,
    receptionpercentage2h real DEFAULT 0,
    receptionpercentageovt real DEFAULT 0,
    receivingyardspertarget1q real DEFAULT 0,
    receivingyardspertarget2q real DEFAULT 0,
    receivingyardspertarget3q real DEFAULT 0,
    receivingyardspertarget4q real DEFAULT 0,
    receivingyardspertarget1h real DEFAULT 0,
    receivingyardspertarget2h real DEFAULT 0,
    receivingyardspertargetovt real DEFAULT 0,
    tackles1q real DEFAULT 0,
    tackles2q real DEFAULT 0,
    tackles3q real DEFAULT 0,
    tackles4q real DEFAULT 0,
    tackles1h real DEFAULT 0,
    tackles2h real DEFAULT 0,
    tacklesovt real DEFAULT 0,
    offensivetouchdowns1q real DEFAULT 0,
    offensivetouchdowns2q real DEFAULT 0,
    offensivetouchdowns3q real DEFAULT 0,
    offensivetouchdowns4q real DEFAULT 0,
    offensivetouchdowns1h real DEFAULT 0,
    offensivetouchdowns2h real DEFAULT 0,
    offensivetouchdownsovt real DEFAULT 0,
    defensivetouchdowns1q real DEFAULT 0,
    defensivetouchdowns2q real DEFAULT 0,
    defensivetouchdowns3q real DEFAULT 0,
    defensivetouchdowns4q real DEFAULT 0,
    defensivetouchdowns1h real DEFAULT 0,
    defensivetouchdowns2h real DEFAULT 0,
    defensivetouchdownsovt real DEFAULT 0,
    specialteamstouchdowns1q real DEFAULT 0,
    specialteamstouchdowns2q real DEFAULT 0,
    specialteamstouchdowns3q real DEFAULT 0,
    specialteamstouchdowns4q real DEFAULT 0,
    specialteamstouchdowns1h real DEFAULT 0,
    specialteamstouchdowns2h real DEFAULT 0,
    specialteamstouchdownsovt real DEFAULT 0,
    touchdowns1q real DEFAULT 0,
    touchdowns2q real DEFAULT 0,
    touchdowns3q real DEFAULT 0,
    touchdowns4q real DEFAULT 0,
    touchdowns1h real DEFAULT 0,
    touchdowns2h real DEFAULT 0,
    touchdownsovt real DEFAULT 0,
    passingyards25d1q real DEFAULT 0,
    passingyards25d2q real DEFAULT 0,
    passingyards25d3q real DEFAULT 0,
    passingyards25d4q real DEFAULT 0,
    passingyards25d1h real DEFAULT 0,
    passingyards25d2h real DEFAULT 0,
    passingyards25dovt real DEFAULT 0,
    passingyards301q real DEFAULT 0,
    passingyards302q real DEFAULT 0,
    passingyards303q real DEFAULT 0,
    passingyards304q real DEFAULT 0,
    passingyards301h real DEFAULT 0,
    passingyards302h real DEFAULT 0,
    passingyards30ovt real DEFAULT 0,
    passingyards201q real DEFAULT 0,
    passingyards202q real DEFAULT 0,
    passingyards203q real DEFAULT 0,
    passingyards204q real DEFAULT 0,
    passingyards201h real DEFAULT 0,
    passingyards202h real DEFAULT 0,
    passingyards20ovt real DEFAULT 0,
    rushingyards10d1q real DEFAULT 0,
    rushingyards10d2q real DEFAULT 0,
    rushingyards10d3q real DEFAULT 0,
    rushingyards10d4q real DEFAULT 0,
    rushingyards10d1h real DEFAULT 0,
    rushingyards10d2h real DEFAULT 0,
    rushingyards10dovt real DEFAULT 0,
    rushingyards151q real DEFAULT 0,
    rushingyards152q real DEFAULT 0,
    rushingyards153q real DEFAULT 0,
    rushingyards154q real DEFAULT 0,
    rushingyards151h real DEFAULT 0,
    rushingyards152h real DEFAULT 0,
    rushingyards15ovt real DEFAULT 0,
    rushingyards201q real DEFAULT 0,
    rushingyards202q real DEFAULT 0,
    rushingyards203q real DEFAULT 0,
    rushingyards204q real DEFAULT 0,
    rushingyards201h real DEFAULT 0,
    rushingyards202h real DEFAULT 0,
    rushingyards20ovt real DEFAULT 0,
    reeptions5d1q real DEFAULT 0,
    reeptions5d2q real DEFAULT 0,
    reeptions5d3q real DEFAULT 0,
    reeptions5d4q real DEFAULT 0,
    reeptions5d1h real DEFAULT 0,
    reeptions5d2h real DEFAULT 0,
    reeptions5dovt real DEFAULT 0,
    reeptions11q real DEFAULT 0,
    reeptions12q real DEFAULT 0,
    reeptions13q real DEFAULT 0,
    reeptions14q real DEFAULT 0,
    reeptions11h real DEFAULT 0,
    reeptions12h real DEFAULT 0,
    reeptions1ovt real DEFAULT 0,
    reeptions01q real DEFAULT 0,
    reeptions02q real DEFAULT 0,
    reeptions03q real DEFAULT 0,
    reeptions04q real DEFAULT 0,
    reeptions01h real DEFAULT 0,
    reeptions02h real DEFAULT 0,
    reeptions0ovt real DEFAULT 0,
    receivingyards20d1q real DEFAULT 0,
    receivingyards20d2q real DEFAULT 0,
    receivingyards20d3q real DEFAULT 0,
    receivingyards20d4q real DEFAULT 0,
    receivingyards20d1h real DEFAULT 0,
    receivingyards20d2h real DEFAULT 0,
    receivingyards20dovt real DEFAULT 0,
    receivingyards151q real DEFAULT 0,
    receivingyards152q real DEFAULT 0,
    receivingyards153q real DEFAULT 0,
    receivingyards154q real DEFAULT 0,
    receivingyards151h real DEFAULT 0,
    receivingyards152h real DEFAULT 0,
    receivingyards15ovt real DEFAULT 0,
    receivingyards101q real DEFAULT 0,
    receivingyards102q real DEFAULT 0,
    receivingyards103q real DEFAULT 0,
    receivingyards104q real DEFAULT 0,
    receivingyards101h real DEFAULT 0,
    receivingyards102h real DEFAULT 0,
    receivingyards10ovt real DEFAULT 0,
    passinginterceptions2d1q real DEFAULT 0,
    passinginterceptions2d2q real DEFAULT 0,
    passinginterceptions2d3q real DEFAULT 0,
    passinginterceptions2d4q real DEFAULT 0,
    passinginterceptions2d1h real DEFAULT 0,
    passinginterceptions2d2h real DEFAULT 0,
    passinginterceptions2dovt real DEFAULT 0,
    passinginterceptions31q real DEFAULT 0,
    passinginterceptions32q real DEFAULT 0,
    passinginterceptions33q real DEFAULT 0,
    passinginterceptions34q real DEFAULT 0,
    passinginterceptions31h real DEFAULT 0,
    passinginterceptions32h real DEFAULT 0,
    passinginterceptions3ovt real DEFAULT 0,
    passinginterceptions11q real DEFAULT 0,
    passinginterceptions12q real DEFAULT 0,
    passinginterceptions13q real DEFAULT 0,
    passinginterceptions14q real DEFAULT 0,
    passinginterceptions11h real DEFAULT 0,
    passinginterceptions12h real DEFAULT 0,
    passinginterceptions1ovt real DEFAULT 0,
    touchdowns6d1q real DEFAULT 0,
    touchdowns6d2q real DEFAULT 0,
    touchdowns6d3q real DEFAULT 0,
    touchdowns6d4q real DEFAULT 0,
    touchdowns6d1h real DEFAULT 0,
    touchdowns6d2h real DEFAULT 0,
    touchdowns6dovt real DEFAULT 0,
    touchdowns51q real DEFAULT 0,
    touchdowns52q real DEFAULT 0,
    touchdowns53q real DEFAULT 0,
    touchdowns54q real DEFAULT 0,
    touchdowns51h real DEFAULT 0,
    touchdowns52h real DEFAULT 0,
    touchdowns5ovt real DEFAULT 0,
    touchdowns41q real DEFAULT 0,
    touchdowns42q real DEFAULT 0,
    touchdowns43q real DEFAULT 0,
    touchdowns44q real DEFAULT 0,
    touchdowns41h real DEFAULT 0,
    touchdowns42h real DEFAULT 0,
    touchdowns4ovt real DEFAULT 0,
    fieldgoalpercentage1q real DEFAULT 0,
    fieldgoalpercentage2q real DEFAULT 0,
    fieldgoalpercentage3q real DEFAULT 0,
    fieldgoalpercentage4q real DEFAULT 0,
    fieldgoalpercentage1h real DEFAULT 0,
    fieldgoalpercentage2h real DEFAULT 0,
    fieldgoalpercentageovt real DEFAULT 0,
    fumblesownrecoveries1q real DEFAULT 0,
    fumblesownrecoveries2q real DEFAULT 0,
    fumblesownrecoveries3q real DEFAULT 0,
    fumblesownrecoveries4q real DEFAULT 0,
    fumblesownrecoveries1h real DEFAULT 0,
    fumblesownrecoveries2h real DEFAULT 0,
    fumblesownrecoveriesovt real DEFAULT 0,
    fumblesoutofbounds1q real DEFAULT 0,
    fumblesoutofbounds2q real DEFAULT 0,
    fumblesoutofbounds3q real DEFAULT 0,
    fumblesoutofbounds4q real DEFAULT 0,
    fumblesoutofbounds1h real DEFAULT 0,
    fumblesoutofbounds2h real DEFAULT 0,
    fumblesoutofboundsovt real DEFAULT 0,
    kickreturnfaircatches1q real DEFAULT 0,
    kickreturnfaircatches2q real DEFAULT 0,
    kickreturnfaircatches3q real DEFAULT 0,
    kickreturnfaircatches4q real DEFAULT 0,
    kickreturnfaircatches1h real DEFAULT 0,
    kickreturnfaircatches2h real DEFAULT 0,
    kickreturnfaircatchesovt real DEFAULT 0,
    puntreturnfaircatches1q real DEFAULT 0,
    puntreturnfaircatches2q real DEFAULT 0,
    puntreturnfaircatches3q real DEFAULT 0,
    puntreturnfaircatches4q real DEFAULT 0,
    puntreturnfaircatches1h real DEFAULT 0,
    puntreturnfaircatches2h real DEFAULT 0,
    puntreturnfaircatchesovt real DEFAULT 0,
    punttouchbacks1q real DEFAULT 0,
    punttouchbacks2q real DEFAULT 0,
    punttouchbacks3q real DEFAULT 0,
    punttouchbacks4q real DEFAULT 0,
    punttouchbacks1h real DEFAULT 0,
    punttouchbacks2h real DEFAULT 0,
    punttouchbacksovt real DEFAULT 0,
    puntinside201q real DEFAULT 0,
    puntinside202q real DEFAULT 0,
    puntinside203q real DEFAULT 0,
    puntinside204q real DEFAULT 0,
    puntinside201h real DEFAULT 0,
    puntinside202h real DEFAULT 0,
    puntinside20ovt real DEFAULT 0,
    puntnetaverage1q real DEFAULT 0,
    puntnetaverage2q real DEFAULT 0,
    puntnetaverage3q real DEFAULT 0,
    puntnetaverage4q real DEFAULT 0,
    puntnetaverage1h real DEFAULT 0,
    puntnetaverage2h real DEFAULT 0,
    puntnetaverageovt real DEFAULT 0,
    extrapointsattempted1q real DEFAULT 0,
    extrapointsattempted2q real DEFAULT 0,
    extrapointsattempted3q real DEFAULT 0,
    extrapointsattempted4q real DEFAULT 0,
    extrapointsattempted1h real DEFAULT 0,
    extrapointsattempted2h real DEFAULT 0,
    extrapointsattemptedovt real DEFAULT 0,
    blockedkickreturntouchdowns1q real DEFAULT 0,
    blockedkickreturntouchdowns2q real DEFAULT 0,
    blockedkickreturntouchdowns3q real DEFAULT 0,
    blockedkickreturntouchdowns4q real DEFAULT 0,
    blockedkickreturntouchdowns1h real DEFAULT 0,
    blockedkickreturntouchdowns2h real DEFAULT 0,
    blockedkickreturntouchdownsovt real DEFAULT 0,
    fieldgoalreturntouchdowns1q real DEFAULT 0,
    fieldgoalreturntouchdowns2q real DEFAULT 0,
    fieldgoalreturntouchdowns3q real DEFAULT 0,
    fieldgoalreturntouchdowns4q real DEFAULT 0,
    fieldgoalreturntouchdowns1h real DEFAULT 0,
    fieldgoalreturntouchdowns2h real DEFAULT 0,
    fieldgoalreturntouchdownsovt real DEFAULT 0,
    safeties1q real DEFAULT 0,
    safeties2q real DEFAULT 0,
    safeties3q real DEFAULT 0,
    safeties4q real DEFAULT 0,
    safeties1h real DEFAULT 0,
    safeties2h real DEFAULT 0,
    safetiesovt real DEFAULT 0,
    fieldgoalshadblocked1q real DEFAULT 0,
    fieldgoalshadblocked2q real DEFAULT 0,
    fieldgoalshadblocked3q real DEFAULT 0,
    fieldgoalshadblocked4q real DEFAULT 0,
    fieldgoalshadblocked1h real DEFAULT 0,
    fieldgoalshadblocked2h real DEFAULT 0,
    fieldgoalshadblockedovt real DEFAULT 0,
    puntshadblocked1q real DEFAULT 0,
    puntshadblocked2q real DEFAULT 0,
    puntshadblocked3q real DEFAULT 0,
    puntshadblocked4q real DEFAULT 0,
    puntshadblocked1h real DEFAULT 0,
    puntshadblocked2h real DEFAULT 0,
    puntshadblockedovt real DEFAULT 0,
    extrapointshadblocked1q real DEFAULT 0,
    extrapointshadblocked2q real DEFAULT 0,
    extrapointshadblocked3q real DEFAULT 0,
    extrapointshadblocked4q real DEFAULT 0,
    extrapointshadblocked1h real DEFAULT 0,
    extrapointshadblocked2h real DEFAULT 0,
    extrapointshadblockedovt real DEFAULT 0,
    puntlong1q real DEFAULT 0,
    puntlong2q real DEFAULT 0,
    puntlong3q real DEFAULT 0,
    puntlong4q real DEFAULT 0,
    puntlong1h real DEFAULT 0,
    puntlong2h real DEFAULT 0,
    puntlongovt real DEFAULT 0,
    blockedkickreturnyards1q real DEFAULT 0,
    blockedkickreturnyards2q real DEFAULT 0,
    blockedkickreturnyards3q real DEFAULT 0,
    blockedkickreturnyards4q real DEFAULT 0,
    blockedkickreturnyards1h real DEFAULT 0,
    blockedkickreturnyards2h real DEFAULT 0,
    blockedkickreturnyardsovt real DEFAULT 0,
    fieldgoalreturnyards1q real DEFAULT 0,
    fieldgoalreturnyards2q real DEFAULT 0,
    fieldgoalreturnyards3q real DEFAULT 0,
    fieldgoalreturnyards4q real DEFAULT 0,
    fieldgoalreturnyards1h real DEFAULT 0,
    fieldgoalreturnyards2h real DEFAULT 0,
    fieldgoalreturnyardsovt real DEFAULT 0,
    puntnetyards1q real DEFAULT 0,
    puntnetyards2q real DEFAULT 0,
    puntnetyards3q real DEFAULT 0,
    puntnetyards4q real DEFAULT 0,
    puntnetyards1h real DEFAULT 0,
    puntnetyards2h real DEFAULT 0,
    puntnetyardsovt real DEFAULT 0,
    specialteamsfumblesforced1q real DEFAULT 0,
    specialteamsfumblesforced2q real DEFAULT 0,
    specialteamsfumblesforced3q real DEFAULT 0,
    specialteamsfumblesforced4q real DEFAULT 0,
    specialteamsfumblesforced1h real DEFAULT 0,
    specialteamsfumblesforced2h real DEFAULT 0,
    specialteamsfumblesforcedovt real DEFAULT 0,
    specialteamsfumblesrecovered1q real DEFAULT 0,
    specialteamsfumblesrecovered2q real DEFAULT 0,
    specialteamsfumblesrecovered3q real DEFAULT 0,
    specialteamsfumblesrecovered4q real DEFAULT 0,
    specialteamsfumblesrecovered1h real DEFAULT 0,
    specialteamsfumblesrecovered2h real DEFAULT 0,
    specialteamsfumblesrecoveredovt real DEFAULT 0,
    miscfumblesforced1q real DEFAULT 0,
    miscfumblesforced2q real DEFAULT 0,
    miscfumblesforced3q real DEFAULT 0,
    miscfumblesforced4q real DEFAULT 0,
    miscfumblesforced1h real DEFAULT 0,
    miscfumblesforced2h real DEFAULT 0,
    miscfumblesforcedovt real DEFAULT 0,
    miscfumblesrecovered1q real DEFAULT 0,
    miscfumblesrecovered2q real DEFAULT 0,
    miscfumblesrecovered3q real DEFAULT 0,
    miscfumblesrecovered4q real DEFAULT 0,
    miscfumblesrecovered1h real DEFAULT 0,
    miscfumblesrecovered2h real DEFAULT 0,
    miscfumblesrecoveredovt real DEFAULT 0,
    temprange1q character(1) DEFAULT 0,
    temprange2q character(1) DEFAULT 0,
    temprange3q character(1) DEFAULT 0,
    temprange4q character(1) DEFAULT 0,
    temprange1h character(1) DEFAULT 0,
    temprange2h character(1) DEFAULT 0,
    temprangeovt character(1) DEFAULT 0,
    wind1q character(1) DEFAULT 0,
    wind2q character(1) DEFAULT 0,
    wind3q character(1) DEFAULT 0,
    wind4q character(1) DEFAULT 0,
    wind1h character(1) DEFAULT 0,
    wind2h character(1) DEFAULT 0,
    windovt character(1) DEFAULT 0,
    wconditions1q character varying(10) DEFAULT 0,
    wconditions2q character varying(10) DEFAULT 0,
    wconditions3q character varying(10) DEFAULT 0,
    wconditions4q character varying(10) DEFAULT 0,
    wconditions1h character varying(10) DEFAULT 0,
    wconditions2h character varying(10) DEFAULT 0,
    wconditionsovt character varying(10) DEFAULT 0,
    temperature1q real DEFAULT 0,
    temperature2q real DEFAULT 0,
    temperature3q real DEFAULT 0,
    temperature4q real DEFAULT 0,
    temperature1h real DEFAULT 0,
    temperature2h real DEFAULT 0,
    temperatureovt real DEFAULT 0,
    humidity1q real DEFAULT 0,
    humidity2q real DEFAULT 0,
    humidity3q real DEFAULT 0,
    humidity4q real DEFAULT 0,
    humidity1h real DEFAULT 0,
    humidity2h real DEFAULT 0,
    humidityovt real DEFAULT 0,
    windspeed1q real DEFAULT 0,
    windspeed2q real DEFAULT 0,
    windspeed3q real DEFAULT 0,
    windspeed4q real DEFAULT 0,
    windspeed1h real DEFAULT 0,
    windspeed2h real DEFAULT 0,
    windspeedovt real DEFAULT 0,
    offensivesnapsplayed1q real DEFAULT 0,
    offensivesnapsplayed2q real DEFAULT 0,
    offensivesnapsplayed3q real DEFAULT 0,
    offensivesnapsplayed4q real DEFAULT 0,
    offensivesnapsplayed1h real DEFAULT 0,
    offensivesnapsplayed2h real DEFAULT 0,
    offensivesnapsplayedovt real DEFAULT 0,
    defensivesnapsplayed1q real DEFAULT 0,
    defensivesnapsplayed2q real DEFAULT 0,
    defensivesnapsplayed3q real DEFAULT 0,
    defensivesnapsplayed4q real DEFAULT 0,
    defensivesnapsplayed1h real DEFAULT 0,
    defensivesnapsplayed2h real DEFAULT 0,
    defensivesnapsplayedovt real DEFAULT 0,
    specialteamssnapsplayed1q real DEFAULT 0,
    specialteamssnapsplayed2q real DEFAULT 0,
    specialteamssnapsplayed3q real DEFAULT 0,
    specialteamssnapsplayed4q real DEFAULT 0,
    specialteamssnapsplayed1h real DEFAULT 0,
    specialteamssnapsplayed2h real DEFAULT 0,
    specialteamssnapsplayedovt real DEFAULT 0,
    offensiveteamsnaps1q real DEFAULT 0,
    offensiveteamsnaps2q real DEFAULT 0,
    offensiveteamsnaps3q real DEFAULT 0,
    offensiveteamsnaps4q real DEFAULT 0,
    offensiveteamsnaps1h real DEFAULT 0,
    offensiveteamsnaps2h real DEFAULT 0,
    offensiveteamsnapsovt real DEFAULT 0,
    defensiveteamsnaps1q real DEFAULT 0,
    defensiveteamsnaps2q real DEFAULT 0,
    defensiveteamsnaps3q real DEFAULT 0,
    defensiveteamsnaps4q real DEFAULT 0,
    defensiveteamsnaps1h real DEFAULT 0,
    defensiveteamsnaps2h real DEFAULT 0,
    defensiveteamsnapsovt real DEFAULT 0,
    specialteamsteamsnaps1q real DEFAULT 0,
    specialteamsteamsnaps2q real DEFAULT 0,
    specialteamsteamsnaps3q real DEFAULT 0,
    specialteamsteamsnaps4q real DEFAULT 0,
    specialteamsteamsnaps1h real DEFAULT 0,
    specialteamsteamsnaps2h real DEFAULT 0,
    specialteamsteamsnapsovt real DEFAULT 0,
    fieldgoalsmade0to191q real DEFAULT 0,
    fieldgoalsmade0to192q real DEFAULT 0,
    fieldgoalsmade0to193q real DEFAULT 0,
    fieldgoalsmade0to194q real DEFAULT 0,
    fieldgoalsmade0to191h real DEFAULT 0,
    fieldgoalsmade0to192h real DEFAULT 0,
    fieldgoalsmade0to19ovt real DEFAULT 0,
    fieldgoalsmade20to291q real DEFAULT 0,
    fieldgoalsmade20to292q real DEFAULT 0,
    fieldgoalsmade20to293q real DEFAULT 0,
    fieldgoalsmade20to294q real DEFAULT 0,
    fieldgoalsmade20to291h real DEFAULT 0,
    fieldgoalsmade20to292h real DEFAULT 0,
    fieldgoalsmade20to29ovt real DEFAULT 0,
    fieldgoalsmade30to391q real DEFAULT 0,
    fieldgoalsmade30to392q real DEFAULT 0,
    fieldgoalsmade30to393q real DEFAULT 0,
    fieldgoalsmade30to394q real DEFAULT 0,
    fieldgoalsmade30to391h real DEFAULT 0,
    fieldgoalsmade30to392h real DEFAULT 0,
    fieldgoalsmade30to39ovt real DEFAULT 0,
    fieldgoalsmade40to491q real DEFAULT 0,
    fieldgoalsmade40to492q real DEFAULT 0,
    fieldgoalsmade40to493q real DEFAULT 0,
    fieldgoalsmade40to494q real DEFAULT 0,
    fieldgoalsmade40to491h real DEFAULT 0,
    fieldgoalsmade40to492h real DEFAULT 0,
    fieldgoalsmade40to49ovt real DEFAULT 0,
    fieldgoalsmade50plus1q real DEFAULT 0,
    fieldgoalsmade50plus2q real DEFAULT 0,
    fieldgoalsmade50plus3q real DEFAULT 0,
    fieldgoalsmade50plus4q real DEFAULT 0,
    fieldgoalsmade50plus1h real DEFAULT 0,
    fieldgoalsmade50plus2h real DEFAULT 0,
    fieldgoalsmade50plusovt real DEFAULT 0,
    pingame integer DEFAULT 0,
    fortyplustot integer DEFAULT 0,
    fortyplus1q integer DEFAULT 0,
    fortyplus2q integer DEFAULT 0,
    fortyplus3q integer DEFAULT 0,
    fortyplusovt integer DEFAULT 0,
    twentyplustot integer DEFAULT 0,
    twentyplus1q integer DEFAULT 0,
    twentyplus2q integer DEFAULT 0,
    twentyplus3q integer DEFAULT 0,
    twentyplus4q integer DEFAULT 0,
    twentyplusovt integer DEFAULT 0,
    longyardtot real DEFAULT 0,
    longyard1q real DEFAULT 0,
    longyard2q real DEFAULT 0,
    longyard3q real DEFAULT 0,
    longyard4q real DEFAULT 0,
    longyardovt real DEFAULT 0,
    playerageinseason integer,
    contractyear character(4) DEFAULT 2000,
    byeweek character(2) DEFAULT '0'::bpchar,
    vsheadcoach character varying(30),
    vsoffensivecoordinator character varying(30),
    vsdefensivecoordinator character varying(30),
    headcoach character varying(30),
    offensivecoordinator character varying(30),
    defensivecoordinator character varying(30),
    longyard1h real,
    longyard2h real,
    fortyplus1h real,
    fortyplus2h real,
    twentyplus1h real,
    twentyplus2h real,
    nightgame boolean,
    primetstart boolean,
    passingcompletions1q integer,
    passingcompletions2q integer,
    passingcompletions3q integer,
    passingcompletions4q integer,
    passingcompletions1h integer,
    passingcompletions2h integer,
    passingcompletionsovt integer,
    fortyplus4q real,
    codewcond1q character(1),
    codewcond2q character(1),
    codewcond3q character(1),
    codewcond4q character(1),
    codewcond1h character(1),
    codewcond2h character(1),
    codewcondovt character(1),
    wconditions public.weathert,
    fieldgoalsmade60plus real DEFAULT 0,
    fieldgoalsmade60plus1q real DEFAULT 0,
    fieldgoalsmade60plus2q real DEFAULT 0,
    fieldgoalsmade60plus3q real DEFAULT 0,
    fieldgoalsmade60plus4q real DEFAULT 0,
    fieldgoalsmade60plus1h real DEFAULT 0,
    fieldgoalsmade60plus2h real DEFAULT 0,
    fieldgoalsmade60plusovt real DEFAULT 0,
    ffp2017 boolean DEFAULT false,
    positionrank integer,
    result character varying(10),
    fantasypointspremall real,
    fantasypointsprem1q real,
    fantasypointsprem2q real,
    fantasypointsprem3q real,
    fantasypointsprem4q real,
    fantasypointspremovt real,
    fantasypointsprem1h real,
    fantasypointsprem2h real,
    currentteam character(3),
    zipofstadium character(8),
    gamedateteam character(11),
    gamedateteamplayer character varying(61),
    gamedateteamshortname character varying(51),
    twopointconversionreturns1q real DEFAULT 0,
    twopointconversionreturns2q real DEFAULT 0,
    twopointconversionreturns3q real DEFAULT 0,
    twopointconversionreturns4q real DEFAULT 0,
    twopointconversionreturns1h real DEFAULT 0,
    twopointconversionreturns2h real DEFAULT 0,
    twopointconversionreturnsovt real DEFAULT 0,
    currentage character(2)
);


ALTER TABLE public.nflplayer OWNER TO cblanton;

--
-- TOC entry 230 (class 1259 OID 176648)
-- Name: nflpall; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpall AS
 SELECT nflplayer.playerid AS id,
    nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS startnightgame,
    nflplayer.primetime AS primegamestart,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoordinator,
    nflplayer.vsdefensivecoordinator AS vsdefcoordinator,
    sum(nflplayer.pingame) AS gamesplayed,
    sum(nflplayer.fantasypoints) AS sumfantasypoints,
    round(((sum(nflplayer.fantasypoints) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS fantasyppg,
    max(nflplayer.fantasypoints) AS pceiling,
    min(nflplayer.fantasypoints) AS pfloor,
    ((sum(nflplayer.passingattempts) + sum(nflplayer.rushingattempts)) + sum(nflplayer.receivingtargets)) AS actions,
    sum(nflplayer.touchdowns) AS tds,
    ((sum(nflplayer.rushingyards) + sum(nflplayer.receivingyards)) + sum(nflplayer.passingyards)) AS scrimmageyards,
    round(((((sum(nflplayer.rushingyards) + sum(nflplayer.receivingyards)) + sum(nflplayer.passingyards)) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS passyardsscrimmageypg,
    sum(nflplayer.passingyards) AS passyards,
    sum(nflplayer.rushingyards) AS rushyards,
    sum(nflplayer.receivingyards) AS receivingyards,
    sum(nflplayer.receptions) AS sumreceptions,
    sum(nflplayer.fortyplustot) AS fortyplus,
    sum(nflplayer.interceptions) AS totalinterceptions,
    sum(nflplayer.fumbles) AS totalfumbles,
    sum(nflplayer.fumbleslost) AS totalfumbleslost,
    sum(nflplayer.twentyplustot) AS twentyplus,
    max(nflplayer.longyardtot) AS longyard,
    sum(nflplayer.rushingyards) AS totalrushingyards,
    round(((sum(nflplayer.passingyards) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS avgpassingyards,
    round((((sum(nflplayer.passingcompletions) / (sum(nflplayer.passingattempts) + (0.000001)::double precision)) * (100)::double precision))::numeric, 1) AS passcomppercent,
    round(((sum(nflplayer.passingyards) / (sum(nflplayer.passingcompletions) + (0.000001)::double precision)))::numeric, 2) AS passyardcomp,
    round(((sum(nflplayer.passingyards) / (sum(nflplayer.passingattempts) + (0.000001)::double precision)))::numeric, 2) AS passyardattempt,
    round(((sum(nflplayer.rushingyards) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS rushpergame,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.receptions))::numeric + 0.000001))::double precision))::numeric, 2) AS yardspercarry,
    sum(nflplayer.receivingyards) AS totreceivingyards,
    sum(nflplayer.receptions) AS totreceptions,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS recyardspergame,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.receivingtargets))::numeric + 0.000001))::double precision))::numeric, 2) AS recyardsattempt,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.receptions))::numeric + 0.000001))::double precision))::numeric, 2) AS recyardscompletion,
    sum(nflplayer.fieldgoalsattempted) AS fga,
    sum(nflplayer.fieldgoalsmade) AS fgm,
    round((((sum(nflplayer.fieldgoalsmade) / (((sum(nflplayer.fieldgoalsattempted))::numeric + 0.000001))::double precision) * (100)::double precision))::numeric, 2) AS fgpercent,
    sum(nflplayer.extrapointsattempted) AS patattempt,
    sum(nflplayer.extrapointsmade) AS patmade,
    (((sum(nflplayer.fieldgoalsmade20to29) + sum(nflplayer.fieldgoalsmade30to39)) + sum(nflplayer.fieldgoalsmade40to49)) + sum(nflplayer.fieldgoalsmade50plus)) AS fieldgoalsover20,
    ((sum(nflplayer.fieldgoalsmade30to39) + sum(nflplayer.fieldgoalsmade40to49)) + sum(nflplayer.fieldgoalsmade50plus)) AS fieldgoalsover30,
    (sum(nflplayer.fieldgoalsmade40to49) + sum(nflplayer.fieldgoalsmade50plus)) AS fieldgoalsover40,
    sum(nflplayer.fieldgoalsmade50plus) AS fieldgoalsover50,
    (sum(nflplayer.assistedtackles) + sum(nflplayer.solotackles)) AS totaltackles,
    sum(nflplayer.solotackles) AS solotackles,
    sum(nflplayer.assistedtackles) AS assistedtackles,
    sum(nflplayer.sacks) AS sacks,
    sum(nflplayer.interceptions) AS interceptions,
    sum(nflplayer.interceptionreturntouchdowns) AS intreturntds,
    sum(nflplayer.fumblesforced) AS forcedfumbles,
    sum(nflplayer.fumblesrecovered) AS fumblerecovery,
    sum(nflplayer.fumblereturntouchdowns) AS fumreturntds,
    (sum(nflplayer.kickreturnyards) + sum(nflplayer.puntreturnyards)) AS krpryards,
    (sum(nflplayer.kickreturntouchdowns) + sum(nflplayer.puntreturntouchdowns)) AS krprtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])) AND (nflplayer.seasontype = 1))
  GROUP BY nflplayer.playerid, nflplayer.playername, nflplayer.playerposition, nflplayer.currentteam, nflplayer.playerageinseason, nflplayer.contractyear, nflplayer.byeweek, nflplayer.nightgame, nflplayer.primetime, nflplayer.season, nflplayer.seasontype, nflplayer.week, nflplayer.newweeks, nflplayer.opponent, nflplayer.homeoraway, nflplayer.grassorturf, nflplayer.indooroutdoor, nflplayer.vsheadcoach, nflplayer.vsoffensivecoordinator, nflplayer.vsdefensivecoordinator
  WITH NO DATA;


ALTER TABLE public.nflpall OWNER TO admin;

--
-- TOC entry 234 (class 1259 OID 176766)
-- Name: nflpall1hsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpall1hsplits2 AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints1h AS ffp,
    nflplayer.passingattempts1h AS patts,
    nflplayer.rushingattempts1h AS ratts,
    nflplayer.receivingtargets1h AS rtars,
    nflplayer.touchdowns1h AS tds,
    nflplayer.passingyards1h AS pyd,
    nflplayer.rushingyards1h AS ruyd,
    nflplayer.receivingyards1h AS reyd,
    nflplayer.receptions1h AS recp,
    nflplayer.fortyplus1h AS fplus,
    nflplayer.passinginterceptions1h AS inter,
    nflplayer.fumbles1h AS fum,
    nflplayer.fumbleslost1h AS fuml,
    nflplayer.twentyplus1h AS tplus,
    nflplayer.longyard1h AS lgy,
    nflplayer.passingcompletions1h AS passc,
    nflplayer.fieldgoalsattempted1h AS fga,
    nflplayer.fieldgoalsmade1h AS fgm,
    nflplayer.fieldgoalshadblocked1h AS fgsblocked,
    nflplayer.extrapointsattempted1h AS patattempt,
    nflplayer.extrapointsmade1h AS patmade,
    nflplayer.extrapointshadblocked1h AS epblocked,
    nflplayer.fieldgoalsmade20to291h AS f20s,
    nflplayer.fieldgoalsmade30to391h AS f30s,
    nflplayer.fieldgoalsmade40to491h AS f40s,
    nflplayer.fieldgoalsmade50plus1h AS f50p,
    nflplayer.fieldgoalsmade60plus1h AS f60p,
    nflplayer.offensivesnapsplayed1h AS osnaps,
    nflplayer.fantasypointsprem1h AS fpprem,
    nflplayer.passingyards25d1h AS pfpyd1,
    nflplayer.passingyards301h AS pfpyd2,
    nflplayer.passingyards201h AS pfpyd3,
    nflplayer.rushingyards10d1h AS pfruy1,
    nflplayer.rushingyards151h AS pfruy2,
    nflplayer.rushingyards201h AS pfruy3,
    nflplayer.reeptions5d1h AS pfrec1,
    nflplayer.reeptions11h AS pfrec2,
    nflplayer.reeptions01h AS pfrec3,
    nflplayer.receivingyards20d1h AS pfryd1,
    nflplayer.receivingyards151h AS pfryd2,
    nflplayer.receivingyards101h AS pfryd3,
    nflplayer.passinginterceptions2d1h AS pfint1,
    nflplayer.passinginterceptions31h AS pfint2,
    nflplayer.passinginterceptions11h AS pfint3,
    nflplayer.touchdowns6d1h AS pftds1,
    nflplayer.touchdowns51h AS pftds2,
    nflplayer.touchdowns41h AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles1h AS solotackles,
    nflplayer.assistedtackles1h AS assistedtackles,
    nflplayer.sacks1h AS sacks,
    nflplayer.interceptions1h AS interceptions,
    nflplayer.interceptionreturntouchdowns1h AS intreturntds,
    nflplayer.fumblesforced1h AS forcedfumbles,
    nflplayer.fumblesrecovered1h AS fumblerecovery,
    nflplayer.fumblereturntouchdowns1h AS fumreturntds,
    nflplayer.kickreturnyards1h AS kryards,
    nflplayer.puntreturnyards1h AS pryards,
    nflplayer.kickreturntouchdowns1h AS krtouchdowns,
    nflplayer.puntreturntouchdowns1h AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpall1hsplits2 OWNER TO admin;

--
-- TOC entry 233 (class 1259 OID 176752)
-- Name: nflpall1qsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpall1qsplits2 AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints1q AS ffp,
    nflplayer.passingattempts1q AS patts,
    nflplayer.rushingattempts1q AS ratts,
    nflplayer.receivingtargets1q AS rtars,
    nflplayer.touchdowns1q AS tds,
    nflplayer.passingyards1q AS pyd,
    nflplayer.rushingyards1q AS ruyd,
    nflplayer.receivingyards1q AS reyd,
    nflplayer.receptions1q AS recp,
    nflplayer.fortyplus1q AS fplus,
    nflplayer.passinginterceptions1q AS inter,
    nflplayer.fumbles1q AS fum,
    nflplayer.fumbleslost1q AS fuml,
    nflplayer.twentyplus1q AS tplus,
    nflplayer.longyard1q AS lgy,
    nflplayer.passingcompletions1q AS passc,
    nflplayer.fieldgoalsattempted1q AS fga,
    nflplayer.fieldgoalsmade1q AS fgm,
    nflplayer.fieldgoalshadblocked1q AS fgsblocked,
    nflplayer.extrapointsattempted1q AS patattempt,
    nflplayer.extrapointsmade1q AS patmade,
    nflplayer.extrapointshadblocked1q AS epblocked,
    nflplayer.fieldgoalsmade20to291q AS f20s,
    nflplayer.fieldgoalsmade30to391q AS f30s,
    nflplayer.fieldgoalsmade40to491q AS f40s,
    nflplayer.fieldgoalsmade50plus1q AS f50p,
    nflplayer.fieldgoalsmade60plus1q AS f60p,
    nflplayer.offensivesnapsplayed1q AS osnaps,
    nflplayer.fantasypointsprem1q AS fpprem,
    nflplayer.passingyards25d1q AS pfpyd1,
    nflplayer.passingyards301q AS pfpyd2,
    nflplayer.passingyards201q AS pfpyd3,
    nflplayer.rushingyards10d1q AS pfruy1,
    nflplayer.rushingyards151q AS pfruy2,
    nflplayer.rushingyards201q AS pfruy3,
    nflplayer.reeptions5d1q AS pfrec1,
    nflplayer.reeptions11q AS pfrec2,
    nflplayer.reeptions01q AS pfrec3,
    nflplayer.receivingyards20d1q AS pfryd1,
    nflplayer.receivingyards151q AS pfryd2,
    nflplayer.receivingyards101q AS pfryd3,
    nflplayer.passinginterceptions2d1q AS pfint1,
    nflplayer.passinginterceptions31q AS pfint2,
    nflplayer.passinginterceptions11q AS pfint3,
    nflplayer.touchdowns6d1q AS pftds1,
    nflplayer.touchdowns51q AS pftds2,
    nflplayer.touchdowns41q AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles1q AS solotackles,
    nflplayer.assistedtackles1q AS assistedtackles,
    nflplayer.sacks1q AS sacks,
    nflplayer.interceptions1q AS interceptions,
    nflplayer.interceptionreturntouchdowns1q AS intreturntds,
    nflplayer.fumblesforced1q AS forcedfumbles,
    nflplayer.fumblesrecovered1q AS fumblerecovery,
    nflplayer.fumblereturntouchdowns1q AS fumreturntds,
    nflplayer.kickreturnyards1q AS kryards,
    nflplayer.puntreturnyards1q AS pryards,
    nflplayer.kickreturntouchdowns1q AS krtouchdowns,
    nflplayer.puntreturntouchdowns1q AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpall1qsplits2 OWNER TO admin;

--
-- TOC entry 235 (class 1259 OID 176782)
-- Name: nflpall2hsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpall2hsplits2 AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints2h AS ffp,
    nflplayer.passingattempts2h AS patts,
    nflplayer.rushingattempts2h AS ratts,
    nflplayer.receivingtargets2h AS rtars,
    nflplayer.touchdowns2h AS tds,
    nflplayer.passingyards2h AS pyd,
    nflplayer.rushingyards2h AS ruyd,
    nflplayer.receivingyards2h AS reyd,
    nflplayer.receptions2h AS recp,
    nflplayer.fortyplus2h AS fplus,
    nflplayer.passinginterceptions2h AS inter,
    nflplayer.fumbles2h AS fum,
    nflplayer.fumbleslost2h AS fuml,
    nflplayer.twentyplus2h AS tplus,
    nflplayer.longyard2h AS lgy,
    nflplayer.passingcompletions2h AS passc,
    nflplayer.fieldgoalsattempted2h AS fga,
    nflplayer.fieldgoalsmade2h AS fgm,
    nflplayer.fieldgoalshadblocked2h AS fgsblocked,
    nflplayer.extrapointsattempted2h AS patattempt,
    nflplayer.extrapointsmade2h AS patmade,
    nflplayer.extrapointshadblocked2h AS epblocked,
    nflplayer.fieldgoalsmade20to292h AS f20s,
    nflplayer.fieldgoalsmade30to392h AS f30s,
    nflplayer.fieldgoalsmade40to492h AS f40s,
    nflplayer.fieldgoalsmade50plus2h AS f50p,
    nflplayer.fieldgoalsmade60plus2h AS f60p,
    nflplayer.offensivesnapsplayed2h AS osnaps,
    nflplayer.fantasypointsprem2h AS fpprem,
    nflplayer.passingyards25d2h AS pfpyd1,
    nflplayer.passingyards302h AS pfpyd2,
    nflplayer.passingyards202h AS pfpyd3,
    nflplayer.rushingyards10d2h AS pfruy1,
    nflplayer.rushingyards152h AS pfruy2,
    nflplayer.rushingyards202h AS pfruy3,
    nflplayer.reeptions5d2h AS pfrec1,
    nflplayer.reeptions12h AS pfrec2,
    nflplayer.reeptions02h AS pfrec3,
    nflplayer.receivingyards20d2h AS pfryd1,
    nflplayer.receivingyards152h AS pfryd2,
    nflplayer.receivingyards102h AS pfryd3,
    nflplayer.passinginterceptions2d2h AS pfint1,
    nflplayer.passinginterceptions32h AS pfint2,
    nflplayer.passinginterceptions12h AS pfint3,
    nflplayer.touchdowns6d2h AS pftds1,
    nflplayer.touchdowns52h AS pftds2,
    nflplayer.touchdowns42h AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles2h AS solotackles,
    nflplayer.assistedtackles2h AS assistedtackles,
    nflplayer.sacks2h AS sacks,
    nflplayer.interceptions2h AS interceptions,
    nflplayer.interceptionreturntouchdowns2h AS intreturntds,
    nflplayer.fumblesforced2h AS forcedfumbles,
    nflplayer.fumblesrecovered2h AS fumblerecovery,
    nflplayer.fumblereturntouchdowns2h AS fumreturntds,
    nflplayer.kickreturnyards2h AS kryards,
    nflplayer.puntreturnyards2h AS pryards,
    nflplayer.kickreturntouchdowns2h AS krtouchdowns,
    nflplayer.puntreturntouchdowns2h AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpall2hsplits2 OWNER TO admin;

--
-- TOC entry 236 (class 1259 OID 176796)
-- Name: nflpall2qsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpall2qsplits2 AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints2h AS ffp,
    nflplayer.passingattempts2h AS patts,
    nflplayer.rushingattempts2h AS ratts,
    nflplayer.receivingtargets2h AS rtars,
    nflplayer.touchdowns2h AS tds,
    nflplayer.passingyards2h AS pyd,
    nflplayer.rushingyards2h AS ruyd,
    nflplayer.receivingyards2h AS reyd,
    nflplayer.receptions2h AS recp,
    nflplayer.fortyplus2h AS fplus,
    nflplayer.passinginterceptions2h AS inter,
    nflplayer.fumbles2h AS fum,
    nflplayer.fumbleslost2h AS fuml,
    nflplayer.twentyplus2h AS tplus,
    nflplayer.longyard2h AS lgy,
    nflplayer.passingcompletions2h AS passc,
    nflplayer.fieldgoalsattempted2h AS fga,
    nflplayer.fieldgoalsmade2h AS fgm,
    nflplayer.fieldgoalshadblocked2h AS fgsblocked,
    nflplayer.extrapointsattempted2h AS patattempt,
    nflplayer.extrapointsmade2h AS patmade,
    nflplayer.extrapointshadblocked2h AS epblocked,
    nflplayer.fieldgoalsmade20to292h AS f20s,
    nflplayer.fieldgoalsmade30to392h AS f30s,
    nflplayer.fieldgoalsmade40to492h AS f40s,
    nflplayer.fieldgoalsmade50plus2h AS f50p,
    nflplayer.fieldgoalsmade60plus2h AS f60p,
    nflplayer.offensivesnapsplayed2h AS osnaps,
    nflplayer.fantasypointsprem2h AS fpprem,
    nflplayer.passingyards25d2h AS pfpyd1,
    nflplayer.passingyards302h AS pfpyd2,
    nflplayer.passingyards202h AS pfpyd3,
    nflplayer.rushingyards10d2h AS pfruy1,
    nflplayer.rushingyards152h AS pfruy2,
    nflplayer.rushingyards202h AS pfruy3,
    nflplayer.reeptions5d2h AS pfrec1,
    nflplayer.reeptions12h AS pfrec2,
    nflplayer.reeptions02h AS pfrec3,
    nflplayer.receivingyards20d2h AS pfryd1,
    nflplayer.receivingyards152h AS pfryd2,
    nflplayer.receivingyards102h AS pfryd3,
    nflplayer.passinginterceptions2d2h AS pfint1,
    nflplayer.passinginterceptions32h AS pfint2,
    nflplayer.passinginterceptions12h AS pfint3,
    nflplayer.touchdowns6d2h AS pftds1,
    nflplayer.touchdowns52q AS pftds2,
    nflplayer.touchdowns42q AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles2q AS solotackles,
    nflplayer.assistedtackles2q AS assistedtackles,
    nflplayer.sacks2q AS sacks,
    nflplayer.interceptions2q AS interceptions,
    nflplayer.interceptionreturntouchdowns2q AS intreturntds,
    nflplayer.fumblesforced2q AS forcedfumbles,
    nflplayer.fumblesrecovered2q AS fumblerecovery,
    nflplayer.fumblereturntouchdowns2q AS fumreturntds,
    nflplayer.kickreturnyards2q AS kryards,
    nflplayer.puntreturnyards2q AS pryards,
    nflplayer.kickreturntouchdowns2q AS krtouchdowns,
    nflplayer.puntreturntouchdowns2q AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpall2qsplits2 OWNER TO admin;

--
-- TOC entry 237 (class 1259 OID 176812)
-- Name: nflpall3qsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpall3qsplits2 AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints3q AS ffp,
    nflplayer.passingattempts3q AS patts,
    nflplayer.rushingattempts3q AS ratts,
    nflplayer.receivingtargets3q AS rtars,
    nflplayer.touchdowns3q AS tds,
    nflplayer.passingyards3q AS pyd,
    nflplayer.rushingyards3q AS ruyd,
    nflplayer.receivingyards3q AS reyd,
    nflplayer.receptions3q AS recp,
    nflplayer.fortyplus3q AS fplus,
    nflplayer.passinginterceptions3q AS inter,
    nflplayer.fumbles3q AS fum,
    nflplayer.fumbleslost3q AS fuml,
    nflplayer.twentyplus3q AS tplus,
    nflplayer.longyard3q AS lgy,
    nflplayer.passingcompletions3q AS passc,
    nflplayer.fieldgoalsattempted3q AS fga,
    nflplayer.fieldgoalsmade3q AS fgm,
    nflplayer.fieldgoalshadblocked3q AS fgsblocked,
    nflplayer.extrapointsattempted3q AS patattempt,
    nflplayer.extrapointsmade3q AS patmade,
    nflplayer.extrapointshadblocked3q AS epblocked,
    nflplayer.fieldgoalsmade20to293q AS f20s,
    nflplayer.fieldgoalsmade30to393q AS f30s,
    nflplayer.fieldgoalsmade40to493q AS f40s,
    nflplayer.fieldgoalsmade50plus3q AS f50p,
    nflplayer.fieldgoalsmade60plus3q AS f60p,
    nflplayer.offensivesnapsplayed3q AS osnaps,
    nflplayer.fantasypointsprem3q AS fpprem,
    nflplayer.passingyards25d3q AS pfpyd1,
    nflplayer.passingyards303q AS pfpyd2,
    nflplayer.passingyards203q AS pfpyd3,
    nflplayer.rushingyards10d3q AS pfruy1,
    nflplayer.rushingyards153q AS pfruy2,
    nflplayer.rushingyards203q AS pfruy3,
    nflplayer.reeptions5d3q AS pfrec1,
    nflplayer.reeptions13q AS pfrec2,
    nflplayer.reeptions03q AS pfrec3,
    nflplayer.receivingyards20d3q AS pfryd1,
    nflplayer.receivingyards153q AS pfryd2,
    nflplayer.receivingyards103q AS pfryd3,
    nflplayer.passinginterceptions2d3q AS pfint1,
    nflplayer.passinginterceptions33q AS pfint2,
    nflplayer.passinginterceptions13q AS pfint3,
    nflplayer.touchdowns6d3q AS pftds1,
    nflplayer.touchdowns53q AS pftds2,
    nflplayer.touchdowns43q AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles3q AS solotackles,
    nflplayer.assistedtackles3q AS assistedtackles,
    nflplayer.sacks3q AS sacks,
    nflplayer.interceptions3q AS interceptions,
    nflplayer.interceptionreturntouchdowns3q AS intreturntds,
    nflplayer.fumblesforced3q AS forcedfumbles,
    nflplayer.fumblesrecovered3q AS fumblerecovery,
    nflplayer.fumblereturntouchdowns3q AS fumreturntds,
    nflplayer.kickreturnyards3q AS kryards,
    nflplayer.puntreturnyards3q AS pryards,
    nflplayer.kickreturntouchdowns3q AS krtouchdowns,
    nflplayer.puntreturntouchdowns3q AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpall3qsplits2 OWNER TO admin;

--
-- TOC entry 238 (class 1259 OID 176820)
-- Name: nflpall4qsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpall4qsplits2 AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints4q AS ffp,
    nflplayer.passingattempts4q AS patts,
    nflplayer.rushingattempts4q AS ratts,
    nflplayer.receivingtargets4q AS rtars,
    nflplayer.touchdowns4q AS tds,
    nflplayer.passingyards4q AS pyd,
    nflplayer.rushingyards4q AS ruyd,
    nflplayer.receivingyards4q AS reyd,
    nflplayer.receptions4q AS recp,
    nflplayer.fortyplus4q AS fplus,
    nflplayer.passinginterceptions4q AS inter,
    nflplayer.fumbles4q AS fum,
    nflplayer.fumbleslost4q AS fuml,
    nflplayer.twentyplus4q AS tplus,
    nflplayer.longyard4q AS lgy,
    nflplayer.passingcompletions4q AS passc,
    nflplayer.fieldgoalsattempted4q AS fga,
    nflplayer.fieldgoalsmade4q AS fgm,
    nflplayer.fieldgoalshadblocked4q AS fgsblocked,
    nflplayer.extrapointsattempted4q AS patattempt,
    nflplayer.extrapointsmade4q AS patmade,
    nflplayer.extrapointshadblocked4q AS epblocked,
    nflplayer.fieldgoalsmade20to294q AS f20s,
    nflplayer.fieldgoalsmade30to394q AS f30s,
    nflplayer.fieldgoalsmade40to494q AS f40s,
    nflplayer.fieldgoalsmade50plus4q AS f50p,
    nflplayer.fieldgoalsmade60plus4q AS f60p,
    nflplayer.offensivesnapsplayed4q AS osnaps,
    nflplayer.fantasypointsprem4q AS fpprem,
    nflplayer.passingyards25d4q AS pfpyd1,
    nflplayer.passingyards304q AS pfpyd2,
    nflplayer.passingyards204q AS pfpyd3,
    nflplayer.rushingyards10d4q AS pfruy1,
    nflplayer.rushingyards154q AS pfruy2,
    nflplayer.rushingyards204q AS pfruy3,
    nflplayer.reeptions5d4q AS pfrec1,
    nflplayer.reeptions14q AS pfrec2,
    nflplayer.reeptions04q AS pfrec3,
    nflplayer.receivingyards20d4q AS pfryd1,
    nflplayer.receivingyards154q AS pfryd2,
    nflplayer.receivingyards104q AS pfryd3,
    nflplayer.passinginterceptions2d4q AS pfint1,
    nflplayer.passinginterceptions34q AS pfint2,
    nflplayer.passinginterceptions14q AS pfint3,
    nflplayer.touchdowns6d4q AS pftds1,
    nflplayer.touchdowns54q AS pftds2,
    nflplayer.touchdowns44q AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles4q AS solotackles,
    nflplayer.assistedtackles4q AS assistedtackles,
    nflplayer.sacks4q AS sacks,
    nflplayer.interceptions4q AS interceptions,
    nflplayer.interceptionreturntouchdowns4q AS intreturntds,
    nflplayer.fumblesforced4q AS forcedfumbles,
    nflplayer.fumblesrecovered4q AS fumblerecovery,
    nflplayer.fumblereturntouchdowns4q AS fumreturntds,
    nflplayer.kickreturnyards4q AS kryards,
    nflplayer.puntreturnyards4q AS pryards,
    nflplayer.kickreturntouchdowns4q AS krtouchdowns,
    nflplayer.puntreturntouchdowns4q AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpall4qsplits2 OWNER TO admin;

--
-- TOC entry 239 (class 1259 OID 176828)
-- Name: nflpallovtsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpallovtsplits2 AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypointsovt AS ffp,
    nflplayer.passingattemptsovt AS patts,
    nflplayer.rushingattemptsovt AS ratts,
    nflplayer.receivingtargetsovt AS rtars,
    nflplayer.touchdownsovt AS tds,
    nflplayer.passingyardsovt AS pyd,
    nflplayer.rushingyardsovt AS ruyd,
    nflplayer.receivingyardsovt AS reyd,
    nflplayer.receptionsovt AS recp,
    nflplayer.fortyplusovt AS fplus,
    nflplayer.passinginterceptionsovt AS inter,
    nflplayer.fumblesovt AS fum,
    nflplayer.fumbleslostovt AS fuml,
    nflplayer.twentyplusovt AS tplus,
    nflplayer.longyardovt AS lgy,
    nflplayer.passingcompletionsovt AS passc,
    nflplayer.fieldgoalsattemptedovt AS fga,
    nflplayer.fieldgoalsmadeovt AS fgm,
    nflplayer.fieldgoalshadblockedovt AS fgsblocked,
    nflplayer.extrapointsattemptedovt AS patattempt,
    nflplayer.extrapointsmadeovt AS patmade,
    nflplayer.extrapointshadblockedovt AS epblocked,
    nflplayer.fieldgoalsmade20to29ovt AS f20s,
    nflplayer.fieldgoalsmade30to39ovt AS f30s,
    nflplayer.fieldgoalsmade40to49ovt AS f40s,
    nflplayer.fieldgoalsmade50plusovt AS f50p,
    nflplayer.fieldgoalsmade60plusovt AS f60p,
    nflplayer.offensivesnapsplayedovt AS osnaps,
    nflplayer.fantasypointspremovt AS fpprem,
    nflplayer.passingyards25dovt AS pfpyd1,
    nflplayer.passingyards30ovt AS pfpyd2,
    nflplayer.passingyards20ovt AS pfpyd3,
    nflplayer.rushingyards10dovt AS pfruy1,
    nflplayer.rushingyards15ovt AS pfruy2,
    nflplayer.rushingyards20ovt AS pfruy3,
    nflplayer.reeptions5dovt AS pfrec1,
    nflplayer.reeptions1ovt AS pfrec2,
    nflplayer.reeptions0ovt AS pfrec3,
    nflplayer.receivingyards20dovt AS pfryd1,
    nflplayer.receivingyards15ovt AS pfryd2,
    nflplayer.receivingyards10ovt AS pfryd3,
    nflplayer.passinginterceptions2dovt AS pfint1,
    nflplayer.passinginterceptions3ovt AS pfint2,
    nflplayer.passinginterceptions1ovt AS pfint3,
    nflplayer.touchdowns6dovt AS pftds1,
    nflplayer.touchdowns5ovt AS pftds2,
    nflplayer.touchdowns4ovt AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotacklesovt AS solotackles,
    nflplayer.assistedtacklesovt AS assistedtackles,
    nflplayer.sacksovt AS sacks,
    nflplayer.interceptionsovt AS interceptions,
    nflplayer.interceptionreturntouchdownsovt AS intreturntds,
    nflplayer.fumblesforcedovt AS forcedfumbles,
    nflplayer.fumblesrecoveredovt AS fumblerecovery,
    nflplayer.fumblereturntouchdownsovt AS fumreturntds,
    nflplayer.kickreturnyardsovt AS kryards,
    nflplayer.puntreturnyardsovt AS pryards,
    nflplayer.kickreturntouchdownsovt AS krtouchdowns,
    nflplayer.puntreturntouchdownsovt AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpallovtsplits2 OWNER TO admin;

--
-- TOC entry 231 (class 1259 OID 176680)
-- Name: nflpallsplits; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpallsplits AS
 SELECT nflplayer.playerid AS id,
    nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS startnightgame,
    nflplayer.primetime AS primegamestart,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoordinator,
    nflplayer.vsdefensivecoordinator AS vsdefcoordinator,
    sum(nflplayer.pingame) AS gamesplayed,
    sum(nflplayer.fantasypoints) AS sumfantasypoints,
    round(((sum(nflplayer.fantasypoints) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS fantasyppg,
    max(nflplayer.fantasypoints) AS pceiling,
    min(nflplayer.fantasypoints) AS pfloor,
    ((sum(nflplayer.passingattempts) + sum(nflplayer.rushingattempts)) + sum(nflplayer.receivingtargets)) AS actions,
    sum(nflplayer.touchdowns) AS tds,
    ((sum(nflplayer.rushingyards) + sum(nflplayer.receivingyards)) + sum(nflplayer.passingyards)) AS scrimmageyards,
    round(((((sum(nflplayer.rushingyards) + sum(nflplayer.receivingyards)) + sum(nflplayer.passingyards)) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS passyardsscrimmageypg,
    sum(nflplayer.passingyards) AS passyards,
    sum(nflplayer.rushingyards) AS rushyards,
    sum(nflplayer.receivingyards) AS receivingyards,
    sum(nflplayer.receptions) AS sumreceptions,
    sum(nflplayer.fortyplustot) AS fortyplus,
    sum(nflplayer.interceptions) AS totalinterceptions,
    sum(nflplayer.fumbles) AS totalfumbles,
    sum(nflplayer.fumbleslost) AS totalfumbleslost,
    sum(nflplayer.twentyplustot) AS twentyplus,
    max(nflplayer.longyardtot) AS longyard,
    sum(nflplayer.rushingyards) AS totalrushingyards,
    round(((sum(nflplayer.passingyards) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS avgpassingyards,
    round((((sum(nflplayer.passingcompletions) / (sum(nflplayer.passingattempts) + (0.000001)::double precision)) * (100)::double precision))::numeric, 1) AS passcomppercent,
    round(((sum(nflplayer.passingyards) / (sum(nflplayer.passingcompletions) + (0.000001)::double precision)))::numeric, 2) AS passyardcomp,
    round(((sum(nflplayer.passingyards) / (sum(nflplayer.passingattempts) + (0.000001)::double precision)))::numeric, 2) AS passyardattempt,
    round(((sum(nflplayer.rushingyards) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS rushpergame,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.receptions))::numeric + 0.000001))::double precision))::numeric, 2) AS yardspercarry,
    sum(nflplayer.receivingyards) AS totreceivingyards,
    sum(nflplayer.receptions) AS totreceptions,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS recyardspergame,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.receivingtargets))::numeric + 0.000001))::double precision))::numeric, 2) AS recyardsattempt,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.receptions))::numeric + 0.000001))::double precision))::numeric, 2) AS recyardscompletion,
    sum(nflplayer.fieldgoalsattempted) AS fga,
    sum(nflplayer.fieldgoalsmade) AS fgm,
    round((((sum(nflplayer.fieldgoalsmade) / (((sum(nflplayer.fieldgoalsattempted))::numeric + 0.000001))::double precision) * (100)::double precision))::numeric, 2) AS fgpercent,
    sum(nflplayer.extrapointsattempted) AS patattempt,
    sum(nflplayer.extrapointsmade) AS patmade,
    (((sum(nflplayer.fieldgoalsmade20to29) + sum(nflplayer.fieldgoalsmade30to39)) + sum(nflplayer.fieldgoalsmade40to49)) + sum(nflplayer.fieldgoalsmade50plus)) AS fieldgoalsover20,
    ((sum(nflplayer.fieldgoalsmade30to39) + sum(nflplayer.fieldgoalsmade40to49)) + sum(nflplayer.fieldgoalsmade50plus)) AS fieldgoalsover30,
    (sum(nflplayer.fieldgoalsmade40to49) + sum(nflplayer.fieldgoalsmade50plus)) AS fieldgoalsover40,
    sum(nflplayer.fieldgoalsmade50plus) AS fieldgoalsover50,
    (sum(nflplayer.assistedtackles) + sum(nflplayer.solotackles)) AS totaltackles,
    sum(nflplayer.solotackles) AS solotackles,
    sum(nflplayer.assistedtackles) AS assistedtackles,
    sum(nflplayer.sacks) AS sacks,
    sum(nflplayer.interceptions) AS interceptions,
    sum(nflplayer.interceptionreturntouchdowns) AS intreturntds,
    sum(nflplayer.fumblesforced) AS forcedfumbles,
    sum(nflplayer.fumblesrecovered) AS fumblerecovery,
    sum(nflplayer.fumblereturntouchdowns) AS fumreturntds,
    (sum(nflplayer.kickreturnyards) + sum(nflplayer.puntreturnyards)) AS krpryards,
    (sum(nflplayer.kickreturntouchdowns) + sum(nflplayer.puntreturntouchdowns)) AS krprtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = 1))
  GROUP BY nflplayer.playerid, nflplayer.playername, nflplayer.playerposition, nflplayer.currentteam, nflplayer.playerageinseason, nflplayer.contractyear, nflplayer.byeweek, nflplayer.nightgame, nflplayer.primetime, nflplayer.season, nflplayer.seasontype, nflplayer.week, nflplayer.newweeks, nflplayer.opponent, nflplayer.homeoraway, nflplayer.grassorturf, nflplayer.indooroutdoor, nflplayer.vsheadcoach, nflplayer.vsoffensivecoordinator, nflplayer.vsdefensivecoordinator
  WITH NO DATA;


ALTER TABLE public.nflpallsplits OWNER TO admin;

--
-- TOC entry 240 (class 1259 OID 176845)
-- Name: nflpallsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpallsplits2 AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints AS ffp,
    nflplayer.passingattempts AS patts,
    nflplayer.rushingattempts AS ratts,
    nflplayer.receivingtargets AS rtars,
    nflplayer.touchdowns AS tds,
    nflplayer.passingyards AS pyd,
    nflplayer.rushingyards AS ruyd,
    nflplayer.receivingyards AS reyd,
    nflplayer.receptions AS recp,
    nflplayer.fortyplustot AS fplus,
    nflplayer.passinginterceptions AS inter,
    nflplayer.fumbles AS fum,
    nflplayer.fumbleslost AS fuml,
    nflplayer.twentyplustot AS tplus,
    nflplayer.longyardtot AS lgy,
    nflplayer.passingcompletions AS passc,
    nflplayer.fieldgoalsattempted AS fga,
    nflplayer.fieldgoalsmade AS fgm,
    nflplayer.fieldgoalshadblocked AS fgsblocked,
    nflplayer.extrapointsattempted AS patattempt,
    nflplayer.extrapointsmade AS patmade,
    nflplayer.extrapointshadblocked AS epblocked,
    nflplayer.fieldgoalsmade20to29 AS f20s,
    nflplayer.fieldgoalsmade30to39 AS f30s,
    nflplayer.fieldgoalsmade40to49 AS f40s,
    nflplayer.fieldgoalsmade50plus AS f50p,
    nflplayer.fieldgoalsmade60plus AS f60p,
    nflplayer.offensivesnapsplayed AS osnaps,
    nflplayer.fantasypointspremall AS fpprem,
    nflplayer.passingyards25d AS pfpyd1,
    nflplayer.passingyards30 AS pfpyd2,
    nflplayer.passingyards20 AS pfpyd3,
    nflplayer.rushingyards10d AS pfruy1,
    nflplayer.rushingyards15 AS pfruy2,
    nflplayer.rushingyards20 AS pfruy3,
    nflplayer.reeptions5d AS pfrec1,
    nflplayer.reeptions1 AS pfrec2,
    nflplayer.reeptions0 AS pfrec3,
    nflplayer.receivingyards20d AS pfryd1,
    nflplayer.receivingyards15 AS pfryd2,
    nflplayer.receivingyards10 AS pfryd3,
    nflplayer.passinginterceptions2d AS pfint1,
    nflplayer.passinginterceptions3 AS pfint2,
    nflplayer.passinginterceptions1 AS pfint3,
    nflplayer.touchdowns6d AS pftds1,
    nflplayer.touchdowns5 AS pftds2,
    nflplayer.touchdowns4 AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles,
    nflplayer.assistedtackles,
    nflplayer.sacks,
    nflplayer.interceptions,
    nflplayer.interceptionreturntouchdowns AS intreturntds,
    nflplayer.fumblesforced AS forcedfumbles,
    nflplayer.fumblesrecovered AS fumblerecovery,
    nflplayer.fumblereturntouchdowns AS fumreturntds,
    nflplayer.kickreturnyards AS kryards,
    nflplayer.puntreturnyards AS pryards,
    nflplayer.kickreturntouchdowns AS krtouchdowns,
    nflplayer.puntreturntouchdowns AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpallsplits2 OWNER TO admin;

--
-- TOC entry 232 (class 1259 OID 176701)
-- Name: nflpalltsndefault; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpalltsndefault AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.team,
    nflplayer.currentteam AS curteam,
    nflplayer.playerageinseason AS age,
    nflplayer.byeweek AS bye,
    sum(nflplayer.pingame) AS gamesplayed,
    sum(nflplayer.fantasypoints) AS sumfantasypoints,
    round(((sum(nflplayer.fantasypoints) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS fantasyppg,
    max(nflplayer.fantasypoints) AS pceiling,
    min(nflplayer.fantasypoints) FILTER (WHERE (nflplayer.pingame = 1)) AS pfloor,
    ((sum(nflplayer.passingattempts) + sum(nflplayer.rushingattempts)) + sum(nflplayer.receivingtargets)) AS act,
    sum(nflplayer.touchdowns) AS tds,
    ((sum(nflplayer.rushingyards) + sum(nflplayer.receivingyards)) + sum(nflplayer.passingyards)) AS syrd,
    round(((((sum(nflplayer.rushingyards) + sum(nflplayer.receivingyards)) + sum(nflplayer.passingyards)) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS sypg,
    sum(nflplayer.passingattempts) AS patt,
    sum(nflplayer.receivingtargets) AS targets,
    sum(nflplayer.passingyards) AS pyrd,
    sum(nflplayer.rushingyards) AS ryrd,
    sum(nflplayer.receivingyards) AS recyrd,
    sum(nflplayer.receptions) AS recp,
    sum(nflplayer.fortyplustot) AS fortyp,
    sum(nflplayer.interceptions) AS tint,
    sum(nflplayer.fumbles) AS fum,
    sum(nflplayer.fumbleslost) AS fml,
    sum(nflplayer.twentyplustot) AS twentyp,
    max(nflplayer.longyardtot) AS lyrd,
    round(((sum(nflplayer.passingyards) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS pypg,
    round((((sum(nflplayer.passingcompletions) / (sum(nflplayer.passingattempts) + (0.000001)::double precision)) * (100)::double precision))::numeric, 1) AS pcp,
    round(((sum(nflplayer.passingyards) / (sum(nflplayer.passingcompletions) + (0.000001)::double precision)))::numeric, 2) AS pyc,
    round(((sum(nflplayer.passingyards) / (sum(nflplayer.passingattempts) + (0.000001)::double precision)))::numeric, 2) AS passyardattempt,
    round(((sum(nflplayer.rushingyards) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS rushpergame,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.receptions))::numeric + 0.000001))::double precision))::numeric, 2) AS yardspercarry,
    sum(nflplayer.passingcompletions) AS comp,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.pingame))::numeric + 0.000001))::double precision))::numeric, 2) AS recyardspergame,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.receivingtargets))::numeric + 0.000001))::double precision))::numeric, 2) AS recyardsattempt,
    round(((sum(nflplayer.receivingyards) / (((sum(nflplayer.receptions))::numeric + 0.000001))::double precision))::numeric, 2) AS recyardscompletion,
    sum(nflplayer.fieldgoalsattempted) AS fga,
    sum(nflplayer.fieldgoalsmade) AS fgm,
    round((((sum(nflplayer.fieldgoalsmade) / (((sum(nflplayer.fieldgoalsattempted))::numeric + 0.000001))::double precision) * (100)::double precision))::numeric, 2) AS fgpercent,
    sum(nflplayer.extrapointsattempted) AS patattempt,
    sum(nflplayer.extrapointsmade) AS patmade,
    round((((sum(nflplayer.extrapointsmade) / (((sum(nflplayer.extrapointsattempted))::numeric + 0.000001))::double precision) * (100)::double precision))::numeric, 2) AS patpercent,
    sum(nflplayer.extrapointshadblocked) AS patblock,
    (((sum(nflplayer.fieldgoalsmade20to29) + sum(nflplayer.fieldgoalsmade30to39)) + sum(nflplayer.fieldgoalsmade40to49)) + sum(nflplayer.fieldgoalsmade50plus)) AS fieldgoalsover20,
    ((sum(nflplayer.fieldgoalsmade30to39) + sum(nflplayer.fieldgoalsmade40to49)) + sum(nflplayer.fieldgoalsmade50plus)) AS fieldgoalsover30,
    (sum(nflplayer.fieldgoalsmade40to49) + sum(nflplayer.fieldgoalsmade50plus)) AS fieldgoalsover40,
    sum(nflplayer.fieldgoalsmade50plus) AS fieldgoalsover50,
    (sum(nflplayer.assistedtackles) + sum(nflplayer.solotackles)) AS totaltackles,
    sum(nflplayer.solotackles) AS solotackles,
    sum(nflplayer.assistedtackles) AS assistedtackles,
    sum(nflplayer.sacks) AS sacks,
    sum(nflplayer.interceptions) AS interceptions,
    sum(nflplayer.interceptionreturntouchdowns) AS intreturntds,
    sum(nflplayer.fumblesforced) AS forcedfumbles,
    sum(nflplayer.fumblesrecovered) AS fumblerecovery,
    sum(nflplayer.fumblereturntouchdowns) AS fumreturntds,
    (sum(nflplayer.kickreturnyards) + sum(nflplayer.puntreturnyards)) AS krpryards,
    (sum(nflplayer.kickreturntouchdowns) + sum(nflplayer.puntreturntouchdowns)) AS krprtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY[('QB'::character varying)::text, ('WR'::character varying)::text, ('K'::character varying)::text, ('RB'::character varying)::text, ('TE'::character varying)::text, ('K'::character varying)::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND (nflplayer.season = '2016'::bpchar) AND (nflplayer.seasontype = 1) AND nflplayer.ffp2017)
  GROUP BY nflplayer.playerposition, nflplayer.playername, nflplayer.team, nflplayer.currentteam, nflplayer.playerageinseason, nflplayer.contractyear, nflplayer.byeweek
  WITH NO DATA;


ALTER TABLE public.nflpalltsndefault OWNER TO admin;

--
-- TOC entry 193 (class 1259 OID 24711)
-- Name: nflprz; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.nflprz (
    playerid bigint,
    seasontype smallint,
    season character(4),
    gamedate date,
    mmonth integer,
    dday integer,
    yyear integer,
    hhour integer,
    mminute integer,
    week smallint,
    newweeks public.weekst,
    primetime character(15),
    dayofweek public.weekday,
    team character(3),
    opponent character(3),
    homeoraway public.haway,
    pnumber character(3),
    playername character varying(50),
    playerposition character varying(10),
    nflpposition public.ppositions,
    positioncategory character(4),
    target integer,
    teamcity character varying(20),
    teamname character varying(20),
    conference character(3),
    division character(7),
    teamkey2 character(3),
    fullteamname character varying(50),
    playerticklessname character varying(50),
    playerfirstname character varying(25),
    playerlastname character varying(25),
    playernickname character varying(50),
    activated boolean,
    played boolean,
    started boolean,
    passingattempts real,
    passingcompletions real,
    passingyards real,
    passingcompletionpercentage real,
    passingyardsperattempt real,
    passingyardspercompletion real,
    passingtouchdowns real,
    passinginterceptions real,
    passingrating real,
    passinglong real,
    passingsacks real,
    passingsackyards real,
    rushingattempts real,
    rushingyards real,
    rushingyardsperattempt real,
    rushingtouchdowns real,
    rushinglong real,
    receivingtargets real,
    receptions real,
    receivingyards real,
    receivingyardsperreception real,
    receivingtouchdowns real,
    receivinglong real,
    fumbles real,
    fumbleslost real,
    puntreturns real,
    puntreturnyards real,
    puntreturnyardsperattempt real,
    puntreturntouchdowns real,
    puntreturnlong real,
    kickreturns real,
    kickreturnyards real,
    kickreturnyardsperattempt real,
    kickreturntouchdowns real,
    kickreturnlong real,
    solotackles real,
    assistedtackles real,
    tacklesforloss real,
    sacks real,
    sackyards real,
    quarterbackhits real,
    passesdefended real,
    fumblesforced real,
    fumblesrecovered real,
    fumblereturnyards real,
    fumblereturntouchdowns real,
    interceptions real,
    interceptionreturnyards real,
    interceptionreturntouchdowns real,
    blockedkicks real,
    specialteamssolotackles real,
    specialteamsassistedtackles real,
    miscsolotackles real,
    miscassistedtackles real,
    punts real,
    puntyards real,
    puntaverage real,
    fieldgoalsattempted real,
    fieldgoalsmade real,
    fieldgoalslongestmade real,
    extrapointsmade real,
    twopointconversionpasses real,
    twopointconversionruns real,
    twopointconversionreceptions real,
    fantasypoints real,
    receptionpercentage real,
    receivingyardspertarget real,
    tackles real,
    offensivetouchdowns real,
    defensivetouchdowns real,
    specialteamstouchdowns real,
    touchdowns real,
    passingyards25d real,
    passingyards30 real,
    passingyards20 real,
    rushingyards10d real,
    rushingyards15 real,
    rushingyards20 real,
    reeptions5d real,
    reeptions1 real,
    reeptions0 real,
    receivingyards20d real,
    receivingyards15 real,
    receivingyards10 real,
    passinginterceptions2d real,
    passinginterceptions3 real,
    passinginterceptions1 real,
    touchdowns6d real,
    touchdowns5 real,
    touchdowns4 real,
    fieldgoalpercentage real,
    fumblesownrecoveries real,
    fumblesoutofbounds real,
    kickreturnfaircatches real,
    puntreturnfaircatches real,
    punttouchbacks real,
    puntinside20 real,
    puntnetaverage real,
    extrapointsattempted real,
    blockedkickreturntouchdowns real,
    fieldgoalreturntouchdowns real,
    safeties real,
    fieldgoalshadblocked real,
    puntshadblocked real,
    extrapointshadblocked real,
    puntlong real,
    blockedkickreturnyards real,
    fieldgoalreturnyards real,
    puntnetyards real,
    specialteamsfumblesforced real,
    specialteamsfumblesrecovered real,
    miscfumblesforced real,
    miscfumblesrecovered real,
    shortname character varying(40),
    playingsurface character varying(15),
    isgameover boolean,
    safetiesallowed smallint,
    stadium character varying(100),
    indooroutdoor public.venuet,
    grassorturf public.fieldt,
    codetemp character(1),
    codewind character(1),
    codewcond character(1),
    temprange public.tempt,
    wind public.windt,
    temperature real,
    humidity real,
    windspeed real,
    offensivesnapsplayed real,
    defensivesnapsplayed real,
    specialteamssnapsplayed real,
    offensiveteamsnaps real,
    defensiveteamsnaps real,
    specialteamsteamsnaps real,
    twopointconversionreturns real,
    fieldgoalsmade0to19 real,
    fieldgoalsmade20to29 real,
    fieldgoalsmade30to39 real,
    fieldgoalsmade40to49 real,
    fieldgoalsmade50plus real,
    injurystatus character varying(50),
    injurybodypart character varying(50),
    injurystartdate date,
    injurynotes text,
    opponentrank character varying(10),
    opponentpositionrank character varying(10),
    injurypractice character varying(50),
    injurypracticedescription character varying(50),
    declaredinactive boolean,
    teamid integer,
    opponentid integer,
    dayofgame date,
    datetime date,
    globalopponentid integer,
    passingattempts1q real,
    passingattempts2q real,
    passingattempts3q real,
    passingattempts4q real,
    passingattempts1h real,
    passingattempts2h real,
    passingattemptsovt real,
    passingyards1q real,
    passingyards2q real,
    passingyards3q real,
    passingyards4q real,
    passingyards1h real,
    passingyards2h real,
    passingyardsovt real,
    passingcompletionpercentage1q real,
    passingcompletionpercentage2q real,
    passingcompletionpercentage3q real,
    passingcompletionpercentage4q real,
    passingcompletionpercentage1h real,
    passingcompletionpercentage2h real,
    passingcompletionpercentageovt real,
    passingyardsperattempt1q real,
    passingyardsperattempt2q real,
    passingyardsperattempt3q real,
    passingyardsperattempt4q real,
    passingyardsperattempt1h real,
    passingyardsperattempt2h real,
    passingyardsperattemptovt real,
    passingyardspercompletion1q real,
    passingyardspercompletion2q real,
    passingyardspercompletion3q real,
    passingyardspercompletion4q real,
    passingyardspercompletion1h real,
    passingyardspercompletion2h real,
    passingyardspercompletionovt real,
    passingtouchdowns1q real,
    passingtouchdowns2q real,
    passingtouchdowns3q real,
    passingtouchdowns4q real,
    passingtouchdowns1h real,
    passingtouchdowns2h real,
    passingtouchdownsovt real,
    passinginterceptions1q real,
    passinginterceptions2q real,
    passinginterceptions3q real,
    passinginterceptions4q real,
    passinginterceptions1h real,
    passinginterceptions2h real,
    passinginterceptionsovt real,
    passingrating1q real,
    passingrating2q real,
    passingrating3q real,
    passingrating4q real,
    passingrating1h real,
    passingrating2h real,
    passingratingovt real,
    passinglong1q real,
    passinglong2q real,
    passinglong3q real,
    passinglong4q real,
    passinglong1h real,
    passinglong2h real,
    passinglongovt real,
    passingsacks1q real,
    passingsacks2q real,
    passingsacks3q real,
    passingsacks4q real,
    passingsacks1h real,
    passingsacks2h real,
    passingsacksovt real,
    passingsackyards1q real,
    passingsackyards2q real,
    passingsackyards3q real,
    passingsackyards4q real,
    passingsackyards1h real,
    passingsackyards2h real,
    passingsackyardsovt real,
    rushingattempts1q real,
    rushingattempts2q real,
    rushingattempts3q real,
    rushingattempts4q real,
    rushingattempts1h real,
    rushingattempts2h real,
    rushingattemptsovt real,
    rushingyards1q real,
    rushingyards2q real,
    rushingyards3q real,
    rushingyards4q real,
    rushingyards1h real,
    rushingyards2h real,
    rushingyardsovt real,
    rushingyardsperattempt1q real,
    rushingyardsperattempt2q real,
    rushingyardsperattempt3q real,
    rushingyardsperattempt4q real,
    rushingyardsperattempt1h real,
    rushingyardsperattempt2h real,
    rushingyardsperattemptovt real,
    rushingtouchdowns1q real,
    rushingtouchdowns2q real,
    rushingtouchdowns3q real,
    rushingtouchdowns4q real,
    rushingtouchdowns1h real,
    rushingtouchdowns2h real,
    rushingtouchdownsovt real,
    rushinglong1q real,
    rushinglong2q real,
    rushinglong3q real,
    rushinglong4q real,
    rushinglong1h real,
    rushinglong2h real,
    rushinglongovt real,
    receivingtargets1q real,
    receivingtargets2q real,
    receivingtargets3q real,
    receivingtargets4q real,
    receivingtargets1h real,
    receivingtargets2h real,
    receivingtargetsovt real,
    receptions1q real,
    receptions2q real,
    receptions3q real,
    receptions4q real,
    receptions1h real,
    receptions2h real,
    receptionsovt real,
    receivingyards1q real,
    receivingyards2q real,
    receivingyards3q real,
    receivingyards4q real,
    receivingyards1h real,
    receivingyards2h real,
    receivingyardsovt real,
    receivingyardsperreception1q real,
    receivingyardsperreception2q real,
    receivingyardsperreception3q real,
    receivingyardsperreception4q real,
    receivingyardsperreception1h real,
    receivingyardsperreception2h real,
    receivingyardsperreceptionovt real,
    receivingtouchdowns1q real,
    receivingtouchdowns2q real,
    receivingtouchdowns3q real,
    receivingtouchdowns4q real,
    receivingtouchdowns1h real,
    receivingtouchdowns2h real,
    receivingtouchdownsovt real,
    receivinglong1q real,
    receivinglong2q real,
    receivinglong3q real,
    receivinglong4q real,
    receivinglong1h real,
    receivinglong2h real,
    receivinglongovt real,
    fumbles1q real,
    fumbles2q real,
    fumbles3q real,
    fumbles4q real,
    fumbles1h real,
    fumbles2h real,
    fumblesovt real,
    fumbleslost1q real,
    fumbleslost2q real,
    fumbleslost3q real,
    fumbleslost4q real,
    fumbleslost1h real,
    fumbleslost2h real,
    fumbleslostovt real,
    puntreturns1q real,
    puntreturns2q real,
    puntreturns3q real,
    puntreturns4q real,
    puntreturns1h real,
    puntreturns2h real,
    puntreturnsovt real,
    puntreturnyards1q real,
    puntreturnyards2q real,
    puntreturnyards3q real,
    puntreturnyards4q real,
    puntreturnyards1h real,
    puntreturnyards2h real,
    puntreturnyardsovt real,
    puntreturnyardsperattempt1q real,
    puntreturnyardsperattempt2q real,
    puntreturnyardsperattempt3q real,
    puntreturnyardsperattempt4q real,
    puntreturnyardsperattempt1h real,
    puntreturnyardsperattempt2h real,
    puntreturnyardsperattemptovt real,
    puntreturntouchdowns1q real,
    puntreturntouchdowns2q real,
    puntreturntouchdowns3q real,
    puntreturntouchdowns4q real,
    puntreturntouchdowns1h real,
    puntreturntouchdowns2h real,
    puntreturntouchdownsovt real,
    puntreturnlong1q real,
    puntreturnlong2q real,
    puntreturnlong3q real,
    puntreturnlong4q real,
    puntreturnlong1h real,
    puntreturnlong2h real,
    puntreturnlongovt real,
    kickreturns1q real,
    kickreturns2q real,
    kickreturns3q real,
    kickreturns4q real,
    kickreturns1h real,
    kickreturns2h real,
    kickreturnsovt real,
    kickreturnyards1q real,
    kickreturnyards2q real,
    kickreturnyards3q real,
    kickreturnyards4q real,
    kickreturnyards1h real,
    kickreturnyards2h real,
    kickreturnyardsovt real,
    kickreturnyardsperattempt1q real,
    kickreturnyardsperattempt2q real,
    kickreturnyardsperattempt3q real,
    kickreturnyardsperattempt4q real,
    kickreturnyardsperattempt1h real,
    kickreturnyardsperattempt2h real,
    kickreturnyardsperattemptovt real,
    kickreturntouchdowns1q real,
    kickreturntouchdowns2q real,
    kickreturntouchdowns3q real,
    kickreturntouchdowns4q real,
    kickreturntouchdowns1h real,
    kickreturntouchdowns2h real,
    kickreturntouchdownsovt real,
    kickreturnlong1q real,
    kickreturnlong2q real,
    kickreturnlong3q real,
    kickreturnlong4q real,
    kickreturnlong1h real,
    kickreturnlong2h real,
    kickreturnlongovt real,
    solotackles1q real,
    solotackles2q real,
    solotackles3q real,
    solotackles4q real,
    solotackles1h real,
    solotackles2h real,
    solotacklesovt real,
    assistedtackles1q real,
    assistedtackles2q real,
    assistedtackles3q real,
    assistedtackles4q real,
    assistedtackles1h real,
    assistedtackles2h real,
    assistedtacklesovt real,
    tacklesforloss1q real,
    tacklesforloss2q real,
    tacklesforloss3q real,
    tacklesforloss4q real,
    tacklesforloss1h real,
    tacklesforloss2h real,
    tacklesforlossovt real,
    sacks1q real,
    sacks2q real,
    sacks3q real,
    sacks4q real,
    sacks1h real,
    sacks2h real,
    sacksovt real,
    sackyards1q real,
    sackyards2q real,
    sackyards3q real,
    sackyards4q real,
    sackyards1h real,
    sackyards2h real,
    sackyardsovt real,
    quarterbackhits1q real,
    quarterbackhits2q real,
    quarterbackhits3q real,
    quarterbackhits4q real,
    quarterbackhits1h real,
    quarterbackhits2h real,
    quarterbackhitsovt real,
    passesdefended1q real,
    passesdefended2q real,
    passesdefended3q real,
    passesdefended4q real,
    passesdefended1h real,
    passesdefended2h real,
    passesdefendedovt real,
    fumblesforced1q real,
    fumblesforced2q real,
    fumblesforced3q real,
    fumblesforced4q real,
    fumblesforced1h real,
    fumblesforced2h real,
    fumblesforcedovt real,
    fumblesrecovered1q real,
    fumblesrecovered2q real,
    fumblesrecovered3q real,
    fumblesrecovered4q real,
    fumblesrecovered1h real,
    fumblesrecovered2h real,
    fumblesrecoveredovt real,
    fumblereturnyards1q real,
    fumblereturnyards2q real,
    fumblereturnyards3q real,
    fumblereturnyards4q real,
    fumblereturnyards1h real,
    fumblereturnyards2h real,
    fumblereturnyardsovt real,
    fumblereturntouchdowns1q real,
    fumblereturntouchdowns2q real,
    fumblereturntouchdowns3q real,
    fumblereturntouchdowns4q real,
    fumblereturntouchdowns1h real,
    fumblereturntouchdowns2h real,
    fumblereturntouchdownsovt real,
    interceptions1q real,
    interceptions2q real,
    interceptions3q real,
    interceptions4q real,
    interceptions1h real,
    interceptions2h real,
    interceptionsovt real,
    interceptionreturnyards1q real,
    interceptionreturnyards2q real,
    interceptionreturnyards3q real,
    interceptionreturnyards4q real,
    interceptionreturnyards1h real,
    interceptionreturnyards2h real,
    interceptionreturnyardsovt real,
    interceptionreturntouchdowns1q real,
    interceptionreturntouchdowns2q real,
    interceptionreturntouchdowns3q real,
    interceptionreturntouchdowns4q real,
    interceptionreturntouchdowns1h real,
    interceptionreturntouchdowns2h real,
    interceptionreturntouchdownsovt real,
    blockedkicks1q real,
    blockedkicks2q real,
    blockedkicks3q real,
    blockedkicks4q real,
    blockedkicks1h real,
    blockedkicks2h real,
    blockedkicksovt real,
    specialteamssolotackles1q real,
    specialteamssolotackles2q real,
    specialteamssolotackles3q real,
    specialteamssolotackles4q real,
    specialteamssolotackles1h real,
    specialteamssolotackles2h real,
    specialteamssolotacklesovt real,
    specialteamsassistedtackles1q real,
    specialteamsassistedtackles2q real,
    specialteamsassistedtackles3q real,
    specialteamsassistedtackles4q real,
    specialteamsassistedtackles1h real,
    specialteamsassistedtackles2h real,
    specialteamsassistedtacklesovt real,
    miscsolotackles1q real,
    miscsolotackles2q real,
    miscsolotackles3q real,
    miscsolotackles4q real,
    miscsolotackles1h real,
    miscsolotackles2h real,
    miscsolotacklesovt real,
    miscassistedtackles1q real,
    miscassistedtackles2q real,
    miscassistedtackles3q real,
    miscassistedtackles4q real,
    miscassistedtackles1h real,
    miscassistedtackles2h real,
    miscassistedtacklesovt real,
    punts1q real,
    punts2q real,
    punts3q real,
    punts4q real,
    punts1h real,
    punts2h real,
    puntsovt real,
    puntyards1q real,
    puntyards2q real,
    puntyards3q real,
    puntyards4q real,
    puntyards1h real,
    puntyards2h real,
    puntyardsovt real,
    puntaverage1q real,
    puntaverage2q real,
    puntaverage3q real,
    puntaverage4q real,
    puntaverage1h real,
    puntaverage2h real,
    puntaverageovt real,
    fieldgoalsattempted1q real,
    fieldgoalsattempted2q real,
    fieldgoalsattempted3q real,
    fieldgoalsattempted4q real,
    fieldgoalsattempted1h real,
    fieldgoalsattempted2h real,
    fieldgoalsattemptedovt real,
    fieldgoalsmade1q real,
    fieldgoalsmade2q real,
    fieldgoalsmade3q real,
    fieldgoalsmade4q real,
    fieldgoalsmade1h real,
    fieldgoalsmade2h real,
    fieldgoalsmadeovt real,
    fieldgoalslongestmade1q real,
    fieldgoalslongestmade2q real,
    fieldgoalslongestmade3q real,
    fieldgoalslongestmade4q real,
    fieldgoalslongestmade1h real,
    fieldgoalslongestmade2h real,
    fieldgoalslongestmadeovt real,
    extrapointsmade1q real,
    extrapointsmade2q real,
    extrapointsmade3q real,
    extrapointsmade4q real,
    extrapointsmade1h real,
    extrapointsmade2h real,
    extrapointsmadeovt real,
    twopointconversionpasses1q real,
    twopointconversionpasses2q real,
    twopointconversionpasses3q real,
    twopointconversionpasses4q real,
    twopointconversionpasses1h real,
    twopointconversionpasses2h real,
    twopointconversionpassesovt real,
    twopointconversionruns1q real,
    twopointconversionruns2q real,
    twopointconversionruns3q real,
    twopointconversionruns4q real,
    twopointconversionruns1h real,
    twopointconversionruns2h real,
    twopointconversionrunsovt real,
    twopointconversionreceptions1q real,
    twopointconversionreceptions2q real,
    twopointconversionreceptions3q real,
    twopointconversionreceptions4q real,
    twopointconversionreceptions1h real,
    twopointconversionreceptions2h real,
    twopointconversionreceptionsovt real,
    fantasypoints1q real,
    fantasypoints2q real,
    fantasypoints3q real,
    fantasypoints4q real,
    fantasypoints1h real,
    fantasypoints2h real,
    fantasypointsovt real,
    receptionpercentage1q real,
    receptionpercentage2q real,
    receptionpercentage3q real,
    receptionpercentage4q real,
    receptionpercentage1h real,
    receptionpercentage2h real,
    receptionpercentageovt real,
    receivingyardspertarget1q real,
    receivingyardspertarget2q real,
    receivingyardspertarget3q real,
    receivingyardspertarget4q real,
    receivingyardspertarget1h real,
    receivingyardspertarget2h real,
    receivingyardspertargetovt real,
    tackles1q real,
    tackles2q real,
    tackles3q real,
    tackles4q real,
    tackles1h real,
    tackles2h real,
    tacklesovt real,
    offensivetouchdowns1q real,
    offensivetouchdowns2q real,
    offensivetouchdowns3q real,
    offensivetouchdowns4q real,
    offensivetouchdowns1h real,
    offensivetouchdowns2h real,
    offensivetouchdownsovt real,
    defensivetouchdowns1q real,
    defensivetouchdowns2q real,
    defensivetouchdowns3q real,
    defensivetouchdowns4q real,
    defensivetouchdowns1h real,
    defensivetouchdowns2h real,
    defensivetouchdownsovt real,
    specialteamstouchdowns1q real,
    specialteamstouchdowns2q real,
    specialteamstouchdowns3q real,
    specialteamstouchdowns4q real,
    specialteamstouchdowns1h real,
    specialteamstouchdowns2h real,
    specialteamstouchdownsovt real,
    touchdowns1q real,
    touchdowns2q real,
    touchdowns3q real,
    touchdowns4q real,
    touchdowns1h real,
    touchdowns2h real,
    touchdownsovt real,
    passingyards25d1q real,
    passingyards25d2q real,
    passingyards25d3q real,
    passingyards25d4q real,
    passingyards25d1h real,
    passingyards25d2h real,
    passingyards25dovt real,
    passingyards301q real,
    passingyards302q real,
    passingyards303q real,
    passingyards304q real,
    passingyards301h real,
    passingyards302h real,
    passingyards30ovt real,
    passingyards201q real,
    passingyards202q real,
    passingyards203q real,
    passingyards204q real,
    passingyards201h real,
    passingyards202h real,
    passingyards20ovt real,
    rushingyards10d1q real,
    rushingyards10d2q real,
    rushingyards10d3q real,
    rushingyards10d4q real,
    rushingyards10d1h real,
    rushingyards10d2h real,
    rushingyards10dovt real,
    rushingyards151q real,
    rushingyards152q real,
    rushingyards153q real,
    rushingyards154q real,
    rushingyards151h real,
    rushingyards152h real,
    rushingyards15ovt real,
    rushingyards201q real,
    rushingyards202q real,
    rushingyards203q real,
    rushingyards204q real,
    rushingyards201h real,
    rushingyards202h real,
    rushingyards20ovt real,
    reeptions5d1q real,
    reeptions5d2q real,
    reeptions5d3q real,
    reeptions5d4q real,
    reeptions5d1h real,
    reeptions5d2h real,
    reeptions5dovt real,
    reeptions11q real,
    reeptions12q real,
    reeptions13q real,
    reeptions14q real,
    reeptions11h real,
    reeptions12h real,
    reeptions1ovt real,
    reeptions01q real,
    reeptions02q real,
    reeptions03q real,
    reeptions04q real,
    reeptions01h real,
    reeptions02h real,
    reeptions0ovt real,
    receivingyards20d1q real,
    receivingyards20d2q real,
    receivingyards20d3q real,
    receivingyards20d4q real,
    receivingyards20d1h real,
    receivingyards20d2h real,
    receivingyards20dovt real,
    receivingyards151q real,
    receivingyards152q real,
    receivingyards153q real,
    receivingyards154q real,
    receivingyards151h real,
    receivingyards152h real,
    receivingyards15ovt real,
    receivingyards101q real,
    receivingyards102q real,
    receivingyards103q real,
    receivingyards104q real,
    receivingyards101h real,
    receivingyards102h real,
    receivingyards10ovt real,
    passinginterceptions2d1q real,
    passinginterceptions2d2q real,
    passinginterceptions2d3q real,
    passinginterceptions2d4q real,
    passinginterceptions2d1h real,
    passinginterceptions2d2h real,
    passinginterceptions2dovt real,
    passinginterceptions31q real,
    passinginterceptions32q real,
    passinginterceptions33q real,
    passinginterceptions34q real,
    passinginterceptions31h real,
    passinginterceptions32h real,
    passinginterceptions3ovt real,
    passinginterceptions11q real,
    passinginterceptions12q real,
    passinginterceptions13q real,
    passinginterceptions14q real,
    passinginterceptions11h real,
    passinginterceptions12h real,
    passinginterceptions1ovt real,
    touchdowns6d1q real,
    touchdowns6d2q real,
    touchdowns6d3q real,
    touchdowns6d4q real,
    touchdowns6d1h real,
    touchdowns6d2h real,
    touchdowns6dovt real,
    touchdowns51q real,
    touchdowns52q real,
    touchdowns53q real,
    touchdowns54q real,
    touchdowns51h real,
    touchdowns52h real,
    touchdowns5ovt real,
    touchdowns41q real,
    touchdowns42q real,
    touchdowns43q real,
    touchdowns44q real,
    touchdowns41h real,
    touchdowns42h real,
    touchdowns4ovt real,
    fieldgoalpercentage1q real,
    fieldgoalpercentage2q real,
    fieldgoalpercentage3q real,
    fieldgoalpercentage4q real,
    fieldgoalpercentage1h real,
    fieldgoalpercentage2h real,
    fieldgoalpercentageovt real,
    fumblesownrecoveries1q real,
    fumblesownrecoveries2q real,
    fumblesownrecoveries3q real,
    fumblesownrecoveries4q real,
    fumblesownrecoveries1h real,
    fumblesownrecoveries2h real,
    fumblesownrecoveriesovt real,
    fumblesoutofbounds1q real,
    fumblesoutofbounds2q real,
    fumblesoutofbounds3q real,
    fumblesoutofbounds4q real,
    fumblesoutofbounds1h real,
    fumblesoutofbounds2h real,
    fumblesoutofboundsovt real,
    kickreturnfaircatches1q real,
    kickreturnfaircatches2q real,
    kickreturnfaircatches3q real,
    kickreturnfaircatches4q real,
    kickreturnfaircatches1h real,
    kickreturnfaircatches2h real,
    kickreturnfaircatchesovt real,
    puntreturnfaircatches1q real,
    puntreturnfaircatches2q real,
    puntreturnfaircatches3q real,
    puntreturnfaircatches4q real,
    puntreturnfaircatches1h real,
    puntreturnfaircatches2h real,
    puntreturnfaircatchesovt real,
    punttouchbacks1q real,
    punttouchbacks2q real,
    punttouchbacks3q real,
    punttouchbacks4q real,
    punttouchbacks1h real,
    punttouchbacks2h real,
    punttouchbacksovt real,
    puntinside201q real,
    puntinside202q real,
    puntinside203q real,
    puntinside204q real,
    puntinside201h real,
    puntinside202h real,
    puntinside20ovt real,
    puntnetaverage1q real,
    puntnetaverage2q real,
    puntnetaverage3q real,
    puntnetaverage4q real,
    puntnetaverage1h real,
    puntnetaverage2h real,
    puntnetaverageovt real,
    extrapointsattempted1q real,
    extrapointsattempted2q real,
    extrapointsattempted3q real,
    extrapointsattempted4q real,
    extrapointsattempted1h real,
    extrapointsattempted2h real,
    extrapointsattemptedovt real,
    blockedkickreturntouchdowns1q real,
    blockedkickreturntouchdowns2q real,
    blockedkickreturntouchdowns3q real,
    blockedkickreturntouchdowns4q real,
    blockedkickreturntouchdowns1h real,
    blockedkickreturntouchdowns2h real,
    blockedkickreturntouchdownsovt real,
    fieldgoalreturntouchdowns1q real,
    fieldgoalreturntouchdowns2q real,
    fieldgoalreturntouchdowns3q real,
    fieldgoalreturntouchdowns4q real,
    fieldgoalreturntouchdowns1h real,
    fieldgoalreturntouchdowns2h real,
    fieldgoalreturntouchdownsovt real,
    safeties1q real,
    safeties2q real,
    safeties3q real,
    safeties4q real,
    safeties1h real,
    safeties2h real,
    safetiesovt real,
    fieldgoalshadblocked1q real,
    fieldgoalshadblocked2q real,
    fieldgoalshadblocked3q real,
    fieldgoalshadblocked4q real,
    fieldgoalshadblocked1h real,
    fieldgoalshadblocked2h real,
    fieldgoalshadblockedovt real,
    puntshadblocked1q real,
    puntshadblocked2q real,
    puntshadblocked3q real,
    puntshadblocked4q real,
    puntshadblocked1h real,
    puntshadblocked2h real,
    puntshadblockedovt real,
    extrapointshadblocked1q real,
    extrapointshadblocked2q real,
    extrapointshadblocked3q real,
    extrapointshadblocked4q real,
    extrapointshadblocked1h real,
    extrapointshadblocked2h real,
    extrapointshadblockedovt real,
    puntlong1q real,
    puntlong2q real,
    puntlong3q real,
    puntlong4q real,
    puntlong1h real,
    puntlong2h real,
    puntlongovt real,
    blockedkickreturnyards1q real,
    blockedkickreturnyards2q real,
    blockedkickreturnyards3q real,
    blockedkickreturnyards4q real,
    blockedkickreturnyards1h real,
    blockedkickreturnyards2h real,
    blockedkickreturnyardsovt real,
    fieldgoalreturnyards1q real,
    fieldgoalreturnyards2q real,
    fieldgoalreturnyards3q real,
    fieldgoalreturnyards4q real,
    fieldgoalreturnyards1h real,
    fieldgoalreturnyards2h real,
    fieldgoalreturnyardsovt real,
    puntnetyards1q real,
    puntnetyards2q real,
    puntnetyards3q real,
    puntnetyards4q real,
    puntnetyards1h real,
    puntnetyards2h real,
    puntnetyardsovt real,
    specialteamsfumblesforced1q real,
    specialteamsfumblesforced2q real,
    specialteamsfumblesforced3q real,
    specialteamsfumblesforced4q real,
    specialteamsfumblesforced1h real,
    specialteamsfumblesforced2h real,
    specialteamsfumblesforcedovt real,
    specialteamsfumblesrecovered1q real,
    specialteamsfumblesrecovered2q real,
    specialteamsfumblesrecovered3q real,
    specialteamsfumblesrecovered4q real,
    specialteamsfumblesrecovered1h real,
    specialteamsfumblesrecovered2h real,
    specialteamsfumblesrecoveredovt real,
    miscfumblesforced1q real,
    miscfumblesforced2q real,
    miscfumblesforced3q real,
    miscfumblesforced4q real,
    miscfumblesforced1h real,
    miscfumblesforced2h real,
    miscfumblesforcedovt real,
    miscfumblesrecovered1q real,
    miscfumblesrecovered2q real,
    miscfumblesrecovered3q real,
    miscfumblesrecovered4q real,
    miscfumblesrecovered1h real,
    miscfumblesrecovered2h real,
    miscfumblesrecoveredovt real,
    temprange1q character(1),
    temprange2q character(1),
    temprange3q character(1),
    temprange4q character(1),
    temprange1h character(1),
    temprange2h character(1),
    temprangeovt character(1),
    wind1q character(1),
    wind2q character(1),
    wind3q character(1),
    wind4q character(1),
    wind1h character(1),
    wind2h character(1),
    windovt character(1),
    wconditions1q character varying(10),
    wconditions2q character varying(10),
    wconditions3q character varying(10),
    wconditions4q character varying(10),
    wconditions1h character varying(10),
    wconditions2h character varying(10),
    wconditionsovt character varying(10),
    temperature1q real,
    temperature2q real,
    temperature3q real,
    temperature4q real,
    temperature1h real,
    temperature2h real,
    temperatureovt real,
    humidity1q real,
    humidity2q real,
    humidity3q real,
    humidity4q real,
    humidity1h real,
    humidity2h real,
    humidityovt real,
    windspeed1q real,
    windspeed2q real,
    windspeed3q real,
    windspeed4q real,
    windspeed1h real,
    windspeed2h real,
    windspeedovt real,
    offensivesnapsplayed1q real,
    offensivesnapsplayed2q real,
    offensivesnapsplayed3q real,
    offensivesnapsplayed4q real,
    offensivesnapsplayed1h real,
    offensivesnapsplayed2h real,
    offensivesnapsplayedovt real,
    defensivesnapsplayed1q real,
    defensivesnapsplayed2q real,
    defensivesnapsplayed3q real,
    defensivesnapsplayed4q real,
    defensivesnapsplayed1h real,
    defensivesnapsplayed2h real,
    defensivesnapsplayedovt real,
    specialteamssnapsplayed1q real,
    specialteamssnapsplayed2q real,
    specialteamssnapsplayed3q real,
    specialteamssnapsplayed4q real,
    specialteamssnapsplayed1h real,
    specialteamssnapsplayed2h real,
    specialteamssnapsplayedovt real,
    offensiveteamsnaps1q real,
    offensiveteamsnaps2q real,
    offensiveteamsnaps3q real,
    offensiveteamsnaps4q real,
    offensiveteamsnaps1h real,
    offensiveteamsnaps2h real,
    offensiveteamsnapsovt real,
    defensiveteamsnaps1q real,
    defensiveteamsnaps2q real,
    defensiveteamsnaps3q real,
    defensiveteamsnaps4q real,
    defensiveteamsnaps1h real,
    defensiveteamsnaps2h real,
    defensiveteamsnapsovt real,
    specialteamsteamsnaps1q real,
    specialteamsteamsnaps2q real,
    specialteamsteamsnaps3q real,
    specialteamsteamsnaps4q real,
    specialteamsteamsnaps1h real,
    specialteamsteamsnaps2h real,
    specialteamsteamsnapsovt real,
    fieldgoalsmade0to191q real,
    fieldgoalsmade0to192q real,
    fieldgoalsmade0to193q real,
    fieldgoalsmade0to194q real,
    fieldgoalsmade0to191h real,
    fieldgoalsmade0to192h real,
    fieldgoalsmade0to19ovt real,
    fieldgoalsmade20to291q real,
    fieldgoalsmade20to292q real,
    fieldgoalsmade20to293q real,
    fieldgoalsmade20to294q real,
    fieldgoalsmade20to291h real,
    fieldgoalsmade20to292h real,
    fieldgoalsmade20to29ovt real,
    fieldgoalsmade30to391q real,
    fieldgoalsmade30to392q real,
    fieldgoalsmade30to393q real,
    fieldgoalsmade30to394q real,
    fieldgoalsmade30to391h real,
    fieldgoalsmade30to392h real,
    fieldgoalsmade30to39ovt real,
    fieldgoalsmade40to491q real,
    fieldgoalsmade40to492q real,
    fieldgoalsmade40to493q real,
    fieldgoalsmade40to494q real,
    fieldgoalsmade40to491h real,
    fieldgoalsmade40to492h real,
    fieldgoalsmade40to49ovt real,
    fieldgoalsmade50plus1q real,
    fieldgoalsmade50plus2q real,
    fieldgoalsmade50plus3q real,
    fieldgoalsmade50plus4q real,
    fieldgoalsmade50plus1h real,
    fieldgoalsmade50plus2h real,
    fieldgoalsmade50plusovt real,
    pingame integer,
    fortyplustot integer,
    fortyplus1q integer,
    fortyplus2q integer,
    fortyplus3q integer,
    fortyplusovt integer,
    twentyplustot integer,
    twentyplus1q integer,
    twentyplus2q integer,
    twentyplus3q integer,
    twentyplus4q integer,
    twentyplusovt integer,
    longyardtot real,
    longyard1q real,
    longyard2q real,
    longyard3q real,
    longyard4q real,
    longyardovt real,
    playerageinseason integer,
    contractyear character(4),
    byeweek character(2),
    vsheadcoach character varying(30),
    vsoffensivecoordinator character varying(30),
    vsdefensivecoordinator character varying(30),
    headcoach character varying(30),
    offensivecoordinator character varying(30),
    defensivecoordinator character varying(30),
    longyard1h real,
    longyard2h real,
    fortyplus1h real,
    fortyplus2h real,
    twentyplus1h real,
    twentyplus2h real,
    nightgame boolean,
    primetstart boolean,
    passingcompletions1q integer,
    passingcompletions2q integer,
    passingcompletions3q integer,
    passingcompletions4q integer,
    passingcompletions1h integer,
    passingcompletions2h integer,
    passingcompletionsovt integer,
    fortyplus4q real,
    codewcond1q character(1),
    codewcond2q character(1),
    codewcond3q character(1),
    codewcond4q character(1),
    codewcond1h character(1),
    codewcond2h character(1),
    codewcondovt character(1),
    wconditions public.weathert,
    fieldgoalsmade60plus real,
    fieldgoalsmade60plus1q real,
    fieldgoalsmade60plus2q real,
    fieldgoalsmade60plus3q real,
    fieldgoalsmade60plus4q real,
    fieldgoalsmade60plus1h real,
    fieldgoalsmade60plus2h real,
    fieldgoalsmade60plusovt real,
    ffp2017 boolean,
    passingattempts5y real,
    passingattempts5y1q real,
    passingattempts5y2q real,
    passingattempts5y3q real,
    passingattempts5y4q real,
    passingattempts5y1h real,
    passingattempts5y2h real,
    passingattempts5yovt real,
    rushingattempts5y real,
    rushingattempts5y1q real,
    rushingattempts5y2q real,
    rushingattempts5y3q real,
    rushingattempts5y4q real,
    rushingattempts5y1h real,
    rushingattempts5y2h real,
    rushingattempts5yovt real,
    receivingtargets5y real,
    receivingtargets5y1q real,
    receivingtargets5y2q real,
    receivingtargets5y3q real,
    receivingtargets5y4q real,
    receivingtargets5y1h real,
    receivingtargets5y2h real,
    receivingtargets5yovt real,
    touchdowns10y real,
    touchdowns10y1q real,
    touchdowns10y2q real,
    touchdowns10y3q real,
    touchdowns10y4q real,
    touchdowns10y1h real,
    touchdowns10y2h real,
    touchdowns10yovt real,
    touchdowns3y real,
    touchdowns3y1q real,
    touchdowns3y2q real,
    touchdowns3y3q real,
    touchdowns3y4q real,
    touchdowns3y1h real,
    touchdowns3y2h real,
    touchdowns3yovt real,
    currentteam character(3),
    passcheck1count integer,
    passcheck1 text,
    gamedateteam character(11),
    gamedateteamplayer character varying(61),
    gamedateteamshortname character varying(51),
    zipofstadium character(8),
    fantasypointspremall real,
    fantasypointsprem1q real,
    fantasypointsprem2q real,
    fantasypointsprem3q real,
    fantasypointsprem4q real,
    fantasypointspremovt real,
    fantasypointsprem1h real,
    fantasypointsprem2h real
);


ALTER TABLE public.nflprz OWNER TO admin;

--
-- TOC entry 241 (class 1259 OID 176860)
-- Name: nflprz1hsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflprz1hsplits2 AS
 SELECT nflprz.playerposition AS tpposition,
    nflprz.playername AS player,
    nflprz.currentteam AS team,
    nflprz.playerageinseason AS age,
    nflprz.contractyear AS contract,
    nflprz.byeweek AS bye,
    nflprz.nightgame AS sng,
    nflprz.primetstart AS pgs,
    nflprz.season AS nflseason,
    nflprz.seasontype AS regseason,
    nflprz.week AS regweek,
    nflprz.newweeks AS seasonweeks,
    nflprz.opponent AS vsteam,
    nflprz.homeoraway AS location,
    nflprz.grassorturf AS playsurface,
    nflprz.indooroutdoor AS inoutdoor,
    nflprz.vsheadcoach AS vshcoach,
    nflprz.vsoffensivecoordinator AS vsoffcoor,
    nflprz.vsdefensivecoordinator AS vsdefcood,
    nflprz.pingame AS gplayed,
    nflprz.fantasypoints1h AS ffp,
    nflprz.passingattempts1h AS patts,
    nflprz.rushingattempts1h AS ratts,
    nflprz.receivingtargets1h AS rtars,
    nflprz.touchdowns1h AS tds,
    nflprz.passingyards1h AS pyd,
    nflprz.rushingyards1h AS ruyd,
    nflprz.receivingyards1h AS reyd,
    nflprz.receptions1h AS recp,
    nflprz.fortyplus1h AS fplus,
    nflprz.passinginterceptions1h AS inter,
    nflprz.fumbles1h AS fum,
    nflprz.fumbleslost1h AS fuml,
    nflprz.twentyplus1h AS tplus,
    nflprz.longyard1h AS lgy,
    nflprz.passingcompletions1h AS passc,
    nflprz.fieldgoalsattempted1h AS fga,
    nflprz.fieldgoalsmade1h AS fgm,
    nflprz.fieldgoalshadblocked1h AS fgsblocked,
    nflprz.extrapointsattempted1h AS patattempt,
    nflprz.extrapointsmade1h AS patmade,
    nflprz.extrapointshadblocked1h AS epblocked,
    nflprz.fieldgoalsmade20to291h AS f20s,
    nflprz.fieldgoalsmade30to391h AS f30s,
    nflprz.fieldgoalsmade40to491h AS f40s,
    nflprz.fieldgoalsmade50plus1h AS f50p,
    nflprz.fieldgoalsmade60plus1h AS f60p,
    nflprz.offensivesnapsplayed1h AS osnaps,
    nflprz.fantasypointsprem1h AS fpprem,
    nflprz.passingyards25d1h AS pfpyd1,
    nflprz.passingyards301h AS pfpyd2,
    nflprz.passingyards201h AS pfpyd3,
    nflprz.rushingyards10d1h AS pfruy1,
    nflprz.rushingyards151h AS pfruy2,
    nflprz.rushingyards201h AS pfruy3,
    nflprz.reeptions5d1h AS pfrec1,
    nflprz.reeptions11h AS pfrec2,
    nflprz.reeptions01h AS pfrec3,
    nflprz.receivingyards20d1h AS pfryd1,
    nflprz.receivingyards151h AS pfryd2,
    nflprz.receivingyards101h AS pfryd3,
    nflprz.passinginterceptions2d1h AS pfint1,
    nflprz.passinginterceptions31h AS pfint2,
    nflprz.passinginterceptions11h AS pfint3,
    nflprz.touchdowns6d1h AS pftds1,
    nflprz.touchdowns51h AS pftds2,
    nflprz.touchdowns41h AS pftds3,
    nflprz.codetemp AS ctemp,
    nflprz.codewind AS cwind,
    nflprz.codewcond AS cwcond,
    nflprz.temprange AS trange,
    nflprz.wind AS windr,
    nflprz.wconditions AS wcond,
    nflprz.playerid AS playerident,
    nflprz.currentteam AS curteam,
    nflprz.solotackles1h AS solotackles,
    nflprz.assistedtackles1h AS assistedtackles,
    nflprz.sacks1h AS sacks,
    nflprz.interceptions1h AS interceptions,
    nflprz.interceptionreturntouchdowns1h AS intreturntds,
    nflprz.fumblesforced1h AS forcedfumbles,
    nflprz.fumblesrecovered1h AS fumblerecovery,
    nflprz.fumblereturntouchdowns1h AS fumreturntds,
    nflprz.kickreturnyards1h AS kryards,
    nflprz.puntreturnyards1h AS pryards,
    nflprz.kickreturntouchdowns1h AS krtouchdowns,
    nflprz.puntreturntouchdowns1h AS prtouchdowns
   FROM public.nflprz
  WHERE (((nflprz.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflprz.ffp2017 AND (nflprz.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflprz1hsplits2 OWNER TO admin;

--
-- TOC entry 242 (class 1259 OID 176867)
-- Name: nflprz1qsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflprz1qsplits2 AS
 SELECT nflprz.playerposition AS tpposition,
    nflprz.playername AS player,
    nflprz.currentteam AS team,
    nflprz.playerageinseason AS age,
    nflprz.contractyear AS contract,
    nflprz.byeweek AS bye,
    nflprz.nightgame AS sng,
    nflprz.primetstart AS pgs,
    nflprz.season AS nflseason,
    nflprz.seasontype AS regseason,
    nflprz.week AS regweek,
    nflprz.newweeks AS seasonweeks,
    nflprz.opponent AS vsteam,
    nflprz.homeoraway AS location,
    nflprz.grassorturf AS playsurface,
    nflprz.indooroutdoor AS inoutdoor,
    nflprz.vsheadcoach AS vshcoach,
    nflprz.vsoffensivecoordinator AS vsoffcoor,
    nflprz.vsdefensivecoordinator AS vsdefcood,
    nflprz.pingame AS gplayed,
    nflprz.fantasypoints1q AS ffp,
    nflprz.passingattempts1q AS patts,
    nflprz.rushingattempts1q AS ratts,
    nflprz.receivingtargets1q AS rtars,
    nflprz.touchdowns1q AS tds,
    nflprz.passingyards1q AS pyd,
    nflprz.rushingyards1q AS ruyd,
    nflprz.receivingyards1q AS reyd,
    nflprz.receptions1q AS recp,
    nflprz.fortyplus1q AS fplus,
    nflprz.passinginterceptions1q AS inter,
    nflprz.fumbles1q AS fum,
    nflprz.fumbleslost1q AS fuml,
    nflprz.twentyplus1q AS tplus,
    nflprz.longyard1q AS lgy,
    nflprz.passingcompletions1q AS passc,
    nflprz.fieldgoalsattempted1q AS fga,
    nflprz.fieldgoalsmade1q AS fgm,
    nflprz.fieldgoalshadblocked1q AS fgsblocked,
    nflprz.extrapointsattempted1q AS patattempt,
    nflprz.extrapointsmade1q AS patmade,
    nflprz.extrapointshadblocked1q AS epblocked,
    nflprz.fieldgoalsmade20to291q AS f20s,
    nflprz.fieldgoalsmade30to391q AS f30s,
    nflprz.fieldgoalsmade40to491q AS f40s,
    nflprz.fieldgoalsmade50plus1q AS f50p,
    nflprz.fieldgoalsmade60plus1q AS f60p,
    nflprz.offensivesnapsplayed1q AS osnaps,
    nflprz.fantasypointsprem1q AS fpprem,
    nflprz.passingyards25d1q AS pfpyd1,
    nflprz.passingyards301q AS pfpyd2,
    nflprz.passingyards201q AS pfpyd3,
    nflprz.rushingyards10d1q AS pfruy1,
    nflprz.rushingyards151q AS pfruy2,
    nflprz.rushingyards201q AS pfruy3,
    nflprz.reeptions5d1q AS pfrec1,
    nflprz.reeptions11q AS pfrec2,
    nflprz.reeptions01q AS pfrec3,
    nflprz.receivingyards20d1q AS pfryd1,
    nflprz.receivingyards151q AS pfryd2,
    nflprz.receivingyards101q AS pfryd3,
    nflprz.passinginterceptions2d1q AS pfint1,
    nflprz.passinginterceptions31q AS pfint2,
    nflprz.passinginterceptions11q AS pfint3,
    nflprz.touchdowns6d1q AS pftds1,
    nflprz.touchdowns51q AS pftds2,
    nflprz.touchdowns41q AS pftds3,
    nflprz.codetemp AS ctemp,
    nflprz.codewind AS cwind,
    nflprz.codewcond AS cwcond,
    nflprz.temprange AS trange,
    nflprz.wind AS windr,
    nflprz.wconditions AS wcond,
    nflprz.playerid AS playerident,
    nflprz.currentteam AS curteam,
    nflprz.solotackles1q AS solotackles,
    nflprz.assistedtackles1q AS assistedtackles,
    nflprz.sacks1q AS sacks,
    nflprz.interceptions1q AS interceptions,
    nflprz.interceptionreturntouchdowns1q AS intreturntds,
    nflprz.fumblesforced1q AS forcedfumbles,
    nflprz.fumblesrecovered1q AS fumblerecovery,
    nflprz.fumblereturntouchdowns1q AS fumreturntds,
    nflprz.kickreturnyards1q AS kryards,
    nflprz.puntreturnyards1q AS pryards,
    nflprz.kickreturntouchdowns1q AS krtouchdowns,
    nflprz.puntreturntouchdowns1q AS prtouchdowns
   FROM public.nflprz
  WHERE (((nflprz.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflprz.ffp2017 AND (nflprz.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflprz1qsplits2 OWNER TO admin;

--
-- TOC entry 243 (class 1259 OID 176874)
-- Name: nflprz2hsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflprz2hsplits2 AS
 SELECT nflprz.playerposition AS tpposition,
    nflprz.playername AS player,
    nflprz.currentteam AS team,
    nflprz.playerageinseason AS age,
    nflprz.contractyear AS contract,
    nflprz.byeweek AS bye,
    nflprz.nightgame AS sng,
    nflprz.primetstart AS pgs,
    nflprz.season AS nflseason,
    nflprz.seasontype AS regseason,
    nflprz.week AS regweek,
    nflprz.newweeks AS seasonweeks,
    nflprz.opponent AS vsteam,
    nflprz.homeoraway AS location,
    nflprz.grassorturf AS playsurface,
    nflprz.indooroutdoor AS inoutdoor,
    nflprz.vsheadcoach AS vshcoach,
    nflprz.vsoffensivecoordinator AS vsoffcoor,
    nflprz.vsdefensivecoordinator AS vsdefcood,
    nflprz.pingame AS gplayed,
    nflprz.fantasypoints2h AS ffp,
    nflprz.passingattempts2h AS patts,
    nflprz.rushingattempts2h AS ratts,
    nflprz.receivingtargets2h AS rtars,
    nflprz.touchdowns2h AS tds,
    nflprz.passingyards2h AS pyd,
    nflprz.rushingyards2h AS ruyd,
    nflprz.receivingyards2h AS reyd,
    nflprz.receptions2h AS recp,
    nflprz.fortyplus2h AS fplus,
    nflprz.passinginterceptions2h AS inter,
    nflprz.fumbles2h AS fum,
    nflprz.fumbleslost2h AS fuml,
    nflprz.twentyplus2h AS tplus,
    nflprz.longyard2h AS lgy,
    nflprz.passingcompletions2h AS passc,
    nflprz.fieldgoalsattempted2h AS fga,
    nflprz.fieldgoalsmade2h AS fgm,
    nflprz.fieldgoalshadblocked2h AS fgsblocked,
    nflprz.extrapointsattempted2h AS patattempt,
    nflprz.extrapointsmade2h AS patmade,
    nflprz.extrapointshadblocked2h AS epblocked,
    nflprz.fieldgoalsmade20to292h AS f20s,
    nflprz.fieldgoalsmade30to392h AS f30s,
    nflprz.fieldgoalsmade40to492h AS f40s,
    nflprz.fieldgoalsmade50plus2h AS f50p,
    nflprz.fieldgoalsmade60plus2h AS f60p,
    nflprz.offensivesnapsplayed2h AS osnaps,
    nflprz.fantasypointsprem2h AS fpprem,
    nflprz.passingyards25d2h AS pfpyd1,
    nflprz.passingyards302h AS pfpyd2,
    nflprz.passingyards202h AS pfpyd3,
    nflprz.rushingyards10d2h AS pfruy1,
    nflprz.rushingyards152h AS pfruy2,
    nflprz.rushingyards202h AS pfruy3,
    nflprz.reeptions5d2h AS pfrec1,
    nflprz.reeptions12h AS pfrec2,
    nflprz.reeptions02h AS pfrec3,
    nflprz.receivingyards20d2h AS pfryd1,
    nflprz.receivingyards152h AS pfryd2,
    nflprz.receivingyards102h AS pfryd3,
    nflprz.passinginterceptions2d2h AS pfint1,
    nflprz.passinginterceptions32h AS pfint2,
    nflprz.passinginterceptions12h AS pfint3,
    nflprz.touchdowns6d2h AS pftds1,
    nflprz.touchdowns52h AS pftds2,
    nflprz.touchdowns42h AS pftds3,
    nflprz.codetemp AS ctemp,
    nflprz.codewind AS cwind,
    nflprz.codewcond AS cwcond,
    nflprz.temprange AS trange,
    nflprz.wind AS windr,
    nflprz.wconditions AS wcond,
    nflprz.playerid AS playerident,
    nflprz.currentteam AS curteam,
    nflprz.solotackles2h AS solotackles,
    nflprz.assistedtackles2h AS assistedtackles,
    nflprz.sacks2h AS sacks,
    nflprz.interceptions2h AS interceptions,
    nflprz.interceptionreturntouchdowns2h AS intreturntds,
    nflprz.fumblesforced2h AS forcedfumbles,
    nflprz.fumblesrecovered2h AS fumblerecovery,
    nflprz.fumblereturntouchdowns2h AS fumreturntds,
    nflprz.kickreturnyards2h AS kryards,
    nflprz.puntreturnyards2h AS pryards,
    nflprz.kickreturntouchdowns2h AS krtouchdowns,
    nflprz.puntreturntouchdowns2h AS prtouchdowns
   FROM public.nflprz
  WHERE (((nflprz.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflprz.ffp2017 AND (nflprz.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflprz2hsplits2 OWNER TO admin;

--
-- TOC entry 245 (class 1259 OID 176887)
-- Name: nflprz2qsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflprz2qsplits2 AS
 SELECT nflprz.playerposition AS tpposition,
    nflprz.playername AS player,
    nflprz.currentteam AS team,
    nflprz.playerageinseason AS age,
    nflprz.contractyear AS contract,
    nflprz.byeweek AS bye,
    nflprz.nightgame AS sng,
    nflprz.primetstart AS pgs,
    nflprz.season AS nflseason,
    nflprz.seasontype AS regseason,
    nflprz.week AS regweek,
    nflprz.newweeks AS seasonweeks,
    nflprz.opponent AS vsteam,
    nflprz.homeoraway AS location,
    nflprz.grassorturf AS playsurface,
    nflprz.indooroutdoor AS inoutdoor,
    nflprz.vsheadcoach AS vshcoach,
    nflprz.vsoffensivecoordinator AS vsoffcoor,
    nflprz.vsdefensivecoordinator AS vsdefcood,
    nflprz.pingame AS gplayed,
    nflprz.fantasypoints2q AS ffp,
    nflprz.passingattempts2q AS patts,
    nflprz.rushingattempts2q AS ratts,
    nflprz.receivingtargets2q AS rtars,
    nflprz.touchdowns2q AS tds,
    nflprz.passingyards2q AS pyd,
    nflprz.rushingyards2q AS ruyd,
    nflprz.receivingyards2q AS reyd,
    nflprz.receptions2q AS recp,
    nflprz.fortyplus2q AS fplus,
    nflprz.passinginterceptions2q AS inter,
    nflprz.fumbles2q AS fum,
    nflprz.fumbleslost2q AS fuml,
    nflprz.twentyplus2q AS tplus,
    nflprz.longyard2q AS lgy,
    nflprz.passingcompletions2q AS passc,
    nflprz.fieldgoalsattempted2q AS fga,
    nflprz.fieldgoalsmade2q AS fgm,
    nflprz.fieldgoalshadblocked2q AS fgsblocked,
    nflprz.extrapointsattempted2q AS patattempt,
    nflprz.extrapointsmade2q AS patmade,
    nflprz.extrapointshadblocked2q AS epblocked,
    nflprz.fieldgoalsmade20to292q AS f20s,
    nflprz.fieldgoalsmade30to392q AS f30s,
    nflprz.fieldgoalsmade40to492q AS f40s,
    nflprz.fieldgoalsmade50plus2q AS f50p,
    nflprz.fieldgoalsmade60plus2q AS f60p,
    nflprz.offensivesnapsplayed2q AS osnaps,
    nflprz.fantasypointsprem2q AS fpprem,
    nflprz.passingyards25d2q AS pfpyd1,
    nflprz.passingyards302q AS pfpyd2,
    nflprz.passingyards202q AS pfpyd3,
    nflprz.rushingyards10d2q AS pfruy1,
    nflprz.rushingyards152q AS pfruy2,
    nflprz.rushingyards202q AS pfruy3,
    nflprz.reeptions5d2q AS pfrec1,
    nflprz.reeptions12q AS pfrec2,
    nflprz.reeptions02q AS pfrec3,
    nflprz.receivingyards20d2q AS pfryd1,
    nflprz.receivingyards152q AS pfryd2,
    nflprz.receivingyards102q AS pfryd3,
    nflprz.passinginterceptions2d2q AS pfint1,
    nflprz.passinginterceptions32q AS pfint2,
    nflprz.passinginterceptions12q AS pfint3,
    nflprz.touchdowns6d2q AS pftds1,
    nflprz.touchdowns52q AS pftds2,
    nflprz.touchdowns42q AS pftds3,
    nflprz.codetemp AS ctemp,
    nflprz.codewind AS cwind,
    nflprz.codewcond AS cwcond,
    nflprz.temprange AS trange,
    nflprz.wind AS windr,
    nflprz.wconditions AS wcond,
    nflprz.playerid AS playerident,
    nflprz.currentteam AS curteam,
    nflprz.solotackles2q AS solotackles,
    nflprz.assistedtackles2q AS assistedtackles,
    nflprz.sacks2q AS sacks,
    nflprz.interceptions2q AS interceptions,
    nflprz.interceptionreturntouchdowns2q AS intreturntds,
    nflprz.fumblesforced2q AS forcedfumbles,
    nflprz.fumblesrecovered2q AS fumblerecovery,
    nflprz.fumblereturntouchdowns2q AS fumreturntds,
    nflprz.kickreturnyards2q AS kryards,
    nflprz.puntreturnyards2q AS pryards,
    nflprz.kickreturntouchdowns2q AS krtouchdowns,
    nflprz.puntreturntouchdowns2q AS prtouchdowns
   FROM public.nflprz
  WHERE (((nflprz.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflprz.ffp2017 AND (nflprz.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflprz2qsplits2 OWNER TO admin;

--
-- TOC entry 244 (class 1259 OID 176879)
-- Name: nflprz3qsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflprz3qsplits2 AS
 SELECT nflprz.playerposition AS tpposition,
    nflprz.playername AS player,
    nflprz.currentteam AS team,
    nflprz.playerageinseason AS age,
    nflprz.contractyear AS contract,
    nflprz.byeweek AS bye,
    nflprz.nightgame AS sng,
    nflprz.primetstart AS pgs,
    nflprz.season AS nflseason,
    nflprz.seasontype AS regseason,
    nflprz.week AS regweek,
    nflprz.newweeks AS seasonweeks,
    nflprz.opponent AS vsteam,
    nflprz.homeoraway AS location,
    nflprz.grassorturf AS playsurface,
    nflprz.indooroutdoor AS inoutdoor,
    nflprz.vsheadcoach AS vshcoach,
    nflprz.vsoffensivecoordinator AS vsoffcoor,
    nflprz.vsdefensivecoordinator AS vsdefcood,
    nflprz.pingame AS gplayed,
    nflprz.fantasypoints3q AS ffp,
    nflprz.passingattempts3q AS patts,
    nflprz.rushingattempts3q AS ratts,
    nflprz.receivingtargets3q AS rtars,
    nflprz.touchdowns3q AS tds,
    nflprz.passingyards3q AS pyd,
    nflprz.rushingyards3q AS ruyd,
    nflprz.receivingyards3q AS reyd,
    nflprz.receptions3q AS recp,
    nflprz.fortyplus3q AS fplus,
    nflprz.passinginterceptions3q AS inter,
    nflprz.fumbles3q AS fum,
    nflprz.fumbleslost3q AS fuml,
    nflprz.twentyplus3q AS tplus,
    nflprz.longyard3q AS lgy,
    nflprz.passingcompletions3q AS passc,
    nflprz.fieldgoalsattempted3q AS fga,
    nflprz.fieldgoalsmade3q AS fgm,
    nflprz.fieldgoalshadblocked3q AS fgsblocked,
    nflprz.extrapointsattempted3q AS patattempt,
    nflprz.extrapointsmade3q AS patmade,
    nflprz.extrapointshadblocked3q AS epblocked,
    nflprz.fieldgoalsmade20to293q AS f20s,
    nflprz.fieldgoalsmade30to393q AS f30s,
    nflprz.fieldgoalsmade40to493q AS f40s,
    nflprz.fieldgoalsmade50plus3q AS f50p,
    nflprz.fieldgoalsmade60plus3q AS f60p,
    nflprz.offensivesnapsplayed3q AS osnaps,
    nflprz.fantasypointsprem3q AS fpprem,
    nflprz.passingyards25d3q AS pfpyd1,
    nflprz.passingyards303q AS pfpyd2,
    nflprz.passingyards203q AS pfpyd3,
    nflprz.rushingyards10d3q AS pfruy1,
    nflprz.rushingyards153q AS pfruy2,
    nflprz.rushingyards203q AS pfruy3,
    nflprz.reeptions5d3q AS pfrec1,
    nflprz.reeptions13q AS pfrec2,
    nflprz.reeptions03q AS pfrec3,
    nflprz.receivingyards20d3q AS pfryd1,
    nflprz.receivingyards153q AS pfryd2,
    nflprz.receivingyards103q AS pfryd3,
    nflprz.passinginterceptions2d3q AS pfint1,
    nflprz.passinginterceptions33q AS pfint2,
    nflprz.passinginterceptions13q AS pfint3,
    nflprz.touchdowns6d3q AS pftds1,
    nflprz.touchdowns53q AS pftds2,
    nflprz.touchdowns43q AS pftds3,
    nflprz.codetemp AS ctemp,
    nflprz.codewind AS cwind,
    nflprz.codewcond AS cwcond,
    nflprz.temprange AS trange,
    nflprz.wind AS windr,
    nflprz.wconditions AS wcond,
    nflprz.playerid AS playerident,
    nflprz.currentteam AS curteam,
    nflprz.solotackles3q AS solotackles,
    nflprz.assistedtackles3q AS assistedtackles,
    nflprz.sacks3q AS sacks,
    nflprz.interceptions3q AS interceptions,
    nflprz.interceptionreturntouchdowns3q AS intreturntds,
    nflprz.fumblesforced3q AS forcedfumbles,
    nflprz.fumblesrecovered3q AS fumblerecovery,
    nflprz.fumblereturntouchdowns3q AS fumreturntds,
    nflprz.kickreturnyards3q AS kryards,
    nflprz.puntreturnyards3q AS pryards,
    nflprz.kickreturntouchdowns3q AS krtouchdowns,
    nflprz.puntreturntouchdowns3q AS prtouchdowns
   FROM public.nflprz
  WHERE (((nflprz.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflprz.ffp2017 AND (nflprz.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflprz3qsplits2 OWNER TO admin;

--
-- TOC entry 246 (class 1259 OID 176894)
-- Name: nflprz4qsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflprz4qsplits2 AS
 SELECT nflprz.playerposition AS tpposition,
    nflprz.playername AS player,
    nflprz.currentteam AS team,
    nflprz.playerageinseason AS age,
    nflprz.contractyear AS contract,
    nflprz.byeweek AS bye,
    nflprz.nightgame AS sng,
    nflprz.primetstart AS pgs,
    nflprz.season AS nflseason,
    nflprz.seasontype AS regseason,
    nflprz.week AS regweek,
    nflprz.newweeks AS seasonweeks,
    nflprz.opponent AS vsteam,
    nflprz.homeoraway AS location,
    nflprz.grassorturf AS playsurface,
    nflprz.indooroutdoor AS inoutdoor,
    nflprz.vsheadcoach AS vshcoach,
    nflprz.vsoffensivecoordinator AS vsoffcoor,
    nflprz.vsdefensivecoordinator AS vsdefcood,
    nflprz.pingame AS gplayed,
    nflprz.fantasypoints4q AS ffp,
    nflprz.passingattempts4q AS patts,
    nflprz.rushingattempts4q AS ratts,
    nflprz.receivingtargets4q AS rtars,
    nflprz.touchdowns4q AS tds,
    nflprz.passingyards4q AS pyd,
    nflprz.rushingyards4q AS ruyd,
    nflprz.receivingyards4q AS reyd,
    nflprz.receptions4q AS recp,
    nflprz.fortyplus4q AS fplus,
    nflprz.passinginterceptions4q AS inter,
    nflprz.fumbles4q AS fum,
    nflprz.fumbleslost4q AS fuml,
    nflprz.twentyplus4q AS tplus,
    nflprz.longyard4q AS lgy,
    nflprz.passingcompletions4q AS passc,
    nflprz.fieldgoalsattempted4q AS fga,
    nflprz.fieldgoalsmade4q AS fgm,
    nflprz.fieldgoalshadblocked4q AS fgsblocked,
    nflprz.extrapointsattempted4q AS patattempt,
    nflprz.extrapointsmade4q AS patmade,
    nflprz.extrapointshadblocked4q AS epblocked,
    nflprz.fieldgoalsmade20to294q AS f20s,
    nflprz.fieldgoalsmade30to394q AS f30s,
    nflprz.fieldgoalsmade40to494q AS f40s,
    nflprz.fieldgoalsmade50plus4q AS f50p,
    nflprz.fieldgoalsmade60plus4q AS f60p,
    nflprz.offensivesnapsplayed4q AS osnaps,
    nflprz.fantasypointsprem4q AS fpprem,
    nflprz.passingyards25d4q AS pfpyd1,
    nflprz.passingyards304q AS pfpyd2,
    nflprz.passingyards204q AS pfpyd3,
    nflprz.rushingyards10d4q AS pfruy1,
    nflprz.rushingyards154q AS pfruy2,
    nflprz.rushingyards204q AS pfruy3,
    nflprz.reeptions5d4q AS pfrec1,
    nflprz.reeptions14q AS pfrec2,
    nflprz.reeptions04q AS pfrec3,
    nflprz.receivingyards20d4q AS pfryd1,
    nflprz.receivingyards154q AS pfryd2,
    nflprz.receivingyards104q AS pfryd3,
    nflprz.passinginterceptions2d4q AS pfint1,
    nflprz.passinginterceptions34q AS pfint2,
    nflprz.passinginterceptions14q AS pfint3,
    nflprz.touchdowns6d4q AS pftds1,
    nflprz.touchdowns54q AS pftds2,
    nflprz.touchdowns44q AS pftds3,
    nflprz.codetemp AS ctemp,
    nflprz.codewind AS cwind,
    nflprz.codewcond AS cwcond,
    nflprz.temprange AS trange,
    nflprz.wind AS windr,
    nflprz.wconditions AS wcond,
    nflprz.playerid AS playerident,
    nflprz.currentteam AS curteam,
    nflprz.solotackles4q AS solotackles,
    nflprz.assistedtackles4q AS assistedtackles,
    nflprz.sacks4q AS sacks,
    nflprz.interceptions4q AS interceptions,
    nflprz.interceptionreturntouchdowns4q AS intreturntds,
    nflprz.fumblesforced4q AS forcedfumbles,
    nflprz.fumblesrecovered4q AS fumblerecovery,
    nflprz.fumblereturntouchdowns4q AS fumreturntds,
    nflprz.kickreturnyards4q AS kryards,
    nflprz.puntreturnyards4q AS pryards,
    nflprz.kickreturntouchdowns4q AS krtouchdowns,
    nflprz.puntreturntouchdowns4q AS prtouchdowns
   FROM public.nflprz
  WHERE (((nflprz.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflprz.ffp2017 AND (nflprz.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflprz4qsplits2 OWNER TO admin;

--
-- TOC entry 253 (class 1259 OID 176981)
-- Name: nflprzallsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflprzallsplits2 AS
 SELECT nflprz.playerposition AS tpposition,
    nflprz.playername AS player,
    nflprz.currentteam AS team,
    nflprz.playerageinseason AS age,
    nflprz.contractyear AS contract,
    nflprz.byeweek AS bye,
    nflprz.nightgame AS sng,
    nflprz.primetstart AS pgs,
    nflprz.season AS nflseason,
    nflprz.seasontype AS regseason,
    nflprz.week AS regweek,
    nflprz.newweeks AS seasonweeks,
    nflprz.opponent AS vsteam,
    nflprz.homeoraway AS location,
    nflprz.grassorturf AS playsurface,
    nflprz.indooroutdoor AS inoutdoor,
    nflprz.vsheadcoach AS vshcoach,
    nflprz.vsoffensivecoordinator AS vsoffcoor,
    nflprz.vsdefensivecoordinator AS vsdefcood,
    nflprz.pingame AS gplayed,
    nflprz.fantasypoints AS ffp,
    nflprz.passingattempts AS patts,
    nflprz.rushingattempts AS ratts,
    nflprz.receivingtargets AS rtars,
    nflprz.touchdowns AS tds,
    nflprz.passingyards AS pyd,
    nflprz.rushingyards AS ruyd,
    nflprz.receivingyards AS reyd,
    nflprz.receptions AS recp,
    nflprz.fortyplustot AS fplus,
    nflprz.passinginterceptions AS inter,
    nflprz.fumbles AS fum,
    nflprz.fumbleslost AS fuml,
    nflprz.twentyplustot AS tplus,
    nflprz.longyardtot AS lgy,
    nflprz.passingcompletions AS passc,
    nflprz.fieldgoalsattempted AS fga,
    nflprz.fieldgoalsmade AS fgm,
    nflprz.fieldgoalshadblocked AS fgsblocked,
    nflprz.extrapointsattempted AS patattempt,
    nflprz.extrapointsmade AS patmade,
    nflprz.extrapointshadblocked AS epblocked,
    nflprz.fieldgoalsmade20to29 AS f20s,
    nflprz.fieldgoalsmade30to39 AS f30s,
    nflprz.fieldgoalsmade40to49 AS f40s,
    nflprz.fieldgoalsmade50plus AS f50p,
    nflprz.fieldgoalsmade60plus AS f60p,
    nflprz.offensivesnapsplayed AS osnaps,
    nflprz.fantasypointspremall AS fpprem,
    nflprz.passingyards25d AS pfpyd1,
    nflprz.passingyards30 AS pfpyd2,
    nflprz.passingyards20 AS pfpyd3,
    nflprz.rushingyards10d AS pfruy1,
    nflprz.rushingyards15 AS pfruy2,
    nflprz.rushingyards20 AS pfruy3,
    nflprz.reeptions5d AS pfrec1,
    nflprz.reeptions1 AS pfrec2,
    nflprz.reeptions0 AS pfrec3,
    nflprz.receivingyards20d AS pfryd1,
    nflprz.receivingyards15 AS pfryd2,
    nflprz.receivingyards10 AS pfryd3,
    nflprz.passinginterceptions2d AS pfint1,
    nflprz.passinginterceptions3 AS pfint2,
    nflprz.passinginterceptions1 AS pfint3,
    nflprz.touchdowns6d AS pftds1,
    nflprz.touchdowns5 AS pftds2,
    nflprz.touchdowns4 AS pftds3,
    nflprz.codetemp AS ctemp,
    nflprz.codewind AS cwind,
    nflprz.codewcond AS cwcond,
    nflprz.temprange AS trange,
    nflprz.wind AS windr,
    nflprz.wconditions AS wcond,
    nflprz.playerid AS playerident,
    nflprz.currentteam AS curteam,
    nflprz.solotackles,
    nflprz.assistedtackles,
    nflprz.sacks,
    nflprz.interceptions AS interception,
    nflprz.interceptionreturntouchdowns AS intreturntds,
    nflprz.fumblesforced AS forcedfumbles,
    nflprz.fumblesrecovered AS fumblerecovery,
    nflprz.fumblereturntouchdowns AS fumreturntds,
    nflprz.kickreturnyards AS kryards,
    nflprz.puntreturnyards AS pryards,
    nflprz.kickreturntouchdowns AS krtouchdowns,
    nflprz.puntreturntouchdowns AS prtouchdowns
   FROM public.nflprz
  WHERE (((nflprz.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflprz.ffp2017 AND (nflprz.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflprzallsplits2 OWNER TO admin;

--
-- TOC entry 247 (class 1259 OID 176928)
-- Name: nflprzovtsplits2; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflprzovtsplits2 AS
 SELECT nflprz.playerposition AS tpposition,
    nflprz.playername AS player,
    nflprz.currentteam AS team,
    nflprz.playerageinseason AS age,
    nflprz.contractyear AS contract,
    nflprz.byeweek AS bye,
    nflprz.nightgame AS sng,
    nflprz.primetstart AS pgs,
    nflprz.season AS nflseason,
    nflprz.seasontype AS regseason,
    nflprz.week AS regweek,
    nflprz.newweeks AS seasonweeks,
    nflprz.opponent AS vsteam,
    nflprz.homeoraway AS location,
    nflprz.grassorturf AS playsurface,
    nflprz.indooroutdoor AS inoutdoor,
    nflprz.vsheadcoach AS vshcoach,
    nflprz.vsoffensivecoordinator AS vsoffcoor,
    nflprz.vsdefensivecoordinator AS vsdefcood,
    nflprz.pingame AS gplayed,
    nflprz.fantasypointsovt AS ffp,
    nflprz.passingattemptsovt AS patts,
    nflprz.rushingattemptsovt AS ratts,
    nflprz.receivingtargetsovt AS rtars,
    nflprz.touchdownsovt AS tds,
    nflprz.passingyardsovt AS pyd,
    nflprz.rushingyardsovt AS ruyd,
    nflprz.receivingyardsovt AS reyd,
    nflprz.receptionsovt AS recp,
    nflprz.fortyplusovt AS fplus,
    nflprz.passinginterceptionsovt AS inter,
    nflprz.fumblesovt AS fum,
    nflprz.fumbleslostovt AS fuml,
    nflprz.twentyplusovt AS tplus,
    nflprz.longyardovt AS lgy,
    nflprz.passingcompletionsovt AS passc,
    nflprz.fieldgoalsattemptedovt AS fga,
    nflprz.fieldgoalsmadeovt AS fgm,
    nflprz.fieldgoalshadblockedovt AS fgsblocked,
    nflprz.extrapointsattemptedovt AS patattempt,
    nflprz.extrapointsmadeovt AS patmade,
    nflprz.extrapointshadblockedovt AS epblocked,
    nflprz.fieldgoalsmade20to29ovt AS f20s,
    nflprz.fieldgoalsmade30to39ovt AS f30s,
    nflprz.fieldgoalsmade40to49ovt AS f40s,
    nflprz.fieldgoalsmade50plusovt AS f50p,
    nflprz.fieldgoalsmade60plusovt AS f60p,
    nflprz.offensivesnapsplayedovt AS osnaps,
    nflprz.fantasypointspremovt AS fpprem,
    nflprz.passingyards25dovt AS pfpyd1,
    nflprz.passingyards30ovt AS pfpyd2,
    nflprz.passingyards20ovt AS pfpyd3,
    nflprz.rushingyards10dovt AS pfruy1,
    nflprz.rushingyards15ovt AS pfruy2,
    nflprz.rushingyards20ovt AS pfruy3,
    nflprz.reeptions5dovt AS pfrec1,
    nflprz.reeptions1ovt AS pfrec2,
    nflprz.reeptions0ovt AS pfrec3,
    nflprz.receivingyards20dovt AS pfryd1,
    nflprz.receivingyards15ovt AS pfryd2,
    nflprz.receivingyards10ovt AS pfryd3,
    nflprz.passinginterceptions2dovt AS pfint1,
    nflprz.passinginterceptions3ovt AS pfint2,
    nflprz.passinginterceptions1ovt AS pfint3,
    nflprz.touchdowns6dovt AS pftds1,
    nflprz.touchdowns5ovt AS pftds2,
    nflprz.touchdowns4ovt AS pftds3,
    nflprz.codetemp AS ctemp,
    nflprz.codewind AS cwind,
    nflprz.codewcond AS cwcond,
    nflprz.temprange AS trange,
    nflprz.wind AS windr,
    nflprz.wconditions AS wcond,
    nflprz.playerid AS playerident,
    nflprz.currentteam AS curteam,
    nflprz.solotacklesovt AS solotackles,
    nflprz.assistedtacklesovt AS assistedtackles,
    nflprz.sacksovt AS sacks,
    nflprz.interceptionsovt AS interceptions,
    nflprz.interceptionreturntouchdownsovt AS intreturntds,
    nflprz.fumblesforcedovt AS forcedfumbles,
    nflprz.fumblesrecoveredovt AS fumblerecovery,
    nflprz.fumblereturntouchdownsovt AS fumreturntds,
    nflprz.kickreturnyardsovt AS kryards,
    nflprz.puntreturnyardsovt AS pryards,
    nflprz.kickreturntouchdownsovt AS krtouchdowns,
    nflprz.puntreturntouchdownsovt AS prtouchdowns
   FROM public.nflprz
  WHERE (((nflprz.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflprz.ffp2017 AND (nflprz.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflprzovtsplits2 OWNER TO admin;

--
-- TOC entry 248 (class 1259 OID 176935)
-- Name: nflpsts; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpsts AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints AS ffp,
    nflplayer.passingattempts AS patts,
    nflplayer.rushingattempts AS ratts,
    nflplayer.receivingtargets AS rtars,
    nflplayer.touchdowns AS tds,
    nflplayer.passingyards AS pyd,
    nflplayer.rushingyards AS ruyd,
    nflplayer.receivingyards AS reyd,
    nflplayer.receptions AS recp,
    nflplayer.fortyplustot AS fplus,
    nflplayer.passinginterceptions AS inter,
    nflplayer.fumbles AS fum,
    nflplayer.fumbleslost AS fuml,
    nflplayer.twentyplustot AS tplus,
    nflplayer.longyardtot AS lgy,
    nflplayer.passingcompletions AS passc,
    nflplayer.fieldgoalsattempted AS fga,
    nflplayer.fieldgoalsmade AS fgm,
    nflplayer.fieldgoalshadblocked AS fgsblocked,
    nflplayer.extrapointsattempted AS patattempt,
    nflplayer.extrapointsmade AS patmade,
    nflplayer.extrapointshadblocked AS epblocked,
    nflplayer.fieldgoalsmade20to29 AS f20s,
    nflplayer.fieldgoalsmade30to39 AS f30s,
    nflplayer.fieldgoalsmade40to49 AS f40s,
    nflplayer.fieldgoalsmade50plus AS f50p,
    nflplayer.fieldgoalsmade60plus AS f60p,
    nflplayer.offensivesnapsplayed AS osnaps,
    nflplayer.fantasypointspremall AS fpprem,
    nflplayer.passingyards25d AS pfpyd1,
    nflplayer.passingyards30 AS pfpyd2,
    nflplayer.passingyards20 AS pfpyd3,
    nflplayer.rushingyards10d AS pfruy1,
    nflplayer.rushingyards15 AS pfruy2,
    nflplayer.rushingyards20 AS pfruy3,
    nflplayer.reeptions5d AS pfrec1,
    nflplayer.reeptions1 AS pfrec2,
    nflplayer.reeptions0 AS pfrec3,
    nflplayer.receivingyards20d AS pfryd1,
    nflplayer.receivingyards15 AS pfryd2,
    nflplayer.receivingyards10 AS pfryd3,
    nflplayer.passinginterceptions2d AS pfint1,
    nflplayer.passinginterceptions3 AS pfint2,
    nflplayer.passinginterceptions1 AS pfint3,
    nflplayer.touchdowns6d AS pftds1,
    nflplayer.touchdowns5 AS pftds2,
    nflplayer.touchdowns4 AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles,
    nflplayer.assistedtackles,
    nflplayer.sacks,
    nflplayer.interceptions,
    nflplayer.interceptionreturntouchdowns AS intreturntds,
    nflplayer.fumblesforced AS forcedfumbles,
    nflplayer.fumblesrecovered AS fumblerecovery,
    nflplayer.fumblereturntouchdowns AS fumreturntds,
    nflplayer.kickreturnyards AS kryards,
    nflplayer.puntreturnyards AS pryards,
    nflplayer.kickreturntouchdowns AS krtouchdowns,
    nflplayer.puntreturntouchdowns AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpsts OWNER TO admin;

--
-- TOC entry 249 (class 1259 OID 176948)
-- Name: nflpsts1h; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpsts1h AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints1h AS ffp,
    nflplayer.passingattempts1h AS patts,
    nflplayer.rushingattempts1h AS ratts,
    nflplayer.receivingtargets1h AS rtars,
    nflplayer.touchdowns1h AS tds,
    nflplayer.passingyards1h AS pyd,
    nflplayer.rushingyards1h AS ruyd,
    nflplayer.receivingyards1h AS reyd,
    nflplayer.receptions1h AS recp,
    nflplayer.fortyplus1h AS fplus,
    nflplayer.passinginterceptions1h AS inter,
    nflplayer.fumbles1h AS fum,
    nflplayer.fumbleslost1h AS fuml,
    nflplayer.twentyplus1h AS tplus,
    nflplayer.longyard1h AS lgy,
    nflplayer.passingcompletions1h AS passc,
    nflplayer.fieldgoalsattempted1h AS fga,
    nflplayer.fieldgoalsmade1h AS fgm,
    nflplayer.fieldgoalshadblocked1h AS fgsblocked,
    nflplayer.extrapointsattempted1h AS patattempt,
    nflplayer.extrapointsmade1h AS patmade,
    nflplayer.extrapointshadblocked1h AS epblocked,
    nflplayer.fieldgoalsmade20to291h AS f20s,
    nflplayer.fieldgoalsmade30to391h AS f30s,
    nflplayer.fieldgoalsmade40to491h AS f40s,
    nflplayer.fieldgoalsmade50plus1h AS f50p,
    nflplayer.fieldgoalsmade60plus1h AS f60p,
    nflplayer.offensivesnapsplayed1h AS osnaps,
    nflplayer.fantasypointsprem1h AS fpprem,
    nflplayer.passingyards25d1h AS pfpyd1,
    nflplayer.passingyards301h AS pfpyd2,
    nflplayer.passingyards201h AS pfpyd3,
    nflplayer.rushingyards10d1h AS pfruy1,
    nflplayer.rushingyards151h AS pfruy2,
    nflplayer.rushingyards201h AS pfruy3,
    nflplayer.reeptions5d1h AS pfrec1,
    nflplayer.reeptions11h AS pfrec2,
    nflplayer.reeptions01h AS pfrec3,
    nflplayer.receivingyards20d1h AS pfryd1,
    nflplayer.receivingyards151h AS pfryd2,
    nflplayer.receivingyards101h AS pfryd3,
    nflplayer.passinginterceptions2d1h AS pfint1,
    nflplayer.passinginterceptions31h AS pfint2,
    nflplayer.passinginterceptions11h AS pfint3,
    nflplayer.touchdowns6d1h AS pftds1,
    nflplayer.touchdowns51h AS pftds2,
    nflplayer.touchdowns41h AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles1h AS solotackles,
    nflplayer.assistedtackles1h AS assistedtackles,
    nflplayer.sacks1h AS sacks,
    nflplayer.interceptions1h AS interceptions,
    nflplayer.interceptionreturntouchdowns1h AS intreturntds,
    nflplayer.fumblesforced1h AS forcedfumbles,
    nflplayer.fumblesrecovered1h AS fumblerecovery,
    nflplayer.fumblereturntouchdowns1h AS fumreturntds,
    nflplayer.kickreturnyards1h AS kryards,
    nflplayer.puntreturnyards1h AS pryards,
    nflplayer.kickreturntouchdowns1h AS krtouchdowns,
    nflplayer.puntreturntouchdowns1h AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpsts1h OWNER TO admin;

--
-- TOC entry 250 (class 1259 OID 176956)
-- Name: nflpsts1q; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpsts1q AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints1q AS ffp,
    nflplayer.passingattempts1q AS patts,
    nflplayer.rushingattempts1q AS ratts,
    nflplayer.receivingtargets1q AS rtars,
    nflplayer.touchdowns1q AS tds,
    nflplayer.passingyards1q AS pyd,
    nflplayer.rushingyards1q AS ruyd,
    nflplayer.receivingyards1q AS reyd,
    nflplayer.receptions1q AS recp,
    nflplayer.fortyplus1q AS fplus,
    nflplayer.passinginterceptions1q AS inter,
    nflplayer.fumbles1q AS fum,
    nflplayer.fumbleslost1q AS fuml,
    nflplayer.twentyplus1q AS tplus,
    nflplayer.longyard1q AS lgy,
    nflplayer.passingcompletions1q AS passc,
    nflplayer.fieldgoalsattempted1q AS fga,
    nflplayer.fieldgoalsmade1q AS fgm,
    nflplayer.fieldgoalshadblocked1q AS fgsblocked,
    nflplayer.extrapointsattempted1q AS patattempt,
    nflplayer.extrapointsmade1q AS patmade,
    nflplayer.extrapointshadblocked1q AS epblocked,
    nflplayer.fieldgoalsmade20to291q AS f20s,
    nflplayer.fieldgoalsmade30to391q AS f30s,
    nflplayer.fieldgoalsmade40to491q AS f40s,
    nflplayer.fieldgoalsmade50plus1q AS f50p,
    nflplayer.fieldgoalsmade60plus1q AS f60p,
    nflplayer.offensivesnapsplayed1q AS osnaps,
    nflplayer.fantasypointsprem1q AS fpprem,
    nflplayer.passingyards25d1q AS pfpyd1,
    nflplayer.passingyards301q AS pfpyd2,
    nflplayer.passingyards201q AS pfpyd3,
    nflplayer.rushingyards10d1q AS pfruy1,
    nflplayer.rushingyards151q AS pfruy2,
    nflplayer.rushingyards201q AS pfruy3,
    nflplayer.reeptions5d1q AS pfrec1,
    nflplayer.reeptions11q AS pfrec2,
    nflplayer.reeptions01q AS pfrec3,
    nflplayer.receivingyards20d1q AS pfryd1,
    nflplayer.receivingyards151q AS pfryd2,
    nflplayer.receivingyards101q AS pfryd3,
    nflplayer.passinginterceptions2d1q AS pfint1,
    nflplayer.passinginterceptions31q AS pfint2,
    nflplayer.passinginterceptions11q AS pfint3,
    nflplayer.touchdowns6d1q AS pftds1,
    nflplayer.touchdowns51q AS pftds2,
    nflplayer.touchdowns41q AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles1q AS solotackles,
    nflplayer.assistedtackles1q AS assistedtackles,
    nflplayer.sacks1q AS sacks,
    nflplayer.interceptions1q AS interceptions,
    nflplayer.interceptionreturntouchdowns1q AS intreturntds,
    nflplayer.fumblesforced1q AS forcedfumbles,
    nflplayer.fumblesrecovered1q AS fumblerecovery,
    nflplayer.fumblereturntouchdowns1q AS fumreturntds,
    nflplayer.kickreturnyards1q AS kryards,
    nflplayer.puntreturnyards1q AS pryards,
    nflplayer.kickreturntouchdowns1q AS krtouchdowns,
    nflplayer.puntreturntouchdowns1q AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpsts1q OWNER TO admin;

--
-- TOC entry 251 (class 1259 OID 176964)
-- Name: nflpsts2h; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpsts2h AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints2h AS ffp,
    nflplayer.passingattempts2h AS patts,
    nflplayer.rushingattempts2h AS ratts,
    nflplayer.receivingtargets2h AS rtars,
    nflplayer.touchdowns2h AS tds,
    nflplayer.passingyards2h AS pyd,
    nflplayer.rushingyards2h AS ruyd,
    nflplayer.receivingyards2h AS reyd,
    nflplayer.receptions2h AS recp,
    nflplayer.fortyplus2h AS fplus,
    nflplayer.passinginterceptions2h AS inter,
    nflplayer.fumbles2h AS fum,
    nflplayer.fumbleslost2h AS fuml,
    nflplayer.twentyplus2h AS tplus,
    nflplayer.longyard2h AS lgy,
    nflplayer.passingcompletions2h AS passc,
    nflplayer.fieldgoalsattempted2h AS fga,
    nflplayer.fieldgoalsmade2h AS fgm,
    nflplayer.fieldgoalshadblocked2h AS fgsblocked,
    nflplayer.extrapointsattempted2h AS patattempt,
    nflplayer.extrapointsmade2h AS patmade,
    nflplayer.extrapointshadblocked2h AS epblocked,
    nflplayer.fieldgoalsmade20to292h AS f20s,
    nflplayer.fieldgoalsmade30to392h AS f30s,
    nflplayer.fieldgoalsmade40to492h AS f40s,
    nflplayer.fieldgoalsmade50plus2h AS f50p,
    nflplayer.fieldgoalsmade60plus2h AS f60p,
    nflplayer.offensivesnapsplayed2h AS osnaps,
    nflplayer.fantasypointsprem2h AS fpprem,
    nflplayer.passingyards25d2h AS pfpyd1,
    nflplayer.passingyards302h AS pfpyd2,
    nflplayer.passingyards202h AS pfpyd3,
    nflplayer.rushingyards10d2h AS pfruy1,
    nflplayer.rushingyards152h AS pfruy2,
    nflplayer.rushingyards202h AS pfruy3,
    nflplayer.reeptions5d2h AS pfrec1,
    nflplayer.reeptions12h AS pfrec2,
    nflplayer.reeptions02h AS pfrec3,
    nflplayer.receivingyards20d2h AS pfryd1,
    nflplayer.receivingyards152h AS pfryd2,
    nflplayer.receivingyards102h AS pfryd3,
    nflplayer.passinginterceptions2d2h AS pfint1,
    nflplayer.passinginterceptions32h AS pfint2,
    nflplayer.passinginterceptions12h AS pfint3,
    nflplayer.touchdowns6d2h AS pftds1,
    nflplayer.touchdowns52h AS pftds2,
    nflplayer.touchdowns42h AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles2h AS solotackles,
    nflplayer.assistedtackles2h AS assistedtackles,
    nflplayer.sacks2h AS sacks,
    nflplayer.interceptions2h AS interceptions,
    nflplayer.interceptionreturntouchdowns2h AS intreturntds,
    nflplayer.fumblesforced2h AS forcedfumbles,
    nflplayer.fumblesrecovered2h AS fumblerecovery,
    nflplayer.fumblereturntouchdowns2h AS fumreturntds,
    nflplayer.kickreturnyards2h AS kryards,
    nflplayer.puntreturnyards2h AS pryards,
    nflplayer.kickreturntouchdowns2h AS krtouchdowns,
    nflplayer.puntreturntouchdowns2h AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpsts2h OWNER TO admin;

--
-- TOC entry 225 (class 1259 OID 31406)
-- Name: nflpsts2q; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpsts2q AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints2q AS ffp,
    nflplayer.passingattempts2q AS patts,
    nflplayer.rushingattempts2q AS ratts,
    nflplayer.receivingtargets2q AS rtars,
    nflplayer.touchdowns2q AS tds,
    nflplayer.passingyards2q AS pyd,
    nflplayer.rushingyards2q AS ruyd,
    nflplayer.receivingyards2q AS reyd,
    nflplayer.receptions2q AS recp,
    nflplayer.fortyplus2q AS fplus,
    nflplayer.passinginterceptions2q AS inter,
    nflplayer.fumbles2q AS fum,
    nflplayer.fumbleslost2q AS fuml,
    nflplayer.twentyplus2q AS tplus,
    nflplayer.longyard2q AS lgy,
    nflplayer.passingcompletions2q AS passc,
    nflplayer.fieldgoalsattempted2q AS fga,
    nflplayer.fieldgoalsmade2q AS fgm,
    nflplayer.fieldgoalshadblocked2q AS fgsblocked,
    nflplayer.extrapointsattempted2q AS patattempt,
    nflplayer.extrapointsmade2q AS patmade,
    nflplayer.extrapointshadblocked2q AS epblocked,
    nflplayer.fieldgoalsmade20to292q AS f20s,
    nflplayer.fieldgoalsmade30to392q AS f30s,
    nflplayer.fieldgoalsmade40to492q AS f40s,
    nflplayer.fieldgoalsmade50plus2q AS f50p,
    nflplayer.fieldgoalsmade60plus2q AS f60p,
    nflplayer.offensivesnapsplayed2q AS osnaps,
    nflplayer.fantasypointsprem2q AS fpprem,
    nflplayer.passingyards25d2q AS pfpyd1,
    nflplayer.passingyards302q AS pfpyd2,
    nflplayer.passingyards202q AS pfpyd3,
    nflplayer.rushingyards10d2q AS pfruy1,
    nflplayer.rushingyards152q AS pfruy2,
    nflplayer.rushingyards202q AS pfruy3,
    nflplayer.reeptions5d2q AS pfrec1,
    nflplayer.reeptions12q AS pfrec2,
    nflplayer.reeptions02q AS pfrec3,
    nflplayer.receivingyards20d2q AS pfryd1,
    nflplayer.receivingyards152q AS pfryd2,
    nflplayer.receivingyards102q AS pfryd3,
    nflplayer.passinginterceptions2d2q AS pfint1,
    nflplayer.passinginterceptions32q AS pfint2,
    nflplayer.passinginterceptions12q AS pfint3,
    nflplayer.touchdowns6d2q AS pftds1,
    nflplayer.touchdowns52q AS pftds2,
    nflplayer.touchdowns42q AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpsts2q OWNER TO admin;

--
-- TOC entry 226 (class 1259 OID 31412)
-- Name: nflpsts3q; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpsts3q AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints3q AS ffp,
    nflplayer.passingattempts3q AS patts,
    nflplayer.rushingattempts3q AS ratts,
    nflplayer.receivingtargets3q AS rtars,
    nflplayer.touchdowns3q AS tds,
    nflplayer.passingyards3q AS pyd,
    nflplayer.rushingyards3q AS ruyd,
    nflplayer.receivingyards3q AS reyd,
    nflplayer.receptions3q AS recp,
    nflplayer.fortyplus3q AS fplus,
    nflplayer.passinginterceptions3q AS inter,
    nflplayer.fumbles3q AS fum,
    nflplayer.fumbleslost3q AS fuml,
    nflplayer.twentyplus3q AS tplus,
    nflplayer.longyard3q AS lgy,
    nflplayer.passingcompletions3q AS passc,
    nflplayer.fieldgoalsattempted3q AS fga,
    nflplayer.fieldgoalsmade3q AS fgm,
    nflplayer.fieldgoalshadblocked3q AS fgsblocked,
    nflplayer.extrapointsattempted3q AS patattempt,
    nflplayer.extrapointsmade3q AS patmade,
    nflplayer.extrapointshadblocked3q AS epblocked,
    nflplayer.fieldgoalsmade20to293q AS f20s,
    nflplayer.fieldgoalsmade30to393q AS f30s,
    nflplayer.fieldgoalsmade40to493q AS f40s,
    nflplayer.fieldgoalsmade50plus3q AS f50p,
    nflplayer.fieldgoalsmade60plus3q AS f60p,
    nflplayer.offensivesnapsplayed3q AS osnaps,
    nflplayer.fantasypointsprem3q AS fpprem,
    nflplayer.passingyards25d3q AS pfpyd1,
    nflplayer.passingyards303q AS pfpyd2,
    nflplayer.passingyards203q AS pfpyd3,
    nflplayer.rushingyards10d3q AS pfruy1,
    nflplayer.rushingyards153q AS pfruy2,
    nflplayer.rushingyards203q AS pfruy3,
    nflplayer.reeptions5d3q AS pfrec1,
    nflplayer.reeptions13q AS pfrec2,
    nflplayer.reeptions03q AS pfrec3,
    nflplayer.receivingyards20d3q AS pfryd1,
    nflplayer.receivingyards153q AS pfryd2,
    nflplayer.receivingyards103q AS pfryd3,
    nflplayer.passinginterceptions2d3q AS pfint1,
    nflplayer.passinginterceptions33q AS pfint2,
    nflplayer.passinginterceptions13q AS pfint3,
    nflplayer.touchdowns6d3q AS pftds1,
    nflplayer.touchdowns53q AS pftds2,
    nflplayer.touchdowns43q AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpsts3q OWNER TO admin;

--
-- TOC entry 252 (class 1259 OID 176973)
-- Name: nflpsts4q; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpsts4q AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypoints4q AS ffp,
    nflplayer.passingattempts4q AS patts,
    nflplayer.rushingattempts4q AS ratts,
    nflplayer.receivingtargets4q AS rtars,
    nflplayer.touchdowns4q AS tds,
    nflplayer.passingyards4q AS pyd,
    nflplayer.rushingyards4q AS ruyd,
    nflplayer.receivingyards4q AS reyd,
    nflplayer.receptions4q AS recp,
    nflplayer.fortyplus4q AS fplus,
    nflplayer.passinginterceptions4q AS inter,
    nflplayer.fumbles4q AS fum,
    nflplayer.fumbleslost4q AS fuml,
    nflplayer.twentyplus4q AS tplus,
    nflplayer.longyard4q AS lgy,
    nflplayer.passingcompletions4q AS passc,
    nflplayer.fieldgoalsattempted4q AS fga,
    nflplayer.fieldgoalsmade4q AS fgm,
    nflplayer.fieldgoalshadblocked4q AS fgsblocked,
    nflplayer.extrapointsattempted4q AS patattempt,
    nflplayer.extrapointsmade4q AS patmade,
    nflplayer.extrapointshadblocked4q AS epblocked,
    nflplayer.fieldgoalsmade20to294q AS f20s,
    nflplayer.fieldgoalsmade30to394q AS f30s,
    nflplayer.fieldgoalsmade40to494q AS f40s,
    nflplayer.fieldgoalsmade50plus4q AS f50p,
    nflplayer.fieldgoalsmade60plus4q AS f60p,
    nflplayer.offensivesnapsplayed4q AS osnaps,
    nflplayer.fantasypointsprem4q AS fpprem,
    nflplayer.passingyards25d4q AS pfpyd1,
    nflplayer.passingyards304q AS pfpyd2,
    nflplayer.passingyards204q AS pfpyd3,
    nflplayer.rushingyards10d4q AS pfruy1,
    nflplayer.rushingyards154q AS pfruy2,
    nflplayer.rushingyards204q AS pfruy3,
    nflplayer.reeptions5d4q AS pfrec1,
    nflplayer.reeptions14q AS pfrec2,
    nflplayer.reeptions04q AS pfrec3,
    nflplayer.receivingyards20d4q AS pfryd1,
    nflplayer.receivingyards154q AS pfryd2,
    nflplayer.receivingyards104q AS pfryd3,
    nflplayer.passinginterceptions2d4q AS pfint1,
    nflplayer.passinginterceptions34q AS pfint2,
    nflplayer.passinginterceptions14q AS pfint3,
    nflplayer.touchdowns6d4q AS pftds1,
    nflplayer.touchdowns54q AS pftds2,
    nflplayer.touchdowns44q AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam,
    nflplayer.solotackles4q AS solotackles,
    nflplayer.assistedtackles4q AS assistedtackles,
    nflplayer.sacks4q AS sacks,
    nflplayer.interceptions4q AS interceptions,
    nflplayer.interceptionreturntouchdowns4q AS intreturntds,
    nflplayer.fumblesforced4q AS forcedfumbles,
    nflplayer.fumblesrecovered4q AS fumblerecovery,
    nflplayer.fumblereturntouchdowns4q AS fumreturntds,
    nflplayer.kickreturnyards4q AS kryards,
    nflplayer.puntreturnyards4q AS pryards,
    nflplayer.kickreturntouchdowns4q AS krtouchdowns,
    nflplayer.puntreturntouchdowns4q AS prtouchdowns
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text, 'CB'::text, 'S'::text, 'SS'::text, 'FS'::text, 'ILB'::text, 'OLB'::text, 'LB'::text, 'NT'::text, 'DT'::text, 'DE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpsts4q OWNER TO admin;

--
-- TOC entry 227 (class 1259 OID 31442)
-- Name: nflpstsovt; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflpstsovt AS
 SELECT nflplayer.playerposition AS tpposition,
    nflplayer.playername AS player,
    nflplayer.currentteam AS team,
    nflplayer.playerageinseason AS age,
    nflplayer.contractyear AS contract,
    nflplayer.byeweek AS bye,
    nflplayer.nightgame AS sng,
    nflplayer.primetstart AS pgs,
    nflplayer.season AS nflseason,
    nflplayer.seasontype AS regseason,
    nflplayer.week AS regweek,
    nflplayer.newweeks AS seasonweeks,
    nflplayer.opponent AS vsteam,
    nflplayer.homeoraway AS location,
    nflplayer.grassorturf AS playsurface,
    nflplayer.indooroutdoor AS inoutdoor,
    nflplayer.vsheadcoach AS vshcoach,
    nflplayer.vsoffensivecoordinator AS vsoffcoor,
    nflplayer.vsdefensivecoordinator AS vsdefcood,
    nflplayer.pingame AS gplayed,
    nflplayer.fantasypointsovt AS ffp,
    nflplayer.passingattemptsovt AS patts,
    nflplayer.rushingattemptsovt AS ratts,
    nflplayer.receivingtargetsovt AS rtars,
    nflplayer.touchdownsovt AS tds,
    nflplayer.passingyardsovt AS pyd,
    nflplayer.rushingyardsovt AS ruyd,
    nflplayer.receivingyardsovt AS reyd,
    nflplayer.receptionsovt AS recp,
    nflplayer.fortyplusovt AS fplus,
    nflplayer.passinginterceptionsovt AS inter,
    nflplayer.fumblesovt AS fum,
    nflplayer.fumbleslostovt AS fuml,
    nflplayer.twentyplusovt AS tplus,
    nflplayer.longyardovt AS lgy,
    nflplayer.passingcompletionsovt AS passc,
    nflplayer.fieldgoalsattemptedovt AS fga,
    nflplayer.fieldgoalsmadeovt AS fgm,
    nflplayer.fieldgoalshadblockedovt AS fgsblocked,
    nflplayer.extrapointsattemptedovt AS patattempt,
    nflplayer.extrapointsmadeovt AS patmade,
    nflplayer.extrapointshadblockedovt AS epblocked,
    nflplayer.fieldgoalsmade20to29ovt AS f20s,
    nflplayer.fieldgoalsmade30to39ovt AS f30s,
    nflplayer.fieldgoalsmade40to49ovt AS f40s,
    nflplayer.fieldgoalsmade50plusovt AS f50p,
    nflplayer.fieldgoalsmade60plusovt AS f60p,
    nflplayer.offensivesnapsplayedovt AS osnaps,
    nflplayer.fantasypointspremovt AS fpprem,
    nflplayer.passingyards25dovt AS pfpyd1,
    nflplayer.passingyards30ovt AS pfpyd2,
    nflplayer.passingyards20ovt AS pfpyd3,
    nflplayer.rushingyards10dovt AS pfruy1,
    nflplayer.rushingyards15ovt AS pfruy2,
    nflplayer.rushingyards20ovt AS pfruy3,
    nflplayer.reeptions5dovt AS pfrec1,
    nflplayer.reeptions1ovt AS pfrec2,
    nflplayer.reeptions0ovt AS pfrec3,
    nflplayer.receivingyards20dovt AS pfryd1,
    nflplayer.receivingyards15ovt AS pfryd2,
    nflplayer.receivingyards10ovt AS pfryd3,
    nflplayer.passinginterceptions2dovt AS pfint1,
    nflplayer.passinginterceptions3ovt AS pfint2,
    nflplayer.passinginterceptions1ovt AS pfint3,
    nflplayer.touchdowns6dovt AS pftds1,
    nflplayer.touchdowns5ovt AS pftds2,
    nflplayer.touchdowns4ovt AS pftds3,
    nflplayer.codetemp AS ctemp,
    nflplayer.codewind AS cwind,
    nflplayer.codewcond AS cwcond,
    nflplayer.temprange AS trange,
    nflplayer.wind AS windr,
    nflplayer.wconditions AS wcond,
    nflplayer.playerid AS playerident,
    nflplayer.currentteam AS curteam
   FROM public.nflplayer
  WHERE (((nflplayer.playerposition)::text = ANY (ARRAY['QB'::text, 'WR'::text, 'K'::text, 'RB'::text, 'TE'::text])) AND nflplayer.ffp2017 AND (nflplayer.seasontype = ANY (ARRAY[1, 3])))
  WITH NO DATA;


ALTER TABLE public.nflpstsovt OWNER TO admin;

--
-- TOC entry 194 (class 1259 OID 25083)
-- Name: nflteam; Type: TABLE; Schema: public; Owner: cblanton
--

CREATE TABLE public.nflteam (
    gamedate date,
    mmonth character(2),
    dday character(2),
    yyear character(4),
    hhour character(2),
    mminute character(2),
    newweeks public.weekst,
    primetime character varying(15),
    dayofweek public.weekday,
    nightgame boolean,
    primetstart boolean,
    keydateteam character(14),
    team character(3),
    opponent character(3),
    seasontype integer,
    season character(4),
    week smallint,
    teambad character(3),
    opponentbad character(3),
    homeoraway public.haway,
    score integer,
    opponentscore integer,
    totalscore integer,
    result character varying(10),
    stadiumbad character varying(50),
    playingsurface character varying(15),
    stadium character varying(50),
    indooroutdoor public.venuet,
    grassorturf public.fieldt,
    codetemp character(1),
    codewind character(1),
    codewcond character(1),
    temprange public.tempt,
    wind public.windt,
    wconditions public.weathert,
    temperature real,
    humidity real,
    windspeed real,
    overunder real,
    pointspread real,
    score1q real,
    score2q real,
    score3q real,
    score4q real,
    scoreovt real,
    score1h real,
    score2h real,
    timeofpossessionminutes integer,
    timeofpossessionseconds integer,
    timeofpossession character varying(15),
    firstdowns real,
    firstdownsbyrushing real,
    firstdownsbyrushing1q real,
    firstdownsbyrushing2q real,
    firstdownsbyrushing3q real,
    firstdownsbyrushing4q real,
    firstdownsbyrushing1h real,
    firstdownsbyrushing2h real,
    firstdownsbyrushingovt real,
    firstdownsbypassing real,
    firstdownsbypassing1q real,
    firstdownsbypassing2q real,
    firstdownsbypassing3q real,
    firstdownsbypassing4q real,
    firstdownsbypassing1h real,
    firstdownsbypassing2h real,
    firstdownsbypassingovt real,
    firstdownsbypenalty real,
    offensiveplays real,
    offensiveplays1q real,
    offensiveplays2q real,
    offensiveplays3q real,
    offensiveplays4q real,
    offensiveplays1h real,
    offensiveplays2h real,
    offensiveplaysovt real,
    offensiveyards real,
    offensiveyards1q real,
    offensiveyards2q real,
    offensiveyards3q real,
    offensiveyards4q real,
    offensiveyards1h real,
    offensiveyards2h real,
    offensiveyardsovt real,
    offensiveyardsperplay real,
    touchdowns real,
    touchdowns1q real,
    touchdowns2q real,
    touchdowns3q real,
    touchdowns4q real,
    touchdowns1h real,
    touchdowns2h real,
    touchdownsovt real,
    rushingattempts real,
    rushingattempts1q real,
    rushingattempts2q real,
    rushingattempts3q real,
    rushingattempts4q real,
    rushingattempts1h real,
    rushingattempts2h real,
    rushingattemptsovt real,
    rushingyards real,
    rushingyards1q real,
    rushingyards2q real,
    rushingyards3q real,
    rushingyards4q real,
    rushingyards1h real,
    rushingyards2h real,
    rushingyardsovt real,
    rushingyardsperattempt real,
    rushingtouchdowns real,
    rushingtouchdowns1q real,
    rushingtouchdowns2q real,
    rushingtouchdowns3q real,
    rushingtouchdowns4q real,
    rushingtouchdowns1h real,
    rushingtouchdowns2h real,
    rushingtouchdownsovt real,
    passingattempts real,
    passingattempts1q real,
    passingattempts2q real,
    passingattempts3q real,
    passingattempts4q real,
    passingattempts1h real,
    passingattempts2h real,
    passingattemptsovt real,
    passingcompletions real,
    passingcompletions1q real,
    passingcompletions2q real,
    passingcompletions3q real,
    passingcompletions4q real,
    passingcompletions1h real,
    passingcompletions2h real,
    passingcompletionsovt real,
    passingyards real,
    passingyards1q real,
    passingyards2q real,
    passingyards3q real,
    passingyards4q real,
    passingyards1h real,
    passingyards2h real,
    passingyardsovt real,
    passingtouchdowns real,
    passingtouchdowns1q real,
    passingtouchdowns2q real,
    passingtouchdowns3q real,
    passingtouchdowns4q real,
    passingtouchdowns1h real,
    passingtouchdowns2h real,
    passingtouchdownsovt real,
    passinginterceptions real,
    passinginterceptions1q real,
    passinginterceptions2q real,
    passinginterceptions3q real,
    passinginterceptions4q real,
    passinginterceptions1h real,
    passinginterceptions2h real,
    passinginterceptionsovt real,
    passingyardsperattempt real,
    passingyardspercompletion real,
    completionpercentage real,
    passerrating real,
    thirddownattempts real,
    thirddownconversions real,
    thirddownpercentage real,
    fourthdownattempts real,
    fourthdownconversions real,
    fourthdownpercentage real,
    redzoneattempts real,
    redzoneconversions real,
    goaltogoattempts real,
    goaltogoconversions real,
    returnyards real,
    penalties real,
    penaltyyards real,
    fumbles real,
    fumbleslost real,
    timessacked real,
    timessacked1q real,
    timessacked2q real,
    timessacked3q real,
    timessacked4q real,
    timessacked1h real,
    timessacked2h real,
    timessackedovt real,
    timessackedyards real,
    quarterbackhits real,
    tacklesforloss real,
    safeties real,
    punts real,
    punts1q real,
    punts2q real,
    punts3q real,
    punts4q real,
    punts1h real,
    punts2h real,
    puntsovt real,
    puntyards real,
    puntaverage real,
    giveaways real,
    giveaways1q real,
    giveaways2q real,
    giveaways3q real,
    giveaways4q real,
    giveaways1h real,
    giveaways2h real,
    giveawaysovt real,
    takeaways real,
    turnoverdifferential real,
    opponentscore1q real,
    opponentscore2q real,
    opponentscore3q real,
    opponentscore4q real,
    opponentscoreovt real,
    opponentscore1h real,
    opponentscore2h real,
    opponenttimeofpossessionminutes integer,
    opponenttimeofpossessionseconds integer,
    opponenttimeofpossession character varying(15),
    opponentfirstdowns real,
    opponentfirstdownsbyrushing real,
    opponentfirstdownsbypassing real,
    opponentfirstdownsbypenalty real,
    opponentoffensiveplays real,
    opponentoffensiveplays1q real,
    opponentoffensiveplays2q real,
    opponentoffensiveplays3q real,
    opponentoffensiveplays4q real,
    opponentoffensiveplays1h real,
    opponentoffensiveplays2h real,
    opponentoffensiveplaysovt real,
    opponentoffensiveyards real,
    opponentoffensiveyards1q real,
    opponentoffensiveyards2q real,
    opponentoffensiveyards3q real,
    opponentoffensiveyards4q real,
    opponentoffensiveyards1h real,
    opponentoffensiveyards2h real,
    opponentoffensiveyardsovt real,
    opponentoffensiveyardsperplay real,
    opponenttouchdowns real,
    opponentrushingattempts real,
    opponentrushingyards real,
    opponentrushingyards1q real,
    opponentrushingyards2q real,
    opponentrushingyards3q real,
    opponentrushingyards4q real,
    opponentrushingyards1h real,
    opponentrushingyards2h real,
    opponentrushingyardsovt real,
    opponentrushingyardsperattempt real,
    opponentrushingtouchdowns real,
    opponentpassingattempts real,
    opponentpassingcompletions real,
    opponentpassingyards real,
    opponentpassingyards1q real,
    opponentpassingyards2q real,
    opponentpassingyards3q real,
    opponentpassingyards4q real,
    opponentpassingyards1h real,
    opponentpassingyards2h real,
    opponentpassingyardsovt real,
    opponentpassingtouchdowns real,
    opponentpassinginterceptions real,
    opponentpassinginterceptions1q real,
    opponentpassinginterceptions2q real,
    opponentpassinginterceptions3q real,
    opponentpassinginterceptions4q real,
    opponentpassinginterceptions1h real,
    opponentpassinginterceptions2h real,
    opponentpassinginterceptionsovt real,
    opponentpassingyardsperattempt real,
    opponentpassingyardspercompletion real,
    opponentcompletionpercentage real,
    opponentpasserrating real,
    opponentthirddownattempts real,
    opponentthirddownconversions real,
    opponentthirddownpercentage real,
    opponentfourthdownattempts real,
    opponentfourthdownconversions real,
    opponentfourthdownpercentage real,
    opponentredzoneattempts real,
    opponentredzoneconversions real,
    opponentgoaltogoattempts real,
    opponentgoaltogoconversions real,
    opponentreturnyards real,
    opponentpenalties real,
    opponentpenaltyyards real,
    opponentfumbles real,
    opponentfumbleslost real,
    opponenttimessacked real,
    opponenttimessackedyards real,
    opponentquarterbackhits real,
    opponenttacklesforloss real,
    opponentsafeties real,
    opponentsafeties1q real,
    opponentsafeties2q real,
    opponentsafeties3q real,
    opponentsafeties4q real,
    opponentsafeties1h real,
    opponentsafeties2h real,
    opponentsafetiesovt real,
    opponentpunts real,
    opponentpuntyards real,
    opponentpuntaverage real,
    opponentgiveaways real,
    opponenttakeaways real,
    opponentturnoverdifferential real,
    redzonepercentage real,
    goaltogopercentage real,
    quarterbackhitsdifferential real,
    tacklesforlossdifferential real,
    quarterbacksacksdifferential real,
    tacklesforlosspercentage real,
    quarterbackhitspercentage real,
    timessackedpercentage real,
    opponentredzonepercentage real,
    opponentgoaltogopercentage real,
    opponentquarterbackhitsdifferential real,
    opponenttacklesforlossdifferential real,
    opponentquarterbacksacksdifferential real,
    opponenttacklesforlosspercentage real,
    opponentquarterbackhitspercentage real,
    opponenttimessackedpercentage real,
    kickoffs real,
    kickoffsinendzone real,
    kickofftouchbacks real,
    puntshadblocked real,
    puntshadblocked1q real,
    puntshadblocked2q real,
    puntshadblocked3q real,
    puntshadblocked4q real,
    puntshadblocked1h real,
    puntshadblocked2h real,
    puntshadblockedovt real,
    puntnetaverage real,
    extrapointkickingattempts real,
    extrapointkickingattempts1q real,
    extrapointkickingattempts2q real,
    extrapointkickingattempts3q real,
    extrapointkickingattempts4q real,
    extrapointkickingattempts1h real,
    extrapointkickingattempts2h real,
    extrapointkickingattemptsovt real,
    extrapointkickingconversions real,
    extrapointkickingconversions1q real,
    extrapointkickingconversions2q real,
    extrapointkickingconversions3q real,
    extrapointkickingconversions4q real,
    extrapointkickingconversions1h real,
    extrapointkickingconversions2h real,
    extrapointkickingconversionsovt real,
    extrapointshadblocked real,
    extrapointshadblocked1q real,
    extrapointshadblocked2q real,
    extrapointshadblocked3q real,
    extrapointshadblocked4q real,
    extrapointshadblocked1h real,
    extrapointshadblocked2h real,
    extrapointshadblockedovt real,
    extrapointpassingattempts real,
    extrapointpassingconversions real,
    extrapointpassingconversions1q real,
    extrapointpassingconversions2q real,
    extrapointpassingconversions3q real,
    extrapointpassingconversions4q real,
    extrapointpassingconversions1h real,
    extrapointpassingconversions2h real,
    extrapointpassingconversionsovt real,
    extrapointrushingattempts real,
    extrapointrushingconversions real,
    extrapointrushingconversions1q real,
    extrapointrushingconversions2q real,
    extrapointrushingconversions3q real,
    extrapointrushingconversions4q real,
    extrapointrushingconversions1h real,
    extrapointrushingconversions2h real,
    extrapointrushingconversionsovt real,
    fieldgoalattempts real,
    fieldgoalattempts1q real,
    fieldgoalattempts2q real,
    fieldgoalattempts3q real,
    fieldgoalattempts4q real,
    fieldgoalattempts1h real,
    fieldgoalattempts2h real,
    fieldgoalattemptsovt real,
    fieldgoalsmade real,
    fieldgoalsmade1q real,
    fieldgoalsmade2q real,
    fieldgoalsmade3q real,
    fieldgoalsmade4q real,
    fieldgoalsmade1h real,
    fieldgoalsmade2h real,
    fieldgoalsmadeovt real,
    fieldgoalshadblocked real,
    fieldgoalshadblocked1q real,
    fieldgoalshadblocked2q real,
    fieldgoalshadblocked3q real,
    fieldgoalshadblocked4q real,
    fieldgoalshadblocked1h real,
    fieldgoalshadblocked2h real,
    fieldgoalshadblockedovt real,
    puntreturns real,
    puntreturnyards real,
    kickreturns real,
    kickreturnyards real,
    interceptionreturns real,
    interceptionreturnyards real,
    opponentkickoffs real,
    opponentkickoffsinendzone real,
    opponentkickofftouchbacks real,
    opponentpuntshadblocked real,
    opponentpuntnetaverage real,
    opponentextrapointkickingattempts real,
    opponentextrapointkickingconversions real,
    opponentextrapointshadblocked real,
    opponentextrapointpassingattempts real,
    opponentextrapointpassingconversions real,
    opponentextrapointrushingattempts real,
    opponentextrapointrushingconversions real,
    opponentfieldgoalattempts real,
    opponentfieldgoalsmade real,
    opponentfieldgoalshadblocked real,
    opponentpuntreturns real,
    opponentpuntreturnyards real,
    opponentkickreturns real,
    opponentkickreturnyards real,
    opponentinterceptionreturns real,
    opponentinterceptionreturnyards real,
    solotackles real,
    assistedtackles real,
    sacks real,
    sacks1q real,
    sacks2q real,
    sacks3q real,
    sacks4q real,
    sacks1h real,
    sacks2h real,
    sacksovt real,
    sackyards real,
    passesdefended real,
    fumblesforced real,
    fumblesrecovered real,
    fumblereturnyards real,
    fumblereturntouchdowns real,
    interceptionreturntouchdowns real,
    blockedkicks real,
    puntreturntouchdowns real,
    puntreturnlong real,
    kickreturntouchdowns real,
    kickreturnlong real,
    blockedkickreturnyards real,
    blockedkickreturntouchdowns real,
    fieldgoalreturnyards real,
    fieldgoalreturntouchdowns real,
    puntnetyards real,
    opponentsolotackles real,
    opponentassistedtackles real,
    opponentsacks real,
    opponentsackyards real,
    opponentpassesdefended real,
    opponentfumblesforced real,
    opponentfumblesrecovered real,
    opponentfumblereturnyards real,
    opponentfumblereturntouchdowns real,
    opponentinterceptionreturntouchdowns real,
    opponentblockedkicks real,
    opponentpuntreturntouchdowns real,
    opponentpuntreturnlong real,
    opponentkickreturntouchdowns real,
    opponentkickreturnlong real,
    opponentblockedkickreturnyards real,
    opponentblockedkickreturntouchdowns real,
    opponentfieldgoalreturnyards real,
    opponentfieldgoalreturntouchdowns real,
    opponentpuntnetyards real,
    teamname character varying(30),
    gamedayofweek public.weekday,
    passingdropbacks real,
    passingdropbacks1q real,
    passingdropbacks2q real,
    passingdropbacks3q real,
    passingdropbacks4q real,
    passingdropbacks1h real,
    passingdropbacks2h real,
    passingdropbacksovt real,
    opponentpassingdropbacks real,
    twopointconversionreturns real,
    opponenttwopointconversionreturns real,
    teamid integer,
    opponentid integer,
    dayofgame date,
    datetime date,
    globalopponentid integer,
    fantasypoints real,
    fantasypoints1q real,
    fantasypoints2q real,
    fantasypoints3q real,
    fantasypoints4q real,
    fantasypointsovt real,
    fantasypoints1h real,
    fantasypoints2h real,
    games smallint,
    defensivetd real,
    defensivetd1q real,
    defensivetd2q real,
    defensivetd3q real,
    defensivetd4q real,
    defensivetd1h real,
    defensivetd2h real,
    defensivetdovt real,
    deffantasy real,
    deffantasy1q real,
    deffantasy2q real,
    deffantasy3q real,
    deffantasy4q real,
    deffantasy1h real,
    deffantasy2h real,
    deffantasyovt real,
    defensivecoordinator character varying(50),
    vsdefensivecoordinator character varying(50),
    offensivecoordinator character varying(50),
    vsoffensivecoordinator character varying(50),
    headcoach character varying(50),
    vsheadcoach character varying(50),
    gamedateteam character(11),
    zipofstadium character(8),
    twopointconversionreturns1q real DEFAULT 0,
    twopointconversionreturns2q real DEFAULT 0,
    twopointconversionreturns3q real DEFAULT 0,
    twopointconversionreturns4q real DEFAULT 0,
    twopointconversionreturns1h real DEFAULT 0,
    twopointconversionreturns2h real DEFAULT 0,
    twopointconversionreturnsovt real DEFAULT 0,
    kickreturntouchdowns1q real DEFAULT 0,
    kickreturntouchdowns2q real DEFAULT 0,
    kickreturntouchdowns3q real DEFAULT 0,
    kickreturntouchdowns4q real DEFAULT 0,
    kickreturntouchdowns1h real DEFAULT 0,
    kickreturntouchdowns2h real DEFAULT 0,
    kickreturntouchdownsovt real DEFAULT 0,
    puntreturntouchdowns1q real DEFAULT 0,
    puntreturntouchdowns2q real DEFAULT 0,
    puntreturntouchdowns3q real DEFAULT 0,
    puntreturntouchdowns4q real DEFAULT 0,
    puntreturntouchdowns1h real DEFAULT 0,
    puntreturntouchdowns2h real DEFAULT 0,
    puntreturntouchdownsovt real DEFAULT 0,
    safeties1q real DEFAULT 0,
    safeties2q real DEFAULT 0,
    safeties3q real DEFAULT 0,
    safeties4q real DEFAULT 0,
    safeties1h real DEFAULT 0,
    safeties2h real DEFAULT 0,
    safetiesovt real DEFAULT 0,
    fumblesrecovered1q real DEFAULT 0,
    fumblesrecovered2q real DEFAULT 0,
    fumblesrecovered3q real DEFAULT 0,
    fumblesrecovered4q real DEFAULT 0,
    fumblesrecovered1h real DEFAULT 0,
    fumblesrecovered2h real DEFAULT 0,
    fumblesrecoveredovt real DEFAULT 0
);


ALTER TABLE public.nflteam OWNER TO cblanton;

--
-- TOC entry 205 (class 1259 OID 26783)
-- Name: nflt1h; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt1h AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score1h AS score,
    nflteam.offensiveplays1h AS offp,
    nflteam.offensiveyards1h AS offyrds,
    nflteam.passingyards1h AS pyrds,
    nflteam.rushingyards1h AS ruyds,
    nflteam.touchdowns1h AS tds,
    nflteam.passingtouchdowns1h AS ptds,
    nflteam.rushingtouchdowns1h AS rtds,
    nflteam.giveaways1h AS gaways,
    nflteam.extrapointpassingconversions1h AS xppassconv,
    nflteam.passingdropbacks1h AS pdropbacks,
    nflteam.passingattempts1h AS patts,
    nflteam.passingcompletions1h AS passc,
    nflteam.passinginterceptions1h AS pinc,
    nflteam.timessacked1h AS tsack,
    nflteam.extrapointrushingconversions1h AS xprushconv,
    nflteam.rushingattempts1h AS ratts,
    nflteam.firstdownsbyrushing1h AS firstdbrush,
    nflteam.firstdownsbypassing1h AS firstdbpass,
    nflteam.extrapointkickingconversions1h AS xpkickconv,
    nflteam.fieldgoalsmade1h AS fgm,
    nflteam.fieldgoalattempts1h AS fga,
    nflteam.extrapointkickingattempts1h AS xpkickatt,
    nflteam.fieldgoalshadblocked1h AS fgblocked,
    nflteam.extrapointshadblocked1h AS xpblocked,
    nflteam.punts1h AS punt,
    nflteam.puntshadblocked1h AS puntblocked,
    nflteam.opponentscore1h AS vsscore,
    nflteam.opponentoffensiveplays1h AS defplay,
    nflteam.opponentoffensiveyards1h AS yrdsallowed,
    nflteam.opponentpassingyards1h AS passyrdsallow,
    nflteam.opponentrushingyards1h AS rushyrdsallow,
    nflteam.defensivetd1h AS deftds,
    nflteam.deffantasy1h AS deffantasypts,
    nflteam.opponentpassinginterceptions1h AS vspassint,
    nflteam.sacks1h AS sacks,
    nflteam.opponentsafeties1h AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nflt1h OWNER TO admin;

--
-- TOC entry 204 (class 1259 OID 26778)
-- Name: nflt1hrz; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt1hrz AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score1h AS score,
    nflteam.offensiveplays1h AS offp,
    nflteam.offensiveyards1h AS offyrds,
    nflteam.passingyards1h AS pyrds,
    nflteam.rushingyards1h AS ruyds,
    nflteam.touchdowns1h AS tds,
    nflteam.passingtouchdowns1h AS ptds,
    nflteam.rushingtouchdowns1h AS rtds,
    nflteam.giveaways1h AS gaways,
    nflteam.extrapointpassingconversions1h AS xppassconv,
    nflteam.passingdropbacks1h AS pdropbacks,
    nflteam.passingattempts1h AS patts,
    nflteam.passingcompletions1h AS passc,
    nflteam.passinginterceptions1h AS pinc,
    nflteam.timessacked1h AS tsack,
    nflteam.extrapointrushingconversions1h AS xprushconv,
    nflteam.rushingattempts1h AS ratts,
    nflteam.firstdownsbyrushing1h AS firstdbrush,
    nflteam.firstdownsbypassing1h AS firstdbpass,
    nflteam.extrapointkickingconversions1h AS xpkickconv,
    nflteam.fieldgoalsmade1h AS fgm,
    nflteam.fieldgoalattempts1h AS fga,
    nflteam.extrapointkickingattempts1h AS xpkickatt,
    nflteam.fieldgoalshadblocked1h AS fgblocked,
    nflteam.extrapointshadblocked1h AS xpblocked,
    nflteam.punts1h AS punt,
    nflteam.puntshadblocked1h AS puntblocked,
    nflteam.opponentscore1h AS vsscore,
    nflteam.opponentoffensiveplays1h AS defplay,
    nflteam.opponentoffensiveyards1h AS yrdsallowed,
    nflteam.opponentpassingyards1h AS passyrdsallow,
    nflteam.opponentrushingyards1h AS rushyrdsallow,
    nflteam.defensivetd1h AS deftds,
    nflteam.deffantasy1h AS deffantasypts,
    nflteam.opponentpassinginterceptions1h AS vspassint,
    nflteam.sacks1h AS sacks,
    nflteam.opponentsafeties1h AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nflt1hrz OWNER TO admin;

--
-- TOC entry 197 (class 1259 OID 26737)
-- Name: nflt1q; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt1q AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score1q AS score,
    nflteam.offensiveplays1q AS offp,
    nflteam.offensiveyards1q AS offyrds,
    nflteam.passingyards1q AS pyrds,
    nflteam.rushingyards1q AS ruyds,
    nflteam.touchdowns1q AS tds,
    nflteam.passingtouchdowns1q AS ptds,
    nflteam.rushingtouchdowns1q AS rtds,
    nflteam.giveaways1q AS gaways,
    nflteam.extrapointpassingconversions1q AS xppassconv,
    nflteam.passingdropbacks1q AS pdropbacks,
    nflteam.passingattempts1q AS patts,
    nflteam.passingcompletions1q AS passc,
    nflteam.passinginterceptions1q AS pinc,
    nflteam.timessacked1q AS tsack,
    nflteam.extrapointrushingconversions1q AS xprushconv,
    nflteam.rushingattempts1q AS ratts,
    nflteam.firstdownsbyrushing1q AS firstdbrush,
    nflteam.firstdownsbypassing1q AS firstdbpass,
    nflteam.extrapointkickingconversions1q AS xpkickconv,
    nflteam.fieldgoalsmade1q AS fgm,
    nflteam.fieldgoalattempts1q AS fga,
    nflteam.extrapointkickingattempts1q AS xpkickatt,
    nflteam.fieldgoalshadblocked1q AS fgblocked,
    nflteam.extrapointshadblocked1q AS xpblocked,
    nflteam.punts1q AS punt,
    nflteam.puntshadblocked1q AS puntblocked,
    nflteam.opponentscore1q AS vsscore,
    nflteam.opponentoffensiveplays1q AS defplay,
    nflteam.opponentoffensiveyards1q AS yrdsallowed,
    nflteam.opponentpassingyards1q AS passyrdsallow,
    nflteam.opponentrushingyards1q AS rushyrdsallow,
    nflteam.defensivetd1q AS deftds,
    nflteam.deffantasy1q AS deffantasypts,
    nflteam.opponentpassinginterceptions1q AS vspassint,
    nflteam.sacks1q AS sacks,
    nflteam.opponentsafeties1q AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nflt1q OWNER TO admin;

--
-- TOC entry 195 (class 1259 OID 26141)
-- Name: nflteamrz; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.nflteamrz (
    gamedate date,
    mmonth character(2),
    dday character(2),
    yyear character(4),
    hhour character(2),
    mminute character(2),
    newweeks public.weekst,
    primetime character varying(15),
    dayofweek public.weekday,
    nightgame boolean,
    primetstart boolean,
    keydateteam character(14),
    team character(3),
    opponent character(3),
    seasontype integer,
    season character(4),
    week smallint,
    teambad character(3),
    opponentbad character(3),
    homeoraway public.haway,
    score integer,
    opponentscore integer,
    totalscore integer,
    result character varying(10),
    stadiumbad character varying(50),
    playingsurface character varying(15),
    stadium character varying(50),
    indooroutdoor public.venuet,
    grassorturf public.fieldt,
    codetemp character(1),
    codewind character(1),
    codewcond character(1),
    temprange public.tempt,
    wind public.windt,
    wconditions public.weathert,
    temperature real,
    humidity real,
    windspeed real,
    overunder real,
    pointspread real,
    score1q real,
    score2q real,
    score3q real,
    score4q real,
    scoreovt real,
    score1h real,
    score2h real,
    timeofpossessionminutes integer,
    timeofpossessionseconds integer,
    timeofpossession character varying(15),
    firstdowns real,
    firstdownsbyrushing real,
    firstdownsbyrushing1q real,
    firstdownsbyrushing2q real,
    firstdownsbyrushing3q real,
    firstdownsbyrushing4q real,
    firstdownsbyrushing1h real,
    firstdownsbyrushing2h real,
    firstdownsbyrushingovt real,
    firstdownsbypassing real,
    firstdownsbypassing1q real,
    firstdownsbypassing2q real,
    firstdownsbypassing3q real,
    firstdownsbypassing4q real,
    firstdownsbypassing1h real,
    firstdownsbypassing2h real,
    firstdownsbypassingovt real,
    firstdownsbypenalty real,
    offensiveplays real,
    offensiveplays1q real,
    offensiveplays2q real,
    offensiveplays3q real,
    offensiveplays4q real,
    offensiveplays1h real,
    offensiveplays2h real,
    offensiveplaysovt real,
    offensiveyards real,
    offensiveyards1q real,
    offensiveyards2q real,
    offensiveyards3q real,
    offensiveyards4q real,
    offensiveyards1h real,
    offensiveyards2h real,
    offensiveyardsovt real,
    offensiveyardsperplay real,
    touchdowns real,
    touchdowns1q real,
    touchdowns2q real,
    touchdowns3q real,
    touchdowns4q real,
    touchdowns1h real,
    touchdowns2h real,
    touchdownsovt real,
    rushingattempts real,
    rushingattempts1q real,
    rushingattempts2q real,
    rushingattempts3q real,
    rushingattempts4q real,
    rushingattempts1h real,
    rushingattempts2h real,
    rushingattemptsovt real,
    rushingyards real,
    rushingyards1q real,
    rushingyards2q real,
    rushingyards3q real,
    rushingyards4q real,
    rushingyards1h real,
    rushingyards2h real,
    rushingyardsovt real,
    rushingyardsperattempt real,
    rushingtouchdowns real,
    rushingtouchdowns1q real,
    rushingtouchdowns2q real,
    rushingtouchdowns3q real,
    rushingtouchdowns4q real,
    rushingtouchdowns1h real,
    rushingtouchdowns2h real,
    rushingtouchdownsovt real,
    passingattempts real,
    passingattempts1q real,
    passingattempts2q real,
    passingattempts3q real,
    passingattempts4q real,
    passingattempts1h real,
    passingattempts2h real,
    passingattemptsovt real,
    passingcompletions real,
    passingcompletions1q real,
    passingcompletions2q real,
    passingcompletions3q real,
    passingcompletions4q real,
    passingcompletions1h real,
    passingcompletions2h real,
    passingcompletionsovt real,
    passingyards real,
    passingyards1q real,
    passingyards2q real,
    passingyards3q real,
    passingyards4q real,
    passingyards1h real,
    passingyards2h real,
    passingyardsovt real,
    passingtouchdowns real,
    passingtouchdowns1q real,
    passingtouchdowns2q real,
    passingtouchdowns3q real,
    passingtouchdowns4q real,
    passingtouchdowns1h real,
    passingtouchdowns2h real,
    passingtouchdownsovt real,
    passinginterceptions real,
    passinginterceptions1q real,
    passinginterceptions2q real,
    passinginterceptions3q real,
    passinginterceptions4q real,
    passinginterceptions1h real,
    passinginterceptions2h real,
    passinginterceptionsovt real,
    passingyardsperattempt real,
    passingyardspercompletion real,
    completionpercentage real,
    passerrating real,
    thirddownattempts real,
    thirddownconversions real,
    thirddownpercentage real,
    fourthdownattempts real,
    fourthdownconversions real,
    fourthdownpercentage real,
    redzoneattempts real,
    redzoneconversions real,
    goaltogoattempts real,
    goaltogoconversions real,
    returnyards real,
    penalties real,
    penaltyyards real,
    fumbles real,
    fumbleslost real,
    timessacked real,
    timessacked1q real,
    timessacked2q real,
    timessacked3q real,
    timessacked4q real,
    timessacked1h real,
    timessacked2h real,
    timessackedovt real,
    timessackedyards real,
    quarterbackhits real,
    tacklesforloss real,
    safeties real,
    punts real,
    punts1q real,
    punts2q real,
    punts3q real,
    punts4q real,
    punts1h real,
    punts2h real,
    puntsovt real,
    puntyards real,
    puntaverage real,
    giveaways real,
    giveaways1q real,
    giveaways2q real,
    giveaways3q real,
    giveaways4q real,
    giveaways1h real,
    giveaways2h real,
    giveawaysovt real,
    takeaways real,
    turnoverdifferential real,
    opponentscore1q real,
    opponentscore2q real,
    opponentscore3q real,
    opponentscore4q real,
    opponentscoreovt real,
    opponentscore1h real,
    opponentscore2h real,
    opponenttimeofpossessionminutes integer,
    opponenttimeofpossessionseconds integer,
    opponenttimeofpossession character varying(15),
    opponentfirstdowns real,
    opponentfirstdownsbyrushing real,
    opponentfirstdownsbypassing real,
    opponentfirstdownsbypenalty real,
    opponentoffensiveplays real,
    opponentoffensiveplays1q real,
    opponentoffensiveplays2q real,
    opponentoffensiveplays3q real,
    opponentoffensiveplays4q real,
    opponentoffensiveplays1h real,
    opponentoffensiveplays2h real,
    opponentoffensiveplaysovt real,
    opponentoffensiveyards real,
    opponentoffensiveyards1q real,
    opponentoffensiveyards2q real,
    opponentoffensiveyards3q real,
    opponentoffensiveyards4q real,
    opponentoffensiveyards1h real,
    opponentoffensiveyards2h real,
    opponentoffensiveyardsovt real,
    opponentoffensiveyardsperplay real,
    opponenttouchdowns real,
    opponentrushingattempts real,
    opponentrushingyards real,
    opponentrushingyards1q real,
    opponentrushingyards2q real,
    opponentrushingyards3q real,
    opponentrushingyards4q real,
    opponentrushingyards1h real,
    opponentrushingyards2h real,
    opponentrushingyardsovt real,
    opponentrushingyardsperattempt real,
    opponentrushingtouchdowns real,
    opponentpassingattempts real,
    opponentpassingcompletions real,
    opponentpassingyards real,
    opponentpassingyards1q real,
    opponentpassingyards2q real,
    opponentpassingyards3q real,
    opponentpassingyards4q real,
    opponentpassingyards1h real,
    opponentpassingyards2h real,
    opponentpassingyardsovt real,
    opponentpassingtouchdowns real,
    opponentpassinginterceptions real,
    opponentpassinginterceptions1q real,
    opponentpassinginterceptions2q real,
    opponentpassinginterceptions3q real,
    opponentpassinginterceptions4q real,
    opponentpassinginterceptions1h real,
    opponentpassinginterceptions2h real,
    opponentpassinginterceptionsovt real,
    opponentpassingyardsperattempt real,
    opponentpassingyardspercompletion real,
    opponentcompletionpercentage real,
    opponentpasserrating real,
    opponentthirddownattempts real,
    opponentthirddownconversions real,
    opponentthirddownpercentage real,
    opponentfourthdownattempts real,
    opponentfourthdownconversions real,
    opponentfourthdownpercentage real,
    opponentredzoneattempts real,
    opponentredzoneconversions real,
    opponentgoaltogoattempts real,
    opponentgoaltogoconversions real,
    opponentreturnyards real,
    opponentpenalties real,
    opponentpenaltyyards real,
    opponentfumbles real,
    opponentfumbleslost real,
    opponenttimessacked real,
    opponenttimessackedyards real,
    opponentquarterbackhits real,
    opponenttacklesforloss real,
    opponentsafeties real,
    opponentsafeties1q real,
    opponentsafeties2q real,
    opponentsafeties3q real,
    opponentsafeties4q real,
    opponentsafeties1h real,
    opponentsafeties2h real,
    opponentsafetiesovt real,
    opponentpunts real,
    opponentpuntyards real,
    opponentpuntaverage real,
    opponentgiveaways real,
    opponenttakeaways real,
    opponentturnoverdifferential real,
    redzonepercentage real,
    goaltogopercentage real,
    quarterbackhitsdifferential real,
    tacklesforlossdifferential real,
    quarterbacksacksdifferential real,
    tacklesforlosspercentage real,
    quarterbackhitspercentage real,
    timessackedpercentage real,
    opponentredzonepercentage real,
    opponentgoaltogopercentage real,
    opponentquarterbackhitsdifferential real,
    opponenttacklesforlossdifferential real,
    opponentquarterbacksacksdifferential real,
    opponenttacklesforlosspercentage real,
    opponentquarterbackhitspercentage real,
    opponenttimessackedpercentage real,
    kickoffs real,
    kickoffsinendzone real,
    kickofftouchbacks real,
    puntshadblocked real,
    puntshadblocked1q real,
    puntshadblocked2q real,
    puntshadblocked3q real,
    puntshadblocked4q real,
    puntshadblocked1h real,
    puntshadblocked2h real,
    puntshadblockedovt real,
    puntnetaverage real,
    extrapointkickingattempts real,
    extrapointkickingattempts1q real,
    extrapointkickingattempts2q real,
    extrapointkickingattempts3q real,
    extrapointkickingattempts4q real,
    extrapointkickingattempts1h real,
    extrapointkickingattempts2h real,
    extrapointkickingattemptsovt real,
    extrapointkickingconversions real,
    extrapointkickingconversions1q real,
    extrapointkickingconversions2q real,
    extrapointkickingconversions3q real,
    extrapointkickingconversions4q real,
    extrapointkickingconversions1h real,
    extrapointkickingconversions2h real,
    extrapointkickingconversionsovt real,
    extrapointshadblocked real,
    extrapointshadblocked1q real,
    extrapointshadblocked2q real,
    extrapointshadblocked3q real,
    extrapointshadblocked4q real,
    extrapointshadblocked1h real,
    extrapointshadblocked2h real,
    extrapointshadblockedovt real,
    extrapointpassingattempts real,
    extrapointpassingconversions real,
    extrapointpassingconversions1q real,
    extrapointpassingconversions2q real,
    extrapointpassingconversions3q real,
    extrapointpassingconversions4q real,
    extrapointpassingconversions1h real,
    extrapointpassingconversions2h real,
    extrapointpassingconversionsovt real,
    extrapointrushingattempts real,
    extrapointrushingconversions real,
    extrapointrushingconversions1q real,
    extrapointrushingconversions2q real,
    extrapointrushingconversions3q real,
    extrapointrushingconversions4q real,
    extrapointrushingconversions1h real,
    extrapointrushingconversions2h real,
    extrapointrushingconversionsovt real,
    fieldgoalattempts real,
    fieldgoalattempts1q real,
    fieldgoalattempts2q real,
    fieldgoalattempts3q real,
    fieldgoalattempts4q real,
    fieldgoalattempts1h real,
    fieldgoalattempts2h real,
    fieldgoalattemptsovt real,
    fieldgoalsmade real,
    fieldgoalsmade1q real,
    fieldgoalsmade2q real,
    fieldgoalsmade3q real,
    fieldgoalsmade4q real,
    fieldgoalsmade1h real,
    fieldgoalsmade2h real,
    fieldgoalsmadeovt real,
    fieldgoalshadblocked real,
    fieldgoalshadblocked1q real,
    fieldgoalshadblocked2q real,
    fieldgoalshadblocked3q real,
    fieldgoalshadblocked4q real,
    fieldgoalshadblocked1h real,
    fieldgoalshadblocked2h real,
    fieldgoalshadblockedovt real,
    puntreturns real,
    puntreturnyards real,
    kickreturns real,
    kickreturnyards real,
    interceptionreturns real,
    interceptionreturnyards real,
    opponentkickoffs real,
    opponentkickoffsinendzone real,
    opponentkickofftouchbacks real,
    opponentpuntshadblocked real,
    opponentpuntnetaverage real,
    opponentextrapointkickingattempts real,
    opponentextrapointkickingconversions real,
    opponentextrapointshadblocked real,
    opponentextrapointpassingattempts real,
    opponentextrapointpassingconversions real,
    opponentextrapointrushingattempts real,
    opponentextrapointrushingconversions real,
    opponentfieldgoalattempts real,
    opponentfieldgoalsmade real,
    opponentfieldgoalshadblocked real,
    opponentpuntreturns real,
    opponentpuntreturnyards real,
    opponentkickreturns real,
    opponentkickreturnyards real,
    opponentinterceptionreturns real,
    opponentinterceptionreturnyards real,
    solotackles real,
    assistedtackles real,
    sacks real,
    sacks1q real,
    sacks2q real,
    sacks3q real,
    sacks4q real,
    sacks1h real,
    sacks2h real,
    sacksovt real,
    sackyards real,
    passesdefended real,
    fumblesforced real,
    fumblesrecovered real,
    fumblereturnyards real,
    fumblereturntouchdowns real,
    interceptionreturntouchdowns real,
    blockedkicks real,
    puntreturntouchdowns real,
    puntreturnlong real,
    kickreturntouchdowns real,
    kickreturnlong real,
    blockedkickreturnyards real,
    blockedkickreturntouchdowns real,
    fieldgoalreturnyards real,
    fieldgoalreturntouchdowns real,
    puntnetyards real,
    opponentsolotackles real,
    opponentassistedtackles real,
    opponentsacks real,
    opponentsackyards real,
    opponentpassesdefended real,
    opponentfumblesforced real,
    opponentfumblesrecovered real,
    opponentfumblereturnyards real,
    opponentfumblereturntouchdowns real,
    opponentinterceptionreturntouchdowns real,
    opponentblockedkicks real,
    opponentpuntreturntouchdowns real,
    opponentpuntreturnlong real,
    opponentkickreturntouchdowns real,
    opponentkickreturnlong real,
    opponentblockedkickreturnyards real,
    opponentblockedkickreturntouchdowns real,
    opponentfieldgoalreturnyards real,
    opponentfieldgoalreturntouchdowns real,
    opponentpuntnetyards real,
    teamname character varying(30),
    gamedayofweek public.weekday,
    passingdropbacks real,
    passingdropbacks1q real,
    passingdropbacks2q real,
    passingdropbacks3q real,
    passingdropbacks4q real,
    passingdropbacks1h real,
    passingdropbacks2h real,
    passingdropbacksovt real,
    opponentpassingdropbacks real,
    twopointconversionreturns real,
    opponenttwopointconversionreturns real,
    teamid integer,
    opponentid integer,
    dayofgame date,
    datetime date,
    globalopponentid integer,
    fantasypoints real,
    fantasypoints1q real,
    fantasypoints2q real,
    fantasypoints3q real,
    fantasypoints4q real,
    fantasypointsovt real,
    fantasypoints1h real,
    fantasypoints2h real,
    games smallint,
    defensivetd real,
    defensivetd1q real,
    defensivetd2q real,
    defensivetd3q real,
    defensivetd4q real,
    defensivetd1h real,
    defensivetd2h real,
    defensivetdovt real,
    deffantasy real,
    deffantasy1q real,
    deffantasy2q real,
    deffantasy3q real,
    deffantasy4q real,
    deffantasy1h real,
    deffantasy2h real,
    deffantasyovt real,
    defensivecoordinator character varying(50),
    vsdefensivecoordinator character varying(50),
    offensivecoordinator character varying(50),
    vsoffensivecoordinator character varying(50),
    headcoach character varying(50),
    vsheadcoach character varying(50),
    gamedateteam character(11),
    zipofstadium character(8)
);


ALTER TABLE public.nflteamrz OWNER TO admin;

--
-- TOC entry 210 (class 1259 OID 26809)
-- Name: nflt1qrz; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt1qrz AS
 SELECT nflteamrz.fantasypoints AS ffp,
    nflteamrz.games AS game,
    nflteamrz.team,
    nflteamrz.nightgame AS sng,
    nflteamrz.primetstart AS pgs,
    nflteamrz.season AS nflseason,
    nflteamrz.seasontype AS regseason,
    nflteamrz.week AS regweek,
    nflteamrz.newweeks AS seasonweeks,
    nflteamrz.opponent AS vsteam,
    nflteamrz.homeoraway AS location,
    nflteamrz.grassorturf AS playsurface,
    nflteamrz.indooroutdoor AS inoutdoor,
    nflteamrz.codetemp AS ctemp,
    nflteamrz.temprange AS tempwords,
    nflteamrz.codewind AS cwind,
    nflteamrz.wind AS wordswind,
    nflteamrz.codewcond AS cwcond,
    nflteamrz.wconditions,
    nflteamrz.vsheadcoach AS vshcoach,
    nflteamrz.vsoffensivecoordinator AS vsoffcoordinator,
    nflteamrz.vsdefensivecoordinator AS vsdefcoordinator,
    nflteamrz.stadium,
    nflteamrz.score1q AS score,
    nflteamrz.offensiveplays1q AS offp,
    nflteamrz.offensiveyards1q AS offyrds,
    nflteamrz.passingyards1q AS pyrds,
    nflteamrz.rushingyards1q AS ruyds,
    nflteamrz.touchdowns1q AS tds,
    nflteamrz.passingtouchdowns1q AS ptds,
    nflteamrz.rushingtouchdowns1q AS rtds,
    nflteamrz.giveaways1q AS gaways,
    nflteamrz.extrapointpassingconversions1q AS xppassconv,
    nflteamrz.passingdropbacks1q AS pdropbacks,
    nflteamrz.passingattempts1q AS patts,
    nflteamrz.passingcompletions1q AS passc,
    nflteamrz.passinginterceptions1q AS pinc,
    nflteamrz.timessacked1q AS tsack,
    nflteamrz.extrapointrushingconversions1q AS xprushconv,
    nflteamrz.rushingattempts1q AS ratts,
    nflteamrz.firstdownsbyrushing1q AS firstdbrush,
    nflteamrz.firstdownsbypassing1q AS firstdbpass,
    nflteamrz.extrapointkickingconversions1q AS xpkickconv,
    nflteamrz.fieldgoalsmade1q AS fgm,
    nflteamrz.fieldgoalattempts1q AS fga,
    nflteamrz.extrapointkickingattempts1q AS xpkickatt,
    nflteamrz.fieldgoalshadblocked1q AS fgblocked,
    nflteamrz.extrapointshadblocked1q AS xpblocked,
    nflteamrz.punts1q AS punt,
    nflteamrz.puntshadblocked1q AS puntblocked,
    nflteamrz.opponentscore1q AS vsscore,
    nflteamrz.opponentoffensiveplays1q AS defplay,
    nflteamrz.opponentoffensiveyards1q AS yrdsallowed,
    nflteamrz.opponentpassingyards1q AS passyrdsallow,
    nflteamrz.opponentrushingyards1q AS rushyrdsallow,
    nflteamrz.defensivetd1q AS deftds,
    nflteamrz.deffantasy1q AS deffantasypts,
    nflteamrz.opponentpassinginterceptions1q AS vspassint,
    nflteamrz.sacks1q AS sacks,
    nflteamrz.opponentsafeties1q AS vssafeties
   FROM public.nflteamrz
  WITH NO DATA;


ALTER TABLE public.nflt1qrz OWNER TO admin;

--
-- TOC entry 207 (class 1259 OID 26793)
-- Name: nflt2h; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt2h AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score2h AS score,
    nflteam.offensiveplays2h AS offp,
    nflteam.offensiveyards2h AS offyrds,
    nflteam.passingyards2h AS pyrds,
    nflteam.rushingyards2h AS ruyds,
    nflteam.touchdowns2h AS tds,
    nflteam.passingtouchdowns2h AS ptds,
    nflteam.rushingtouchdowns2h AS rtds,
    nflteam.giveaways2h AS gaways,
    nflteam.extrapointpassingconversions2h AS xppassconv,
    nflteam.passingdropbacks2h AS pdropbacks,
    nflteam.passingattempts2h AS patts,
    nflteam.passingcompletions2h AS passc,
    nflteam.passinginterceptions2h AS pinc,
    nflteam.timessacked2h AS tsack,
    nflteam.extrapointrushingconversions2h AS xprushconv,
    nflteam.rushingattempts2h AS ratts,
    nflteam.firstdownsbyrushing2h AS firstdbrush,
    nflteam.firstdownsbypassing2h AS firstdbpass,
    nflteam.extrapointkickingconversions2h AS xpkickconv,
    nflteam.fieldgoalsmade2h AS fgm,
    nflteam.fieldgoalattempts2h AS fga,
    nflteam.extrapointkickingattempts2h AS xpkickatt,
    nflteam.fieldgoalshadblocked2h AS fgblocked,
    nflteam.extrapointshadblocked2h AS xpblocked,
    nflteam.punts2h AS punt,
    nflteam.puntshadblocked2h AS puntblocked,
    nflteam.opponentscore2h AS vsscore,
    nflteam.opponentoffensiveplays2h AS defplay,
    nflteam.opponentoffensiveyards2h AS yrdsallowed,
    nflteam.opponentpassingyards2h AS passyrdsallow,
    nflteam.opponentrushingyards2h AS rushyrdsallow,
    nflteam.defensivetd2h AS deftds,
    nflteam.deffantasy2h AS deffantasypts,
    nflteam.opponentpassinginterceptions2h AS vspassint,
    nflteam.sacks2h AS sacks,
    nflteam.opponentsafeties2h AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nflt2h OWNER TO admin;

--
-- TOC entry 206 (class 1259 OID 26788)
-- Name: nflt2hrz; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt2hrz AS
 SELECT nflteamrz.fantasypoints AS ffp,
    nflteamrz.games AS game,
    nflteamrz.team,
    nflteamrz.nightgame AS sng,
    nflteamrz.primetstart AS pgs,
    nflteamrz.season AS nflseason,
    nflteamrz.seasontype AS regseason,
    nflteamrz.week AS regweek,
    nflteamrz.newweeks AS seasonweeks,
    nflteamrz.opponent AS vsteam,
    nflteamrz.homeoraway AS location,
    nflteamrz.grassorturf AS playsurface,
    nflteamrz.indooroutdoor AS inoutdoor,
    nflteamrz.codetemp AS ctemp,
    nflteamrz.temprange AS tempwords,
    nflteamrz.codewind AS cwind,
    nflteamrz.wind AS wordswind,
    nflteamrz.codewcond AS cwcond,
    nflteamrz.wconditions,
    nflteamrz.vsheadcoach AS vshcoach,
    nflteamrz.vsoffensivecoordinator AS vsoffcoordinator,
    nflteamrz.vsdefensivecoordinator AS vsdefcoordinator,
    nflteamrz.stadium,
    nflteamrz.score2h AS score,
    nflteamrz.offensiveplays2h AS offp,
    nflteamrz.offensiveyards2h AS offyrds,
    nflteamrz.passingyards2h AS pyrds,
    nflteamrz.rushingyards2h AS ruyds,
    nflteamrz.touchdowns2h AS tds,
    nflteamrz.passingtouchdowns2h AS ptds,
    nflteamrz.rushingtouchdowns2h AS rtds,
    nflteamrz.giveaways2h AS gaways,
    nflteamrz.extrapointpassingconversions2h AS xppassconv,
    nflteamrz.passingdropbacks2h AS pdropbacks,
    nflteamrz.passingattempts2h AS patts,
    nflteamrz.passingcompletions2h AS passc,
    nflteamrz.passinginterceptions2h AS pinc,
    nflteamrz.timessacked2h AS tsack,
    nflteamrz.extrapointrushingconversions2h AS xprushconv,
    nflteamrz.rushingattempts2h AS ratts,
    nflteamrz.firstdownsbyrushing2h AS firstdbrush,
    nflteamrz.firstdownsbypassing2h AS firstdbpass,
    nflteamrz.extrapointkickingconversions2h AS xpkickconv,
    nflteamrz.fieldgoalsmade2h AS fgm,
    nflteamrz.fieldgoalattempts2h AS fga,
    nflteamrz.extrapointkickingattempts2h AS xpkickatt,
    nflteamrz.fieldgoalshadblocked2h AS fgblocked,
    nflteamrz.extrapointshadblocked2h AS xpblocked,
    nflteamrz.punts2h AS punt,
    nflteamrz.puntshadblocked2h AS puntblocked,
    nflteamrz.opponentscore2h AS vsscore,
    nflteamrz.opponentoffensiveplays2h AS defplay,
    nflteamrz.opponentoffensiveyards2h AS yrdsallowed,
    nflteamrz.opponentpassingyards2h AS passyrdsallow,
    nflteamrz.opponentrushingyards2h AS rushyrdsallow,
    nflteamrz.defensivetd2h AS deftds,
    nflteamrz.deffantasy2h AS deffantasypts,
    nflteamrz.opponentpassinginterceptions2h AS vspassint,
    nflteamrz.sacks2h AS sacks,
    nflteamrz.opponentsafeties2h AS vssafeties
   FROM public.nflteamrz
  WITH NO DATA;


ALTER TABLE public.nflt2hrz OWNER TO admin;

--
-- TOC entry 199 (class 1259 OID 26753)
-- Name: nflt2q; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt2q AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score2q AS score,
    nflteam.offensiveplays2q AS offp,
    nflteam.offensiveyards2q AS offyrds,
    nflteam.passingyards2q AS pyrds,
    nflteam.rushingyards2q AS ruyds,
    nflteam.touchdowns2q AS tds,
    nflteam.passingtouchdowns2q AS ptds,
    nflteam.rushingtouchdowns2q AS rtds,
    nflteam.giveaways2q AS gaways,
    nflteam.extrapointpassingconversions2q AS xppassconv,
    nflteam.passingdropbacks2q AS pdropbacks,
    nflteam.passingattempts2q AS patts,
    nflteam.passingcompletions2q AS passc,
    nflteam.passinginterceptions2q AS pinc,
    nflteam.timessacked2q AS tsack,
    nflteam.extrapointrushingconversions2q AS xprushconv,
    nflteam.rushingattempts2q AS ratts,
    nflteam.firstdownsbyrushing2q AS firstdbrush,
    nflteam.firstdownsbypassing2q AS firstdbpass,
    nflteam.extrapointkickingconversions2q AS xpkickconv,
    nflteam.fieldgoalsmade2q AS fgm,
    nflteam.fieldgoalattempts2q AS fga,
    nflteam.extrapointkickingattempts2q AS xpkickatt,
    nflteam.fieldgoalshadblocked2q AS fgblocked,
    nflteam.extrapointshadblocked2q AS xpblocked,
    nflteam.punts2q AS punt,
    nflteam.puntshadblocked2q AS puntblocked,
    nflteam.opponentscore2q AS vsscore,
    nflteam.opponentoffensiveplays2q AS defplay,
    nflteam.opponentoffensiveyards2q AS yrdsallowed,
    nflteam.opponentpassingyards2q AS passyrdsallow,
    nflteam.opponentrushingyards2q AS rushyrdsallow,
    nflteam.defensivetd2q AS deftds,
    nflteam.deffantasy2q AS deffantasypts,
    nflteam.opponentpassinginterceptions2q AS vspassint,
    nflteam.sacks2q AS sacks,
    nflteam.opponentsafeties2q AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nflt2q OWNER TO admin;

--
-- TOC entry 198 (class 1259 OID 26748)
-- Name: nflt2qrz; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt2qrz AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score2q AS score,
    nflteam.offensiveplays2q AS offp,
    nflteam.offensiveyards2q AS offyrds,
    nflteam.passingyards2q AS pyrds,
    nflteam.rushingyards2q AS ruyds,
    nflteam.touchdowns2q AS tds,
    nflteam.passingtouchdowns2q AS ptds,
    nflteam.rushingtouchdowns2q AS rtds,
    nflteam.giveaways2q AS gaways,
    nflteam.extrapointpassingconversions2q AS xppassconv,
    nflteam.passingdropbacks2q AS pdropbacks,
    nflteam.passingattempts2q AS patts,
    nflteam.passingcompletions2q AS passc,
    nflteam.passinginterceptions2q AS pinc,
    nflteam.timessacked2q AS tsack,
    nflteam.extrapointrushingconversions2q AS xprushconv,
    nflteam.rushingattempts2q AS ratts,
    nflteam.firstdownsbyrushing2q AS firstdbrush,
    nflteam.firstdownsbypassing2q AS firstdbpass,
    nflteam.extrapointkickingconversions2q AS xpkickconv,
    nflteam.fieldgoalsmade2q AS fgm,
    nflteam.fieldgoalattempts2q AS fga,
    nflteam.extrapointkickingattempts2q AS xpkickatt,
    nflteam.fieldgoalshadblocked2q AS fgblocked,
    nflteam.extrapointshadblocked2q AS xpblocked,
    nflteam.punts2q AS punt,
    nflteam.puntshadblocked2q AS puntblocked,
    nflteam.opponentscore2q AS vsscore,
    nflteam.opponentoffensiveplays2q AS defplay,
    nflteam.opponentoffensiveyards2q AS yrdsallowed,
    nflteam.opponentpassingyards2q AS passyrdsallow,
    nflteam.opponentrushingyards2q AS rushyrdsallow,
    nflteam.defensivetd2q AS deftds,
    nflteam.deffantasy2q AS deffantasypts,
    nflteam.opponentpassinginterceptions2q AS vspassint,
    nflteam.sacks2q AS sacks,
    nflteam.opponentsafeties2q AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nflt2qrz OWNER TO admin;

--
-- TOC entry 200 (class 1259 OID 26758)
-- Name: nflt3q; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt3q AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score3q AS score,
    nflteam.offensiveplays3q AS offp,
    nflteam.offensiveyards3q AS offyrds,
    nflteam.passingyards3q AS pyrds,
    nflteam.rushingyards3q AS ruyds,
    nflteam.touchdowns3q AS tds,
    nflteam.passingtouchdowns3q AS ptds,
    nflteam.rushingtouchdowns3q AS rtds,
    nflteam.giveaways3q AS gaways,
    nflteam.extrapointpassingconversions3q AS xppassconv,
    nflteam.passingdropbacks3q AS pdropbacks,
    nflteam.passingattempts3q AS patts,
    nflteam.passingcompletions3q AS passc,
    nflteam.passinginterceptions3q AS pinc,
    nflteam.timessacked3q AS tsack,
    nflteam.extrapointrushingconversions3q AS xprushconv,
    nflteam.rushingattempts3q AS ratts,
    nflteam.firstdownsbyrushing3q AS firstdbrush,
    nflteam.firstdownsbypassing3q AS firstdbpass,
    nflteam.extrapointkickingconversions3q AS xpkickconv,
    nflteam.fieldgoalsmade3q AS fgm,
    nflteam.fieldgoalattempts3q AS fga,
    nflteam.extrapointkickingattempts3q AS xpkickatt,
    nflteam.fieldgoalshadblocked3q AS fgblocked,
    nflteam.extrapointshadblocked3q AS xpblocked,
    nflteam.punts3q AS punt,
    nflteam.puntshadblocked3q AS puntblocked,
    nflteam.opponentscore3q AS vsscore,
    nflteam.opponentoffensiveplays3q AS defplay,
    nflteam.opponentoffensiveyards3q AS yrdsallowed,
    nflteam.opponentpassingyards3q AS passyrdsallow,
    nflteam.opponentrushingyards3q AS rushyrdsallow,
    nflteam.defensivetd3q AS deftds,
    nflteam.deffantasy3q AS deffantasypts,
    nflteam.opponentpassinginterceptions3q AS vspassint,
    nflteam.sacks3q AS sacks,
    nflteam.opponentsafeties3q AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nflt3q OWNER TO admin;

--
-- TOC entry 201 (class 1259 OID 26763)
-- Name: nflt3qrz; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt3qrz AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score3q AS score,
    nflteam.offensiveplays3q AS offp,
    nflteam.offensiveyards3q AS offyrds,
    nflteam.passingyards3q AS pyrds,
    nflteam.rushingyards3q AS ruyds,
    nflteam.touchdowns3q AS tds,
    nflteam.passingtouchdowns3q AS ptds,
    nflteam.rushingtouchdowns3q AS rtds,
    nflteam.giveaways3q AS gaways,
    nflteam.extrapointpassingconversions3q AS xppassconv,
    nflteam.passingdropbacks3q AS pdropbacks,
    nflteam.passingattempts3q AS patts,
    nflteam.passingcompletions3q AS passc,
    nflteam.passinginterceptions3q AS pinc,
    nflteam.timessacked3q AS tsack,
    nflteam.extrapointrushingconversions3q AS xprushconv,
    nflteam.rushingattempts3q AS ratts,
    nflteam.firstdownsbyrushing3q AS firstdbrush,
    nflteam.firstdownsbypassing3q AS firstdbpass,
    nflteam.extrapointkickingconversions3q AS xpkickconv,
    nflteam.fieldgoalsmade3q AS fgm,
    nflteam.fieldgoalattempts3q AS fga,
    nflteam.extrapointkickingattempts3q AS xpkickatt,
    nflteam.fieldgoalshadblocked3q AS fgblocked,
    nflteam.extrapointshadblocked3q AS xpblocked,
    nflteam.punts3q AS punt,
    nflteam.puntshadblocked3q AS puntblocked,
    nflteam.opponentscore3q AS vsscore,
    nflteam.opponentoffensiveplays3q AS defplay,
    nflteam.opponentoffensiveyards3q AS yrdsallowed,
    nflteam.opponentpassingyards3q AS passyrdsallow,
    nflteam.opponentrushingyards3q AS rushyrdsallow,
    nflteam.defensivetd3q AS deftds,
    nflteam.deffantasy3q AS deffantasypts,
    nflteam.opponentpassinginterceptions3q AS vspassint,
    nflteam.sacks3q AS sacks,
    nflteam.opponentsafeties3q AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nflt3qrz OWNER TO admin;

--
-- TOC entry 203 (class 1259 OID 26773)
-- Name: nflt4q; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt4q AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score4q AS score,
    nflteam.offensiveplays4q AS offp,
    nflteam.offensiveyards4q AS offyrds,
    nflteam.passingyards4q AS pyrds,
    nflteam.rushingyards4q AS ruyds,
    nflteam.touchdowns4q AS tds,
    nflteam.passingtouchdowns4q AS ptds,
    nflteam.rushingtouchdowns4q AS rtds,
    nflteam.giveaways4q AS gaways,
    nflteam.extrapointpassingconversions4q AS xppassconv,
    nflteam.passingdropbacks4q AS pdropbacks,
    nflteam.passingattempts4q AS patts,
    nflteam.passingcompletions4q AS passc,
    nflteam.passinginterceptions4q AS pinc,
    nflteam.timessacked4q AS tsack,
    nflteam.extrapointrushingconversions4q AS xprushconv,
    nflteam.rushingattempts4q AS ratts,
    nflteam.firstdownsbyrushing4q AS firstdbrush,
    nflteam.firstdownsbypassing4q AS firstdbpass,
    nflteam.extrapointkickingconversions4q AS xpkickconv,
    nflteam.fieldgoalsmade4q AS fgm,
    nflteam.fieldgoalattempts4q AS fga,
    nflteam.extrapointkickingattempts4q AS xpkickatt,
    nflteam.fieldgoalshadblocked4q AS fgblocked,
    nflteam.extrapointshadblocked4q AS xpblocked,
    nflteam.punts4q AS punt,
    nflteam.puntshadblocked4q AS puntblocked,
    nflteam.opponentscore4q AS vsscore,
    nflteam.opponentoffensiveplays4q AS defplay,
    nflteam.opponentoffensiveyards4q AS yrdsallowed,
    nflteam.opponentpassingyards4q AS passyrdsallow,
    nflteam.opponentrushingyards4q AS rushyrdsallow,
    nflteam.defensivetd4q AS deftds,
    nflteam.deffantasy4q AS deffantasypts,
    nflteam.opponentpassinginterceptions4q AS vspassint,
    nflteam.sacks4q AS sacks,
    nflteam.opponentsafeties4q AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nflt4q OWNER TO admin;

--
-- TOC entry 202 (class 1259 OID 26768)
-- Name: nflt4qrz; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nflt4qrz AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score4q AS score,
    nflteam.offensiveplays4q AS offp,
    nflteam.offensiveyards4q AS offyrds,
    nflteam.passingyards4q AS pyrds,
    nflteam.rushingyards4q AS ruyds,
    nflteam.touchdowns4q AS tds,
    nflteam.passingtouchdowns4q AS ptds,
    nflteam.rushingtouchdowns4q AS rtds,
    nflteam.giveaways4q AS gaways,
    nflteam.extrapointpassingconversions4q AS xppassconv,
    nflteam.passingdropbacks4q AS pdropbacks,
    nflteam.passingattempts4q AS patts,
    nflteam.passingcompletions4q AS passc,
    nflteam.passinginterceptions4q AS pinc,
    nflteam.timessacked4q AS tsack,
    nflteam.extrapointrushingconversions4q AS xprushconv,
    nflteam.rushingattempts4q AS ratts,
    nflteam.firstdownsbyrushing4q AS firstdbrush,
    nflteam.firstdownsbypassing4q AS firstdbpass,
    nflteam.extrapointkickingconversions4q AS xpkickconv,
    nflteam.fieldgoalsmade4q AS fgm,
    nflteam.fieldgoalattempts4q AS fga,
    nflteam.extrapointkickingattempts4q AS xpkickatt,
    nflteam.fieldgoalshadblocked4q AS fgblocked,
    nflteam.extrapointshadblocked4q AS xpblocked,
    nflteam.punts4q AS punt,
    nflteam.puntshadblocked4q AS puntblocked,
    nflteam.opponentscore4q AS vsscore,
    nflteam.opponentoffensiveplays4q AS defplay,
    nflteam.opponentoffensiveyards4q AS yrdsallowed,
    nflteam.opponentpassingyards4q AS passyrdsallow,
    nflteam.opponentrushingyards4q AS rushyrdsallow,
    nflteam.defensivetd4q AS deftds,
    nflteam.deffantasy4q AS deffantasypts,
    nflteam.opponentpassinginterceptions4q AS vspassint,
    nflteam.sacks4q AS sacks,
    nflteam.opponentsafeties4q AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nflt4qrz OWNER TO admin;

--
-- TOC entry 221 (class 1259 OID 27732)
-- Name: nfltall; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nfltall AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.score,
    nflteam.offensiveplays AS offp,
    nflteam.offensiveyards AS offyrds,
    nflteam.passingyards AS pyrds,
    nflteam.rushingyards AS ruyds,
    nflteam.touchdowns AS tds,
    nflteam.passingtouchdowns AS ptds,
    nflteam.rushingtouchdowns AS rtds,
    nflteam.giveaways AS gaways,
    nflteam.extrapointpassingconversions AS xppassconv,
    nflteam.passingdropbacks AS pdropbacks,
    nflteam.passingattempts AS patts,
    nflteam.passingcompletions AS passc,
    nflteam.passinginterceptions AS pinc,
    nflteam.timessacked AS tsack,
    nflteam.extrapointrushingconversions AS xprushconv,
    nflteam.rushingattempts AS ratts,
    nflteam.firstdownsbyrushing AS firstdbrush,
    nflteam.firstdownsbypassing AS firstdbpass,
    nflteam.extrapointkickingconversions AS xpkickconv,
    nflteam.fieldgoalsmade AS fgm,
    nflteam.fieldgoalattempts AS fga,
    nflteam.extrapointkickingattempts AS xpkickatt,
    nflteam.fieldgoalshadblocked AS fgblocked,
    nflteam.extrapointshadblocked AS xpblocked,
    nflteam.punts AS punt,
    nflteam.puntshadblocked AS puntblocked,
    nflteam.opponentscore AS vsscore,
    nflteam.opponentoffensiveplays AS defplay,
    nflteam.opponentoffensiveyards AS yrdsallowed,
    nflteam.opponentpassingyards AS passyrdsallow,
    nflteam.opponentrushingyards AS rushyrdsallow,
    nflteam.defensivetd AS deftds,
    nflteam.deffantasy AS deffantasypts,
    nflteam.opponentpassinginterceptions AS vspassint,
    nflteam.sacks,
    nflteam.opponentsafeties AS vssafeties
   FROM public.nflteam
  WHERE (nflteam.seasontype = 1)
  WITH NO DATA;


ALTER TABLE public.nfltall OWNER TO admin;

--
-- TOC entry 196 (class 1259 OID 26732)
-- Name: nfltallrz; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nfltallrz AS
 SELECT nflteamrz.fantasypoints AS ffp,
    nflteamrz.games AS game,
    nflteamrz.team,
    nflteamrz.nightgame AS sng,
    nflteamrz.primetstart AS pgs,
    nflteamrz.season AS nflseason,
    nflteamrz.seasontype AS regseason,
    nflteamrz.week AS regweek,
    nflteamrz.newweeks AS seasonweeks,
    nflteamrz.opponent AS vsteam,
    nflteamrz.homeoraway AS location,
    nflteamrz.grassorturf AS playsurface,
    nflteamrz.indooroutdoor AS inoutdoor,
    nflteamrz.codetemp AS ctemp,
    nflteamrz.temprange AS tempwords,
    nflteamrz.codewind AS cwind,
    nflteamrz.wind AS wordswind,
    nflteamrz.codewcond AS cwcond,
    nflteamrz.wconditions,
    nflteamrz.vsheadcoach AS vshcoach,
    nflteamrz.vsoffensivecoordinator AS vsoffcoordinator,
    nflteamrz.vsdefensivecoordinator AS vsdefcoordinator,
    nflteamrz.stadium,
    nflteamrz.score,
    nflteamrz.offensiveplays AS offp,
    nflteamrz.offensiveyards AS offyrds,
    nflteamrz.passingyards AS pyrds,
    nflteamrz.rushingyards AS ruyds,
    nflteamrz.touchdowns AS tds,
    nflteamrz.passingtouchdowns AS ptds,
    nflteamrz.rushingtouchdowns AS rtds,
    nflteamrz.giveaways AS gaways,
    nflteamrz.extrapointpassingconversions AS xppassconv,
    nflteamrz.passingdropbacks AS pdropbacks,
    nflteamrz.passingattempts AS patts,
    nflteamrz.passingcompletions AS passc,
    nflteamrz.passinginterceptions AS pinc,
    nflteamrz.timessacked AS tsack,
    nflteamrz.extrapointrushingconversions AS xprushconv,
    nflteamrz.rushingattempts AS ratts,
    nflteamrz.firstdownsbyrushing AS firstdbrush,
    nflteamrz.firstdownsbypassing AS firstdbpass,
    nflteamrz.extrapointkickingconversions AS xpkickconv,
    nflteamrz.fieldgoalsmade AS fgm,
    nflteamrz.fieldgoalattempts AS fga,
    nflteamrz.extrapointkickingattempts AS xpkickatt,
    nflteamrz.fieldgoalshadblocked AS fgblocked,
    nflteamrz.extrapointshadblocked AS xpblocked,
    nflteamrz.punts AS punt,
    nflteamrz.puntshadblocked AS puntblocked,
    nflteamrz.opponentscore AS vsscore,
    nflteamrz.opponentoffensiveplays AS defplay,
    nflteamrz.opponentoffensiveyards AS yrdsallowed,
    nflteamrz.opponentpassingyards AS passyrdsallow,
    nflteamrz.opponentrushingyards AS rushyrdsallow,
    nflteamrz.defensivetd AS deftds,
    nflteamrz.deffantasy AS deffantasypts,
    nflteamrz.opponentpassinginterceptions AS vspassint,
    nflteamrz.sacks,
    nflteamrz.opponentsafeties AS vssafeties
   FROM public.nflteamrz
  WITH NO DATA;


ALTER TABLE public.nfltallrz OWNER TO admin;

--
-- TOC entry 209 (class 1259 OID 26803)
-- Name: nfltovt; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nfltovt AS
 SELECT nflteam.fantasypoints AS ffp,
    nflteam.games AS game,
    nflteam.team,
    nflteam.nightgame AS sng,
    nflteam.primetstart AS pgs,
    nflteam.season AS nflseason,
    nflteam.seasontype AS regseason,
    nflteam.week AS regweek,
    nflteam.newweeks AS seasonweeks,
    nflteam.opponent AS vsteam,
    nflteam.homeoraway AS location,
    nflteam.grassorturf AS playsurface,
    nflteam.indooroutdoor AS inoutdoor,
    nflteam.codetemp AS ctemp,
    nflteam.temprange AS tempwords,
    nflteam.codewind AS cwind,
    nflteam.wind AS wordswind,
    nflteam.codewcond AS cwcond,
    nflteam.wconditions,
    nflteam.vsheadcoach AS vshcoach,
    nflteam.vsoffensivecoordinator AS vsoffcoordinator,
    nflteam.vsdefensivecoordinator AS vsdefcoordinator,
    nflteam.stadium,
    nflteam.scoreovt AS score,
    nflteam.offensiveplaysovt AS offp,
    nflteam.offensiveyardsovt AS offyrds,
    nflteam.passingyardsovt AS pyrds,
    nflteam.rushingyardsovt AS ruyds,
    nflteam.touchdownsovt AS tds,
    nflteam.passingtouchdownsovt AS ptds,
    nflteam.rushingtouchdownsovt AS rtds,
    nflteam.giveawaysovt AS gaways,
    nflteam.extrapointpassingconversionsovt AS xppassconv,
    nflteam.passingdropbacksovt AS pdropbacks,
    nflteam.passingattemptsovt AS patts,
    nflteam.passingcompletionsovt AS passc,
    nflteam.passinginterceptionsovt AS pinc,
    nflteam.timessackedovt AS tsack,
    nflteam.extrapointrushingconversionsovt AS xprushconv,
    nflteam.rushingattemptsovt AS ratts,
    nflteam.firstdownsbyrushingovt AS firstdbrush,
    nflteam.firstdownsbypassingovt AS firstdbpass,
    nflteam.extrapointkickingconversionsovt AS xpkickconv,
    nflteam.fieldgoalsmadeovt AS fgm,
    nflteam.fieldgoalattemptsovt AS fga,
    nflteam.extrapointkickingattemptsovt AS xpkickatt,
    nflteam.fieldgoalshadblockedovt AS fgblocked,
    nflteam.extrapointshadblockedovt AS xpblocked,
    nflteam.puntsovt AS punt,
    nflteam.puntshadblockedovt AS puntblocked,
    nflteam.opponentscoreovt AS vsscore,
    nflteam.opponentoffensiveplaysovt AS defplay,
    nflteam.opponentoffensiveyardsovt AS yrdsallowed,
    nflteam.opponentpassingyardsovt AS passyrdsallow,
    nflteam.opponentrushingyardsovt AS rushyrdsallow,
    nflteam.defensivetdovt AS deftds,
    nflteam.deffantasyovt AS deffantasypts,
    nflteam.opponentpassinginterceptionsovt AS vspassint,
    nflteam.sacksovt AS sacks,
    nflteam.opponentsafetiesovt AS vssafeties
   FROM public.nflteam
  WITH NO DATA;


ALTER TABLE public.nfltovt OWNER TO admin;

--
-- TOC entry 208 (class 1259 OID 26798)
-- Name: nfltovtrz; Type: MATERIALIZED VIEW; Schema: public; Owner: admin
--

CREATE MATERIALIZED VIEW public.nfltovtrz AS
 SELECT nflteamrz.fantasypoints AS ffp,
    nflteamrz.games AS game,
    nflteamrz.team,
    nflteamrz.nightgame AS sng,
    nflteamrz.primetstart AS pgs,
    nflteamrz.season AS nflseason,
    nflteamrz.seasontype AS regseason,
    nflteamrz.week AS regweek,
    nflteamrz.newweeks AS seasonweeks,
    nflteamrz.opponent AS vsteam,
    nflteamrz.homeoraway AS location,
    nflteamrz.grassorturf AS playsurface,
    nflteamrz.indooroutdoor AS inoutdoor,
    nflteamrz.codetemp AS ctemp,
    nflteamrz.temprange AS tempwords,
    nflteamrz.codewind AS cwind,
    nflteamrz.wind AS wordswind,
    nflteamrz.codewcond AS cwcond,
    nflteamrz.wconditions,
    nflteamrz.vsheadcoach AS vshcoach,
    nflteamrz.vsoffensivecoordinator AS vsoffcoordinator,
    nflteamrz.vsdefensivecoordinator AS vsdefcoordinator,
    nflteamrz.stadium,
    nflteamrz.scoreovt AS score,
    nflteamrz.offensiveplaysovt AS offp,
    nflteamrz.offensiveyardsovt AS offyrds,
    nflteamrz.passingyardsovt AS pyrds,
    nflteamrz.rushingyardsovt AS ruyds,
    nflteamrz.touchdownsovt AS tds,
    nflteamrz.passingtouchdownsovt AS ptds,
    nflteamrz.rushingtouchdownsovt AS rtds,
    nflteamrz.giveawaysovt AS gaways,
    nflteamrz.extrapointpassingconversionsovt AS xppassconv,
    nflteamrz.passingdropbacksovt AS pdropbacks,
    nflteamrz.passingattemptsovt AS patts,
    nflteamrz.passingcompletionsovt AS passc,
    nflteamrz.passinginterceptionsovt AS pinc,
    nflteamrz.timessackedovt AS tsack,
    nflteamrz.extrapointrushingconversionsovt AS xprushconv,
    nflteamrz.rushingattemptsovt AS ratts,
    nflteamrz.firstdownsbyrushingovt AS firstdbrush,
    nflteamrz.firstdownsbypassingovt AS firstdbpass,
    nflteamrz.extrapointkickingconversionsovt AS xpkickconv,
    nflteamrz.fieldgoalsmadeovt AS fgm,
    nflteamrz.fieldgoalattemptsovt AS fga,
    nflteamrz.extrapointkickingattemptsovt AS xpkickatt,
    nflteamrz.fieldgoalshadblockedovt AS fgblocked,
    nflteamrz.extrapointshadblockedovt AS xpblocked,
    nflteamrz.puntsovt AS punt,
    nflteamrz.puntshadblockedovt AS puntblocked,
    nflteamrz.opponentscoreovt AS vsscore,
    nflteamrz.opponentoffensiveplaysovt AS defplay,
    nflteamrz.opponentoffensiveyardsovt AS yrdsallowed,
    nflteamrz.opponentpassingyardsovt AS passyrdsallow,
    nflteamrz.opponentrushingyardsovt AS rushyrdsallow,
    nflteamrz.defensivetdovt AS deftds,
    nflteamrz.deffantasyovt AS deffantasypts,
    nflteamrz.opponentpassinginterceptionsovt AS vspassint,
    nflteamrz.sacksovt AS sacks,
    nflteamrz.opponentsafetiesovt AS vssafeties
   FROM public.nflteamrz
  WITH NO DATA;


ALTER TABLE public.nfltovtrz OWNER TO admin;

--
-- TOC entry 188 (class 1259 OID 20934)
-- Name: playertest; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.playertest (
    season character(4),
    mmonth integer,
    dday integer,
    playername character varying(50),
    team character(3)
);


ALTER TABLE public.playertest OWNER TO admin;

--
-- TOC entry 222 (class 1259 OID 30363)
-- Name: playerupdate; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.playerupdate (
    "Pos" character(2),
    player character varying(40)
);


ALTER TABLE public.playerupdate OWNER TO admin;

--
-- TOC entry 216 (class 1259 OID 26979)
-- Name: premiumscoring; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.premiumscoring (
    premuserid bigint NOT NULL,
    scoringname character varying(20),
    pfpyd character(6),
    pfruy character(6),
    pfrec character(6),
    pfryd character(6),
    pfint character(6),
    pftds character(6),
    premiumfullsql character varying(80),
    premiumscoreid bigint NOT NULL
);


ALTER TABLE public.premiumscoring OWNER TO admin;

--
-- TOC entry 215 (class 1259 OID 26977)
-- Name: premiumscoring_premiumscoreid_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.premiumscoring_premiumscoreid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.premiumscoring_premiumscoreid_seq OWNER TO admin;

--
-- TOC entry 3376 (class 0 OID 0)
-- Dependencies: 215
-- Name: premiumscoring_premiumscoreid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.premiumscoring_premiumscoreid_seq OWNED BY public.premiumscoring.premiumscoreid;


--
-- TOC entry 217 (class 1259 OID 27160)
-- Name: season; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.season (
    thisweek boolean,
    season character(4),
    seasoninteger integer,
    weeknumber character(2),
    weeknumberinteger smallint,
    last2weekssql character varying(10),
    last4weekssql character varying(20),
    last8weekssql character varying(40),
    last12weekssql character varying(60),
    weekbackinteger01 smallint,
    weekbackinteger02 smallint,
    weekbackinteger03 smallint,
    weekbackinteger04 smallint,
    weekbackinteger05 smallint,
    weekbackinteger06 smallint,
    weekbackinteger07 smallint,
    weekbackinteger08 smallint,
    weekbackinteger09 smallint,
    weekbackinteger10 smallint,
    weekbackinteger11 smallint,
    weekbackinteger12 smallint
);


ALTER TABLE public.season OWNER TO admin;

--
-- TOC entry 192 (class 1259 OID 24704)
-- Name: srusers; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.srusers (
    userid integer NOT NULL,
    firstname character varying(25) NOT NULL,
    lastname character varying(25) NOT NULL,
    email character varying(50) NOT NULL,
    username character varying(25) NOT NULL,
    pswhash character varying(128),
    zip character(5),
    gender character(1),
    promocode character varying(15),
    urlsignup character varying(15),
    bmonth character(2),
    bday character(2),
    newsletter boolean,
    datefirstsignup date,
    accounttype integer,
    status character varying(15),
    currentperiodstart date,
    currentperiodend date,
    freezedate date,
    endeddate date,
    totalamountspent real,
    trialstart date,
    trialend date,
    refundcost real,
    refunddate date,
    accountnumber character varying(25)
);


ALTER TABLE public.srusers OWNER TO admin;

--
-- TOC entry 191 (class 1259 OID 24702)
-- Name: srusers_userid_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.srusers_userid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.srusers_userid_seq OWNER TO admin;

--
-- TOC entry 3379 (class 0 OID 0)
-- Dependencies: 191
-- Name: srusers_userid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.srusers_userid_seq OWNED BY public.srusers.userid;


--
-- TOC entry 212 (class 1259 OID 26891)
-- Name: stadiums; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.stadiums (
    stadiumkey integer NOT NULL,
    stadiumname character varying(50) NOT NULL,
    zipcode character(5) NOT NULL,
    datefrom date NOT NULL,
    dateto date,
    hometeam character(3) NOT NULL
);


ALTER TABLE public.stadiums OWNER TO admin;

--
-- TOC entry 211 (class 1259 OID 26889)
-- Name: stadiums_stadiumkey_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.stadiums_stadiumkey_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stadiums_stadiumkey_seq OWNER TO admin;

--
-- TOC entry 3381 (class 0 OID 0)
-- Dependencies: 211
-- Name: stadiums_stadiumkey_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.stadiums_stadiumkey_seq OWNED BY public.stadiums.stadiumkey;


--
-- TOC entry 228 (class 1259 OID 33151)
-- Name: stador; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.stador (
    teamkey character(3),
    stadiumorientation integer
);


ALTER TABLE public.stador OWNER TO admin;

--
-- TOC entry 220 (class 1259 OID 27613)
-- Name: teammarketing; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.teammarketing (
    teamkey character(3),
    primarycolor character varying(10),
    secondary character varying(10),
    fullteamname character varying(50)
);


ALTER TABLE public.teammarketing OWNER TO admin;

--
-- TOC entry 218 (class 1259 OID 27252)
-- Name: updatecurrentplayer; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.updatecurrentplayer (
    teambad character(3),
    pnumber character(2),
    player character varying(50),
    pposition character(2),
    age smallint,
    height character(10),
    weight integer,
    experience character(2),
    college character varying(30),
    cteam character(3)
);


ALTER TABLE public.updatecurrentplayer OWNER TO admin;

--
-- TOC entry 213 (class 1259 OID 26922)
-- Name: weathertable; Type: TABLE; Schema: public; Owner: cblanton
--

CREATE TABLE public.weathertable (
    week smallint NOT NULL,
    zip character(5) NOT NULL,
    dateofgame date,
    dayofgame public.weekday,
    weatherkey character(16) NOT NULL,
    kickoff character(5),
    ampm character(2),
    hometeam character varying(50),
    hometeamkey character(3) NOT NULL,
    awayteam character varying(50),
    awayteamkey character(3),
    startofweather character(5),
    starthour character(2),
    startminute character(2),
    timeincrement character(5),
    stadiumname character varying(50),
    stadiumkey integer,
    displayweek boolean,
    thisweek boolean,
    weatherupdated boolean,
    temperature character varying(10),
    tempcode1 character(1),
    tempcode2 character(1),
    windspeed character varying(10),
    winddirection character varying(5),
    wconditions character varying(50),
    nightorday character varying(10),
    graphic character varying(10),
    windc1 character(1),
    windc2 character(1),
    wcond1 character(1),
    wcond2 character(1),
    chanceofpercipitation character varying(5),
    humidity character varying(5),
    kickoffrow boolean DEFAULT false,
    timevalue smallint,
    stringdateofgame character(10),
    stadiumgraphic character varying(50) DEFAULT 'err'::character varying NOT NULL,
    stadiumorientation integer,
    lat character(7),
    long character(7),
    elevation character(5),
    wrcity character varying,
    wrstate character varying(3)
);


ALTER TABLE public.weathertable OWNER TO cblanton;

-- StatRoute Team Grid Display and new Signup/Subscribe
create table public.teammaster as select distinct team,teamname,fullteamname from nflplayer;
CREATE TABLE public.subscription
(
    firstname character varying(25) NOT NULL,
    lastname character varying(25) COLLATE NOT NULL,
    email character varying(50) NOT NULL,
    device character varying(128),
    ip character varying(50),
    location character varying(50),
    created_at date NOT NULL
)
--
-- TOC entry 3125 (class 2604 OID 26982)
-- Name: premiumscoring premiumscoreid; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.premiumscoring ALTER COLUMN premiumscoreid SET DEFAULT nextval('public.premiumscoring_premiumscoreid_seq'::regclass);


--
-- TOC entry 3086 (class 2604 OID 24707)
-- Name: srusers userid; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.srusers ALTER COLUMN userid SET DEFAULT nextval('public.srusers_userid_seq'::regclass);


--
-- TOC entry 3122 (class 2604 OID 26894)
-- Name: stadiums stadiumkey; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.stadiums ALTER COLUMN stadiumkey SET DEFAULT nextval('public.stadiums_stadiumkey_seq'::regclass);


--
-- TOC entry 3141 (class 2606 OID 26984)
-- Name: premiumscoring pkscoringid; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.premiumscoring
    ADD CONSTRAINT pkscoringid PRIMARY KEY (premiumscoreid);


--
-- TOC entry 3130 (class 2606 OID 26855)
-- Name: srusers premuserids; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.srusers
    ADD CONSTRAINT premuserids UNIQUE (userid);


--
-- TOC entry 3132 (class 2606 OID 24709)
-- Name: srusers srusers_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.srusers
    ADD CONSTRAINT srusers_pkey PRIMARY KEY (userid, email, firstname, lastname, username);


--
-- TOC entry 3139 (class 2606 OID 26896)
-- Name: stadiums stadiumkeys; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.stadiums
    ADD CONSTRAINT stadiumkeys PRIMARY KEY (stadiumkey);


--
-- TOC entry 3126 (class 1259 OID 27673)
-- Name: nflpgdtp; Type: INDEX; Schema: public; Owner: cblanton
--

CREATE INDEX nflpgdtp ON public.nflplayer USING btree (gamedateteamplayer);


--
-- TOC entry 3127 (class 1259 OID 27675)
-- Name: nflpgdts; Type: INDEX; Schema: public; Owner: cblanton
--

CREATE INDEX nflpgdts ON public.nflplayer USING btree (gamedateteamshortname);


--
-- TOC entry 3133 (class 1259 OID 27674)
-- Name: nflprzgdtp; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX nflprzgdtp ON public.nflprz USING btree (gamedateteamplayer);


--
-- TOC entry 3134 (class 1259 OID 27676)
-- Name: nflprzgdts; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX nflprzgdts ON public.nflprz USING btree (gamedateteamshortname);


--
-- TOC entry 3142 (class 1259 OID 31411)
-- Name: nflpsts2qhelp; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX nflpsts2qhelp ON public.nflpsts2q USING btree (player, tpposition, nflseason, seasonweeks, location, playsurface, ctemp, regseason, regweek, vshcoach, vsoffcoor, vsdefcood, vsteam);


--
-- TOC entry 3143 (class 1259 OID 31417)
-- Name: nflpsts3qhelp; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX nflpsts3qhelp ON public.nflpsts3q USING btree (player, tpposition, nflseason, seasonweeks, location, playsurface, ctemp, regseason, regweek, vshcoach, vsoffcoor, vsdefcood, vsteam);


--
-- TOC entry 3144 (class 1259 OID 31453)
-- Name: nflpstsovthelp; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX nflpstsovthelp ON public.nflpstsovt USING btree (player, tpposition, nflseason, seasonweeks, location, playsurface, ctemp, regseason, regweek, vshcoach, vsoffcoor, vsdefcood, vsteam);


--
-- TOC entry 3135 (class 1259 OID 27651)
-- Name: rzgdt; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX rzgdt ON public.nflprz USING btree (gamedateteam);


--
-- TOC entry 3128 (class 1259 OID 27648)
-- Name: teamgdtp; Type: INDEX; Schema: public; Owner: cblanton
--

CREATE INDEX teamgdtp ON public.nflplayer USING btree (gamedateteam);


--
-- TOC entry 3137 (class 1259 OID 27650)
-- Name: teamgdtrz; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX teamgdtrz ON public.nflteamrz USING btree (gamedateteam);


--
-- TOC entry 3136 (class 1259 OID 27649)
-- Name: teamgdtt; Type: INDEX; Schema: public; Owner: cblanton
--

CREATE INDEX teamgdtt ON public.nflteam USING btree (gamedateteam);


--
-- TOC entry 3145 (class 2606 OID 27270)
-- Name: forgotten forgottenuser; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.forgotten
    ADD CONSTRAINT forgottenuser FOREIGN KEY (userid) REFERENCES public.srusers(userid);


--
-- TOC entry 3316 (class 0 OID 0)
-- Dependencies: 259
-- Name: FUNCTION compose_session_replication_role(role text); Type: ACL; Schema: public; Owner: focker
--

REVOKE ALL ON FUNCTION public.compose_session_replication_role(role text) FROM PUBLIC;
GRANT ALL ON FUNCTION public.compose_session_replication_role(role text) TO admin;


--
-- TOC entry 3317 (class 0 OID 0)
-- Dependencies: 267
-- Name: FUNCTION kill_all_connections(); Type: ACL; Schema: public; Owner: focker
--

REVOKE ALL ON FUNCTION public.kill_all_connections() FROM PUBLIC;
GRANT ALL ON FUNCTION public.kill_all_connections() TO admin;


--
-- TOC entry 3318 (class 0 OID 0)
-- Dependencies: 284
-- Name: FUNCTION upgrade_postgis_23x(); Type: ACL; Schema: public; Owner: focker
--

REVOKE ALL ON FUNCTION public.upgrade_postgis_23x() FROM PUBLIC;
GRANT ALL ON FUNCTION public.upgrade_postgis_23x() TO admin;


--
-- TOC entry 3319 (class 0 OID 0)
-- Dependencies: 214
-- Name: TABLE allplayerlookup; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.allplayerlookup TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.allplayerlookup TO user1;
GRANT ALL ON TABLE public.allplayerlookup TO user2;
GRANT ALL ON TABLE public.allplayerlookup TO user3;
GRANT ALL ON TABLE public.allplayerlookup TO user4;


--
-- TOC entry 3320 (class 0 OID 0)
-- Dependencies: 190
-- Name: TABLE depthchart; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.depthchart TO readonly;
GRANT ALL ON TABLE public.depthchart TO cblanton;
GRANT ALL ON TABLE public.depthchart TO johnathan;
GRANT ALL ON TABLE public.depthchart TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.depthchart TO user1;
GRANT ALL ON TABLE public.depthchart TO user2;
GRANT ALL ON TABLE public.depthchart TO user3;
GRANT ALL ON TABLE public.depthchart TO user4;


--
-- TOC entry 3321 (class 0 OID 0)
-- Dependencies: 224
-- Name: TABLE fffteam; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.fffteam TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.fffteam TO user1;
GRANT ALL ON TABLE public.fffteam TO user2;
GRANT ALL ON TABLE public.fffteam TO user3;
GRANT ALL ON TABLE public.fffteam TO user4;


--
-- TOC entry 3322 (class 0 OID 0)
-- Dependencies: 189
-- Name: TABLE ffplayers2017; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.ffplayers2017 TO cblanton;
GRANT SELECT ON TABLE public.ffplayers2017 TO readonly;
GRANT ALL ON TABLE public.ffplayers2017 TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.ffplayers2017 TO user1;
GRANT ALL ON TABLE public.ffplayers2017 TO user2;
GRANT ALL ON TABLE public.ffplayers2017 TO user3;
GRANT ALL ON TABLE public.ffplayers2017 TO user4;


--
-- TOC entry 3323 (class 0 OID 0)
-- Dependencies: 219
-- Name: TABLE forgotten; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.forgotten TO readonly;
GRANT ALL ON TABLE public.forgotten TO cblanton;
GRANT ALL ON TABLE public.forgotten TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.forgotten TO user1;
GRANT ALL ON TABLE public.forgotten TO user2;
GRANT ALL ON TABLE public.forgotten TO user3;
GRANT ALL ON TABLE public.forgotten TO user4;


--
-- TOC entry 3324 (class 0 OID 0)
-- Dependencies: 223
-- Name: TABLE fvs; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.fvs TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.fvs TO user1;
GRANT ALL ON TABLE public.fvs TO user2;
GRANT ALL ON TABLE public.fvs TO user3;
GRANT ALL ON TABLE public.fvs TO user4;


--
-- TOC entry 3325 (class 0 OID 0)
-- Dependencies: 229
-- Name: TABLE newnew; Type: ACL; Schema: public; Owner: cblanton
--

GRANT ALL ON TABLE public.newnew TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.newnew TO user1;
GRANT ALL ON TABLE public.newnew TO user2;
GRANT ALL ON TABLE public.newnew TO user3;
GRANT ALL ON TABLE public.newnew TO user4;


--
-- TOC entry 3326 (class 0 OID 0)
-- Dependencies: 187
-- Name: TABLE nflplayer; Type: ACL; Schema: public; Owner: cblanton
--

GRANT ALL ON TABLE public.nflplayer TO johnathan;
GRANT SELECT ON TABLE public.nflplayer TO read2;
GRANT ALL ON TABLE public.nflplayer TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflplayer TO user1;
GRANT ALL ON TABLE public.nflplayer TO user2;
GRANT ALL ON TABLE public.nflplayer TO user3;
GRANT ALL ON TABLE public.nflplayer TO user4;


--
-- TOC entry 3327 (class 0 OID 0)
-- Dependencies: 230
-- Name: TABLE nflpall; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpall TO jeff;
GRANT ALL ON TABLE public.nflpall TO cblanton;
GRANT SELECT ON TABLE public.nflpall TO readonly;
GRANT SELECT ON TABLE public.nflpall TO read2;
GRANT ALL ON TABLE public.nflpall TO user1;
GRANT ALL ON TABLE public.nflpall TO user2;
GRANT ALL ON TABLE public.nflpall TO user3;
GRANT ALL ON TABLE public.nflpall TO user4;


--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 234
-- Name: TABLE nflpall1hsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpall1hsplits2 TO cblanton;
GRANT SELECT ON TABLE public.nflpall1hsplits2 TO readonly;
GRANT ALL ON TABLE public.nflpall1hsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflpall1hsplits2 TO read2;
GRANT ALL ON TABLE public.nflpall1hsplits2 TO user1;
GRANT ALL ON TABLE public.nflpall1hsplits2 TO user2;
GRANT ALL ON TABLE public.nflpall1hsplits2 TO user3;
GRANT ALL ON TABLE public.nflpall1hsplits2 TO user4;


--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 233
-- Name: TABLE nflpall1qsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpall1qsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflpall1qsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflpall1qsplits2 TO read2;
GRANT SELECT ON TABLE public.nflpall1qsplits2 TO readonly;
GRANT ALL ON TABLE public.nflpall1qsplits2 TO user1;
GRANT ALL ON TABLE public.nflpall1qsplits2 TO user2;
GRANT ALL ON TABLE public.nflpall1qsplits2 TO user3;
GRANT ALL ON TABLE public.nflpall1qsplits2 TO user4;


--
-- TOC entry 3330 (class 0 OID 0)
-- Dependencies: 235
-- Name: TABLE nflpall2hsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpall2hsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflpall2hsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflpall2hsplits2 TO read2;
GRANT SELECT ON TABLE public.nflpall2hsplits2 TO readonly;
GRANT ALL ON TABLE public.nflpall2hsplits2 TO user1;
GRANT ALL ON TABLE public.nflpall2hsplits2 TO user2;
GRANT ALL ON TABLE public.nflpall2hsplits2 TO user3;
GRANT ALL ON TABLE public.nflpall2hsplits2 TO user4;


--
-- TOC entry 3331 (class 0 OID 0)
-- Dependencies: 236
-- Name: TABLE nflpall2qsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpall2qsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflpall2qsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflpall2qsplits2 TO read2;
GRANT SELECT ON TABLE public.nflpall2qsplits2 TO readonly;
GRANT ALL ON TABLE public.nflpall2qsplits2 TO user1;
GRANT ALL ON TABLE public.nflpall2qsplits2 TO user2;
GRANT ALL ON TABLE public.nflpall2qsplits2 TO user3;
GRANT ALL ON TABLE public.nflpall2qsplits2 TO user4;


--
-- TOC entry 3332 (class 0 OID 0)
-- Dependencies: 237
-- Name: TABLE nflpall3qsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpall3qsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflpall3qsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflpall3qsplits2 TO read2;
GRANT SELECT ON TABLE public.nflpall3qsplits2 TO readonly;
GRANT ALL ON TABLE public.nflpall3qsplits2 TO user1;
GRANT ALL ON TABLE public.nflpall3qsplits2 TO user2;
GRANT ALL ON TABLE public.nflpall3qsplits2 TO user3;
GRANT ALL ON TABLE public.nflpall3qsplits2 TO user4;


--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE nflpall4qsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpall4qsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflpall4qsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflpall4qsplits2 TO read2;
GRANT SELECT ON TABLE public.nflpall4qsplits2 TO readonly;
GRANT ALL ON TABLE public.nflpall4qsplits2 TO user1;
GRANT ALL ON TABLE public.nflpall4qsplits2 TO user2;
GRANT ALL ON TABLE public.nflpall4qsplits2 TO user3;
GRANT ALL ON TABLE public.nflpall4qsplits2 TO user4;


--
-- TOC entry 3334 (class 0 OID 0)
-- Dependencies: 239
-- Name: TABLE nflpallovtsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpallovtsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflpallovtsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflpallovtsplits2 TO read2;
GRANT SELECT ON TABLE public.nflpallovtsplits2 TO readonly;
GRANT ALL ON TABLE public.nflpallovtsplits2 TO user1;
GRANT ALL ON TABLE public.nflpallovtsplits2 TO user2;
GRANT ALL ON TABLE public.nflpallovtsplits2 TO user3;
GRANT ALL ON TABLE public.nflpallovtsplits2 TO user4;


--
-- TOC entry 3335 (class 0 OID 0)
-- Dependencies: 231
-- Name: TABLE nflpallsplits; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpallsplits TO cblanton;
GRANT ALL ON TABLE public.nflpallsplits TO jeff;
GRANT SELECT ON TABLE public.nflpallsplits TO read2;
GRANT SELECT ON TABLE public.nflpallsplits TO readonly;
GRANT ALL ON TABLE public.nflpallsplits TO user1;
GRANT ALL ON TABLE public.nflpallsplits TO user2;
GRANT ALL ON TABLE public.nflpallsplits TO user3;
GRANT ALL ON TABLE public.nflpallsplits TO user4;


--
-- TOC entry 3336 (class 0 OID 0)
-- Dependencies: 240
-- Name: TABLE nflpallsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.nflpallsplits2 TO read2;
GRANT ALL ON TABLE public.nflpallsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflpallsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflpallsplits2 TO readonly;
GRANT ALL ON TABLE public.nflpallsplits2 TO user1;
GRANT ALL ON TABLE public.nflpallsplits2 TO user2;
GRANT ALL ON TABLE public.nflpallsplits2 TO user3;
GRANT ALL ON TABLE public.nflpallsplits2 TO user4;


--
-- TOC entry 3337 (class 0 OID 0)
-- Dependencies: 232
-- Name: TABLE nflpalltsndefault; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpalltsndefault TO cblanton;
GRANT ALL ON TABLE public.nflpalltsndefault TO jeff;
GRANT SELECT ON TABLE public.nflpalltsndefault TO readonly;
GRANT SELECT ON TABLE public.nflpalltsndefault TO read2;
GRANT ALL ON TABLE public.nflpalltsndefault TO user1;
GRANT ALL ON TABLE public.nflpalltsndefault TO user2;
GRANT ALL ON TABLE public.nflpalltsndefault TO user3;
GRANT ALL ON TABLE public.nflpalltsndefault TO user4;


--
-- TOC entry 3338 (class 0 OID 0)
-- Dependencies: 193
-- Name: TABLE nflprz; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflprz TO johnathan;
GRANT ALL ON TABLE public.nflprz TO cblanton;
GRANT SELECT ON TABLE public.nflprz TO read2;
GRANT ALL ON TABLE public.nflprz TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflprz TO user1;
GRANT ALL ON TABLE public.nflprz TO user2;
GRANT ALL ON TABLE public.nflprz TO user3;
GRANT ALL ON TABLE public.nflprz TO user4;


--
-- TOC entry 3339 (class 0 OID 0)
-- Dependencies: 241
-- Name: TABLE nflprz1hsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflprz1hsplits2 TO jeff;
GRANT ALL ON TABLE public.nflprz1hsplits2 TO cblanton;
GRANT SELECT ON TABLE public.nflprz1hsplits2 TO read2;
GRANT SELECT ON TABLE public.nflprz1hsplits2 TO readonly;
GRANT ALL ON TABLE public.nflprz1hsplits2 TO user1;
GRANT ALL ON TABLE public.nflprz1hsplits2 TO user2;
GRANT ALL ON TABLE public.nflprz1hsplits2 TO user3;
GRANT ALL ON TABLE public.nflprz1hsplits2 TO user4;


--
-- TOC entry 3340 (class 0 OID 0)
-- Dependencies: 242
-- Name: TABLE nflprz1qsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflprz1qsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflprz1qsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflprz1qsplits2 TO read2;
GRANT SELECT ON TABLE public.nflprz1qsplits2 TO readonly;
GRANT ALL ON TABLE public.nflprz1qsplits2 TO user1;
GRANT ALL ON TABLE public.nflprz1qsplits2 TO user2;
GRANT ALL ON TABLE public.nflprz1qsplits2 TO user3;
GRANT ALL ON TABLE public.nflprz1qsplits2 TO user4;


--
-- TOC entry 3341 (class 0 OID 0)
-- Dependencies: 243
-- Name: TABLE nflprz2hsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.nflprz2hsplits2 TO readonly;
GRANT SELECT ON TABLE public.nflprz2hsplits2 TO read2;
GRANT ALL ON TABLE public.nflprz2hsplits2 TO jeff;
GRANT ALL ON TABLE public.nflprz2hsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflprz2hsplits2 TO user1;
GRANT ALL ON TABLE public.nflprz2hsplits2 TO user2;
GRANT ALL ON TABLE public.nflprz2hsplits2 TO user3;
GRANT ALL ON TABLE public.nflprz2hsplits2 TO user4;


--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 245
-- Name: TABLE nflprz2qsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.nflprz2qsplits2 TO read2;
GRANT SELECT ON TABLE public.nflprz2qsplits2 TO readonly;
GRANT ALL ON TABLE public.nflprz2qsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflprz2qsplits2 TO jeff;
GRANT ALL ON TABLE public.nflprz2qsplits2 TO user1;
GRANT ALL ON TABLE public.nflprz2qsplits2 TO user2;
GRANT ALL ON TABLE public.nflprz2qsplits2 TO user3;
GRANT ALL ON TABLE public.nflprz2qsplits2 TO user4;


--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 244
-- Name: TABLE nflprz3qsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflprz3qsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflprz3qsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflprz3qsplits2 TO read2;
GRANT SELECT ON TABLE public.nflprz3qsplits2 TO readonly;
GRANT ALL ON TABLE public.nflprz3qsplits2 TO user1;
GRANT ALL ON TABLE public.nflprz3qsplits2 TO user2;
GRANT ALL ON TABLE public.nflprz3qsplits2 TO user3;
GRANT ALL ON TABLE public.nflprz3qsplits2 TO user4;


--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 246
-- Name: TABLE nflprz4qsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.nflprz4qsplits2 TO read2;
GRANT SELECT ON TABLE public.nflprz4qsplits2 TO readonly;
GRANT ALL ON TABLE public.nflprz4qsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflprz4qsplits2 TO jeff;
GRANT ALL ON TABLE public.nflprz4qsplits2 TO user1;
GRANT ALL ON TABLE public.nflprz4qsplits2 TO user2;
GRANT ALL ON TABLE public.nflprz4qsplits2 TO user3;
GRANT ALL ON TABLE public.nflprz4qsplits2 TO user4;


--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 253
-- Name: TABLE nflprzallsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflprzallsplits2 TO user1;
GRANT ALL ON TABLE public.nflprzallsplits2 TO user2;
GRANT ALL ON TABLE public.nflprzallsplits2 TO user3;
GRANT ALL ON TABLE public.nflprzallsplits2 TO user4;


--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 247
-- Name: TABLE nflprzovtsplits2; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflprzovtsplits2 TO cblanton;
GRANT ALL ON TABLE public.nflprzovtsplits2 TO jeff;
GRANT SELECT ON TABLE public.nflprzovtsplits2 TO readonly;
GRANT SELECT ON TABLE public.nflprzovtsplits2 TO read2;
GRANT ALL ON TABLE public.nflprzovtsplits2 TO user1;
GRANT ALL ON TABLE public.nflprzovtsplits2 TO user2;
GRANT ALL ON TABLE public.nflprzovtsplits2 TO user3;
GRANT ALL ON TABLE public.nflprzovtsplits2 TO user4;


--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 248
-- Name: TABLE nflpsts; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpsts TO jeff;
GRANT ALL ON TABLE public.nflpsts TO cblanton;
GRANT SELECT ON TABLE public.nflpsts TO read2;
GRANT SELECT ON TABLE public.nflpsts TO readonly;
GRANT ALL ON TABLE public.nflpsts TO user1;
GRANT ALL ON TABLE public.nflpsts TO user2;
GRANT ALL ON TABLE public.nflpsts TO user3;
GRANT ALL ON TABLE public.nflpsts TO user4;


--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 249
-- Name: TABLE nflpsts1h; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.nflpsts1h TO readonly;
GRANT SELECT ON TABLE public.nflpsts1h TO read2;
GRANT ALL ON TABLE public.nflpsts1h TO jeff;
GRANT ALL ON TABLE public.nflpsts1h TO cblanton;
GRANT ALL ON TABLE public.nflpsts1h TO user1;
GRANT ALL ON TABLE public.nflpsts1h TO user2;
GRANT ALL ON TABLE public.nflpsts1h TO user3;
GRANT ALL ON TABLE public.nflpsts1h TO user4;


--
-- TOC entry 3349 (class 0 OID 0)
-- Dependencies: 250
-- Name: TABLE nflpsts1q; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.nflpsts1q TO readonly;
GRANT SELECT ON TABLE public.nflpsts1q TO read2;
GRANT ALL ON TABLE public.nflpsts1q TO jeff;
GRANT ALL ON TABLE public.nflpsts1q TO cblanton;
GRANT ALL ON TABLE public.nflpsts1q TO user1;
GRANT ALL ON TABLE public.nflpsts1q TO user2;
GRANT ALL ON TABLE public.nflpsts1q TO user3;
GRANT ALL ON TABLE public.nflpsts1q TO user4;


--
-- TOC entry 3350 (class 0 OID 0)
-- Dependencies: 251
-- Name: TABLE nflpsts2h; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflpsts2h TO cblanton;
GRANT ALL ON TABLE public.nflpsts2h TO read2;
GRANT SELECT ON TABLE public.nflpsts2h TO jeff;
GRANT SELECT ON TABLE public.nflpsts2h TO readonly;
GRANT ALL ON TABLE public.nflpsts2h TO user1;
GRANT ALL ON TABLE public.nflpsts2h TO user2;
GRANT ALL ON TABLE public.nflpsts2h TO user3;
GRANT ALL ON TABLE public.nflpsts2h TO user4;


--
-- TOC entry 3351 (class 0 OID 0)
-- Dependencies: 225
-- Name: TABLE nflpsts2q; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.nflpsts2q TO readonly;
GRANT ALL ON TABLE public.nflpsts2q TO cblanton;
GRANT SELECT ON TABLE public.nflpsts2q TO read2;
GRANT ALL ON TABLE public.nflpsts2q TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflpsts2q TO user1;
GRANT ALL ON TABLE public.nflpsts2q TO user2;
GRANT ALL ON TABLE public.nflpsts2q TO user3;
GRANT ALL ON TABLE public.nflpsts2q TO user4;


--
-- TOC entry 3352 (class 0 OID 0)
-- Dependencies: 226
-- Name: TABLE nflpsts3q; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.nflpsts3q TO readonly;
GRANT ALL ON TABLE public.nflpsts3q TO cblanton;
GRANT SELECT ON TABLE public.nflpsts3q TO read2;
GRANT ALL ON TABLE public.nflpsts3q TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflpsts3q TO user1;
GRANT ALL ON TABLE public.nflpsts3q TO user2;
GRANT ALL ON TABLE public.nflpsts3q TO user3;
GRANT ALL ON TABLE public.nflpsts3q TO user4;


--
-- TOC entry 3353 (class 0 OID 0)
-- Dependencies: 252
-- Name: TABLE nflpsts4q; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.nflpsts4q TO readonly;
GRANT ALL ON TABLE public.nflpsts4q TO cblanton;
GRANT ALL ON TABLE public.nflpsts4q TO jeff;
GRANT SELECT ON TABLE public.nflpsts4q TO read2;
GRANT ALL ON TABLE public.nflpsts4q TO user1;
GRANT ALL ON TABLE public.nflpsts4q TO user2;
GRANT ALL ON TABLE public.nflpsts4q TO user3;
GRANT ALL ON TABLE public.nflpsts4q TO user4;


--
-- TOC entry 3354 (class 0 OID 0)
-- Dependencies: 227
-- Name: TABLE nflpstsovt; Type: ACL; Schema: public; Owner: admin
--

GRANT SELECT ON TABLE public.nflpstsovt TO readonly;
GRANT ALL ON TABLE public.nflpstsovt TO cblanton;
GRANT SELECT ON TABLE public.nflpstsovt TO read2;
GRANT ALL ON TABLE public.nflpstsovt TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflpstsovt TO user1;
GRANT ALL ON TABLE public.nflpstsovt TO user2;
GRANT ALL ON TABLE public.nflpstsovt TO user3;
GRANT ALL ON TABLE public.nflpstsovt TO user4;


--
-- TOC entry 3355 (class 0 OID 0)
-- Dependencies: 194
-- Name: TABLE nflteam; Type: ACL; Schema: public; Owner: cblanton
--

GRANT SELECT ON TABLE public.nflteam TO readonly;
GRANT SELECT ON TABLE public.nflteam TO read2;
GRANT ALL ON TABLE public.nflteam TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflteam TO user1;
GRANT ALL ON TABLE public.nflteam TO user2;
GRANT ALL ON TABLE public.nflteam TO user3;
GRANT ALL ON TABLE public.nflteam TO user4;


--
-- TOC entry 3356 (class 0 OID 0)
-- Dependencies: 205
-- Name: TABLE nflt1h; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt1h TO johnathan;
GRANT ALL ON TABLE public.nflt1h TO cblanton;
GRANT SELECT ON TABLE public.nflt1h TO readonly;
GRANT SELECT ON TABLE public.nflt1h TO read2;
GRANT ALL ON TABLE public.nflt1h TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt1h TO user1;
GRANT ALL ON TABLE public.nflt1h TO user2;
GRANT ALL ON TABLE public.nflt1h TO user3;
GRANT ALL ON TABLE public.nflt1h TO user4;


--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 204
-- Name: TABLE nflt1hrz; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt1hrz TO johnathan;
GRANT ALL ON TABLE public.nflt1hrz TO cblanton;
GRANT SELECT ON TABLE public.nflt1hrz TO readonly;
GRANT SELECT ON TABLE public.nflt1hrz TO read2;
GRANT ALL ON TABLE public.nflt1hrz TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt1hrz TO user1;
GRANT ALL ON TABLE public.nflt1hrz TO user2;
GRANT ALL ON TABLE public.nflt1hrz TO user3;
GRANT ALL ON TABLE public.nflt1hrz TO user4;


--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 197
-- Name: TABLE nflt1q; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt1q TO johnathan;
GRANT ALL ON TABLE public.nflt1q TO cblanton;
GRANT SELECT ON TABLE public.nflt1q TO readonly;
GRANT SELECT ON TABLE public.nflt1q TO read2;
GRANT ALL ON TABLE public.nflt1q TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt1q TO user1;
GRANT ALL ON TABLE public.nflt1q TO user2;
GRANT ALL ON TABLE public.nflt1q TO user3;
GRANT ALL ON TABLE public.nflt1q TO user4;


--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 195
-- Name: TABLE nflteamrz; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflteamrz TO cblanton;
GRANT SELECT ON TABLE public.nflteamrz TO read2;
GRANT ALL ON TABLE public.nflteamrz TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflteamrz TO user1;
GRANT ALL ON TABLE public.nflteamrz TO user2;
GRANT ALL ON TABLE public.nflteamrz TO user3;
GRANT ALL ON TABLE public.nflteamrz TO user4;


--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 210
-- Name: TABLE nflt1qrz; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt1qrz TO johnathan;
GRANT ALL ON TABLE public.nflt1qrz TO cblanton;
GRANT SELECT ON TABLE public.nflt1qrz TO readonly;
GRANT SELECT ON TABLE public.nflt1qrz TO read2;
GRANT ALL ON TABLE public.nflt1qrz TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt1qrz TO user1;
GRANT ALL ON TABLE public.nflt1qrz TO user2;
GRANT ALL ON TABLE public.nflt1qrz TO user3;
GRANT ALL ON TABLE public.nflt1qrz TO user4;


--
-- TOC entry 3361 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE nflt2h; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt2h TO johnathan;
GRANT ALL ON TABLE public.nflt2h TO cblanton;
GRANT SELECT ON TABLE public.nflt2h TO readonly;
GRANT SELECT ON TABLE public.nflt2h TO read2;
GRANT ALL ON TABLE public.nflt2h TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt2h TO user1;
GRANT ALL ON TABLE public.nflt2h TO user2;
GRANT ALL ON TABLE public.nflt2h TO user3;
GRANT ALL ON TABLE public.nflt2h TO user4;


--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 206
-- Name: TABLE nflt2hrz; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt2hrz TO johnathan;
GRANT ALL ON TABLE public.nflt2hrz TO cblanton;
GRANT SELECT ON TABLE public.nflt2hrz TO readonly;
GRANT SELECT ON TABLE public.nflt2hrz TO read2;
GRANT ALL ON TABLE public.nflt2hrz TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt2hrz TO user1;
GRANT ALL ON TABLE public.nflt2hrz TO user2;
GRANT ALL ON TABLE public.nflt2hrz TO user3;
GRANT ALL ON TABLE public.nflt2hrz TO user4;


--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 199
-- Name: TABLE nflt2q; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt2q TO johnathan;
GRANT ALL ON TABLE public.nflt2q TO cblanton;
GRANT SELECT ON TABLE public.nflt2q TO readonly;
GRANT SELECT ON TABLE public.nflt2q TO read2;
GRANT ALL ON TABLE public.nflt2q TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt2q TO user1;
GRANT ALL ON TABLE public.nflt2q TO user2;
GRANT ALL ON TABLE public.nflt2q TO user3;
GRANT ALL ON TABLE public.nflt2q TO user4;


--
-- TOC entry 3364 (class 0 OID 0)
-- Dependencies: 198
-- Name: TABLE nflt2qrz; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt2qrz TO johnathan;
GRANT ALL ON TABLE public.nflt2qrz TO cblanton;
GRANT SELECT ON TABLE public.nflt2qrz TO readonly;
GRANT SELECT ON TABLE public.nflt2qrz TO read2;
GRANT ALL ON TABLE public.nflt2qrz TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt2qrz TO user1;
GRANT ALL ON TABLE public.nflt2qrz TO user2;
GRANT ALL ON TABLE public.nflt2qrz TO user3;
GRANT ALL ON TABLE public.nflt2qrz TO user4;


--
-- TOC entry 3365 (class 0 OID 0)
-- Dependencies: 200
-- Name: TABLE nflt3q; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt3q TO johnathan;
GRANT ALL ON TABLE public.nflt3q TO cblanton;
GRANT SELECT ON TABLE public.nflt3q TO readonly;
GRANT SELECT ON TABLE public.nflt3q TO read2;
GRANT ALL ON TABLE public.nflt3q TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt3q TO user1;
GRANT ALL ON TABLE public.nflt3q TO user2;
GRANT ALL ON TABLE public.nflt3q TO user3;
GRANT ALL ON TABLE public.nflt3q TO user4;


--
-- TOC entry 3366 (class 0 OID 0)
-- Dependencies: 201
-- Name: TABLE nflt3qrz; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt3qrz TO johnathan;
GRANT ALL ON TABLE public.nflt3qrz TO cblanton;
GRANT SELECT ON TABLE public.nflt3qrz TO readonly;
GRANT SELECT ON TABLE public.nflt3qrz TO read2;
GRANT ALL ON TABLE public.nflt3qrz TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt3qrz TO user1;
GRANT ALL ON TABLE public.nflt3qrz TO user2;
GRANT ALL ON TABLE public.nflt3qrz TO user3;
GRANT ALL ON TABLE public.nflt3qrz TO user4;


--
-- TOC entry 3367 (class 0 OID 0)
-- Dependencies: 203
-- Name: TABLE nflt4q; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt4q TO johnathan;
GRANT ALL ON TABLE public.nflt4q TO cblanton;
GRANT SELECT ON TABLE public.nflt4q TO readonly;
GRANT SELECT ON TABLE public.nflt4q TO read2;
GRANT ALL ON TABLE public.nflt4q TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt4q TO user1;
GRANT ALL ON TABLE public.nflt4q TO user2;
GRANT ALL ON TABLE public.nflt4q TO user3;
GRANT ALL ON TABLE public.nflt4q TO user4;


--
-- TOC entry 3368 (class 0 OID 0)
-- Dependencies: 202
-- Name: TABLE nflt4qrz; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nflt4qrz TO johnathan;
GRANT ALL ON TABLE public.nflt4qrz TO cblanton;
GRANT SELECT ON TABLE public.nflt4qrz TO readonly;
GRANT SELECT ON TABLE public.nflt4qrz TO read2;
GRANT ALL ON TABLE public.nflt4qrz TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nflt4qrz TO user1;
GRANT ALL ON TABLE public.nflt4qrz TO user2;
GRANT ALL ON TABLE public.nflt4qrz TO user3;
GRANT ALL ON TABLE public.nflt4qrz TO user4;


--
-- TOC entry 3369 (class 0 OID 0)
-- Dependencies: 221
-- Name: TABLE nfltall; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nfltall TO cblanton;
GRANT SELECT ON TABLE public.nfltall TO readonly;
GRANT SELECT ON TABLE public.nfltall TO read2;
GRANT ALL ON TABLE public.nfltall TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nfltall TO user1;
GRANT ALL ON TABLE public.nfltall TO user2;
GRANT ALL ON TABLE public.nfltall TO user3;
GRANT ALL ON TABLE public.nfltall TO user4;


--
-- TOC entry 3370 (class 0 OID 0)
-- Dependencies: 196
-- Name: TABLE nfltallrz; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nfltallrz TO cblanton;
GRANT ALL ON TABLE public.nfltallrz TO johnathan;
GRANT SELECT ON TABLE public.nfltallrz TO readonly;
GRANT SELECT ON TABLE public.nfltallrz TO read2;
GRANT ALL ON TABLE public.nfltallrz TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nfltallrz TO user1;
GRANT ALL ON TABLE public.nfltallrz TO user2;
GRANT ALL ON TABLE public.nfltallrz TO user3;
GRANT ALL ON TABLE public.nfltallrz TO user4;


--
-- TOC entry 3371 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE nfltovt; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nfltovt TO johnathan;
GRANT ALL ON TABLE public.nfltovt TO cblanton;
GRANT SELECT ON TABLE public.nfltovt TO readonly;
GRANT SELECT ON TABLE public.nfltovt TO read2;
GRANT ALL ON TABLE public.nfltovt TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nfltovt TO user1;
GRANT ALL ON TABLE public.nfltovt TO user2;
GRANT ALL ON TABLE public.nfltovt TO user3;
GRANT ALL ON TABLE public.nfltovt TO user4;


--
-- TOC entry 3372 (class 0 OID 0)
-- Dependencies: 208
-- Name: TABLE nfltovtrz; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.nfltovtrz TO johnathan;
GRANT ALL ON TABLE public.nfltovtrz TO cblanton;
GRANT SELECT ON TABLE public.nfltovtrz TO readonly;
GRANT SELECT ON TABLE public.nfltovtrz TO read2;
GRANT ALL ON TABLE public.nfltovtrz TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.nfltovtrz TO user1;
GRANT ALL ON TABLE public.nfltovtrz TO user2;
GRANT ALL ON TABLE public.nfltovtrz TO user3;
GRANT ALL ON TABLE public.nfltovtrz TO user4;


--
-- TOC entry 3373 (class 0 OID 0)
-- Dependencies: 188
-- Name: TABLE playertest; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.playertest TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.playertest TO user1;
GRANT ALL ON TABLE public.playertest TO user2;
GRANT ALL ON TABLE public.playertest TO user3;
GRANT ALL ON TABLE public.playertest TO user4;


--
-- TOC entry 3374 (class 0 OID 0)
-- Dependencies: 222
-- Name: TABLE playerupdate; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.playerupdate TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.playerupdate TO user1;
GRANT ALL ON TABLE public.playerupdate TO user2;
GRANT ALL ON TABLE public.playerupdate TO user3;
GRANT ALL ON TABLE public.playerupdate TO user4;


--
-- TOC entry 3375 (class 0 OID 0)
-- Dependencies: 216
-- Name: TABLE premiumscoring; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.premiumscoring TO cblanton;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.premiumscoring TO readonly;
GRANT ALL ON TABLE public.premiumscoring TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.premiumscoring TO user1;
GRANT ALL ON TABLE public.premiumscoring TO user2;
GRANT ALL ON TABLE public.premiumscoring TO user3;
GRANT ALL ON TABLE public.premiumscoring TO user4;


--
-- TOC entry 3377 (class 0 OID 0)
-- Dependencies: 217
-- Name: TABLE season; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.season TO cblanton;
GRANT SELECT ON TABLE public.season TO readonly;
GRANT ALL ON TABLE public.season TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.season TO user1;
GRANT ALL ON TABLE public.season TO user2;
GRANT ALL ON TABLE public.season TO user3;
GRANT ALL ON TABLE public.season TO user4;


--
-- TOC entry 3378 (class 0 OID 0)
-- Dependencies: 192
-- Name: TABLE srusers; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.srusers TO cblanton;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.srusers TO readonly;
GRANT ALL ON TABLE public.srusers TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.srusers TO user1;
GRANT ALL ON TABLE public.srusers TO user2;
GRANT ALL ON TABLE public.srusers TO user3;
GRANT ALL ON TABLE public.srusers TO user4;


--
-- TOC entry 3380 (class 0 OID 0)
-- Dependencies: 212
-- Name: TABLE stadiums; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.stadiums TO cblanton;
GRANT SELECT ON TABLE public.stadiums TO readonly;
GRANT ALL ON TABLE public.stadiums TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.stadiums TO user1;
GRANT ALL ON TABLE public.stadiums TO user2;
GRANT ALL ON TABLE public.stadiums TO user3;
GRANT ALL ON TABLE public.stadiums TO user4;


--
-- TOC entry 3382 (class 0 OID 0)
-- Dependencies: 228
-- Name: TABLE stador; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.stador TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.stador TO user1;
GRANT ALL ON TABLE public.stador TO user2;
GRANT ALL ON TABLE public.stador TO user3;
GRANT ALL ON TABLE public.stador TO user4;


--
-- TOC entry 3383 (class 0 OID 0)
-- Dependencies: 220
-- Name: TABLE teammarketing; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.teammarketing TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.teammarketing TO user1;
GRANT ALL ON TABLE public.teammarketing TO user2;
GRANT ALL ON TABLE public.teammarketing TO user3;
GRANT ALL ON TABLE public.teammarketing TO user4;


--
-- TOC entry 3384 (class 0 OID 0)
-- Dependencies: 218
-- Name: TABLE updatecurrentplayer; Type: ACL; Schema: public; Owner: admin
--

GRANT ALL ON TABLE public.updatecurrentplayer TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.updatecurrentplayer TO user1;
GRANT ALL ON TABLE public.updatecurrentplayer TO user2;
GRANT ALL ON TABLE public.updatecurrentplayer TO user3;
GRANT ALL ON TABLE public.updatecurrentplayer TO user4;


--
-- TOC entry 3385 (class 0 OID 0)
-- Dependencies: 213
-- Name: TABLE weathertable; Type: ACL; Schema: public; Owner: cblanton
--

REVOKE ALL ON TABLE public.weathertable FROM cblanton;
GRANT ALL ON TABLE public.weathertable TO cblanton WITH GRANT OPTION;
GRANT SELECT,INSERT,UPDATE ON TABLE public.weathertable TO wpredictor;
GRANT SELECT ON TABLE public.weathertable TO readonly;
GRANT ALL ON TABLE public.weathertable TO jeff WITH GRANT OPTION;
GRANT ALL ON TABLE public.weathertable TO user1;
GRANT ALL ON TABLE public.weathertable TO user2;
GRANT ALL ON TABLE public.weathertable TO user3;
GRANT ALL ON TABLE public.weathertable TO user4;


-- Completed on 2018-03-15 19:50:46

--
-- PostgreSQL database dump complete
--

