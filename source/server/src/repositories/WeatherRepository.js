const Promise = require('promise');
const db = require('../database');
const model = require('../model/WeatherModel');


module.exports = {
    getGameStadium: function (weatherKey) {
        return new Promise(function (resolve, reject) {
            db.oneOrNone(model.buildGameStadiumQuery(), {
                weatherKey: weatherKey
            }).then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    },
    getGameGraphData: function (weatherKey) {
        return new Promise(function (resolve, reject) {
            db.any(model.buildGameGraphDataQuery(), {
                weatherKey: weatherKey
            }).then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    },
    getTeamStats: function (team, weatherKey) {
        return new Promise(function (resolve, reject) {
            const playerQueries = model.buildPlayerQueries();
            const teamQueries = model.buildTeamQueries();

            const parameters = {
                team: team,
                weatherKey: weatherKey
            };
            Promise.all([
                db.any(playerQueries[0], parameters),
                db.any(playerQueries[1], parameters),
                db.any(playerQueries[2], parameters),
                db.any(playerQueries[3], parameters),
                db.any(playerQueries[4], parameters),
                db.any(playerQueries[5], parameters),
                db.any(teamQueries[0], parameters),
                db.any(teamQueries[1], parameters),
            ]).then(function (results) {
                resolve({
                    'players': {
                        'QB': results[0],
                        'RB': results[1],
                        'WR1': results[2],
                        'WR2': results[3],
                        'TE': results[4],
                        'K': results[5],
                    },
                    'team': Object.assign({}, (results[6].length > 0) ? results[6][0] : {}, (results[7].length > 0) ? results[7][0] : {})
                });
            }).catch(function (err) {
                reject(err);
            });
        });
    }
};
