const db = require('../database');
const stripe = require('../stripe');
const Promise = require('promise');

const NEW_USER_INSERT = (
    'INSERT INTO srusers (userid, firstname, lastname, email, username, pswhash, zip, gender, promocode, urlsignup, bmonth, bday, newsletter, datefirstsignup, accounttype, status, currentperiodstart, currentperiodend, freezedate, endeddate, totalamountspent, trialstart, trialend, refundcost, refunddate, accountnumber) ' +
    'VALUES ((SELECT COALESCE(MAX(userid), 0) + 1 FROM srusers), ${firstname},  ${lastname},  ${email},  ${username},  crypt(${password}, gen_salt(\'bf\')),  ${zip},  ${gender},  ${promocode}, ${urlsignup}, ${bmonth}, ${bday},  ${newsletter},  ${datefirstsignup}, ${accounttype}, ${status}, ${periodstart}, ${periodends}, null, null, ${amount}, null, null, null, null, ${customer});'
);
// ------------------------------------------------------(userid, firstname, lastname, ---------email, -------username, -------------------------------pswhash, zip, ----gender, --promocode, urlsignup, -----bmonth, bday, -----newsletter, datefirstsignup, -accounttype, status, currentperiodstart, currentperiodend, freezedate, endeddate, totalamountspent, trialstart, trialend, refundcost, refunddate, accountnumber)
const NEW_SUBSCRIPTION_INSERT = (
    'INSERT INTO subscription (firstname, lastname, email, device,ip,location) ' +
    'VALUES (${firstname},  ${lastname},  ${email},  ${device},  ${ip}, ${location} );'
);
const EMAIL_ALREADY_USED_SELECT = (
    'SELECT COUNT(*) AS count from srusers where email = ${email};'
);
const EMAIL_SUBSCRIBED_SELECT = (
    'SELECT COUNT(*) AS count from subscription where email = ${email};'
);
const USERNAME_ALREADY_USED_SELECT = (
    'SELECT COUNT(*) AS count from srusers where username = ${username} AND userid != ${exclude};'
);

const SIGNIN_USER_SELECT = (
    'SELECT * from srusers where username = ${username} and pswhash = crypt(${password}, pswhash);'
);

const GET_USER_SELECT = (
    'SELECT * from srusers where userid = ${userid};'
);

const GET_USER_BY_EMAIL_OR_USERNAME = (
    'SELECT * from srusers where username = ${userNameOrEmail} OR email = ${userNameOrEmail};'
);

const DELETE_ALL_USER_FORGOT_PASS_HASHES = (
    'UPDATE forgotten SET deleted_at = CURRENT_DATE WHERE userid = ${userid}'
);

const INSERT_USER_FORGOT_HASH = (
    'INSERT INTO forgotten (userid, hash, valid_unitl, created_at) VALUES (${userid}, ${hash}, CURRENT_DATE + INTERVAL \'7 day\', CURRENT_DATE)'
);

const SELECT_VALID_USER_FORGOT_HASH = (
    'SELECT userid FROM forgotten WHERE hash = ${hash} AND deleted_at IS NULL AND valid_unitl >= CURRENT_DATE'
);

const UPDATE_USER_PASSWORD = (
    'UPDATE srusers SET pswhash = crypt(${newPassword}, gen_salt(\'bf\')) WHERE userid = ${userid}'
);

const UPDATE_USER_STRIPE_CUSTOMER = (
    'UPDATE srusers SET accountnumber = ${customerId} WHERE userid = ${userid}'
);

const UPDATE_USER_SUBSCTIPTION_USERNAME = (
    'UPDATE srusers SET currentperiodstart = ${periodstart}, currentperiodend = ${periodends},  accounttype = ${accounttype}, status = ${status}, totalamountspent = totalamountspent + ${amount} WHERE username = ${username}'
);

const UPDATE_USER_SUBSCTIPTION_USERID = (
    'UPDATE srusers SET currentperiodstart = ${periodstart}, currentperiodend = ${periodends},  accounttype = ${accounttype}, status = ${status}, totalamountspent = totalamountspent + ${amount} WHERE userid = ${userid}'
);

const UPDATE_USER_PLAN_USERID = (
    'UPDATE srusers SET currentperiodstart = ${currentperiodstart}, currentperiodend = ${currentperiodend},  accounttype = ${accounttype}, status = ${status}, totalamountspent = totalamountspent + ${amount} WHERE userid = ${userid}'
);

const UPDATE_USER_FIRST_NAME = (
    'UPDATE srusers SET firstname = ${firstname} WHERE userid = ${userid}'
);

const UPDATE_USER_LAST_NAME = (
    'UPDATE srusers SET lastname = ${lastname} WHERE userid = ${userid}'
);

const UPDATE_USER_ZIP = (
    'UPDATE srusers SET zip = ${zip} WHERE userid = ${userid}'
);

const UPDATE_USER_NEWSLETTER = (
    'UPDATE srusers SET newsletter = ${newsletter} WHERE userid = ${userid}'
);

const UPDATE_USER_USERNAME = (
    'UPDATE srusers SET username = ${username} WHERE userid = ${userid}'
);


module.exports = {

    signUpUser: function (values) {
        return new Promise(function (resolve, reject) {
            values.datefirstsignup = new Date();
            db.any(NEW_USER_INSERT, values).then(function () {
                resolve();
            }, function (err) {
                reject(err);
            });
        });
    },

    getUserByUserID: function (userid, loadCustomer = true) {
        return new Promise(function (resolve, reject) {
            db.one(GET_USER_SELECT, {
                userid: userid
            }).then(function (user) {
                const customerId = user.accountnumber;

                delete user.pswhash;
                delete user.accountnumber;
                // TODO: get customer card/active plan information
                if (loadCustomer && customerId) {
                    stripe.getCustomer(customerId).then(function (customer) {
                        user.customer = customer;
                    }, function () {
                        user.customer = null;
                    }).then(function () {
                        resolve(user);
                    });
                } else {
                    user.customer = null;
                    resolve(user);
                }
            }, function () {
                reject();
            });
        });
    },

    signInUser: function (username, password) {
        return new Promise(function (resolve, reject) {
            db.one(SIGNIN_USER_SELECT, {
                username: username,
                password: password
            }).then(function (user) {
                delete user.pswhash;
                resolve(user);
            }, function () {
                reject();
            });

        });
    },

    isEmailAvailable: function (email) {
        return new Promise(function (resolve, reject) {
            
            db.one(EMAIL_ALREADY_USED_SELECT, {
                email: email
            }).then(function (row) {
                resolve(parseInt(row.count, 10) === 0);
            }, function () {
                reject();
            });
        });
    },
    isEmailSubscribed: function (email) {
        return new Promise(function (resolve, reject) {
            
            db.one(EMAIL_SUBSCRIBED_SELECT, {
                email: email
            }).then(function (row) {
                resolve(parseInt(row.count, 10) === 0);
            }, function () {
                reject();
            });
        });
    },
    isUsernameAvailable: function (username, exludeUserId = -1) {
        return new Promise(function (resolve, reject) {
            db.one(USERNAME_ALREADY_USED_SELECT, {
                exclude: exludeUserId,
                username: username
            }).then(function (row) {
                resolve(parseInt(row.count, 10) === 0);
            }, function () {
                reject();
            });
        });
    },

    getUserByUserNameOrEmail: function (userNameOrEmail) {
        return new Promise(function (resolve, reject) {
            db.one(GET_USER_BY_EMAIL_OR_USERNAME, {
                userNameOrEmail: userNameOrEmail
            }).then(function (userRow) {
                resolve(userRow);
            }).catch(function (err) {
                reject(err);
            });
        });
    },

    removeAllUserForgotPasswordHashes: function (userid) {
        return new Promise(function (resolve, reject) {
            db.none(DELETE_ALL_USER_FORGOT_PASS_HASHES, {
                userid: userid
            }).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },

    addUserForgotPasswordHash: function (userid, hash) {
        return new Promise(function (resolve, reject) {
            db.none(INSERT_USER_FORGOT_HASH, {
                userid: userid,
                hash: hash
            }).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },

    isUserForgotPasswordHashValid: function (hash) {
        return new Promise(function (resolve, reject) {
            db.one(SELECT_VALID_USER_FORGOT_HASH, {
                hash: hash
            }).then(function (forgottenRow) {
                resolve(forgottenRow.userid);
            }).catch(function (err) {
                reject(err);
            });
        });
    },

    updateUserPassword: function (userid, newPassword) {
        return new Promise(function (resolve, reject) {
            db.none(UPDATE_USER_PASSWORD, {
                userid: userid,
                newPassword: newPassword
            }).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },

    updateUserStripeCustomer: function (userid, newCustomerId) {
        return new Promise(function (resolve, reject) {
            db.none(UPDATE_USER_STRIPE_CUSTOMER, {
                userid: userid,
                customerId: newCustomerId
            }).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },

    updateUserSubsctiprionByUserId: function (userid, data) {
        return new Promise(function (resolve, reject) {
            db.none(UPDATE_USER_SUBSCTIPTION_USERID,
                Object.assign({
                    userid: userid,
                }, data)
            ).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },

    updateUserSubsctiprionByUsername: function (username, data) {
        return new Promise(function (resolve, reject) {
            db.none(UPDATE_USER_SUBSCTIPTION_USERNAME,
                Object.assign({
                    username: username,
                }, data)
            ).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },

    checkAndInvalidatePromoCode: function (promocode) {
        // TODO: proper promo code validation and invalidation
        if (promocode) {
            return Promise.reject();
        }
        return Promise.resolve();
    },

    revalidatePromoCode: function (promocode) {
        return Promise.resolve();
    },

    updateUserPlan: function (userid, data) {
        return new Promise(function (resolve, reject) {
            db.none(UPDATE_USER_PLAN_USERID,
                Object.assign({
                    userid: userid,
                }, data)
            ).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },

    updateUserFirstName: function (userid, firstname) {
        return new Promise(function (resolve, reject) {
            db.none(UPDATE_USER_FIRST_NAME,
                {
                    userid: userid,
                    firstname: firstname
                }
            ).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },
    updateUserLastName: function (userid, lastname) {
        return new Promise(function (resolve, reject) {
            db.none(UPDATE_USER_LAST_NAME,
                {
                    userid: userid,
                    lastname: lastname
                }
            ).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },
    updateUserZIP: function (userid, zip) {
        return new Promise(function (resolve, reject) {
            db.none(UPDATE_USER_ZIP,
                {
                    userid: userid,
                    zip: zip
                }
            ).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },
    updateUserNewsletter: function (userid, newsletter) {
        return new Promise(function (resolve, reject) {
            db.none(UPDATE_USER_NEWSLETTER,
                {
                    userid: userid,
                    newsletter: newsletter
                }
            ).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },
    updateUserUsername: function (userid, username) {
        return new Promise(function (resolve, reject) {
            db.none(UPDATE_USER_USERNAME,
                {
                    userid: userid,
                    username: username
                }
            ).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    },
    addSubscription: function (firstname,lastname,
        email,device,ip,location) {

        return new Promise(function (resolve, reject) {
            db.none(NEW_SUBSCRIPTION_INSERT,
                {
                    firstname : firstname,
                    lastname : lastname,
                    email : email,
                    device : device,
                    ip : ip,
                    location : location
                }
            ).then(function () {
                resolve();
            }).catch(function (err) {
                reject(err);
            });
        });
    }
};