module.exports = {
    getCurrentNFLWeek: function () {
        return window.NFL_UTILS.week;
    },
    getCurrentNFLYear: function () {
        return window.NFL_UTILS.season;
    }
};
