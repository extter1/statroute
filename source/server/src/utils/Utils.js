
module.exports = {
    getSafe: function (fn) {
        try {
            return fn();
        } catch (e) {
            return undefined;
        }
    },
    importAll: function (r) {
        var images = {};
        r.keys().forEach(function (item) {
            images[item.replace('./', '').replace(/\.(png|jpe?g|svg)$/, '')] = r(item);
        });
        return images;
    },
};