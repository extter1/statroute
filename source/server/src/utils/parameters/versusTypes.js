module.exports = {
   'TEAM': 'Vs. Team',
   'HC': 'Vs. Head Coach',
   'OC': 'Vs. Offensive Coordinator',
   'DC': 'Vs. Defensive Coordinator'
};