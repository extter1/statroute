module.exports = {
    'THIS_SEASON': 'This season',
    '2_WEEKS': 'Past 2 Weeks',
    '4_WEEKS': 'Past 4 Weeks',
    '8_WEEKS': 'Past 8 Weeks',
    '12_WEEKS': 'Past 12 Weeks',
 
    '1_SEASON': 'Past Season',
    '3_SEASONS': 'Past 3 Seasons',
    '5_SEASONS': 'Past 5 Seasons',
    '10_SEASONS': 'Past 10 Seasons'
 }; 