module.exports = {
    nflp: {
        'ALL': 'All positions',
        'QB': 'Quarterback',
        'RB': 'Running Back',
        'WR': 'Wide Receiver',
        'TE': 'Tight End',
        'K': 'Kicker'
    },
    nflt: {
        'OFF': 'All Offense',
        'PAS': 'Passing',
        'RUS': 'Rushing',
        'REC': 'Receiving',
        'KIC': 'Kicking',
        'DEF': 'All Defense'
    },
    // unsupported?
    unsupported: ['CB', 'DT', 'OLB', 'S', 'DE', 'G', 'OT', 'P', 'C', 'ILB', 'LS', 'FS', 'SS', 'SS', 'NT', 'FB']
};