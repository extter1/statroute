module.exports = {
    TEMPERATURES: {
        'ALL': 'All Temperatures',
        'VCOLD': 'Very Cold (<20)',
        'COLD': 'Cold (21-40)',
        'FAIR': 'Fair (41-64)',
        'WARM': 'Warm (65-80)',
        'HOT': 'Hot (81-99)',
        'VHOT': 'Very Hot (99+)'

    },
    CONDITIONS: {
        'ALL': 'All',
        'CLEAR': 'Clear',
        'OVERCAST': 'Overcast',
        'WINDY': 'Windy (5-10 mph)',
        'VWINDY': 'Very Windy (10+ mph)',
        'RAIN': 'Wet/Rain',
        'HRAIN': 'Heavy Rain',
        'SNOW': 'Snow',
        'HSNOW': 'Heavy Snow'
    }
};