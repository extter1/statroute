const debug = require('debug')('statroute:db');

const pgp = require('pg-promise')({
    error: function (err, e) {
        debug(err.message);
        if (e.cn) {
            debug('There was an error while creating new connection to DB');
        }
    }
});

const db = pgp({
 host: process.env.STATROUTE_DB_HOST,
    port: process.env.STATROUTE_DB_PORT,
    database: process.env.STATROUTE_DB_DATABASE,
    user: process.env.STATROUTE_DB_USER,
    password: process.env.STATROUTE_DB_PASSWORD,
    ssl: process.env.STATROUTE_DB_SSL,


});

db.connect()
    .then(function (obj) {
        obj.done(); // success, release connection;
    })
    .catch(function (error) {
    });

module.exports = db;
