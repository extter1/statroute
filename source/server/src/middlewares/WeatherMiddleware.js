const parameterError = require('../utils/Errors').invalidParameters;

const teams = require('../utils/parameters/teams');

const validateGames = function (req, res, next) {
    const week = req.query.week || 0;
    {
        const number = parseInt(week, 10);
        if (isNaN(number)) {
            next(parameterError());
        }
        req.query.week = (number === 0) ? 1 : number;
    }

    next();
};

const validateTeamAbbr = function (req, res, next) {
    const team = req.query.team || null;
    if(team === null || !Object.keys(teams).includes(team)) {
        next(parameterError(false, 'team'));
    }

    next();
};

const validateWeatherKey = function (req, res, next) {
    const weatherKey = req.query.weatherKey || null;
    if(weatherKey === null || weatherKey.trim().length === 0) {
        next(parameterError(false, 'weatherKey'));
    }

    next();
};

module.exports = {
    validateGames: validateGames,
    validateWeatherKey: validateWeatherKey,
    validateTeamAbbr: validateTeamAbbr
};