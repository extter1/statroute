SELECT
  stat1 AS passyardsallowed,
  stat2 AS rushyardsallowed,
  stat3 as defensivetds,
  stat4 as interceptions,
  stat5 as sacks
FROM depthchart
WHERE (team = ${team} AND weatherkey = ${weatherKey} AND tpposition IN ('CB'))