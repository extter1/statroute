SELECT
  team,
  tpposition,
  playername,
  stat1 AS fantasypointsperGame,
  stat2 AS fantasyfloor,
  stat3 AS fantasyceiling,
  stat4 AS scrimmageyards,
  stat5 AS touchdowns,
  stat6 AS fumbleslost,
  stat7 AS totalgames
FROM depthchart
WHERE (team = ${team} AND weatherkey = ${weatherKey} AND tpposition IN ('TE'))
