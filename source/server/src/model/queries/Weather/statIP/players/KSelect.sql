SELECT
  playername,
  stat1 AS fantasypointsperGame,
  stat2 AS fantasyfloor,
  stat3 AS fantasyceiling,
  stat4 AS fgattempts,
  stat5 AS fgmade,
  stat6 AS long,
  stat7 AS totalgames
FROM depthchart
WHERE (team = ${team} AND weatherkey = ${weatherKey} AND tpposition IN ('K'))