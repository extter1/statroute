SELECT
  stat1 AS gpsumofgamesplayed,
  stat2 AS pfsumofpointsforthatoffense,
  stat3 AS pasumofthepointsgivenupbythatoffense,
  stat4 AS fantasypointspergame,
  stat5 AS fantasyfloor,
  stat6 AS fantasyceiling,
  stat7 AS yardsallowed
FROM depthchart
WHERE (team = ${team} AND weatherkey = ${weatherKey} AND tpposition IN ('DE'))