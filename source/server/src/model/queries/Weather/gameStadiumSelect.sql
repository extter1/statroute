SELECT
  hometeamkey,
  awayteamkey,
  weatherkey,
  stadiumgraphic,
  stadiumname,
  stadiumorientation,
  winddirection,
  windspeed,
  comp.maxwindspeed AS maxwindspeed,
  windc1,
  zip,
  lat,
  long,
  elevation
FROM weathertable, (SELECT MAX(windspeed) AS maxwindspeed FROM weathertable WHERE (weatherkey = ${weatherKey})) AS comp
WHERE (weatherkey = ${weatherKey})
LIMIT 1