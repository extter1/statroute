const pgpFormat = require('pg-promise').as.format;
const debug = require('debug')('sql');

const Model = require('./Model');

const GAME_STADIUM_SELECT = Model.loadSQLQueryFile('/queries/Weather/gameStadiumSelect.sql');
const GAME_GRAPH_DATA_SELECT = Model.loadSQLQueryFile('/queries/Weather/gameGraphSelect.sql');

const PLAYER_SELECTS = [
    Model.loadSQLQueryFile('/queries/Weather/statIP/players/QBSelect.sql'),
    Model.loadSQLQueryFile('/queries/Weather/statIP/players/RBSelect.sql'),
    Model.loadSQLQueryFile('/queries/Weather/statIP/players/WR1Select.sql'),
    Model.loadSQLQueryFile('/queries/Weather/statIP/players/WR2Select.sql'),
    Model.loadSQLQueryFile('/queries/Weather/statIP/players/TESelect.sql'),
    Model.loadSQLQueryFile('/queries/Weather/statIP/players/KSelect.sql')
];

const TEAM_SELECTS = [
    Model.loadSQLQueryFile('/queries/Weather/statIP/teamStatsSelect1.sql'),
    Model.loadSQLQueryFile('/queries/Weather/statIP/teamStatsSelect2.sql')
];

module.exports = {
    buildGameStadiumQuery: function () {
        return GAME_STADIUM_SELECT;
    },
    buildGameGraphDataQuery: function () {
        return GAME_GRAPH_DATA_SELECT;
    },
    buildPlayerQueries: function () {
        return PLAYER_SELECTS;
    },
    buildTeamQueries: function () {
        return TEAM_SELECTS;
    }
};
