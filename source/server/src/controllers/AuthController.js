const userRepository = require('../repositories/UserRepository');
const Promise = require('promise');
const crypto = require('crypto-extra');
const moment = require('moment');

const mailer = require('../mailer');
const stripe = require('../stripe');

const PaymentsUtils = require('../utils/PaymentsUtils');
const errors = require('../utils/Errors');

module.exports = {
    postSignUp: function (req, res, next) {
        const data = Object.assign({}, req.body);
        const token = data.token;
        const three_d_secure = data.three_d_secure || false;

        let promoCodeValid = false;
        let chargeAmount = 0;

        Promise.all([
            userRepository.isUsernameAvailable(req.body.username),
            userRepository.isEmailAvailable(req.body.email),
            // check and invalidate promocode if present
            userRepository.checkAndInvalidatePromoCode(req.body.promocode)
        ]).then(function (results) {
            promoCodeValid = true;
            const plan = PaymentsUtils.plans[data.plan];
            data.amount = (data.promocode) ? plan.promoPrice : plan.price;
            chargeAmount = data.amount;
            if (results[0] && results[1]) {
                // create new stripe customer
                return stripe.createCustomer(token, data.email).then(function (customer) {
                    data.customer = customer.id;

                    if (three_d_secure) {
                        data.amount = 0;
                        return Promise.resolve(false);
                    }

                    // TODO: add free plan
                    if (data.plan !== 'lifetime') {
                        let stripeCoupon = null;
                        if (data.promocode) {
                            stripeCoupon = plan.couponCode;
                        }
                        // create new subscription for customer (use prome codes accordicly)
                        return stripe.createSubscription(customer.id, req.body.plan, stripeCoupon);
                    } else {
                        // charge one time payment for lifetime subscription
                        return stripe.chargeCustomer(customer.id, data.amount);
                    }
                }).then(function (subscriptionOrPayment) {
                    // generate data to DB (dates, subscription types, ...)

                    data.datefirstsignup = moment().toDate();
                    if (!data.promocode) data.promocode = null;

                    if (subscriptionOrPayment) {
                        // TODO: add free plan
                        data.periodstart = moment().toDate();
                        data.periodends = null;
                        if (data.plan !== 'lifetime') {
                            data.periodstart = moment.unix(subscriptionOrPayment.current_period_start).toDate();
                            data.periodends = moment.unix(subscriptionOrPayment.current_period_end).toDate();
                        }
                        data.accounttype = 2;
                        data.status = 'Active';
                    } else {
                        data.periodstart = null;
                        data.periodends = null;
                        data.accounttype = 1;
                        data.status = null;
                    }

                    // create new user in DB
                    return userRepository.signUpUser(data);
                });
            } else {
                return Promise.reject(errors.invalidParameters('Username or email is already used'));
            }
        }).then(function () {
            if (three_d_secure) {
                return stripe.create3DSecureSource(token, chargeAmount, false, {
                    customer: data.customer,
                    username: data.username,
                    plan: data.plan,
                    promocode: data.promocode
                }).then(function (theeDSourceToken) {
                    return Promise.resolve(theeDSourceToken);
                });
            } else {
                Promise.resolve();
            }
        }).then(function (source) {
            mailer.sendWelcomeEmail(req.body.email);

            const data = {};

            if (three_d_secure) {
                data.three_d_secure = source;
            }

            res.status(200).json({
                status: 'success',
                data: data
            });
        }).catch(function (error, isStripeError = false) {
            userRepository.revalidatePromoCode((promoCodeValid) ? req.body.promocode : null).then(() => {
                return Promise.reject();
            }).catch(() => {
                if (isStripeError) {
                    res.status(200).json({
                        error: error
                    });
                } else {
                    next(error || errors.serverError);
                }
            });
        });
    },
    postSignIn: function (req, res, next) {
        userRepository.signInUser(req.body.username, req.body.password).then(function (user) {
            req.session.userid = user.userid;
            res.sendStatus(204);
        }, function () {
            next(errors.userInvalidCredential);
        });
    },
    deleteSignIn: function (req, res, next) {
        delete req.session.userid;
        res.sendStatus(204);
    },
    getLoggedInUser: function (req, res, next) {
        userRepository.getUserByUserID(req.session.userid).then(function (user) {
            res.status(200).json({
                status: 'success',
                data: user
            });
        }, function () {
            next(errors.invalidParameters);
        });
    },
    getIsEmailAvailable: function (req, res, next) {
        userRepository.isEmailAvailable(req.query.email).then(function (isAvailable) {
            res.status(200).json({
                status: 'success',
                data: isAvailable
            });
        }, function () {
            next(errors.serverError);
        });
    },
    getIsUsernameAvailable: function (req, res, next) {
        userRepository.isUsernameAvailable(req.query.username).then(function (isAvailable) {
            res.status(200).json({
                status: 'success',
                data: isAvailable
            });
        }, function () {
            next(errors.serverError);
        });
    },
    postForgotPasswordRequest: function (req, res, next) {
        userRepository.getUserByUserNameOrEmail(req.body.emailorusername).then(function (user) {
            // todo: generate and add hash to database, invalidate all previous hashes
            const hash = crypto.randomString(128);

            userRepository.removeAllUserForgotPasswordHashes(user.userid).then(function () {
                return userRepository.addUserForgotPasswordHash(user.userid, hash);
            }).then(function () {
                mailer.sendPasswordChangeEmail(user.email, user.firstname, hash);
            }).then(function () {
                res.sendStatus(204);
            }).catch(function (err) {
                next(err);
            });
        }).catch(function () {
            next(errors.notFound);
        });
    },
    getForgotPasswordHashValid: function (req, res, next) {
        userRepository.isUserForgotPasswordHashValid(req.query.hash).then(function () {
            res.sendStatus(204);
        }, function () {
            next(errors.serverError);
        });
    },
    postForgotPasswordReset: function (req, res, next) {

        userRepository.isUserForgotPasswordHashValid(req.body.hash).then(function (userId) {
            userRepository.removeAllUserForgotPasswordHashes(userId).then(() => {
                return userRepository.updateUserPassword(userId, req.body.password);
            }).then(() => {
                res.sendStatus(204);
            }).catch(function (err) {
                next(err);
            });

        }, function () {
            next(errors.notFound);
        });
    },
    postUserCard: function (req, res, next) {
        const token = req.body.token;
        const three_d_secure = req.body.three_d_secure || false;
        let customer = null;
        let user = null;
        userRepository.getUserByUserID(req.session.userid).then(function (usr) {
            user = usr;
            if (user.customer) {
                customer = user.customer;
                // TODO: getCustomer
                // TODO: add new card (first so it gets validated before removing old one)
                return stripe.addCustomerCard(customer.id, token);
            } else {
                // TODO: create new customer with card
                return stripe.createCustomer(token, user.email).then(function (cust) {
                    customer = cust;
                    // TODO: write new id to DB
                    return userRepository.updateUserStripeCustomer(user.userid, customer.id);
                });
            }
        }).then(function () {
            if (three_d_secure) {
                return stripe.create3DSecureSource(token, 100, true, {
                    customer: customer.id
                });
            } else if (!three_d_secure && user.customer) {
                // TODO: remove old card
                return stripe.removeCustomerCard(customer.id, customer.default_source);
            }
        }).then(function (source) {
            const data = {};

            if (three_d_secure) {
                data.three_d_secure = source;
            }

            res.status(200).json({
                status: 'success',
                data: data
            });
        }).catch(function (error, isStripeError = false) {
            if (isStripeError) {
                res.status(200).json({
                    error: error
                });
            } else {
                next(error || errors.serverError);
            }
        });
    },
    complete3DSecure: function (req, res, next) {
        const plan = PaymentsUtils.plans[req.query.plan];
        let customer = null;
        let promoCodeValid = false;

        return stripe.getCustomer(req.query.customer).then(function (cust) {
            customer = cust;
            // create charge
            return stripe.getSource(req.query.source).then(function (source) {
                // validate secret
                if (source.client_secret !== req.query.client_secret) {
                    return Promise.reject({
                        message: 'Invalid request'
                    });
                }
                promoCodeValid = true;
                return stripe.chargeSource(source.id, req.query.amount, 'StatRoute - new card validation charge');
            }).then(function (charge) {
                if (req.query.refund) {
                    return stripe.refundCharge(charge.id);
                } else if (plan && ['monthly', 'yearly'].includes(req.query.plan)) {

                    const trialEnd = moment().add(plan.interval, 'days').unix();
                    return stripe.createSubscription(customer.id, req.query.plan,
                        (req.query.promocode) ? plan.couponCode : null, trialEnd);
                }
            }).then(function (subscriptionOrRefund) {
                if (plan) {
                    const data = {};

                    data.periodstart = moment().toDate();
                    data.periodends = null;
                    if (req.query.plan !== 'lifetime') {
                        data.periodstart = moment.unix(subscriptionOrRefund.current_period_start).toDate();
                        data.periodends = moment.unix(subscriptionOrRefund.current_period_end).toDate();
                    }
                    data.accounttype = 2;
                    data.status = 'Active';
                    data.amount = req.query.amount;
                    return userRepository.updateUserSubsctiprionByUsername(req.query.username, data);
                }
            }).then(function () {
                if (!plan && customer.sources.data.length > 1) {
                    // TODO: remove default card
                    return stripe.removeCustomerCard(customer.id, customer.default_source);
                }
            });
        }).then(function () {
            res.render('3DSuccess', {
                message: 'Card successfully added to your StatRoute account.',
                success: true
            });
        }, function (err) {
            userRepository.revalidatePromoCode((promoCodeValid) ? req.query.promocode : null).then(() => {
                return Promise.reject();
            }).catch(() => {
                if (!plan) {
                    const promises = [];
                    customer.sources.data.forEach(function (src) {
                        if (src.id !== customer.default_source) {
                            promises.push(stripe.removeCustomerCard(customer.id, src.id));
                        }
                    });

                    if (promises.length === 0) {
                        promises.push(stripe.removeCustomerCard(customer.id, customer.default_source));
                    }

                    // TODO: remove new card cards (or default if no other is available)
                    return Promise.all(promises).then(function () {
                        res.render('3DSuccess', {
                            message: err.message,
                            success: false
                        });
                    });
                } else {
                    // TODO: remove card since it was not validated correctly
                    stripe.removeCustomerCard(customer.id, customer.default_source).then(function () {
                        res.render('3DSuccess', {
                            message: err.message,
                            success: false
                        });
                    });
                }
            });
        });
    },
    postUserPlan: function (req, res, next) {
        const newPlan = PaymentsUtils.plans[req.body.plan];
        const newPlanCode = req.body.plan;

        const billingInfoError = {message: 'Missing billing information for given user.'};
        const samePlan = {message: 'This is your current subscription plan.'};
        const lifetimeSubscription = {message: 'User has already lifetime subscription no other subscription is needed.'};

        let promoCodeValid = false;
        let three_d_secure = false;
        let user = null;
        const data = {promocode: req.query.promocode, amount: 0};
        // TODO: get current user from DB, validate and invalidate promocode
        Promise.all([
            userRepository.getUserByUserID(req.session.userid),
            userRepository.checkAndInvalidatePromoCode(req.body.promocode),
        ]).then(([urs]) => {
            user = urs;
            promoCodeValid = true;

            // TODO: check if stripe customer is created
            if (!user.customer) {
                return Promise.reject(billingInfoError);
            }
            // TODO: check if customer has any payment source present
            if (user.customer.sources.total_count === 0) {
                return Promise.reject(billingInfoError);
            }
            // TODO: check if there is active subscription
            const activeSubscription = user.customer.subscriptions.data.find((sub) => {
                return !['canceled', 'unpaid'].includes(sub.status) && !sub.cancel_at_period_end;
            });
            if (activeSubscription) {
                if (activeSubscription.plan.id === newPlanCode) {
                    return Promise.reject(samePlan);
                }

                if (newPlanCode === 'lifetime') {
                    data.amount = (data.promocode) ? newPlan.promoPrice : newPlan.price;

                    return stripe.cancelSubscription(activeSubscription.id).then(function () {
                        // TODO: get default payment source
                        const source = user.customer.sources.data.find((source) => {
                            return source.id === user.customer.default_source;
                        });

                        // TODO: handle 3D secure
                        if (source.card.three_d_secure !== 'not_supported') {
                            return stripe.create3DSecureSource(source.id, data.amount, false, {
                                customer: user.customer.id,
                                username: user.username,
                                plan: newPlanCode,
                                promocode: (req.query.promocode) ? newPlan.couponCode : null
                            }).then(function (theeDSourceToken) {
                                three_d_secure = true;
                                data.amount = 0;
                                return Promise.resolve(theeDSourceToken);
                            });
                        }

                        return stripe.chargeCustomer(user.customer.id, data.amount, 'Lifetime premium plan');
                    });
                }

                // TODO: remove current subscription
                // TODO: add new subscription starting when old one ends
                return stripe.upgradeUserPlan(
                    user.customer.id, activeSubscription.id, newPlanCode, (req.query.promocode) ? newPlan.couponCode : null);
            } else {
                data.amount = (data.promocode) ? newPlan.promoPrice : newPlan.price;
                // TODO: check if user has lifetime plan
                console.log('here');
                if (user.currentperiodstart && user.currentperiodend === null) {
                    return Promise.reject((newPlanCode === 'lifetime') ? samePlan : lifetimeSubscription);
                }

                // TODO: get default payment source
                const source = user.customer.sources.data.find((source) => {
                    return source.id === user.customer.default_source;
                });

                // TODO: handle 3D secure
                if (source.card.three_d_secure !== 'not_supported') {
                    return stripe.create3DSecureSource(source.id, data.amount, false, {
                        customer: user.customer.id,
                        username: user.username,
                        plan: newPlanCode,
                        promocode: (req.query.promocode) ? newPlan.couponCode : null
                    }).then(function (theeDSourceToken) {
                        data.amount = 0;
                        three_d_secure = true;
                        return Promise.resolve(theeDSourceToken);
                    });
                }

                // TODO: create new subscription for user
                if (newPlanCode !== 'lifetime') {
                    let stripeCoupon = null;
                    if (data.promocode) {
                        stripeCoupon = newPlan.couponCode;
                    }
                    // TODO: create new subscription for customer (use prome codes accordicly)
                    return stripe.createSubscription(user.customer.id, newPlanCode, stripeCoupon);
                } else {
                    // TODO: charge one time payment for lifetime subscription
                    return stripe.chargeCustomer(user.customer.id, data.amount, 'Lifetime premium plan');
                }
            }
        }).then((subscriptionOrPayment) => {
            if (three_d_secure) {
                return Promise.resolve(subscriptionOrPayment);
            }
            // TODO: generate new user data
            if (subscriptionOrPayment) {
                data.currentperiodstart = moment().toDate();
                data.currentperiodend = null;
                if (newPlanCode !== 'lifetime') {
                    data.currentperiodstart = moment.unix(subscriptionOrPayment.current_period_start).toDate();
                    data.currentperiodend = moment.unix(subscriptionOrPayment.current_period_end).toDate();
                }
                data.accounttype = 2;
                data.status = 'Active';
            } else {
                data.currentperiodstart = null;
                data.currentperiodend = null;
                data.accounttype = 1;
                data.status = null;
            }
            // TODO: update DB accordingly

            return userRepository.updateUserPlan(user.userid, data);

        }).then((result) => {
            const data = {};

            if (three_d_secure) {
                data.three_d_secure = result;
            }

            res.status(200).json({
                status: 'success',
                data: data
            });
        }).catch((error) => {
            userRepository.revalidatePromoCode((promoCodeValid) ? req.body.promocode : null).then(() => {
                return Promise.reject();
            }).catch(() => {
                console.log(error);
                res.status(200).json({
                    error: {
                        message: error.message || 'Error occurred'
                    }
                });
            });
        });
    },

    deleteUserPlan: function (req, res, next) {
        userRepository.getUserByUserID(req.session.userid).then(function (user) {
            // TODO: check if there is active subscription
            const activeSubscription = user.customer.subscriptions.data.find((sub) => {
                return !['canceled', 'unpaid'].includes(sub.status) && !sub.cancel_at_period_end;
            });
            if (!activeSubscription) {
                return Promise.reject();
            }
            return stripe.cancelSubscription(activeSubscription.id);
        }).then(function () {
            res.sendStatus(204);
        }).catch(function () {
            res.sendStatus(422);
        });
    },

    postFirstName: function (req, res, next) {
        userRepository.updateUserFirstName(req.session.userid, req.body.firstname).then(function () {
            res.sendStatus(204);
        }, function (err) {
            next(err);
        });
    },
    postLastName: function (req, res, next) {
        userRepository.updateUserLastName(req.session.userid, req.body.lastname).then(function () {
            res.sendStatus(204);
        }, function (err) {
            next(err);
        });
    },
    postZIP: function (req, res, next) {
        userRepository.updateUserZIP(req.session.userid, req.body.zip).then(function () {
            res.sendStatus(204);
        }, function (err) {
            next(err);
        });
    },
    postNewsletter: function (req, res, next) {
        userRepository.updateUserNewsletter(req.session.userid, req.body.newsletter).then(function () {
            res.sendStatus(204);
        }, function (err) {
            next(err);
        });
    },
    postUsername: function (req, res, next) {
        userRepository.isUsernameAvailable(req.body.username, req.session.userid).then(function (result) {
            if(!result) return Promise.reject({message: 'Username already used'});
            return userRepository.updateUserUsername(req.session.userid, req.body.username);
        }).then(function () {
            res.sendStatus(204);
        }, function (err) {
            res.status(200).json({
                error: err.message
            });
        });
    },
    postPassword: function (req, res, next) {
        let passwordCheck = false;
        userRepository.getUserByUserID(req.session.userid, false).then(function (user) {
            return userRepository.signInUser(user.username, req.body.oldpassword).then(function () {
                passwordCheck = true;
                return userRepository.updateUserPassword(req.session.userid, req.body.password);
            });
        }).then(function () {
            res.sendStatus(204);
        }, function (err) {
            if (!passwordCheck) {
                res.status(200).json({
                    error: 'Invalid current password'
                });
            }
            next(err);
        });
    },
    postSubscribe: function (req, res, next) {

        userRepository.isEmailSubscribed(req.body.email).then(
            function(zeroEmailCount){
                
                if(zeroEmailCount === false){
                    res.status(422).json({
                        error:'Email is already subscribed'
                    });
                }else{
                    // Email Doesn't exist. Create Subscription
                    return userRepository.addSubscription(req.body.firstname, req.body.lastname,
                        req.body.email,req.body.device,req.body.ip,req.body.location);
                }

            },
            function(error){
                console.log("Failure "+error);
                next(errors.serverError);
            }
        ).then(
            function(){
                res.status(200).json({
                    status: 'success'                    
                });
            },
            function(err){
                console.log(err);
                res.status(422).json({
                    error:'Error processing the request'
                });
            }
        );
    }

};