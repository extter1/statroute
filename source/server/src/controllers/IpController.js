const IPRepository = require('../repositories/IPRepository');

const parseScoringValues = function(data){
    let mapper = {
        "rush yards": "RushingYards_Factor",
        "receiving yards": "ReceivingYards_Factor",
        "pass yards": "PassingYards_Factor",
        "passing touchdowns": "PassingTouchdowns_Factor",
        "receptions": "Receptions_Factor",
        "qb Interceptions": "passingInterceptions_Factor"
    }
    let scoringValues = {};
    for(let i = 0; i< data.length; i++){
        let key = mapper[data[i].paramname];
        scoringValues[key] = data[i].assnvalue
    }
    return scoringValues;
}

const getScoringValuesForLeague = function(data, league = 'Default'){
    for(let i = 0; i< data.length; i++){
        if(data[i].leaguename.toLowerCase() === league.toLowerCase()){
            return data[i].scoringLevers;
        }
    }
    //Fallback to first scoring levers if no default or requested league name found
    return data[0].scoringLevers;
}

const getPlayers = function (req, res, next) {
    IPRepository.getScoringLevers(req).then(data => {
        let scoringValues = getScoringValuesForLeague(data, req.query.league);
        let promise;
        if(req.query.search) {
            promise = IPRepository.getSpecificPlayerRecordsWithNumberOfRecords(req, parseScoringValues(scoringValues));
        } else {
            promise = IPRepository.getPlayersWithNumberOfRecords(req, parseScoringValues(scoringValues));
        }
        promise.then(
            function (players) {
                res.status(200)
                    .json({
                        status: 'success',
                        data: players
                    });
            }, function (error) {
                next(error);
            }
        );

    });
};

const getPlayersAutoComplete = function (req, res, next) {

    IPRepository.getPlayerNamesAutoComplete(req).then(
        function (players) {
            res.status(200)
                .json({
                    status: 'success',
                    data: players
                });
        }, function (error) {
            next(error);
        }
    );
};

const getTeams = function (req, res, next) {

    IPRepository.getTeamsWithNumberOfRecords(req).then(
        function (teams) {
            res.status(200)
                .json({
                    status: 'success',
                    data: teams
                });
        }, function (error) {
            next(error);
        }
    )

};

const saveScoringLevers = function (req, res, next) {
    IPRepository.saveScoringLevers(req).then(
        function () {
            res.status(200)
                .json({
                    status: 'success',
                });
        }, function (error) {
            next(error);
        }
    )
}

const updateScoringLevers = function (req, res, next) {
    IPRepository.updateScoringLevers(req).then(
        function () {
            res.status(200)
                .json({
                    status: 'success',
                });
        }, function (error) {
            next(error);
        }
    )
}

const getScoringLevers = function (req, res, next) {
    IPRepository.getScoringLevers(req).then(
        function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data : data
                });
        }, function (error) {
            next(error);
        }
    )
}

const getDefaultScoringLevers = function (req, res, next) {
    IPRepository.getDefaultScoringLevers().then(
        function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data : data
                });
        }, function (error) {
            console.log(error);
            next(error);
        }
    )
}

module.exports = {
    getPlayers: getPlayers,
    getPlayersAutoComplete: getPlayersAutoComplete,
    saveScoringLevers: saveScoringLevers,
    getTeams: getTeams,
    saveScoringLevers: saveScoringLevers,
    getScoringLevers: getScoringLevers,
    updateScoringLevers: updateScoringLevers,
    getDefaultScoringLevers: getDefaultScoringLevers
};
