const router = require('express').Router();
const mailer = require('../mailer');

const validator = require('../middlewares/AuthMiddleware');
const controller = require('../controllers/AuthController');

router.post('/signup', validator.validateSignUp, controller.postSignUp);
router.post('/subscribe', controller.postSubscribe);

router.post('/signin', validator.validateSignIn, controller.postSignIn);
router.delete('/signin', validator.validateLoggedIn, controller.deleteSignIn);

router.get('/user', validator.validateLoggedIn, controller.getLoggedInUser);
router.post('/user/card', validator.validateLoggedIn, validator.validateSourceToken, controller.postUserCard);
router.post('/user/plan', validator.validateLoggedIn, validator.validateUserPlan, controller.postUserPlan);
router.delete('/user/plan', validator.validateLoggedIn, controller.deleteUserPlan);

router.post('/user/firstName', validator.validateLoggedIn, validator.validateFirstName, controller.postFirstName);
router.post('/user/lastName', validator.validateLoggedIn, validator.validateLastName, controller.postLastName);
router.post('/user/ZIP', validator.validateLoggedIn, validator.validateZIP, controller.postZIP);
router.post('/user/newsletter', validator.validateLoggedIn, validator.validateNewsletter, controller.postNewsletter);

router.post('/user/username', validator.validateLoggedIn, validator.validateUsername, controller.postUsername);
router.post('/user/password', validator.validateLoggedIn, validator.validatePasswordChange, controller.postPassword);


router.get('/complete-3D-secure-payment', controller.complete3DSecure);

router.get('/isEmailAvailable', validator.validateEmail, controller.getIsEmailAvailable);
router.get('/isUsernameAvailable', validator.validateUsername, controller.getIsUsernameAvailable);

router.post('/forgotPasswordRequest', validator.validateUsernameOrEmail, controller.postForgotPasswordRequest);
router.post('/forgotPasswordReset', validator.validateHash, validator.validatePassword, controller.postForgotPasswordReset);
router.get('/isForgotPasswordHashValid', validator.validateHash, controller.getForgotPasswordHashValid);


module.exports = router;
