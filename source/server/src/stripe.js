const Promise = require('promise');
const querystring = require('querystring');

const PaymentsUtils = require('./utils/PaymentsUtils');
const stripe = require('stripe')(PaymentsUtils.keys.SECRET);


module.exports = {
    stripe: stripe,
    createCustomer: function (token, email) {
        return new Promise(function (resolve, reject) {
            stripe.customers.create(
                {
                    description: 'Customer for ' + email,
                    source: token
                }, function (err, customer) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(customer);
                });
        });
    },
    getCustomer: function (customerId) {
        return new Promise(function (resolve, reject) {
            stripe.customers.retrieve(
                customerId,
                function (err, subscription) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(subscription);
                }
            );
        });
    },
    createSubscription: function (customerId, plan, coupon = null, trialEnd = 'now') {
        return new Promise(function (resolve, reject) {
            const reqObj = {
                customer: customerId,
                items: [
                    {
                        plan: plan,
                    },
                ],
                trial_end: trialEnd
            };
            if (coupon !== null) reqObj.coupon = coupon;

            stripe.subscriptions.create(
                reqObj, function (err, subscription) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(subscription);
                }
            );
        });
    },
    chargeCustomer: function (customerId, amount, description = null) {
        return new Promise(function (resolve, reject) {
            stripe.charges.create(
                {
                    amount: amount,
                    currency: 'usd',
                    description: description,
                    customer: customerId
                }, function (err, change) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(change);
                }
            );
        });
    },
    chargeSource: function (sourceId, amount, description = null) {
        return new Promise(function (resolve, reject) {
            stripe.charges.create(
                {
                    amount: amount,
                    currency: 'usd',
                    description: description,
                    source: sourceId
                }, function (err, charge) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(charge);
                }
            );
        });
    },
    refundCharge: function (chargeId) {
        return new Promise(function (resolve, reject) {
            stripe.refunds.create(
                {
                    charge: chargeId,
                }, function (err, charge) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(charge);
                }
            );
        });
    },
    addCustomerCard: function (customerId, token) {
        return new Promise(function (resolve, reject) {
            stripe.customers.createSource(
                customerId,
                {
                    source: token
                }, function (err, card) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(card);
                }
            );
        });
    },
    removeCustomerCard: function (customerId, sourceId) {
        return new Promise(function (resolve, reject) {
            stripe.customers.deleteCard(
                customerId,
                sourceId,
                function (err, confirmation) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(confirmation);
                }
            );
        });
    },
    create3DSecureSource: function (originalSourceId, amount, refund = false, subscriptionObj) {
        return new Promise(function (resolve, reject) {
            const params = {
                amount: amount
            };

            if (refund) params.refund = true;
            Object.assign(params, subscriptionObj);

            stripe.sources.create(
                {
                    amount: amount,
                    currency: 'usd',
                    type: 'three_d_secure',
                    three_d_secure: {
                        card: originalSourceId,
                    },
                    redirect: {
                        return_url: process.env.EMAIL_BASEURL + '/api/auth/complete-3D-secure-payment?'
                        + (querystring.stringify(params))
                    },
                }, function (err, three_d_secure_source) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(three_d_secure_source);
                }
            );
        });
    },

    getSource: function (sourceId) {
        return new Promise(function (resolve, reject) {
            stripe.sources.retrieve(
                sourceId,
                function (err, three_d_secure_source) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(three_d_secure_source);
                }
            );
        });
    },

    // upgradeUserPlan: function (customerId, subscriptionId, planName, couponCode,) {
    //     // return new Promise(function (resolve, reject) {
    //     //     stripe.subscriptions.del(subscriptionId,
    //     //         {
    //     //             at_period_end: true
    //     //         },
    //     //         function (err, subscription) {
    //     //             if (err) {
    //     //                 reject(err, true);
    //     //             }
    //     //             resolve(subscription);
    //     //         }
    //     //     );
    //     // })
    //     // .then((oldSubscription) => {
    //     //     return new Promise(function (resolve, reject) {
    //     //         // const reqObj = {
    //     //         //     customer: customerId,
    //     //         //     items: [
    //     //         //         {
    //     //         //             plan: planName,
    //     //         //         },
    //     //         //     ],
    //     //         //     trial_end: (oldSubscription.status === 'trialing' && oldSubscription.trial_end) || oldSubscription.current_period_end
    //     //         // };
    //     //         if (couponCode !== null) reqObj.coupon = couponCode;

    //     //         // stripe.subscriptions.create(
    //     //         //     reqObj,
    //     //         //     function (err, subscription) {
    //     //         //         if (err) {
    //     //         //             reject(err, true);
    //     //         //         }
    //     //         //         resolve(subscription);
    //     //         //     }
    //     //         // );
    //     //     });
    //     // });
    // },

    cancelSubscription: function (subscriptionId) {
        return new Promise(function (resolve, reject) {
            stripe.subscriptions.del(subscriptionId,
                {
                    at_period_end: true
                },
                function (err, subscription) {
                    if (err) {
                        reject(err, true);
                    }
                    resolve(subscription);
                }
            );
        });
    }

};