import React from 'react';
import Popup from 'react-popup';

import './Popups.css';

class Popups extends React.Component {

    componentDidMount() {
        this.paymentPopup();
    }

    paymentPopup() {
        Popup.registerPlugin('component', (component, closeOnOutsideClick = true, skipCurrentPopup = false) => {
            Popup.create({
                title: null,
                content: component,
                closeOnOutsideClick: closeOnOutsideClick
            }, skipCurrentPopup);
        });
    }

    render() {
        return (
            <Popup className="statroute-popup"/>
        );
    }
}

Popups.propTypes = {};

export default Popups;
