import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

import './PagePopup.css';

class PagePopup extends React.Component {

    constructor(props) {
        super(props);

        this.States = {
            LOADING: 1,
            ERROR: 2,
            LOADED: 3
        };

        this.state = {
            state: this.States.LOADING,
            data: ''
        };

        this._loadPageData(props.slug);
    }

    _loadPageData(slug) {
        axios.get('/api/pages/' + slug).then((res) => {
            this.setState({
                state: this.States.LOADED,
                data: res.data
            });
        }, () => {
            this.setState({
                state: this.States.ERROR
            });
        });
    }

    render() {
        return (
            <div className="Comp-PagePopup">
                <div className="article">
                    {this.state.state === this.States.LOADING
                    && <div>Loading...</div>}
                    {this.state.state === this.States.LOADED
                    && <div dangerouslySetInnerHTML={{__html: this.state.data}}/>}
                    {this.state.state === this.States.ERROR
                    && <div>Page not found</div>}
                </div>
            </div>
        );
    }
}

PagePopup.propTypes = {
    slug: PropTypes.string.isRequired
};

export default PagePopup;
