import React from 'react';
import PropTypes from 'prop-types';

import {importAll} from '../../../utils/Utils';

import './CardImage.css';

const cards = importAll(require.context('../../../resources/basic/images/card/dark', false, /\.(png|jpe?g|svg)$/));

class CardImage extends React.Component {

    render() {
        return (
            <span className="Comp-CardImage">
                <img src={cards[this.props.cardType]} alt={this.props.cardType}
                     title={this.props.cardType}/>
            </span>
        );
    }
}

CardImage.propTypes = {
    cardType: PropTypes.string.isRequired
};

export default CardImage;
