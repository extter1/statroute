import React from 'react';
import Popup from 'react-popup';
import PropTypes from 'prop-types';
import axios from 'axios';

import CardImage from './CardImage';
import NewBillingInformation from './NewBillingInformation/NewBillingInformation';
import IframePopup from './IframePopup';
import PopupMessage from '../../PopupMessage';

class CardInformation extends React.Component {

    constructor() {
        super();

        this.state = {
            error: null
        };
    }

    handleUpdatePaymentInfo(event) {
        event.preventDefault();

        Popup.plugins().component(
            <NewBillingInformation onSuccess={this.handleChangeCard.bind(this)} error={this.state.error}/>
            , false);
    }

    handleChangeCard(result) {
        const success = (
            <PopupMessage title={'Your card changed'}>
                <h2>Your billing information was successfully updated.</h2>
            </PopupMessage>
        );

        const error = (
            <PopupMessage title={'Error occurred'}>
                <h2>Error occurred while authorizing your card. Please try again later.</h2>
            </PopupMessage>
        );

        // TODO: send API request
        axios.post('/api/auth/user/card', {
            token: result.token,
            three_d_secure: result.three_d_secure
        }).then((res) => {
            // TODO: receive confirm or error from API
            if (res.data.error) {
                // TODO: pass error to Popup
                this.setState({
                    error: res.data.error
                });
            } else {
                Popup.close();
                if (res.data.data.three_d_secure) {
                    const three_d_secure = res.data.data.three_d_secure;

                    Popup.plugins().component(
                        <IframePopup src={three_d_secure.redirect.url} title="3D Secure validation"
                                     onClose={(result) => {
                                         this.props.onForceUserLoad();
                                         Popup.close();
                                         Popup.plugins().component((result)?success:error);
                                     }}/>, false, false);

                } else {
                    // TODO: force user information reload and close Popup
                    this.props.onForceUserLoad();
                    Popup.plugins().component(success);
                }
            }
        }, () => {
            Popup.close();
            Popup.plugins().component(error);
        });
    }


    render() {
        const hasCard = !!this.props.sources;

        return (
            <div className="Comp-CardInformation item">
                {hasCard
                && this.props.sources.map((source) => {
                    const card = source.card;
                    return (
                        <div key={source.id} className="left">
                            <span
                                className="lleft">{card.funding && card.funding.replace(/\b\w/, (c) => c.toLocaleUpperCase())}
                                Card</span>
                            <div className="rleft">
                                <div className="myacc_creditcard">
                                    <div className="top">
                                        <CardImage cardType={card.brand}/>
                                        <span className="card_numb">**** **** **** {card.last4}</span>
                                    </div>
                                    <div className="bottom hidden">
                                        <a href="#wip">Billing Details</a>
                                        <a href="#wip">Payment History</a>
                                        <a href="#wip">Export</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })}

                {!hasCard
                && <div className="left">
                    <span>No payment information</span>
                </div>}

                <div className="right">
                    <a href="#newCard" onClick={this.handleUpdatePaymentInfo.bind(this)}>Update Payment info</a>
                </div>
            </div>
        );
    }
}

CardInformation.propTypes = {
    sources: PropTypes.array,
    onForceUserLoad: PropTypes.func.isRequired,
};

export default CardInformation;
