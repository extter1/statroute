import React from 'react';
import Popup from 'react-popup';
import PropTypes from 'prop-types';
import moment from 'moment';

import Utils from '../../../utils/Utils';
import ChangePlan from './ChangePlan/ChangePlan';
import PopupMessage from '../../PopupMessage';

import './PlanDetail.css';

class PlanDetail extends React.Component {

    constructor() {
        super();

        this.state = {
            error: null
        };
    }

    handleUpdateUserPlan(event) {
        event.preventDefault();

        const hasSource = (Utils.getSafe(() => this.props.user.data.customer.sources.data) || []).length !== 0;

        Popup.plugins().component(
            <ChangePlan hasSource={hasSource} onSuccess={this.handleChangePlan.bind(this)} error={this.state.error}/>
            , false);
    }

    handleChangePlan(result) {

        const success = (
            <PopupMessage title={'Your plan changed'}>
                <h2>Your plan was successfully changed.</h2>
            </PopupMessage>
        );

        const error = (
            <PopupMessage title={'Error occurred'}>
                <h2>Error occurred while authorizing your card. Please try again later.</h2>
            </PopupMessage>
        );


        Popup.close();
        if (result !== null)
            Popup.plugins().component((result) ? success : error);
        if (result) {
            this.props.onForceUserLoad();
        }
    }

    render() {
        const activeSubscriptions = (Utils.getSafe(() => this.props.user.data.customer.subscriptions.data) || []).filter((sub) => {
            return !['canceled', 'unpaid'].includes(sub.status);
        }).reverse();

        const activeSubscription = activeSubscriptions.find((sub) => {
            return !['canceled', 'unpaid'].includes(sub.status) && !sub.cancel_at_period_end;
        });

        const hasSubscription = !!activeSubscriptions.length;
        const hasLifetime = (this.props.user.data.currentperiodstart && this.props.user.data.currentperiodend === null);

        return (
            <div className="Comp-PlanDetail item">
                <div className="left">
                    <span className="lleft">Active Plan</span>
                    <span className="rleft">
                        {!hasLifetime && hasSubscription && activeSubscriptions.map((subscription, index) => {
                            if(index < activeSubscriptions.length - ((activeSubscription) ? 2 : 1)) return null;
                            const codeoff = Utils.getSafe(() => subscription.discount.coupon.amount_off) || 0;
                            const amount = subscription.plan.amount - codeoff;

                            const ending = (subscription.cancel_at_period_end
                                && '(ending on ' + moment.unix(subscription.current_period_end).format('l') + ')');

                            const nextPayment = (!subscription.cancel_at_period_end
                                && '(next payment on ' + moment.unix((subscription.status === 'trialing' && subscription.trial_end) || subscription.current_period_end).format('l') + ')');

                            return (
                                <span key={subscription.id}
                                      className="rleft-inner">{subscription.plan.id.replace(/\b\w/, (c) => c.toLocaleUpperCase())}
                                    @ ${amount / 100} {ending} {nextPayment}</span>
                            );
                        })}
                        {hasLifetime && <span className="rleft-inner">Lifetime</span>}
                        {!hasSubscription && !hasLifetime && <span className="rleft-inner">No active plan</span>}
                    </span>
                </div>
                <div className="right">
                    {!hasLifetime && <a href="#change-plan" onClick={this.handleUpdateUserPlan.bind(this)}>Change plan</a>}
                </div>
            </div>
        );
    }
}

PlanDetail.propTypes = {
    user: PropTypes.object,
    onForceUserLoad: PropTypes.func.isRequired,
};

export default PlanDetail;
