import React from 'react';
import PropTypes from 'prop-types';

import './IframePopup.css';

class IframePopup extends React.Component {

    componentDidMount() {

        if (window.addEventListener) {
            window.addEventListener('message', this.onMessage.bind(this), false);
        }
        else if (window.attachEvent) {
            window.attachEvent('onmessage', this.onMessage.bind(this));
        }
    }

    componentWillUnmount() {
        if(window.removeEventListener) {
            window.removeEventListener('message', this.onMessage.bind(this), false);
        } else if(window.detachEvent) {
            window.detachEvent('onmessage', this.onMessage.bind(this));
        }
    }

    onMessage(event) {
        const data = event.data;
        if(data.func && data.func === 'closePopup') {
            this.props.onClose(data.success);
        }
    }


    render() {
        return (
            <div className="Comp-IframePopup">
                <iframe title={this.props.title} src={this.props.src} frameBorder="0"/>
            </div>
        );
    }
}

IframePopup.propTypes = {
    src: PropTypes.string.isRequired,
    title: PropTypes.string,
    onClose: PropTypes.func
};

export default IframePopup;
