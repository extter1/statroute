import React from 'react';
import PropTypes from 'prop-types';
import {Elements} from 'react-stripe-elements';

import PaymentForm from '../../../Auth/SignUp/SignUpPageThree/PaymentForm';

import './NewBillingInformation.css';

class NewBillingInformation extends React.Component {

    constructor() {
        super();

        this.state = {
            token: null,
            three_d_secure: false
        };

        this.getToken = this.getToken.bind(this);
    }

    validate(evt) {
        evt.preventDefault();

        this.paymentForm.getWrappedInstance().proceed().then((result) => {
            if(this.props.onSuccess) {
                this.props.onSuccess({
                    token: result.source.id,
                    three_d_secure: result.three_d_secure
                });
            }
            this.setState({
                token: result.source.id,
                three_d_secure: result.three_d_secure
            });
        }, () => {
            // payment error
        });
    }

    getToken() {
        return {
            token: this.state.token,
            three_d_secure: this.state.three_d_secure
        };
    }

    render() {
        return (
            <Elements>
                <div className="Comp-NewBillingInformation">
                    <p className="h1">Add new card</p>
                    <div className="clearfix"/>
                    <div className="block inputs">
                        <PaymentForm ref={(paymentForm) => this.paymentForm = paymentForm} error={this.props.error}/>
                        <button onClick={this.validate.bind(this)} type="button" className="buttonarrow">Confirm</button>
                        <div className="clearfix"/>
                    </div>
                </div>
            </Elements>
        );
    }
}

NewBillingInformation.propTypes = {
    onSuccess: PropTypes.func,
    error: PropTypes.object,
};

export default NewBillingInformation;
