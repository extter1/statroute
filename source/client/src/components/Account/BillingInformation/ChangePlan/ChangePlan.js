import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import Popup from 'react-popup';
import axios from 'axios';

import PlanSelection from '../../../Auth/SignUp/SignUpPageTwo/PlanSelection';
import PromoCode from '../../../Auth/SignUp/SignUpPageTwo/PromoCode';

import './ChangePlan.css';
import IframePopup from '../IframePopup';

class ChangePlan extends React.Component {

    constructor() {
        super();

        this.state = {
            plan: '',
            promoCode: null,

            planError: false,
            error: null
        };
    }

    validate(event) {
        event.preventDefault();

        if (!this.props.hasSource) this.props.onSuccess(null);

        let error = false;
        if (this.state.planError || !this.state.plan) {
            error = true;
            this.setState({
                planError: true
            });
        }

        if (!error) {
            this.setState({
                error: null
            });

            axios.post('/api/auth/user/plan', {
                plan: this.state.plan,
                promocode: this.state.promoCode
            }).then((res) => {
                if(res.data.error) {
                    this.setState({
                        error: res.data.error
                    });
                } else {
                    console.log(res.data.data);
                    if (res.data.data.three_d_secure) {
                        const three_d_secure = res.data.data.three_d_secure;

                        Popup.plugins().component(
                            <IframePopup src={three_d_secure.redirect.url} title="3D Secure validation"
                                         onClose={(result) => {
                                             Popup.close();
                                             this.props.onSuccess(result);
                                         }}/>, false, true);

                    } else {
                        this.props.onSuccess(true);
                    }
                }
            }, (error) => {
                this.setState({
                    error: error
                });
            });
        }
    }

    handlePlanChange(plan) {
        this.setState({
            plan: plan,
            planError: false
        });
    }

    handlePromoCodeChange(newCode) {
        this.setState({
            promoCode: newCode
        });
    }

    render() {
        const buttonClasses = ClassNames('buttonarrow', {
            'disabled': !this.props.hasSource
        });

        const title = (this.props.hasSource) ? null : 'You need to set your billing information first.';
        return (
            <div className="Comp-ChangePlan">
                <p className="h1">Change your plan</p>
                <div className="clearfix"/>
                <div className="block selectplan">
                    <PlanSelection hasPromeCode={!!this.state.promoCode} value={this.state.plan}
                                   onChange={this.handlePlanChange.bind(this)}/>
                    {this.state.planError &&
                    <p className="error">Please select one subscription plan from above.</p>}
                </div>
                <div className="block inputs" ref={(container) => {this.container = container}}>
                    <div className="item">
                        <PromoCode onChange={this.handlePromoCodeChange.bind(this)} value={this.state.promoCode}/>
                        <div className="clearfix"/>
                    </div>
                    <div className="item">
                        {!this.props.hasSource && <p className="error">{title}</p>}
                        {this.state.error !== null && <p className="error">{this.state.error.message}</p>}
                        <button ref={(button) => this.button = button}  onClick={this.validate.bind(this)} type="button"
                                className={buttonClasses}>{this.props.hasSource ? 'Confirm' : 'Close'}</button>
                        <div className="clearfix"/>
                    </div>
                </div>
            </div>
        );
    }
}

ChangePlan.propTypes = {
    hasSource: PropTypes.bool.isRequired,
    onSuccess: PropTypes.func.isRequired,
    error: PropTypes.object,
};

export default ChangePlan;
