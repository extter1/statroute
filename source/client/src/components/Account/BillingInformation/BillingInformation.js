import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Popup from 'react-popup';

import CardInformation from './CardInformation';
import PlanDetail from './PlanDetail';
import PopupMessage from '../../PopupMessage';
import Utils from '../../../utils/Utils';

import './BillingInformation.css';


class BillingInformation extends React.Component {

    render() {

        const hasLifetime = (this.props.user.data.currentperiodstart && this.props.user.data.currentperiodend === null);

        return (
            <div className="Comp-BillingInformation">
                <div className="container">

                    <h1>membership and billing</h1>

                    <div className="box">
                        <h2>Plan Details</h2>

                        <PlanDetail user={Utils.getSafe(() => this.props.user)}
                                    onForceUserLoad={this.props.onForceUserLoad}/>
                    </div>

                    <div className="box">
                        <h2>Billing Information</h2>

                        <CardInformation sources={Utils.getSafe(() => this.props.user.data.customer.sources.data)}
                                         onForceUserLoad={this.props.onForceUserLoad}/>
                    </div>

                    <div className="box">
                        <h2>Freeze account</h2>

                        <div className="item">
                            <div className="left">
                                <div className="lleft">Cancel your active plan</div>
                            </div>
                            <div className="right">
                                {!hasLifetime && <a href="#cancel-plan" onClick={(event) => {
                                    event.preventDefault();

                                    const success = (
                                        <PopupMessage title={'Your plan changed'}>
                                            <h2>Your plan was marked as canceled, your premium status will end at the
                                                end of this billing cycle.</h2>
                                        </PopupMessage>
                                    );

                                    const error = (
                                        <PopupMessage title={'Error occurred'}>
                                            <h2>Error occurred. Please try again later.</h2>
                                        </PopupMessage>
                                    );

                                    axios.delete('/api/auth/user/plan').then(() => {
                                        this.props.onForceUserLoad();
                                        Popup.plugins().component(success);
                                    }, () => {
                                        Popup.plugins().component(error);
                                    });
                                }}>
                                    Cancel plan
                                </a>}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

BillingInformation.propTypes = {
    user: PropTypes.object.isRequired,
    onForceUserLoad: PropTypes.func.isRequired,
};

export default BillingInformation;
