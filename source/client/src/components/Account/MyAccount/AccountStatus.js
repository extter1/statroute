import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

class AccountStatus extends React.Component {

    render() {
        const isActive = (this.props.user.data.status && this.props.user.data.status.toLocaleLowerCase() === 'active');

        return (
            <div className="Comp-AccountStatus">
                <h2>Account status</h2>
                <div className="item">
                    <div className="left">
                        {isActive && <span className="acc-active yes">Active</span>}
                        {!isActive && <span className="acc-active no">Non-active</span>}
                    </div>
                    {isActive && <div className="right">
                        <Link to="/account/billing">Edit Membership</Link>
                    </div>}
                </div>
            </div>
        );
    }
}

AccountStatus.propTypes = {
    user: PropTypes.object.isRequired,
};

export default AccountStatus;
