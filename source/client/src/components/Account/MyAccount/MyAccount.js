import React from 'react';
import PropTypes from 'prop-types';
import AccountStatus from './AccountStatus';
import SignInInformation from './SignInInformation';
import PersonalInformation from './PersonalInformation';

import './MyAccount.css';

class MyAccount extends React.Component {

    render() {

        return (
            <div className="Comp-MyAccount">
                <div className="container">
                    <h1>MY ACCOUNT</h1>

                    <div className="box">
                        <AccountStatus user={this.props.user}/>
                    </div>

                    <div className="box">
                        <SignInInformation user={this.props.user} onForceUserLoad={this.props.onForceUserLoad}/>
                        
                        <PersonalInformation user={this.props.user} onForceUserLoad={this.props.onForceUserLoad}/>
                    </div>
                </div>
            </div>
        );
    }
}

MyAccount.propTypes = {
    user: PropTypes.object.isRequired,
    onForceUserLoad: PropTypes.func.isRequired,
};

export default MyAccount;
