import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

class LastNamePopup extends React.Component {

    constructor(props) {
        super();

        this.state = {
            value: props.lastName || '',
            error: null
        };
    }

    handleSubmit(event) {
        event.preventDefault();

        axios.post('/api/auth/user/lastName', {
            lastname: this.state.value
        }).then((res) => {
            if(res.data.error) {
                this.setState({
                    error: res.data.error
                });
            } else {
                this.handleClose(true);
            }
        }, (err) => {
            this.setState({
                error: 'Invalid value'
            });
        });
    }

    handleChange(event) {
        this.setState({
            value: event.target.value
        });
    }

    handleClose(result) {
        if (this.props.onClose) {
            this.props.onClose(result);
        }
    }

    render() {
        return (
            <div className="Comp-LastNamePopup prompt-popup">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <label className="h2" htmlFor="lastName">Enter new last name:</label>
                    <input type="text" id="lastName" placeholder="Your last name"
                           value={this.state.value} onChange={this.handleChange.bind(this)}/>
                    {this.state.error !== null && <p className="error">{this.state.error}</p>}
                    <button type="submit">Save</button>
                    <button onClick={() => this.handleClose(false)} className="cancel" type="button">Cancel</button>
                </form>
            </div>
        );
    }
}

LastNamePopup.propTypes = {
    lastName: PropTypes.string.isRequired,
    onClose: PropTypes.func
};

export default LastNamePopup;
