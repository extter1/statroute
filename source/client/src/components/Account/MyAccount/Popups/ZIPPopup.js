import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

class ZIPPopup extends React.Component {

    constructor(props) {
        super();

        this.state = {
            value: props.zip || '',
            error: null
        };
    }

    handleSubmit(event) {
        event.preventDefault();

        axios.post('/api/auth/user/zip', {
            zip: this.state.value
        }).then((res) => {
            if(res.data.error) {
                this.setState({
                    error: res.data.error
                });
            } else {
                this.handleClose(true);
            }
        }, (err) => {
            this.setState({
                error: 'Invalid value'
            });
        });
    }

    handleChange(event) {
        if(event.target.value.length > 5) return;
        const val = event.target.value.replace(/[^0-9]/g, '');
        this.setState({
            value: val
        });
    }

    handleClose(result) {
        if (this.props.onClose) {
            this.props.onClose(result);
        }
    }

    render() {
        return (
            <div className="Comp-ZIPPopup prompt-popup">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <label className="h2" htmlFor="ZIP">Enter new ZIP:</label>
                    <input type="text" id="ZIP" placeholder="Your ZIP"
                           value={this.state.value} onChange={this.handleChange.bind(this)}/>
                    {this.state.error !== null && <p className="error">{this.state.error}</p>}
                    <button type="submit">Save</button>
                    <button onClick={() => this.handleClose(false)} className="cancel" type="button">Cancel</button>
                </form>
            </div>
        );
    }
}

ZIPPopup.propTypes = {
    zip: PropTypes.string.isRequired,
    onClose: PropTypes.func
};

export default ZIPPopup;
