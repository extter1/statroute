import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

class PasswordPopup extends React.Component {

    constructor(props) {
        super();

        this.state = {
            oldPassword: '',
            password: '',
            passwordAgain: '',
            error: null
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        if(this.state.password !== this.state.passwordAgain) {
            this.setState({
                error: 'New password fields doesn\'t match.'
            });
            return;
        }

        axios.post('/api/auth/user/password', {
            oldpassword: this.state.oldPassword,
            password: this.state.password,
        }).then((res) => {
            if(res.data.error) {
                this.setState({
                    error: res.data.error
                });
            } else {
                this.handleClose(true);
            }
        }, (err) => {
            this.setState({
                error: 'Invalid value'
            });
        });
    }

    handleChangeOldPassword(event) {
        this.setState({
            oldPassword: event.target.value
        });
    }
    handleChangePassword(event) {
        this.setState({
            password: event.target.value,
            error: null
        });
    }
    handleChangePasswordAgain(event) {
        this.setState({
            passwordAgain: event.target.value,
            error: null
        });
    }

    handleClose(result) {
        if (this.props.onClose) {
            this.props.onClose(result);
        }
    }

    render() {
        return (
            <div className="Comp-FirstNamePopup prompt-popup">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <label className="h2" htmlFor="oldPassword">Current password:</label>
                    <input type="password" id="oldPassword" placeholder="Your current password"
                           value={this.state.value} onChange={this.handleChangeOldPassword.bind(this)}/>

                    <label className="h2" htmlFor="password">Enter new password:</label>
                    <input type="password" id="password" placeholder="New password"
                           value={this.state.value} onChange={this.handleChangePassword.bind(this)}/>
                    <input type="password" id="passwordAgain" placeholder="New password again"
                           value={this.state.value} onChange={this.handleChangePasswordAgain.bind(this)}/>
                    {this.state.error !== null && <p className="error">{this.state.error}</p>}

                    <button type="submit">Save</button>
                    <button onClick={() => this.handleClose(false)} className="cancel" type="button">Cancel</button>
                </form>
            </div>
        );
    }
}

PasswordPopup.propTypes = {
    onClose: PropTypes.func
};

export default PasswordPopup;
