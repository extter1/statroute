import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

class NewsletterPopup extends React.Component {

    constructor(props) {
        super();

        this.state = {
            value: props.newsletter || false,
            error: null
        };
    }

    handleSubmit(event) {
        event.preventDefault();

        axios.post('/api/auth/user/newsletter', {
            newsletter: this.state.value
        }).then((res) => {
            if(res.data.error) {
                this.setState({
                    error: res.data.error
                });
            } else {
                this.handleClose(true);
            }
        }, (err) => {
            this.setState({
                error: err.message
            });
        });
    }

    handleChange(event) {
        this.setState({
            value: event.target.checked
        });
    }

    handleClose(result) {
        if (this.props.onClose) {
            this.props.onClose(result);
        }
    }

    render() {
        return (
            <div className="Comp-LastNamePopup prompt-popup">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <p className="h2">Newsletter settings</p>
                    <div className="checkbox">
                        <input type="checkbox" id="newsletter_123219412"
                               checked={this.state.value} onChange={this.handleChange.bind(this)}/>
                        <label htmlFor="newsletter_123219412"> receive newsletter</label>
                    </div>
                    {this.state.error !== null && <p className="error">{this.state.error}</p>}
                    <button type="submit">Save</button>
                    <button onClick={() => this.handleClose(false)} className="cancel" type="button">Cancel</button>
                </form>
            </div>
        );
    }
}

NewsletterPopup.propTypes = {
    newsletter: PropTypes.bool.isRequired,
    onClose: PropTypes.func
};

export default NewsletterPopup;
