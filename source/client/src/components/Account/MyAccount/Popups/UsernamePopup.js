import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

class UsernamePopup extends React.Component {

    constructor(props) {
        super();

        this.state = {
            value: props.username || '',
            error: null
        };
    }

    handleSubmit(event) {
        event.preventDefault();

        axios.post('/api/auth/user/username', {
            username: this.state.value
        }).then((res) => {
            if(res.data.error) {
                this.setState({
                    error: res.data.error
                });
            } else {
                this.handleClose(true);
            }
        }, (err) => {
            this.setState({
                error: 'Invalid value'
            });
        });
    }

    handleChange(event) {
        this.setState({
            value: event.target.value
        });
    }

    handleClose(result) {
        if (this.props.onClose) {
            this.props.onClose(result);
        }
    }

    render() {
        return (
            <div className="Comp-FirstNamePopup prompt-popup">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <label className="h2" htmlFor="username">Enter new username:</label>
                    <input type="text" id="username" placeholder="Your username"
                           value={this.state.value} onChange={this.handleChange.bind(this)}/>
                    {this.state.error !== null && <p className="error">{this.state.error}</p>}
                    <button type="submit">Save</button>
                    <button onClick={() => this.handleClose(false)} className="cancel" type="button">Cancel</button>
                </form>
            </div>
        );
    }
}

UsernamePopup.propTypes = {
    username: PropTypes.string.isRequired,
    onClose: PropTypes.func
};

export default UsernamePopup;
