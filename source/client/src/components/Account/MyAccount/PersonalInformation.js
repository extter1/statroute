import React from 'react';
import Popup from 'react-popup';
import ClassNames from 'classnames';
import PropTypes from 'prop-types';
import FirstNamePopup from './Popups/FirstNamePopup';
import LastNamePopup from './Popups/LastNamePopup';
import PopupMessage from '../../PopupMessage';
import ZIPPopup from './Popups/ZIPPopup';
import NewsletterPopup from './Popups/NewsletterPopup';

class PersonalInformation extends React.Component {

    handleClose(result) {
        Popup.close();
        if(result) {
            this.props.onForceUserLoad();
            Popup.plugins().component(
                <PopupMessage title={'Success'}>
                    <h2>Your information was successfully changed.</h2>
                </PopupMessage>
            );
        }
    }

    render() {
        return (
            <div className="Comp-PersonalInformation">
                <h2>Personal Information</h2>
                <div className="item">
                    <div className="left">
                        <span className="lleft">First Name</span>
                        <span className="rleft">{this.props.user.data.firstname}</span>
                    </div>
                    <div className="right">
                        {<a onClick={(evt) => {
                            evt.preventDefault();

                            Popup.plugins().component(
                                <FirstNamePopup firstName={this.props.user.data.firstname} onClose={this.handleClose.bind(this)}/>,
                            false);

                        }} href="#first-name">Change first name</a>}
                    </div>
                </div>
                <div className="item">
                    <div className="left">
                        <span className="lleft">Last Name</span>
                        <span className="rleft">{this.props.user.data.lastname}</span>
                    </div>
                    <div className="right">
                        {<a onClick={(evt) => {
                            evt.preventDefault();

                            Popup.plugins().component(
                                <LastNamePopup lastName={this.props.user.data.lastname} onClose={this.handleClose.bind(this)}/>,
                            false);

                        }} href="#last-name">Change last name</a>}
                    </div>
                </div>
                <div className="item">
                    <div className="left">
                        <span className="lleft">ZIP</span>
                        <span className="rleft">{this.props.user.data.zip}</span>
                    </div>
                    <div className="right">
                        {<a onClick={(evt) => {
                            evt.preventDefault();

                            Popup.plugins().component(
                                <ZIPPopup zip={this.props.user.data.zip} onClose={this.handleClose.bind(this)}/>,
                                false);

                        }} href="#zip">Change ZIP</a>}
                    </div>
                </div>
                <div className="item">
                    <div className="left">
                        <span className="lleft">Receive Newsletter</span>
                        <span className="rleft">
                                    <span className={ClassNames('acc-active', {
                                        yes: this.props.user.data.newsletter,
                                        no: !this.props.user.data.newsletter,
                                    })}>{this.props.user.data.newsletter ? 'Yes' : 'No'}</span>
                                </span>
                    </div>
                    <div className="right">
                        {<a onClick={(evt) => {
                            evt.preventDefault();

                            Popup.plugins().component(
                                <NewsletterPopup newsletter={this.props.user.data.newsletter} onClose={this.handleClose.bind(this)}/>,
                                false);

                        }} href="#newsletter">Change newsletter</a>}
                    </div>
                </div>
            </div>
        );
    }
}

PersonalInformation.propTypes = {
    user: PropTypes.object.isRequired,
    onForceUserLoad: PropTypes.func.isRequired,
};

export default PersonalInformation;
