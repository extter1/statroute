import React from 'react';
import Popup from 'react-popup';
import PropTypes from 'prop-types';
import UsernamePopup from './Popups/UsernamePopup';
import PopupMessage from '../../PopupMessage';
import PasswordPopup from './Popups/PasswordPopup';

class SignInInformation extends React.Component {

    handleClose(result) {
        Popup.close();
        if(result) {
            this.props.onForceUserLoad();
            Popup.plugins().component(
                <PopupMessage title={'Success'}>
                    <h2>Your information was successfully changed.</h2>
                </PopupMessage>
            );
        }
    }

    render() {
        return (
            <div className="Comp-SignInInformation">
                <h2>Sign in Information</h2>
                <div className="item">
                    <div className="left">
                        <span className="lleft">Username</span>
                        <span className="rleft">{this.props.user.data.username}</span>
                    </div>
                    <div className="right">
                        {<a onClick={(evt) => {
                            evt.preventDefault();

                            Popup.plugins().component(
                                <UsernamePopup username={this.props.user.data.username} onClose={this.handleClose.bind(this)}/>,
                                false);

                        }} href="#username">Change username</a>}
                    </div>
                </div>
                <div className="item">
                    <div className="left">
                        <span className="lleft">Password</span>
                        <span className="rleft">*********</span>
                    </div>
                    <div className="right">
                        {<a onClick={(evt) => {
                            evt.preventDefault();

                            Popup.plugins().component(
                                <PasswordPopup onClose={this.handleClose.bind(this)}/>,
                                false);

                        }} href="#password">Change password</a>}
                    </div>
                </div>
            </div>
        );
    }
}

SignInInformation.propTypes = {
    user: PropTypes.object.isRequired,
    onForceUserLoad: PropTypes.func.isRequired,
};

export default SignInInformation;
