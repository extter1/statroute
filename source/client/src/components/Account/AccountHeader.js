import React from 'react';
import {NavLink} from 'react-router-dom';

class AccountHeader extends React.Component {

    render() {
        return (
            <div className="Comp-AccountHeader">
                <div className="container myacc_zalozky_cont">
                    <div className="myacc_zalozky">
                        <NavLink exact to="/account">My account</NavLink>
                        <NavLink exact to="/account/billing">Membership and billing</NavLink>
                        {false && <NavLink exact to="/account/refer">Refer a friend</NavLink>}
                    </div>
                </div>
            </div>
        );
    }
}

AccountHeader.propTypes = {};

export default AccountHeader;
