import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';

import MyAccount from './MyAccount/MyAccount';
import ReferAFriend from './ReferAFriend/ReferAFriend';
import BillingInformation from './BillingInformation/BillingInformation';
import AccountHeader from './AccountHeader';

import './AccountRouter.css';

class AccountRouter extends React.Component {

    render() {
        return (
            <div className="Comp-AccountRouter">
                <section className="article myacc">
                    <AccountHeader/>
                    <Switch>
                        <Route exact path='/account/billing'
                               render={(props) => (
                                   <BillingInformation {...props} user={this.props.user} onForceUserLoad={this.props.onForceUserLoad}/>
                               )}/>
                        />
                        <Route exact path='/account/refer'
                               render={(props) => (
                                   <ReferAFriend {...props} user={this.props.user}/>
                               )}/>
                        />
                        <Route exact path='/account'
                               render={(props) => (
                                   <MyAccount {...props} user={this.props.user} onForceUserLoad={this.props.onForceUserLoad}/>
                               )}/>
                        />
                        <Redirect to='/account'/>
                    </Switch>
                </section>
            </div>
        );
    }
}

AccountRouter.propTypes = {
    user: PropTypes.object.isRequired,
    onForceUserLoad: PropTypes.func.isRequired,
};

export default AccountRouter;
