import React from 'react';

import twitter from '../../../resources/basic/images/twitter.png';
import facebook from '../../../resources/basic/images/facebook.png';

class ReferAFriend extends React.Component {

    render() {
        return (
            <div className="Comp-ReferAFriend">
                <div className="container">

                    <h1>Refer a friend</h1>

                    <div className="box">
                        <h2>Refer code</h2>
                        <div className="item">
                            <div className="left refer">
                                <input type="text" onClick={(evt) => evt.target.setSelectionRange(0, evt.target.value.length)} value="https://www.statroute.com/sign-up?refer_id=FDgfGfg4524F" readOnly/>

                                <div className="sto">
                                    <input type="text" placeholder="Generate refer link"/>
                                        <button type="submit">Generate</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="box">
                        <h2>Connect social media account</h2>
                        <div className="item">
                            <div className="left">
                                <span className="myacc_social"><img src={facebook} alt="Facebook" title="Facebook" /> Facebook account</span>
                            </div>
                            <div className="right">
                                <a href="#wip">Share</a>
                            </div>
                        </div>
                        <div className="item">
                            <div className="left">
                                <span className="myacc_social"><img src={twitter} alt="Twitter" title="Twitter" /> Twitter account</span>
                            </div>
                            <div className="right">
                                <a href="#wip">Share</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

ReferAFriend.propTypes = {};

export default ReferAFriend;
