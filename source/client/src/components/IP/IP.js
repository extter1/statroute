import React from 'react';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';
import ClassNames from 'classnames';
import axios from 'axios';

import Filters from "./Filters/Filters";
import Table from "./Table/Table";
import TableHeader from "./Table/TableHeader/TableHeader";

import './IP.css';

class IP extends React.Component {

    constructor() {
        super();
        this.state = {
            tableData: {
                columns: [
                    {
                        "title": "Rank",
                        "field": "rownum"
                    },
                    {
                        "title": "Player",
                        "field": "player"
                    },
                    {
                        "title": "Position",
                        "field": "positions"
                    },
                    {
                        "title": "Team",
                        "field": "teams"
                    },
                    {
                        "title": "Age",
                        "field": "age"
                    },
                    {
                        "title": "Bye Week",
                        "field": "bye"
                    },
                    {
                        "title": "Games",
                        "field": "gamesplayed"
                    },
                    {
                        "title": "Fantasy PPG",
                        "field": "fantasyppg"
                    },
                    {
                        "title": "Fantasy PTS",
                        "field": "ffp"
                    },
                    {
                        "title": "Ceiling",
                        "field": "pceiling"
                    },
                    {
                        "title": "Floor",
                        "field": "pfloor"
                    },
                    {
                        "title": "TD's",
                        "field": "tds"
                    },
                    {
                        "title": "Scrim. Yards",
                        "field": "scrimmageyards"
                    },
                    {
                        "title": "Scrim. YPG",
                        "field": "passyardsscrimmageypg"
                    },
                    {
                        "title": "Pass Yards",
                        "field": "pyd"
                    },
                    {
                        "title": "Rush YDS/GM",
                        "field": "ruyd"
                    },
                    {
                        "title": "REC. Yards",
                        "field": "reyd"
                    },
                    {
                        "title": "Receptions",
                        "field": "receptions"
                    },
                    {
                        "title": "Actions",
                        "field": "actions"
                    },
                    {
                        "title": "20+",
                        "field": "twentyplus"
                    },
                    {
                        "title": "40+",
                        "field": "fortyplus"
                    },
                    {
                        "title": "FUM",
                        "field": "fumbles"
                    },
                    {
                        "title": "FUML",
                        "field": "fumbleslost"
                    }
                ], rows: []
            },
            filters: this._getInitialFilters(),
            parameters: this._getInitialParameters(),
            nflp: true,
            compare: [],
            savedFilters: [],
            newcompare:[]
        };
    }

    _getInitialParameters() {
        return {
            '_page': 0,
            order: 'DESC',
            orderBy: false,
        };
    }

    _getInitialFilters() {
        return {
            search: '',
            searchName: '',
           // team: [],
            positions: [],
            season: '1_SEASON',
            splits: {
                WEEKS: [],
                QUARTERS: ['ALL'],
            },
            weather: {
                TEMPERATURES: ['ALL'],
                CONDITIONS: ['ALL']
            },
            fields: ['ALL'],
            versus: '',
            versus_val: '',
            versus_team: '',
        };
    }

    componentDidMount() {
        this._getRows();
                        const script = document.createElement("script");
script.id="hs-script-loader";
        script.src = "//js.hs-scripts.com/4413266.js";
        script.async = true;

        document.body.appendChild(script);
    }

    setRowNum(page, perPage, maxValue, order){
        if(order === 'ASC'){
            return this.getRowNumForDesc(page, perPage, maxValue);
        }else if(order === 'DESC'){
            return this.getRowNumForAsc(page, perPage, maxValue);
        }else{
            return this.getRowNumForAsc(page, perPage, maxValue);
        }
    }

    getRowNumForAsc(page, perPage, maxValue){
        let rowNum = [];
        let lastValue = page * perPage;
        let firstValue = (page * perPage) - perPage + 1;
        while(firstValue <= lastValue){
            if(firstValue > parseInt(maxValue)) break;
            rowNum.push({rownum: firstValue++});
        }
        return rowNum;
    }

    getRowNumForDesc(page, perPage, maxValue){
        let rowNum = [];
        let lastValue =  maxValue - (page - 1) * perPage - perPage + 1;
        let firstValue = maxValue - (page - 1) * perPage;
        while(lastValue <= firstValue){
            if(firstValue < 1) break;
            rowNum.push({rownum: firstValue--});
        }
        return rowNum;
     }
     
     mergeRows(data, rowList){
        return data.map((item, index) => {
            item.rownum = rowList[index].rownum;
            return item;
        });
     }
    

    _getRows(leaguename) {
        const parameters = Object.assign({}, this.state.filters, this.state.parameters);
        parameters.team = JSON.stringify(parameters.team);
        parameters.positions = JSON.stringify(parameters.positions);
        parameters.splits = JSON.stringify(parameters.splits);
        parameters.weather = JSON.stringify(parameters.weather);
        parameters.fields = JSON.stringify(parameters.fields);
        if(leaguename){
            parameters.league = leaguename;
        }
        if (parameters.orderBy === false) {
            delete parameters.orderBy;
            delete parameters.order;
        }
        delete parameters.searchName;
        delete parameters.versus_team;

        axios.get('/api/ip/' + ((this.state.nflp) ? 'players' : 'teams'), {
            params: parameters
        }).then((data) => {
            let rowNum = this.setRowNum(this.state.parameters._page + 1, 25, data.data.data.count, parameters.order);
            const pages = Math.ceil(data.data.data.count / 25);
            if (this.state.parameters._page > pages) {
                this.handleParametarsChange({
                    '_page': 0
                });
            }
            data.data.data.rows = this.mergeRows(data.data.data.rows, rowNum);
            this.setState({
                tableData: data.data.data
            });
        }).catch(() => {
            this.handleClearFilters();
        });
    }

    handleFiltersChange(newFilters, reset = false) {
        if (reset) {
            newFilters = Object.assign({}, this._getInitialFilters(), newFilters);
        } else {
            //newFilters.search = '';
        }
       if( newFilters.team != undefined){
       if( newFilters.team[0] == ""){
           delete newFilters.team;
       }
       }
        this.setState({
            filters: newFilters
        }, () => {
            this._getRows();
        });

    }

    handleSaveScoringLevers(leaguename){
        //console.log("scoring levers change");
        this._getRows(leaguename);
    }

    handleParametarsChange(newParameter) {
        this.setState((oldState) => {
            const newParameters = Object.assign({}, oldState.parameters, newParameter);
            return {
                parameters: newParameters
            };
        }, () => {
            this._getRows();
        });
    }

    handleClearFilters(forceReload = false) {
        if (!window._.isEqual(this.state.filters, this._getInitialFilters()) || forceReload) {
            this.handleFiltersChange(this._getInitialFilters());
        }

        if (!window._.isEqual(this.state.parameters, this._getInitialParameters()) || forceReload) {
            this.handleParametarsChange(this._getInitialParameters());
        }
    }

    handleModeChange(clearFilters = true) {
        this.setState((oldState) => {
            return {
                nflp: !oldState.nflp,
            };
        }, () => {
            if (clearFilters)
                this.handleClearFilters(true);
        });
    }

    handleCompareChange(playerObject, isInCompare = false) {
         console.log('xxxcompare', playerObject.columns);
        // console.log('xxxstate',this.state.nflp);
        // if(this.state.nflp){
        //     playerObject.columns = [
        //             {
        //                 "title": "Rank",
        //                 "field": "rownum"
        //             },
        //             {
        //                 "title": "Player",
        //                 "field": "player"
        //             },
        //             {
        //                 "title": "Position",
        //                 "field": "positions"
        //             },
        //             {
        //                 "title": "Team",
        //                 "field": "teams"
        //             },
        //             {
        //                 "title": "Age",
        //                 "field": "age"
        //             },
        //             {
        //                 "title": "Bye Week",
        //                 "field": "bye"
        //             },
        //             {
        //                 "title": "Games",
        //                 "field": "gamesplayed"
        //             },
        //             {
        //                 "title": "Fantasy PPG",
        //                 "field": "fantasyppg"
        //             },
        //             {
        //                 "title": "Fantasy PTS",
        //                 "field": "ffp"
        //             },
        //             {
        //                 "title": "Ceiling",
        //                 "field": "pceiling"
        //             },
        //             {
        //                 "title": "Floor",
        //                 "field": "pfloor"
        //             },
        //             {
        //                 "title": "TD's",
        //                 "field": "tds"
        //             },
        //             {
        //                 "title": "Pass Yards",
        //                 "field": "pyd"
        //             },
        //             {
        //                 "title": "Rush YDS/GM",
        //                 "field": "ruyd"
        //             },
        //             {
        //                 "title": "REC. Yards",
        //                 "field": "reyd"
        //             },
        //             {
        //                 "title": "Receptions",
        //                 "field": "receptions"
        //             },
        //             {
        //                 "title": "Actions",
        //                 "field": "actions"
        //             },
        //             {
        //                 "title": "Pass Attempts",
        //                 "field": "passingattempts"
        //             },
        //             {
        //                 "title": "Pass Intercept",
        //                 "field": "totalinterceptions"
        //             },
        //             {
        //                 "title": "Rush Attempts",
        //                 "field": "reydperattempt"
        //             },
        //             {
        //                 "title": "Pass Receptions",
        //                 "field": "fumbleslost"
        //             },
        //             {
        //                 "title": "Re Targets",
        //                 "field": "fumbleslost"
        //             },
        //             {
        //                 "title":"Field Attempts",
        //                 "field":"fieldgoalmade"
        //             },
        //             {
        //                 "title":"30+",
        //                 "field":"fgthirtyplus"
        //             },
        //             {
        //                 "title":"40+",
        //                 "field":"fgfortyplus"
        //             },
        //             ,
        //             {
        //                 "title":"50+",
        //                 "field":"fgfiftyplus"
        //             }
        //         ];
        // };
        this.setState((oldState) => {
            const compare = oldState.compare.slice();
            const isChecked = compare.reduce((res, rowObject) => {
                return res || window._.isEqual(rowObject, playerObject);
            }, false);

            if (isInCompare && !isChecked) {
                compare.push(playerObject);
            } else if (!isInCompare && isChecked) {
                compare.splice(compare.indexOf(playerObject), 1);
            } else return {};

            if (compare.length === 0 && !isInCompare) {
                window.popesClose();
            }

            return {
                compare: compare
            };
        });
    }

    handleOrderChange(key) {
        this.setState((oldState) => {
            let order = oldState.parameters.order;
            let orderBy = oldState.parameters.orderBy;

            if (!key) {
                order = 'DESC';
                orderBy = false;
            } else if (orderBy === key) {
                if (order === 'DESC') {
                    order = 'ASC';
                } else {
                    order = 'DESC';
                    orderBy = false;
                }
            } else {
                order = 'DESC';
                orderBy = key;
            }

            return {
                parameters: Object.assign({}, oldState.parameters, {
                    order: order,
                    orderBy: orderBy
                })
            }
        }, () => {
            this._getRows();
        });
    }

    render() {
        const classes = ClassNames('compare-button', {
            'popes-open': (this.state.compare.length >= 2),
            'disabled': !(this.state.compare.length >= 2) && this.props.user.premium,
            'compare-button_get_premium': !this.props.user.premium
        });

        const title = ((this.props.user.premium) ?
            ((this.state.compare.length < 2) ? 'You must select at least two players for compare function to work.' : null)
            : 'Compare functionality is for premium users only.');
            //let historicalPlayers = '';
            //if([4].includes(this.props.user.data.accounttype)){
                let historicalPlayers = (
                    <div className="historicalPlayersWrapper">
                        <input 
                            name="historicalPlayers" 
                            type="checkbox" 
                            id="historicalPlayers"
                            className="table-checkbox"
                            checked={this.state.showHistoricalPlares} 
                            onChange={this.handleInputChange} 
                        />
                        <label className="table-checkbox-label" htmlFor="historicalPlayers"> 
                        <span className="historicalPlayersText">Show Historical Players</span>
                        </label>
                    </div>
                );
            //}
        return (
            <DocumentTitle title="Stats IP - StatRoute">
                <div className="mainwrap stat-ip">
                    <a className={classes} data-original-title={title} data-toggle="tooltip" data-placement="left"
                       data-popid="pop-compare">COMPARE selected</a>

                    <Filters filters={this.state.filters} nflp={this.state.nflp} premium={this.props.user.premium}
                             onModeChange={this.handleModeChange.bind(this)}
                             onFiltersChange={this.handleFiltersChange.bind(this)}/>
                    <div className="mainright">
                    {historicalPlayers}
                        <TableHeader filters={this.state.filters} nflp={this.state.nflp}
                                     premium={this.props.user.premium}
                                     onScoringLeversSave={this.handleSaveScoringLevers.bind(this)}
                                     accounttype={this.props.user.data.accounttype}
                                     onClearFilters={this.handleClearFilters.bind(this)}
                                     onModeChange={this.handleModeChange.bind(this)}
                                     onFiltersChange={this.handleFiltersChange.bind(this)}
                        />
                        <Table tableData={this.state.tableData} compare={this.state.compare} newcompare= {this.state.newcompare}
                               filters={this.state.filters} premium={this.props.user.premium}  accounttype={this.props.user.data.accounttype}
                               parameters={this.state.parameters}
                               onCompareChange={this.handleCompareChange.bind(this)}
                               onOrderChange={this.handleOrderChange.bind(this)}
                               onParameterChange={this.handleParametarsChange.bind(this)}
                        />
                    </div>
                </div>
            </DocumentTitle>
        );
    }
}

IP.propTypes = {
    user: PropTypes.object.isRequired
};

export default IP;

