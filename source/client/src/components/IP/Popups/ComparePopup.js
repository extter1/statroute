import React from 'react';
import PropTypes from 'prop-types';
import {CSSTransitionGroup} from 'react-transition-group';

import teams from '../../../utils/parameters/teams';
import newcompares from '../../../utils/parameters/compare';
import GenerateTitleFromFilter from '../../../utils/GenerateTitleFromFilter';

import './CompatePopup.css';

const jQuery = window.jQuery;

class ComparePopup extends React.Component {

    render() {
        var nflpcompare = newcompares.nflpcompare;
        var nfltcompare = newcompares.nfltcompare;
        jQuery(this.popup).trigger('render.popups');
        return (
            <div className="popes-content" id="pop-compare" ref={(popup) => this.popup = popup}>
                <div className={'pop-inner compare-selected' + ((!this.props.premium) ? ' get_premium' : '')}>
                    <a className="popes-close"><span className="sr-only">Close popup</span></a>

                    {this.props.premium && <div className="getpremiumbox">
                        <div className="box">
                            <p>This awesome feature is available for <strong>Premium Users</strong> only. Want access?
                            </p>
                            <a href="#premium">GET PREMIUM</a>
                        </div>
                    </div>}

                    <CSSTransitionGroup
                        transitionName="compare-popup"
                        transitionEnterTimeout={500}
                        transitionLeaveTimeout={300}>

                        {this.props.compare.map((compareObject) => {
                            return (
                                <div key={compareObject.row.rownum} className="compare_box">
                                    <div className="name">
                                        <a onClick={(evt) => {
                                            this.props.onCompareChange(compareObject, false);
                                        }} className="box-close"><span
                                            className="sr-only">Remove from compare</span></a>
                                        <span>{compareObject.row.player} {compareObject.row.team}</span>
                                        <p>
                                            {compareObject.row.tpposition}{(teams[compareObject.row.teams]) && teams[compareObject.row.teams].fullName}<br/>
                                            Data Set Rank {compareObject.row.rownum}
                                        </p>
                                    </div>
                                    <div className="vs">
                                        <span>{GenerateTitleFromFilter(compareObject.filters)}</span>
                                    </div>
                                    <div className="data">
                                        {compareObject.columns.map((column,index) => {
                                            var isStatic = ['fantasyppg', 'ffp', 'pceiling','pfloor'].includes(nflpcompare[index].field);
                                            console.log(compareObject.filters);
                                            var nflp = (compareObject.row['team'] == undefined);
                                            if(nflp){
                                            if(compareObject.row['positions'] == 'QB'){
                                               var isStatic = ['fantasyppg', 'ffp', 'pceiling','pfloor','pyd','tds','passingattempts','totalinterceptions'].includes(nflpcompare[index].field);
                                            }
                                                 if(compareObject.row['positions'] == 'RB'){
                                               var isStatic = ['fantasyppg', 'ffp', 'pceiling','pfloor','ruyd','tds','reydperattempt','totalinterceptions'].includes(nflpcompare[index].field);
                                            }
                                                     if(compareObject.row['positions'] == 'WR' ||compareObject.row['positions'] == 'TE'){
                                               var isStatic = ['fantasyppg', 'ffp', 'pceiling','pfloor','reyd','tds','fumbleslost','fumbleslost'].includes(nflpcompare[index].field);
                                            }
                                                       if(compareObject.row['positions'] == 'K'){
                                               var isStatic = ['fantasyppg', 'ffp', 'pceiling','pfloor','fieldgoalmade','fgthirtyplus','fgfiftyplus','fgfortyplus'].includes(nflpcompare[index].field);
                                            }
                                        }
                                        else{
                                            if(compareObject.filters.positions[0] !=undefined){
                                                console.log(compareObject.filters.positions[0]);
                                            if(compareObject.filters.positions[0] == 'DEF'){
                                               var isStatic = ['dppg', 'sacks', 'dffpg','safeties'].includes(nfltcompare[index].field);
                                            }
                                                 if(compareObject.filters.positions[0] == 'PAS'){
                                               var isStatic = ['pyardsallowed', 'deftds', 'intercepts','dffpg'].includes(nfltcompare[index].field);
                                               console.log(index);
                                            }
                                                     if(compareObject.filters.positions[0] == 'RUS'){
                                               var isStatic = ['ryardsallowed', 'yardsallowed', 'dffpg','safeties'].includes(nfltcompare[index].field);
                                            }
                                                       if(compareObject.filters.positions[0] == 'REC'){
                                               var isStatic = true;   }
                                                          if(compareObject.filters.positions[0] == 'KIC'){
                                               var isStatic =true  }
                                                            if(compareObject.filters.positions[0] == 'OFF'){
                                               var isStatic = ['yardsallowed', 'deftds', 'dffpg'].includes(nfltcompare[index].field);
                                            }
                                            }
                                            else{
                                                var isStatic= true;
                                            }
                                        }
                                            if(isStatic && nflp){
                                            return (
                                                <div key={column.field} className="item">
                                                    <div className="left">{nflpcompare[index].title}</div>
                                                    <div className="right">{compareObject.row[column.field]}</div>
                                                </div>
                                            );
                                            }
                                            if(isStatic && !nflp){
                                                    return (
                                                <div key={column.field} className="item">
                                                    <div className="left">{nfltcompare[index].title}</div>
                                                    <div className="right">{compareObject.row[column.field]}</div>
                                                </div>
                                            );
                                            }
                                        })}
                                    </div>
                                    {/*<a href="#add" className="add"><img src="images/add.png" alt="" title=""/> ADD TO MY
                                        LINEUP</a>*/}
                                </div>
                            );
                        })}
                    </CSSTransitionGroup>
                </div>
            </div>
        );
    }
}

ComparePopup.propTypes = {
    premium: PropTypes.bool.isRequired,
    compare: PropTypes.array.isRequired,
    onCompareChange: PropTypes.func.isRequired,
};

export default ComparePopup;