import React from 'react';
import PropTypes from 'prop-types';

import teams from '../../../utils/parameters/teams';

import './Teams.css';

class Teams extends React.Component {
    constructor() {
        super();
        this.state = {
            filterVal :[],
            filterName :''
        }
    }

    handleTeamChange(team) {
        const teamVal =[];
        teamVal.push(team);
        const newFilters = Object.assign({}, this.props.filters, {
            team: teamVal
        });
        this.props.onFiltersChange(newFilters);
        this.setState({
            filterVal: team,
            filterName:team
        });
        console.log(this.state.filterVal);
    }
    handleRegionChange(team) {
        // const newFilters = Object.assign({}, this.props.filters, {
        //     team: team
        // });
        // this.props.onFiltersChange(newFilters);
        if(team =='afc'){
            var regionteams = ['BUF','MIA','NEW','NYJ','BAL','CIN','CLE','PIT','DEN','KAN','LAC','OAK','JAX','TEN','HOU','IND'];
              this.setState({
            filterName: 'AFC'
        });
        }
        if(team =='afceast'){
            var regionteams = ['BUF','MIA','NEW','NYJ'];
                     this.setState({
            filterName: 'AFC EAST'
        });
        }
        if(team=='afcnorth'){
            var regionteams = ['BAL','CIN','CLE','PIT'];
                          this.setState({
            filterName: 'AFC NORTH'
        });
        }
        if(team=='afcwest'){
            var regionteams = ['DEN','KAN','LAC','OAK'];
                          this.setState({
            filterName: 'AFC WEST'
        });
        }
        if(team=='afcsouth'){
            var regionteams = ['JAX','TEN','HOU','IND'];
                          this.setState({
            filterName: 'AFC SOUTH'
        });
        }
        if(team=='nfc'){
            var regionteams = ['DAL','NYG','PHI','WAS','DET','CHI','MIN','GNB','ARI','LAR','SFO','SEA','ATL','NOR','TAM','CAR'];
                          this.setState({
            filterName: 'NFC'
        });
        }
        if(team=='nfceast'){
            var regionteams = ['DAL','NYG','PHI','WAS'];
                          this.setState({
            filterName: 'NFC EAST'
        });
        }
        if(team=='nfcnorth'){
            var regionteams = ['DET','CHI','MIN','GNB'];
                          this.setState({
            filterName: 'NFC NORTH'
        });
        }
        if(team=='nfcwest'){
            var regionteams = ['ARI','LAR','SFO','SEA'];
                          this.setState({
            filterName: 'NFC WEST'
        });
        }
        if(team=='nfcsouth'){
            var regionteams = ['ATL','NOR','TAM','CAR'];
                          this.setState({
            filterName: 'NFC SOUTH'
        });
        }
        this.setState({
            filterVal: regionteams
        });
              const newFilters = Object.assign({}, this.props.filters, {
            team: regionteams
        });
        console.log(newFilters);
        this.props.onFiltersChange(newFilters);
    }
    handleNewRegionChange(evt) {
        const newTeam = evt.target.getAttribute('data-region');
        console.log(newTeam);
         this.handleRegionChange(newTeam);
    }
    handleNewTeamChange(evt) {
        const newTeam = (evt.target.checked) ? evt.target.value : '';
        this.handleTeamChange(newTeam);
    }

    render() {
        const afceastTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'afceast');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
       const afcnorthTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'afcnorth');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
             const afcwestTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'afcwest');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
             const afcsouthTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'afcsouth');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
        const nfceastTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'nfceast');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
     const nfcnorthTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'nfcnorth');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
             const nfcwestTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'nfcwest');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
             const nfcsouthTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'nfcsouth');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
        return (
            <div className="Comp-Teams">
                <div className="navwrap">
                    <a className="navwrap_main-a">TEAMS</a>
                    <div className="navcontent_1 teams-1 halfline">
                        <div className="col">
                            <span className="slash-name team-title" onClick={this.handleNewRegionChange.bind(this)} data-region="afc">Afc</span>
                             <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="afceast">AFC EAST</span>
                            {afceastTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                        <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr +' '+ 'teamCheck-'+teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                    <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="afcnorth">AFC NORTH</span>
                            {afcnorthTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                         <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                             <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="afcwest">AFC WEST</span>
                            {afcwestTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                       <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                              <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="afcsouth">AFC SOUTH</span>
                            {afcsouthTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                      <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                        </div>
                        <div className="col">
                            <span className="slash-name team-title" onClick={this.handleNewRegionChange.bind(this)} data-region="nfc">NFC</span>
                            <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="nfceast">NFC EAST</span>
                            {nfceastTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                       <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                         <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="nfcnorth">NFC NORTH</span>
                            {nfcnorthTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                        <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                          <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="nfcwest">NFC WEST</span>
                            {nfcwestTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                        <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                            <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="nfcsouth">NFC SOUTH</span>
                            {nfcsouthTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                        <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                        </div>
                        <button onClick={() => {
                            // close menu
                        }} className="ok-button">OK
                        </button>
                    </div>
                </div>
                {this.props.filters.team
                && <span className={'leftfilter teamColor-'}>{this.state.filterName} <a
                    onClick={() => this.handleTeamChange('')}><span
                    className="sr-only">Remove Filter</span></a></span>}
            </div>
        );
    }
}

Teams.propTypes = {
    filters: PropTypes.object.isRequired,
    onFiltersChange: PropTypes.func.isRequired,
};

export default Teams;