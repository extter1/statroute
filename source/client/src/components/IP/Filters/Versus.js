import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';

import versusTypes from '../../../utils/parameters/versusTypes';
import teams from '../../../utils/parameters/teams';

class Versus extends React.Component {
    constructor() {
        super();
        this.state = {
            filterVal :[]
        }
    }

    handleVersusChange(type, value, teamAbbr) {
        const newFilters = Object.assign({}, this.props.filters, {
            versus: type,
            versus_val: value,
            versus_team: teamAbbr
        });
        this.props.onFiltersChange(newFilters);
    }

    handleNewVersusChange(evt, type, value, teamAbbr) {
        if (evt.target.checked) {
            this.handleVersusChange(type, value, teamAbbr);
        } else {
            this.handleVersusChange('', '', '');
        }
    }
    handleTeamChange(team) {
        const newFilters = Object.assign({}, this.props.filters, {
            team: team
        });
        this.props.onFiltersChange(newFilters);
        this.setState({
            filterVal: team
        });
        console.log(this.state.filterVal);
    }
    handleRegionChange(team) {
        const newFilters = Object.assign({}, this.props.filters, {
            team: team
        });
        this.props.onFiltersChange(newFilters);
        if(team =='afc'){
            var regionteams = ['BUF','MIA','NEW','NYJ','BAL','CIN','CLE','PIT','DEN','KAN','LAC','OAK','JAX','TEN','HOU','IND'];
        }
        if(team =='afceast'){
            var regionteams = ['BUF','MIA','NEW','NYJ'];
        }
        if(team=='afcnorth'){
            var regionteams = ['BAL','CIN','CLE','PIT'];
        }
        if(team=='afcwest'){
            var regionteams = ['DEN','KAN','LAC','OAK'];
        }
        if(team=='afcsouth'){
            var regionteams = ['JAX','TEN','HOU','IND'];
        }
        if(team=='nfc'){
            var regionteams = ['DAL','NYG','PHI','WAS','DET','CHI','MIN','GNB','ARI','LAR','SFO','SEA','ATL','NOR','TAM','CAR'];
        }
        if(team=='nfceast'){
            var regionteams = ['DAL','NYG','PHI','WAS'];
        }
        if(team=='nfcnorth'){
            var regionteams = ['DET','CHI','MIN','GNB'];
        }
        if(team=='nfcwest'){
            var regionteams = ['ARI','LAR','SFO','SEA'];
        }
        if(team=='nfcsouth'){
            var regionteams = ['ATL','NOR','TAM','CAR'];
        }
        this.setState({
            filterVal: regionteams
        });
    }
    handleNewRegionChange(evt) {
        const newTeam = evt.target.getAttribute('data-region');
        console.log(newTeam);
         this.handleRegionChange(newTeam);
    }
    handleNewTeamChange(evt) {
        const newTeam = (evt.target.checked) ? evt.target.value : '';
        this.handleTeamChange(newTeam);
    }

    render() {
        const afceastTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'afceast');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
       const afcnorthTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'afcnorth');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
             const afcwestTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'afcwest');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
             const afcsouthTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'afcsouth');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
        const nfceastTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'nfceast');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
     const nfcnorthTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'nfcnorth');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
             const nfcwestTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'nfcwest');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
             const nfcsouthTeams = Object.keys(teams).filter((teamAbr) => {
            return (teams[teamAbr].team == 'nfcsouth');
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });
        const afcTeams = Object.keys(teams).filter((teamAbr) => {
            return teams[teamAbr].AFC;
        }).sort((teamAbr1, teamAbr2) => {
            return teams[teamAbr1].index - teams[teamAbr2].index;
        });

        // const nfcTeams = Object.keys(teams).filter((teamAbr) => {
        //     return !teams[teamAbr].AFC;
        // }).sort((teamAbr1, teamAbr2) => {
        //     return teams[teamAbr1].index - teams[teamAbr2].index;
        // });

        const classes = ClassNames('navwrap', {
            'nav_get_premium': !this.props.premium
        });

        return (
            <div className="Comp-Versus">
                <div className={classes}>
                    <a className="navwrap_main-a">VERSUS</a>
                    <div className="navcontent_1 versus-1">
                        <div className="col">
                            {Object.keys(versusTypes).map((versusTypeKey) => {
                                return (
                                    <div key={versusTypeKey} className="navwrap2">
                                        <div className="navwrap_main-a" key={versusTypeKey}>
                                            <input checked={this.props.filters.versus === versusTypeKey}
                                                   type="checkbox"/>
                                            <label>{versusTypes[versusTypeKey]}</label>
                                        </div>
                                        <div className="navcontent_2 halfline">
                                        <div className="col">
                            <span className="slash-name team-title" onClick={this.handleNewRegionChange.bind(this)} data-region="afc">Afc</span>
                             <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="afceast">AFC EAST</span>
                            {afceastTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                        <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr +' '+ 'teamCheck-'+teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                    <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="afcnorth">AFC NORTH</span>
                            {afcnorthTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                         <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                             <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="afcwest">AFC WEST</span>
                            {afcwestTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                       <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                              <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="afcsouth">AFC SOUTH</span>
                            {afcsouthTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                      <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                        </div>
                        <div className="col">
                            <span className="slash-name team-title" onClick={this.handleNewRegionChange.bind(this)} data-region="nfc">NFC</span>
                            <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="nfceast">NFC EAST</span>
                            {nfceastTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                       <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                         <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="nfcnorth">NFC NORTH</span>
                            {nfcnorthTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                        <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                          <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="nfcwest">NFC WEST</span>
                            {nfcwestTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                        <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                                            <span className="slash-name slash-name1" onClick={this.handleNewRegionChange.bind(this)} data-region="nfcsouth">NFC SOUTH</span>
                            {nfcsouthTeams.map((teamAbbr) => {
                                return (
                                    <div key={teamAbbr}>
                                        <input checked={this.state.filterVal.includes(teamAbbr)}
                                               id={'team_' + teamAbbr} value={teamAbbr} type="checkbox"
                                               name="positions[]" onChange={this.handleNewTeamChange.bind(this)}/>
                                        <label className={'team-' + teamAbbr} htmlFor={'team_' + teamAbbr}>{teams[teamAbbr].fullName}</label>
                                    </div>
                                );
                            })}
                        </div>
                                            {/* <div className="col">
                                                <span className="slash-name">Afc</span>
                                                {afcTeams.map((teamAbr) => {
                                                    const text = (versusTypeKey === 'TEAM')
                                                        ? teams[teamAbr].fullName : (teams[teamAbr][versusTypeKey] + ', ' + teamAbr);
                                                    const value = (versusTypeKey === 'TEAM')
                                                        ? teamAbr : (teams[teamAbr][versusTypeKey]);
                                                    return (
                                                        <div key={teamAbr}>
                                                            <input checked={this.props.filters.versus_val === value}
                                                                   id={'vs' + versusTypeKey + '_' + teamAbr} value={teamAbr} type="checkbox"
                                                                   name="positions[]"
                                                                   onChange={(evt) => this.handleNewVersusChange(evt, versusTypeKey, value, teamAbr)}/>
                                                            <label className={'team-' + teamAbr} htmlFor={'vs' + versusTypeKey + '_' + teamAbr}>{text}</label>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                            <div className="col">
                                                <span className="slash-name">Nfc</span>
                                                {nfcTeams.map((teamAbr) => {
                                                    const text = (versusTypeKey === 'TEAM')
                                                        ? teams[teamAbr].fullName : (teams[teamAbr][versusTypeKey] + ', ' + teamAbr);
                                                    const value = (versusTypeKey === 'TEAM')
                                                        ? teamAbr : (teams[teamAbr][versusTypeKey]);
                                                    return (
                                                        <div key={teamAbr}>
                                                            <input checked={this.props.filters.versus_val === value}
                                                                   id={'vs' + versusTypeKey + '_' + teamAbr} value={teamAbr} type="checkbox"
                                                                   name="positions[]"
                                                                   onChange={(evt) => this.handleNewVersusChange(evt, versusTypeKey, value, teamAbr)}/>
                                                            <label className={'team-' + teamAbr} htmlFor={'vs' + versusTypeKey + '_' + teamAbr}>{text}</label>
                                                        </div>
                                                    );
                                                })}
                                            </div> */}
                                            <button onClick={() => {
                                                // close
                                            }} className="ok-button">OK
                                            </button>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
                {this.props.filters.versus
                && <span className={'leftfilter teamColor-'+this.props.filters.versus_team}>vs. {(this.props.filters.versus === 'TEAM') ? teams[this.props.filters.versus_val].fullName : (this.props.filters.versus + ' ' + this.props.filters.versus_val)} <a
                    onClick={() => this.handleVersusChange('', '')}><span
                    className="sr-only">Remove Filter</span></a></span>}
            </div>
        );
    }
}

Versus.propTypes = {
    filters: PropTypes.object.isRequired,
    onFiltersChange: PropTypes.func.isRequired,
    premium: PropTypes.bool.isRequired,
};

export default Versus;