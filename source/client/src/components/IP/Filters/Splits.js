import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';

import splits from '../../../utils/parameters/splits';

import './Splits.css';

class Splits extends React.Component {

    handleWeeksChange(weeks) {
        // if (weeks.length > 0) this.handleQuartersChange(['ALL']);

        let season = this.props.filters.season;
        if (weeks.length > 0 && (!season.includes('SEASON') || season === 'THIS_SEASON')) {
            season = '';
        }

        const newFilters = Object.assign({}, this.props.filters, {
            splits: Object.assign(this.props.filters.splits, {WEEKS: weeks}),
            season: season
        });
        this.props.onFiltersChange(newFilters);
    }

    handleQuartersChange(quarters) {
        // if (quarters.length > 0) this.handleWeeksChange([]);
        const newFilters = Object.assign({}, this.props.filters, {
            splits: Object.assign(this.props.filters.splits, {QUARTERS: quarters})
        });
        this.props.onFiltersChange(newFilters);
    }

    handleInputWeeksChange(evt) {
        const weeks = this.props.filters.splits.WEEKS.slice();

        if(weeks.length >= 0 && weeks.includes('ALL')) {
            weeks.splice(weeks.indexOf('ALL'), 1);
        }

        if (weeks.includes(evt.target.value)) {
            if (!evt.target.checked) {
                weeks.splice(weeks.indexOf(evt.target.value), 1);
            }
        } else if (evt.target.checked) {
            if(evt.target.value === 'ALL') {
                weeks.splice(0, weeks.length);
            }
            weeks.push(evt.target.value);
        }
        weeks.sort((a, b) => {
            const keys = Object.keys(splits.WEEKS);
            return keys.indexOf(a) - keys.indexOf(b);
        });

        this.handleWeeksChange(weeks);
    }

    handleInputQuartersChange(evt) {
        const quarters = this.props.filters.splits.QUARTERS.slice();

        if(quarters.length >= 0 && quarters.includes('ALL')) {
            quarters.splice(quarters.indexOf('ALL'), 1);
        }

        if (quarters.includes(evt.target.value)) {
            if (!evt.target.checked) {
                quarters.splice(quarters.indexOf(evt.target.value), 1);
            }
        } else if (evt.target.checked) {
            if(evt.target.value === 'ALL') {
                quarters.splice(0, quarters.length);
            }
            quarters.push(evt.target.value);
        }
        quarters.sort((a, b) => {
            const keys = Object.keys(splits.QUARTERS);
            return keys.indexOf(a) - keys.indexOf(b);
        });

        if(quarters.length === 0) {
            quarters.push('ALL');
        }

        this.handleQuartersChange(quarters);
    }

    render() {
        return (
            <div className="Comp-Splits">
                <div className="navwrap">
                    <a className="navwrap_main-a">Splits</a>
                    <div className="navcontent_1 spilts-1 halfline">
                        <div className="col no-pad">
                            <form id="weeksForm">
                            <input 
                                               id={'quater_' + 'ALL'} value='ALL' type="checkbox"
                                               />
                                        <label htmlFor={'quater_' + 'ALL'} className="quater-label">All Quaters</label>
                                {Object.keys(splits.QUATER).map((quater,index) => {
                                return (
                                    <div key={quater}>
                                        <input 
                                               id={'quater_' + quater} value={quater} type="checkbox"
                                               />
                                        <label htmlFor={'quater_' + quater} className="quater-label">{splits.QUATER[quater]}</label>
                                        <div className="">
                                        {Object.keys(splits.HALFS).map((half) => {
                                    
                                    return (
                                        <span key={half}>
                                            <input 
                                                   id={'half_' + half} value={half} type="checkbox"
                                                  />
                                            <label htmlFor={'half_' + half} className="half-label">{splits.HALFS[half]}</label>
    
                                       </span>
                                    
                                    );
                                  
                                })}
                                  </div>
                                    </div>
                                    
                                );
                                })}
                            </form>
                        </div>
                        <div className="col no-pad">
                            <form id="fieldForm">
                            <input 
                                               id={'fields_' + 'ALL'} value='ALL' type="checkbox"
                                               />
                                        <label htmlFor={'fields_' + 'ALL'} className="quater-label">All Fields</label>
                                {Object.keys(splits.FIELDS).map((fields,index) => {
                                return (
                                    <div key={fields}>
                                        <input 
                                               id={'fields_' + fields} value={fields} type="checkbox"
                                               />
                                        <label htmlFor={'fields_' + fields} className="field-label">{splits.FIELDS[fields]}</label>
                                    </div>
                                    
                                );
                                })}
                            </form>
                        </div>
                        <div className="col no-pad">
                            <form id="scenarioForm">
                            <input 
                                               id={'scenario_' + 'ALL'} value='ALL' type="checkbox"
                                               />
                                        <label htmlFor={'scenario_' + 'ALL'} className="quater-label">All Scenarios</label>
                                {Object.keys(splits.SCENARIO).map((scenario,index) => {
                                return (
                                    <div key={scenario}>
                                        <input 
                                               id={'scenario_' + scenario} value={scenario} type="checkbox"
                                               />
                                        <label htmlFor={'scenario_' + scenario} className="field-label">{splits.SCENARIO[scenario]}</label>
                                    </div>
                                    
                                );
                                })}
                            </form>
                        </div>
                        <button onClick={() => {
                            // close
                        }} className="ok-button">OK
                        </button>
                    </div>
                </div>
                {(this.props.filters.splits.WEEKS.length !== 0 || !this.props.filters.splits.QUARTERS.includes('ALL'))
                && <span className="leftfilter">Clear splits
                    <a onClick={() => {
                        this.handleWeeksChange([]);
                        this.handleQuartersChange(['ALL']);
                    }}><span className="sr-only">Remove Filter</span></a>
                </span>
                }
            </div>
        );
    }
}

Splits.propTypes = {
    filters: PropTypes.object.isRequired,
    onFiltersChange: PropTypes.func.isRequired,
};

export default Splits;