import React from 'react';
import PropTypes from 'prop-types';
import seasons from '../../../utils/parameters/seasons';

class Seasons extends React.Component {

    handleSeasonChange(season) {
        let splits = this.props.filters.splits;
        console.log(splits);
        if(season == 'THIS_SEASON')
        {
            season='';
        }
        const newFilters = Object.assign({}, this.props.filters, {
            season: season,
            splits: splits
        });
        this.props.onFiltersChange(newFilters);
    }

    render() {
        const seasonVal = 'THIS_SEASON';
        return (
          
            <div>
                <div className="navwrap">
                    <a className="navwrap_main-a">SEASONS</a>
                    <div className={'navcontent_1 seasons-1 halfline'}>
                        <div className="col">
                                    <div key={seasonVal}>
                                        <input
                                               id={'season_' + seasonVal} value={seasonVal} type="checkbox"/>
                                        <label htmlFor={'season_' + seasonVal}>{seasons.SEASON[seasonVal]}</label>

                                        {Object.keys(seasons.THI_SEASON).map((seasonWeek) => {
                                    
                                    return (
                                     <div key={seasonWeek}>
                                            <input   checked={this.props.filters.season === ''}
                                                   id={'season_' + seasonWeek} value={seasonWeek} type="checkbox"
                                                   onChange={() => this.handleSeasonChange(seasonWeek)}/>
                                            <label htmlFor={'season_' + seasonWeek}>{seasons.THI_SEASON[seasonWeek]}</label>
    
                                        </div>
                                        
                                    );
                                })}
                                    </div>
                        </div>
                        <div className="col">
                            {Object.keys(seasons.SEASON).map((seasonAbr) => {
                                if (seasonAbr === 'THIS_SEASON') return false;
                         
                                return (
                                    <div key={seasonAbr}>
                                        <input checked={this.props.filters.season === seasonAbr}
                                               id={'season_' + seasonAbr} value={seasonAbr} type="checkbox"
                                               onChange={() => this.handleSeasonChange(seasonAbr)}/>
                                        <label htmlFor={'season_' + seasonAbr}>{seasons.SEASON[seasonAbr]}</label>

                                        {Object.keys(seasons.OTHR_SEASON).map((seasonWeek) => {
                                    
                                    return (
                                        this.props.filters.season === seasonAbr && <div key={seasonWeek} className="">
                                            <input 
                                                   id={'season_' + seasonWeek} value={seasonWeek} type="checkbox"
                                                   onChange={() => this.handleSeasonChange(seasonWeek)}/>
                                            <label htmlFor={'season_' + seasonWeek}>{seasons.OTHR_SEASON[seasonWeek]}</label>
    
                                        </div>
                                    
                                    );
                                })}
                                    </div>
                                    
                                );
                            })}
                        </div>
                        <button onClick={() => {

                        }} className="ok-button">OK
                        </button>
                    </div>
                </div>
                {this.props.filters.season &&
                    <span className="leftfilter">{seasons[this.props.filters.season]} <a onClick={() => {
                        this.handleSeasonChange('');
                    }}><span className="sr-only">Remove Filter</span></a></span>
                }
            </div>
        );
    }
}

Seasons.propTypes = {
    filters: PropTypes.object.isRequired,
    onFiltersChange: PropTypes.func.isRequired,
};

export default Seasons;