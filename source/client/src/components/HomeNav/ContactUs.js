import React from 'react';
import ReactDOM from 'react-dom';
import './ContactUs.css';
import { Glyphicon,Grid,Col,Row } from 'react-bootstrap';
import pin from '../../resources/basic/images/pin.png';
const jQuery = window.$;
export default class ContactUs extends React.Component {
  
  constructor(props) {
    super(props);
}
componentDidMount() {
   window.scrollTo(0, 0);
                  const script = document.createElement("script");
script.id="hs-script-loader";
  script.src = "//js.hs-scripts.com/4413266.js";
  script.async = true;

  document.body.appendChild(script);
}
handleBlur(event)  {
  const val = event.target.value;
  if(val != ''){
    const input = jQuery(event.target);
    input.addClass('has-val');
  }
}
  render() {
    return (
    <div className="contact-us">
    <img src={pin} className="pin-img hide-xs"/>
        <span className="contact-title">Get In Touch</span>

        <Grid className="contact-card">
        <Row>
        {/* <Col className="contact-info hide-othr  visible-xs"  xs={12}>
           <Grid>
        <Row className="side-title">
        <span>Contact Information</span> </Row>
        <Row className="mb30">
        <Col lg={2} xs={2}><span > <Glyphicon glyph="map-marker" className = "color-grey f25" /></span></Col><Col lg={10} xs={10}> <span className="color-grey">6004 S Kipling Pkwy, Littleton, CO 80127, USA</span></Col>
        </Row>
              <Row className="mb30">
        <Col lg={2} xs={2}><span > <Glyphicon glyph="earphone" className = "color-grey f25" /></span></Col><Col lg={10} xs={10}> <span className="color-grey">(600) (123) (12346)</span></Col>
        </Row>
                    <Row className="mb30">
        <Col lg={2} xs={2}><span > <Glyphicon glyph="envelope" className = "color-grey f25" /></span></Col><Col lg={10} xs={10}> <span className="color-grey">info@statroute.com</span></Col>
        </Row>

          <Row><Col><span></span> <span></span>
          <span></span>
          </Col> </Row>
        </Grid>
         </Col> */}
        {/* <Col lg={7}  sm={7} xs={12} className="xs-card">
        <Grid>
<Row>



         
            <Col lg={6} sm={6} >
						<div className="wrap-input100 validate-input m-t-35 m-b-30" data-validate = "first name">
							<input className={'input100'} type="serach" name="name" onBlur={this.handleBlur}/>
							<span className="focus-input100" data-placeholder="First Name">
              </span>
						</div>
            </Col>
            <Col lg={6} sm={6}>
            <div className="wrap-input100 validate-input m-t-35 m-b-30" data-validate = "last name">
							<input className={'input100'} type="serach" name="name" onBlur={this.handleBlur}/>
							<span className="focus-input100" data-placeholder="Last Name">
              </span>
						</div>
            </Col>
            </Row><Row>
            <Col lg={6} sm={6}>
            <div className="wrap-input100 validate-input  m-b-30" data-validate = "Email">
            <input className={'input100'} type="serach" name="email" onBlur={this.handleBlur}/>
            <span className="focus-input100" data-placeholder="Email">
            </span>
						</div>
            </Col>
            <Col lg={6} sm={6}>
						<div className="wrap-input100 validate-input m-b-45" data-validate="Phone">
            <input className={'input100'} type="serach" name="phone" onBlur={this.handleBlur}/>
            <span className="focus-input100" data-placeholder="Phone">
            </span>
						</div>
            </Col>
            </Row><Row>
            <Col lg={12}>
            <div className="wrap-input100 validate-input m-b-45" data-validate="message">
            <textarea className={'input100'} type="serach" name="message" onBlur={this.handleBlur}/>
            <span className="focus-input100" data-placeholder="Message">
            </span>
						</div>
</Col>
						<div className="">
							<button className="contact-button">
              <Glyphicon glyph="send" className = "color-white f20" />
							</button>
						</div>
   </Row>
          </Grid>
         </Col> */}
        <Col lg={12} className="contact-info"  sm={12}  xs={12}>
           <Grid>
        <Row className="side-title">
        <span>Contact Information</span> </Row>
        <Row className="mb30">
        <Col lg={2} sm={2}  xs={2}><span > <Glyphicon glyph="map-marker" className = "color-grey f25" /></span></Col><Col lg={10} sm={10} xs={10}> <span className="color-grey">6004 S Kipling Pkwy, Littleton, CO 80127, USA</span></Col>
        </Row>
              <Row className="mb30">
        <Col lg={2} sm={2} xs={2}><span > <Glyphicon glyph="earphone" className = "color-grey f25" /></span></Col><Col lg={10} sm={10} xs={10}> <span className="color-grey">(720) 588-3606</span></Col>
        </Row>
                    <Row className="mb30">
        <Col lg={2} sm={2} xs={2}><span > <Glyphicon glyph="envelope" className = "color-grey f25" /></span></Col><Col lg={10} sm={10} xs={10}> <span className="color-grey">info@statroute.com</span></Col>
        </Row>

          <Row><Col><span></span> <span></span>
          <span></span>
          </Col> </Row>
        </Grid>
         </Col>
        </Row>
        </Grid>
    </div>
    );
  }}
// }
// SignUpModal.propTypes = {
//   onUserSubscribe: PropTypes.func.isRequired,
// };