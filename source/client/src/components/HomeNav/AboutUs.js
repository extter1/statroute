import React from 'react';
import ReactDOM from 'react-dom';
import './AboutUs.css';
import {Link} from 'react-router-dom';
import { Glyphicon,Grid,Col,Row } from 'react-bootstrap';
const jQuery = window.$;
export default class AboutUs extends React.Component {
  
  constructor(props) {
    super(props);
}
componentDidMount() {
  window.scrollTo(0, 0);
  const script = document.createElement("script");
script.id="hs-script-loader";
script.src = "//js.hs-scripts.com/4413266.js";
script.async = true;

document.body.appendChild(script);
}
  render() {
    return (
      <div className="about-cont">
    <div className="about-us">
        <div className="abt-title1">GET THE REAL STATS,</div> 
        <div className="abt-title2">DOMINATE YOUR LEAGUE</div>
    </div>
    <div className="abt-title">ABOUT US</div>
    <Grid>
<Row>
  <Col lg={3} className= "story"> Our Story 
  <span> <hr className="hr"/> </span>
  </Col>

  <Col lg={9}> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
  </p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
 </p><p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
  </p> </Col>
   </Row>
</Grid>
<Grid className="odd-bg pb-55">
   <Row className="story-padding">
     <div className ="story">
     Our Principles
     <span> <hr className="hr-princip"/> </span>
     </div>
      </Row>

      <Row className="priciple-div pb-55">
     <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
       Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
         </Row>

         <Row className="features">   
           <Col lg={4} sm={4}>
           <div className="pb-20"><Glyphicon glyph="flash" className = "f50 color-red" /></div>
           <div className="pb-20 abt-tag">REAL TIME DATA </div>
           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna. </p>
            </Col>
           <Col lg={4} sm={4}>
           <div className="pb-20"><Glyphicon glyph="check" className = "f50 color-red" /></div>
           <div className="pb-20 abt-tag">PRECISE DATA </div>
           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna. </p>
          </Col>
           <Col lg={4} sm={4}> 
           <div className="pb-20"><Glyphicon glyph="tasks" className = "f50 color-red" /></div>
           <div className="pb-20 abt-tag">CUSTOMIZABLE DASHBOARD</div>
           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna. </p>
         </Col>
            </Row>
      </Grid>
      <div className="goal-div">
     <div className="brief-title">WHY CHOOSE STATROUTE? </div>
     <Grid className="pt40">
     <Row>
     <Col lg={4} sm={4} xs={12}>
     <div className="goal-pnt">&gt; 1000 </div>
     <div className="goal-title">DATA POINTS </div> </Col>
        <Col lg={4} sm={4} xs={12}>
        <div className="goal-pnt">&lt; 7secs</div>
        <div  className="goal-title">REAL STATS </div> </Col>
         <Col lg={4} sm={4} xs={12}>
         <div className="goal-pnt">100% </div>
         <div className="goal-title">ACCURACY </div> </Col>
       </Row>
     </Grid>

</div>
      <Grid className="pb-30 odd-bg">
<Row className="abt-cnct">Get in touch with the pioneers in sports data. </Row>
<Row><button className="abt-connect"><Link to="/">SIGN UP TODAY</Link> </button> </Row>
      </Grid>
    </div>
    );
  }}
// }
// SignUpModal.propTypes = {
//   onUserSubscribe: PropTypes.func.isRequired,
// };