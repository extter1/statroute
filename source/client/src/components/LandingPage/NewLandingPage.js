import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import { Glyphicon,Grid,Col,Row } from 'react-bootstrap';
import './NewLandingPage.css';
import Slider from 'react-slick'
import PlayerNews from "./PlayerNews";
import NewsCarousel from "./NewsCarousel";
import SignUpModal from "../Signup/SignIn";
import last from "../../resources/basic/images/last-bar.png";
import arrow from "../../resources/basic/images/arrow.png";
import t1 from "../../resources/basic/images/testimonial/testimonial1.png";
import t2 from "../../resources/basic/images/testimonial/testimonial2.png";
import t3 from "../../resources/basic/images/testimonial/testimonial3.png";
class NewLandingPage extends React.Component {

    componentDidMount() {
        window.$("body").addClass('website');
        window.scrollTo(0, 0);
                const script = document.createElement("script");
script.id="hs-script-loader";
        script.src = "//js.hs-scripts.com/4413266.js";
        script.async = true;

        document.body.appendChild(script);
    }

    componentWillUnmount() {
        window.$("body").removeClass('website');
    }
    slideDown = () => {
        window.$('html, body').animate({scrollTop: '+=680px'}, 800);
      }

    render(props) {
        var settings = {
            dots: false,
            arrows:true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            className: 'slides',
          };
        return (
            <div className="Comp-LandingPage website">
                <section className="podheadnew">
                <div className="dots-img">
                    <div className="container">
                    <h4 className="text-center fnt-fly land-message">Tired of the so-called "experts" telling you what to do?</h4>
                  <h1 className="welcome-h">WELCOME TO STATROUTE</h1>
                  <h4 className="text-center fnt-fly land-message2">We provide you with the data you need to win on your own terms.</h4>
                 
                    </div>

                    <div>

                   
                    <SignUpModal  signupinfo={this.props.signupinfo}/>
                    </div>
                    <div>
                   <Glyphicon glyph="menu-down" className="glyph-position color-white" onClick={this.slideDown}/>
                    </div>
                    </div>
                </section>

                {/* <section className="redline">
                    <div className="container">
                        <h2>Welcome to the New Standard for Stats Delivery</h2>
                        <p>
                            StatRoute will allow millions of Fantasy Football Participants to locate any data point, for
                            any player or team in 7-seconds or less.<br/>
                            Now, for the first time ever, you can make decisions based on relevance and real data.
                        </p>
                    </div>
                </section> */}

                <section className="intro_schedulenew">
                    <div className="container">
                   <Grid className="full-width">
  <Row className="show-grid">
    <Col lg={4} sm={5} md={4} mdOffset={1}  lgOffset={0}  smOffset={0} className="pt10p">
    <div className="about-title">ABOUT US</div>
<div className="about-detail">StatRoute will allow millions of Fantasy Football Participants to locate any data point, for any player or team in 7-seconds or less.
Now, for the first time ever, you can make decisions based on relevance and real data.</div>
<div className="know-moremob">
    <Link to="/about-us" className="know-more"><span className="color-white">Know More <Glyphicon glyph="menu-right" className="color-white"/></span></Link>
    </div>

    <div></div>
    </Col>
 <Col lg={2}>
 </Col>
    <Col lg={6} sm={7} md={7}>
                        <div className="sto">
                            <div className="iframe-yotuube-wrapper">
                                <iframe width="100%" height="450" src="https://www.youtube.com/embed/6sSdaQVZtP0?rel=0"
                                        frameBorder="0" allowFullScreen title="Our youtube channel"/>
                            </div>
                        </div>
                        </Col>
                        </Row>
    </Grid>
                    </div>
                </section>
<section className="pt40">        
    <div>
      
        <span>  <img src={arrow} className="testimonial-img"/><span className="testimonial-title"> Testimonials</span></span></div>
        <div className="container" >
            <Slider {...settings}>

 <div>                  
 
  <Grid className="cont-width pt10p pb10p">
  <Row className="show-grid">
  <Col lg={1} className="pr0 hidden-xs" smHidden ><div className="img-spc " style={{ backgroundImage: `url(${t1})` }}></div></Col>
    <Col lg={3} className="pl15 pt25 hidden-xs" smHidden>

<div className="color-blk">As a fantasy football analyst we always try to find that one stat to help us have the edge over others. This site has done the hard job and made it easy for anyone to find those specific stats!  </div>
    <div className="color-blk pt25 testimonial-name">Justin Pickle</div>
    <div className="color-blk pt10 testimonial-pos">The Hateful 8 Fantasy Football Podcast</div>
    </Col>

     <Col lg={1} sm={1} className="pr0 hidden-xs"><div className="img-spc " style={{ backgroundImage: `url(${t2})` }}> </div></Col>
    <Col lg={3} sm={5} className="pl15 pt25 hidden-xs">

<div className="color-blk ">The information StatRoute provides is invaluable and definitely gives us an edge when doing research. The platform is very intuitive and straightforward. No more digging stats from multiple websites and google searches, it’s all right at our fingertips </div>
    <div className="color-blk pt25 testimonial-name">Geoff Lambert</div>
    <div className="color-blk pt10 testimonial-pos">GoingFor2.com</div>
    </Col>

   <Col lg={1} sm={1} xs={12} className="pr0" ><div className="img-spc " style={{ backgroundImage: `url(${t3})` }}> </div></Col>
    <Col lg={3} sm={5} xs={12} className="pl15 pt25">
<div className="color-blk test-report">Our writers cannot wait to get their hands on this. We love what you guys are doing there!" </div>
    <div className="color-blk pt25 testimonial-name">Alex Mueller</div>
    <div className="color-blk pt10 testimonial-pos">Pro Sports Fandom</div>
    </Col>
                        </Row>
                       
    </Grid></div>


{/* 



 <div>                  
 
  <Grid className="cont-width pt10p pb10p">
  <Row className="show-grid">
  <Col lg={1} className="pr0 hidden-xs" smHidden ><div className="img-spc "> </div></Col>
    <Col lg={3} className="pl15 pt25 hidden-xs" smHidden>

<div className="color-blk">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et 
    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
    commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
    pariatur. </div>
    <div className="color-blk pt25 testimonial-name">Name</div>
    <div className="color-blk pt10 testimonial-pos">Position</div>
    </Col>

     <Col lg={1} sm={1} className="pr0 hidden-xs"><div className="img-spc "> </div></Col>
    <Col lg={3} sm={5} className="pl15 pt25 hidden-xs">

<div className="color-blk">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et 
    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
    commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
    pariatur. </div>
    <div className="color-blk pt25 testimonial-name">Name</div>
    <div className="color-blk pt10 testimonial-pos">Position</div>
    </Col>

    <Col lg={1} sm={1} xs={12} className="pr0" ><div className="img-spc "> </div></Col>
    <Col lg={3} sm={5} xs={12} className="pl15 pt25">
<div className="color-blk test-report">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et 
    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
    commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
    pariatur. </div>
    <div className="color-blk pt25 testimonial-name">Name</div>
    <div className="color-blk pt10 testimonial-pos">Position</div>
    </Col>
                        </Row>
                       
    </Grid></div> */}

      </Slider>
      </div>
      </section>
       <img src={last} className="img-full"/>
            </div>



        );
    }
}

NewLandingPage.propTypes = {
    user: PropTypes.object.isRequired
};

export default NewLandingPage;
