import React from 'react';
import {withRouter} from 'react-router';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';

import SignUpPageOne from './SignUpPageOne';
import SignUpPageTwo from './SignUpPageTwo/SignUpPageTwo';
import SignUpPageThree from './SignUpPageThree/SignUpPageThree';

import './SignUp.css';


class SignUp extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            error: null,
            values: {
                urlsignup: props.location.pathname + props.location.search,

                // test values
                // username: 'simecekjiri',
                // email: 'simecekjiri95@gmail.com',
                // plan: 'lifetime',
                // promocode: 'TEST'
            }
        };
    }

    handleChangePage(page, values) {
        this.setState((oldState) => {
            const newState = {
                page: ((page === 4) ? 3 : page) // TODO: change back to 3 when third set up is added
            };

            if (values) newState.values = Object.assign({}, oldState.values, values);
            return newState;
        }, () => {
            if (page === 4) {
                this.props.onUserSignUp(this.state.values).then(() => {
                    this.props.history.push('/sign-in');
                }, (error) => {
                    const newState = {};
                    if(error) {
                        newState.error = error;
                    } else {
                        newState.page = 1;
                    }
                    this.setState(newState);
                });
            }
        });
    }

    render() {
        return (
            <DocumentTitle title="Sign Up - StatRoute">
                <div className="Comp-SignUp">
                    {this.state.page === 1 &&
                    <SignUpPageOne onPageChange={this.handleChangePage.bind(this)}
                                   values={this.state.values} error={this.state.error}/>}
                    {this.state.page === 2 &&
                    <SignUpPageTwo onPageChange={this.handleChangePage.bind(this)}
                                   values={this.state.values} error={this.state.error}/>}
                    {this.state.page === 3 &&
                    <SignUpPageThree onPageChange={this.handleChangePage.bind(this)}
                                     values={this.state.values} error={this.state.error}/>}
                </div>
            </DocumentTitle>
        );
    }
}

SignUp.propTypes = {
    onUserSignUp: PropTypes.func.isRequired,
};

export default withRouter(SignUp);