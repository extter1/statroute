import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';


const ProgressButton = (props) => {
    const isClickAble = props.number < props.currentNumber;

    const classNames = ClassNames({
        active: props.number === props.currentNumber,
        clickable: isClickAble
    });

    return (
        <a onClick={() => {
            if(isClickAble)
                props.onPageChange(props.number);
        }} className={classNames}>
            <span className="circle">{props.number}</span>
            <span className="text">{props.children}</span>
        </a>
    );
};

ProgressButton.propTypes = {
    number: PropTypes.number.isRequired,
    currentNumber: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
};

class SignUpProgress extends React.Component {

    render() {
        return (
            <div className="Comp-SignUpProgress block progress">
                <ProgressButton number={1} currentNumber={this.props.currentNumber} onPageChange={this.props.onPageChange}>
                    Personal Info
                </ProgressButton>
                <ProgressButton number={2} currentNumber={this.props.currentNumber} onPageChange={this.props.onPageChange}>
                    Plan Selection
                </ProgressButton>
                <ProgressButton number={3} currentNumber={this.props.currentNumber} onPageChange={this.props.onPageChange}>
                    Payment
                </ProgressButton>
            </div>
        );
    }
}

SignUpProgress.propTypes = {
    currentNumber: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
};

export default SignUpProgress;
