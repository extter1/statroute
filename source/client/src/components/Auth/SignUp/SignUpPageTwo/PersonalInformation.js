import React from 'react';
import Popup from 'react-popup';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import moment from 'moment';
import axios from 'axios';
import PromoCode from './PromoCode';

import './PersonalInformation.css';
import PagePopup from '../../../Pages/PagePopup';

class PersonalInformation extends React.Component {

    constructor(props) {
        super(props);

        const today = moment();

        this.state = {
            firstName: props.values.firstname || '',
            lastName: props.values.lastname || '',
            username: props.values.username || '',
            zip: props.values.zip || '',
            gender: props.values.gender || '',
            byear: props.values.byear || today.year(),
            bmon: props.values.bmonth || today.month() + 1,
            bday: props.values.bday || today.date(),
            promoCode: props.values.promoCode || null,
            newsletter: props.values.newsletter || false,
            termsAndConditions: false,

            firstNameEmptyError: false,
            lastNameEmptyError: false,
            usernameEmptyError: false,
            usernameTakenError: false,
            zipInvalidError: false,
            zipEmptyError: false,
            genderEmptyError: false,
            bdayInvalidError: false,
            termsAndConditionsRequiredError: false,

            usernameChecked: true

        };

        this.validate = this.validate.bind(this);
    }

    handleFirstNameChange(evt) {
        const val = evt.target.value;
        if (val.length > 25) return;
        this.setState({
            firstName: val,
            firstNameEmptyError: false
        });
    }

    handleLastNameChange(evt) {
        const val = evt.target.value;
        if (val.length > 25) return;
        this.setState({
            lastName: val,
            lastNameEmptyError: false
        });
    }

    handleUsernameChange(evt) {
        const val = evt.target.value.replace(/\s+/, '');
        if (val.length > 25) return;
        this.setState({
            username: val,
            usernameEmptyError: false,
            usernameChecked: false
        });
        if (val.length === 0) return;

        axios.get('/api/auth/isUsernameAvailable', {
            params: {
                username: val
            }
        }).then((res) => {
            this.setState({
                usernameTakenError: !res.data.data,
                usernameChecked: true
            });
        }, () => {
            this.setState({
                usernameEmptyError: true,
                usernameChecked: true
            });
        });
    }

    handleZipChange(evt) {
        const val = evt.target.value;

        const newState = {
            zip: val,
            zipEmptyError: false
        };

        if (val.length > 0 && !/^[0-9]{5}$/.test(val)) {
            newState.zipInvalidError = true;
        } else {
            newState.zipInvalidError = false;
        }
        this.setState(newState);
    }

    handleGenderChange(evt) {
        const val = evt.target.value;

        const newState = {
            gender: val
        };

        if (!['F', 'M'].includes(val)) {
            newState.genderEmptyError = true;
        } else {
            newState.genderEmptyError = false;
        }
        this.setState(newState);
    }

    handleBirthdayChange(year, mon, day) {
        const newState = {
            byear: year,
            bmon: mon,
            bday: day
        };

        if (moment(year + '-' + mon + '-' + day, 'YYYY-M-D').add(1, 'days').isSameOrAfter(moment())) {
            newState.bdayInvalidError = true;
        } else {
            newState.bdayInvalidError = false;
        }

        this.setState(newState);
    }

    handlePromoCodeChange(promoCode) {
        this.setState({
            promoCode: promoCode
        }, () => {
            this.props.onPromoCodeChange(promoCode);
        });
    }

    handleNewsletterChange(evt) {
        this.setState({
            newsletter: evt.target.checked
        });
    }

    handleTermsAndConditionsChange(evt) {
        this.setState({
            termsAndConditions: evt.target.checked,
            termsAndConditionsRequiredError: !evt.target.checked
        });
    }

    validate() {
        const newState = {};
        let hasError = false;

        if (this.state.firstNameEmptyError || this.state.firstName.length === 0) {
            newState.firstNameEmptyError = true;
            hasError = true;
        }

        if (this.state.lastNameEmptyError || this.state.lastName.length === 0) {
            newState.lastNameEmptyError = true;
            hasError = true;
        }

        if (this.state.usernameEmptyError || !this.state.usernameChecked || this.state.username.length === 0) {
            newState.usernameEmptyError = true;
            hasError = true;
        }

        if (this.state.zipEmptyError || this.state.zip.length === 0) {
            newState.zipEmptyError = true;
            hasError = true;
        }

        if (this.state.zipInvalidError || !/^[0-9]{5}$/.test(this.state.zip)) {
            newState.zipInvalidError = true;
            hasError = true;
        }

        if (this.state.genderEmptyError || !['F', 'M'].includes(this.state.gender)) {
            newState.genderEmptyError = true;
            hasError = true;
        }

        if (this.state.bdayInvalidError || moment(this.state.byear + '-' + this.state.bmon + '-' + this.state.bday, 'YYYY-M-D')
                .add(1, 'days').isSameOrAfter(moment())) {
            newState.bdayInvalidError = true;
            hasError = true;
        }

        if (this.state.termsAndConditionsRequiredError || !this.state.termsAndConditions) {
            newState.termsAndConditionsRequiredError = true;
            hasError = true;
        }

        this.setState(newState);

        return !hasError;
    }

    getValues() {
        return {
            firstname: this.state.firstName,
            lastname: this.state.lastName,
            username: this.state.username,
            zip: this.state.zip,
            gender: this.state.gender,
            bday: this.state.bday,
            bmonth: this.state.bmon,
            byear: this.state.byear,
            promocode: this.state.promoCode,
            newsletter: this.state.newsletter
        };
    }

    render() {
        return (
            <div className="Comp-PersonalInformation">
                <h2>Personal information</h2>
                <input className={(this.state.firstNameEmptyError) ? 'error' : null} type="text"
                       value={this.state.firstName} placeholder="First Name"
                       onChange={this.handleFirstNameChange.bind(this)}/>
                {this.state.firstNameEmptyError && <p className="error">Please enter your First Name.</p>}


                <input className={(this.state.lastNameEmptyError) ? 'error' : null} type="text"
                       value={this.state.lastName} placeholder="Last Name"
                       onChange={this.handleLastNameChange.bind(this)}/>
                {this.state.lastNameEmptyError && <p className="error">Please enter your Last Name.</p>}

                <input className={(this.state.usernameEmptyError || this.state.usernameTakenError) ? 'error' : null}
                       type="text"
                       value={this.state.username} placeholder="Username"
                       onChange={this.handleUsernameChange.bind(this)}/>
                {this.state.usernameEmptyError && <p className="error">Please enter your Username.</p>}
                {this.state.usernameTakenError &&
                <p className="error">Username has already been taken. Please try again.</p>}

                <input className={(this.state.zipInvalidError || this.state.zipEmptyError) ? 'error' : null} type="text"
                       value={this.state.zip}
                       placeholder="ZIP Code"
                       onChange={this.handleZipChange.bind(this)}/>
                {this.state.zipInvalidError && <p className="error">Please enter valid Zip Code.</p>}
                {this.state.zipEmptyError &&
                <p className="error">Username has already been taken. Please try again.</p>}

                <div className="regdata">
                    <div className="item">
                        <span className="left">Gender:</span>
                        <div className="right">
                            <input id="male" type="radio" name="gender" value="M"
                                   className={(this.state.genderEmptyError) ? 'error' : null}
                                   checked={this.state.gender === 'M'}
                                   onChange={this.handleGenderChange.bind(this)}
                            />
                            <label className="radiolab" htmlFor="male">Male</label>
                            <input id="female" type="radio" name="gender" value="F"
                                   className={(this.state.genderEmptyError) ? 'error' : null}
                                   checked={this.state.gender === 'F'}
                                   onChange={this.handleGenderChange.bind(this)}
                            />
                            <label className="radiolab" htmlFor="female">Female</label>
                        </div>
                    </div>
                    {this.state.genderEmptyError && <p className="error">Please select your Gender.</p>}


                    <div className="item">
                        <span className="left dateofbirth">Date of Birth:</span>
                        <div className="right">
                            <div className={'nice-select month' + ((this.state.bdayInvalidError) ? ' error' : '')}
                                 tabIndex="0">
                                <span className="current">{('00' + (this.state.bmon)).substr(-2)}</span>
                                <ul className="list">
                                    {Array.from(new Array(12).keys()).map((number) => {
                                        const value = ('00' + (number + 1)).substr(-2);
                                        let classNames = ClassNames('option', {
                                            selected: this.state.bmon === number + 1
                                        });

                                        return (
                                            <li key={number} className={classNames} value={number + 1}
                                                onClick={() => {
                                                    this.handleBirthdayChange(this.state.byear, number + 1, this.state.bday);
                                                }}>{value}</li>
                                        );
                                    })}
                                </ul>
                            </div>

                            <div className={'nice-select day' + ((this.state.bdayInvalidError) ? ' error' : '')}
                                 tabIndex="0">
                                <span className="current">{('00' + (this.state.bday)).substr(-2)}</span>
                                <ul className="list">
                                    {Array.from(new Array(31).keys()).map((number) => {
                                        const value = ('00' + (number + 1)).substr(-2);
                                        let classNames = ClassNames('option', {
                                            selected: this.state.bday === number + 1
                                        });

                                        return (
                                            <li key={number} className={classNames} onClick={() => {
                                                this.handleBirthdayChange(this.state.byear, this.state.bmon, number + 1);
                                            }}>{value}</li>
                                        );
                                    })}
                                </ul>
                            </div>

                            <div className={'nice-select year' + ((this.state.bdayInvalidError) ? ' error' : '')}
                                 tabIndex="0">
                                <span className="current">{('0000' + (this.state.byear)).substr(-4)}</span>
                                <ul className="list">
                                    {Array.from(new Array(moment().year() - 1900 + 1).keys()).reverse().map((number) => {
                                        const value = ('0000' + (number + 1900)).substr(-4);
                                        let classNames = ClassNames('option', {
                                            selected: this.state.byear === number + 1
                                        });

                                        return (
                                            <li key={number} className={classNames} onClick={() => {
                                                this.handleBirthdayChange(number + 1900, this.state.bmon, this.state.bday);
                                            }}>{value}</li>
                                        );
                                    })}
                                </ul>
                            </div>

                        </div>
                        {this.state.bdayInvalidError && <p className="error">Please select valid Birthday.</p>}
                    </div>

                    <PromoCode onChange={this.handlePromoCodeChange.bind(this)} value={this.state.promoCode}/>

                    <div className="item newsletter">
                        <input type="checkbox" id="newsletter_signup" checked={this.state.newsletter}
                               onChange={this.handleNewsletterChange.bind(this)}/>
                        <label className="checklab" htmlFor="newsletter_signup">Sign up for a
                            newsletter</label>
                    </div>

                    <div className="item">
                        <input className={(this.state.termsAndConditionsRequiredError) ? 'error' : null}
                               type="checkbox" id="terms_and_conditions"
                               checked={this.state.termsAndConditions}
                               onChange={this.handleTermsAndConditionsChange.bind(this)}/>
                        <label className="checklab terms" htmlFor="terms_and_conditions">I agree with Terms &
                            Conditions <a
                                href="/pages/terms-and-conditions" onClick={(event) => {
                                event.preventDefault();

                                Popup.plugins().component(<PagePopup slug="terms-and-conditions"/>);
                            }} target="_blank">(Open)</a></label>
                    </div>
                    {this.state.termsAndConditionsRequiredError &&
                    <p className="error">Please check the box confirming you agree with our Terms and Conditions.</p>}
                </div>
            </div>
        );
    }
}

PersonalInformation.propTypes = {
    onPromoCodeChange: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
};

export default PersonalInformation;
