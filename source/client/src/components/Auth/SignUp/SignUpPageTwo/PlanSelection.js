import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';

import './PlanSelection.css';

import {plans} from '../../../../utils/PaymentsUtils';

class PlanSelection extends React.Component {

    render() {
        return (
            <div className="Comp-PlanSelection">
                <h2>Select Your Plan</h2>

                {Object.keys(plans).map((planName) => {
                    const plan = plans[planName];
                    return (
                        <div key={planName} className="plan">
                            <input id={planName} checked={this.props.value === planName} value={planName}
                                   type="radio"
                                   name="plan" onChange={(evt) => this.props.onChange(evt.target.value)}/>
                            <label className={ClassNames({
                                'big': plan.isBig
                            })} htmlFor={planName}>
                                <span className="premium">PREMIUM</span>
                                <span className="what">{planName}</span>
                                <span
                                    className="price">${(this.props.hasPromeCode) ? (plan.promoPrice / 100) : (plan.price / 100)}</span>
                                <span className="often">{plan.paid}</span>
                            </label>
                        </div>
                    );
                })}
            </div>
        );
    }
}

PlanSelection.propTypes = {
    hasPromeCode: PropTypes.bool.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};

export default PlanSelection;
