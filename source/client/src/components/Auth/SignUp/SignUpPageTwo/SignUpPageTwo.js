import React from 'react';
import PropTypes from 'prop-types';

import SignUpProgress from '../SignUpProgress';
import PlanSelection from './PlanSelection';
import PersonalInformation from './PersonalInformation';

import {plans} from '../../../../utils/PaymentsUtils';


class SignUpPageTwo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            plan: props.values.plan || '',
            planError: false,

            hasPromoCode: false
        };
    }

    handlePlanChange(plan) {
        if (!Object.keys(plans).includes(plan)) return;

        this.setState({
            plan: plan,
            planError: false
        });
    }

    validateValues() {
        const newState = {};
        let hasError = !this.personalInformation.validate();

        if (this.state.planError || !Object.keys(plans).includes(this.state.plan)) {
            newState.planError = true;
            hasError = true;
        }

        this.setState(newState);
        return !hasError;
    }

    render() {
        return (
            <section className="signinupwrap">
                <div className="container">

                    <div className="signinupform">
                        <div className="block top">
                            <h1>sign up today</h1>
                        </div>
                        <SignUpProgress currentNumber={2} onPageChange={this.props.onPageChange}/>

                        <div className="block selectplan">
                            <PlanSelection hasPromeCode={this.state.hasPromoCode} value={this.state.plan}
                                           onChange={this.handlePlanChange.bind(this)}/>

                            {this.state.planError &&
                            <p className="error">Please select one subscription plan from above.</p>}
                        </div>

                        <div className="block inputs">

                            <PersonalInformation
                                ref={(instance) => this.personalInformation = instance}
                                onPromoCodeChange={(promoCode) => this.setState({hasPromoCode: promoCode !== null})}
                                values={this.props.values}
                            />

                            <button type="button" className="buttonarrow" onClick={() => {
                                if (this.validateValues())
                                    this.props.onPageChange(3, Object.assign({
                                        plan: this.state.plan
                                    }, this.personalInformation.getValues()));
                            }}>Next
                            </button>

                        </div>

                    </div>

                </div>
            </section>
        );
    }
}

SignUpPageTwo.propTypes = {
    onPageChange: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
    error: PropTypes.object,
};

export default SignUpPageTwo;