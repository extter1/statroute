import React from 'react';
import PropTypes from 'prop-types';

class PromoCode extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            promoCode: props.value || '',
            valid: !!props.value,
            checked: false
        };
    }

    handlePromoCodeInputChange(evt) {
        if (this.state.valid) return;

        this.setState({
            promoCode: evt.target.value,
            checked: false
        });
    }

    _validatePromoCode(evt) {
        evt.preventDefault();
        if (this.state.valid) return;
        if (this.state.promoCode.length === 0) return;

        this.setState({
            valid: false,   // TODO: connect to endpoint for code validation
            checked: true
        }, () => {
            if(this.state.valid)
                this.props.onChange(this.state.promoCode);
        });
    }

    _removePromoCode(evt) {
        evt.preventDefault();
        if (!this.state.valid) return;

        this.setState({
            valid: false,
            checked: false,
            promoCode: ''
        }, () => {
            this.props.onChange(null);
        });
    }

    render() {
        return (
            <div className="Comp-PromoCode item">
                <div className="">
                    <span className="left">Promo Code</span>
                    <div className="right promo">
                        <input type="text" value={this.state.promoCode} disabled={this.state.valid}
                               placeholder="Enter Your Promo Code Here"
                               onChange={this.handlePromoCodeInputChange.bind(this)}/>
                        {!this.state.valid &&
                        <button type="button" onClick={this._validatePromoCode.bind(this)}>Apply</button>}
                        {this.state.valid &&
                        <button type="button" onClick={this._removePromoCode.bind(this)}>Remove</button>}
                    </div>
                </div>
                {this.state.checked && !this.state.valid &&
                    <p className="error">Promo Code is not valid.</p>}
            </div>
        );
    }
}

PromoCode.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string
};

export default PromoCode;
