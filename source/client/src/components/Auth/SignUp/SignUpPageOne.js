import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import axios from 'axios';
import SignUpProgress from './SignUpProgress';

class SignUpPageOne extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: props.values.email || '',
            password: props.values.password || '',
            password2: props.values.password || '',

            emailEmptyError: false,
            emailInvalidError: false,
            emailTakenError: false,

            passwordEmptyError: false,
            passwordInvalidError: false,

            password2NotMatchingError: false
        };

        if (this.state.email.length !== 0) {
            this._checkEmail(this.state.email);
        }
    }


    _checkEmail(email) {
        axios.get('/api/auth/isEmailAvailable', {
            params: {
                email: email
            }
        }).then((res) => {
            this.setState({
                emailInvalidError: false,
                emailTakenError: !res.data.data,
                emailChecked: true
            });
        }, () => {
            this.setState({
                emailInvalidError: true,
                emailTakenError: false,
                emailChecked: true
            });
        });
    }

    handleEmailChange(evt) {
        const val = evt.target.value;

        const newState = {
            email: val,
            emailEmptyError: false,
            emailTakenError: false,
            emailInvalidError: false
        };

        if (val.length > 50) return;

        // eslint-disable-next-line
        if (val.length > 0 && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(val)) {
            this._checkEmail(val);
        }

        this.setState(newState);
    }

    handlePasswordChange(evt) {
        this.setState({
            password: evt.target.value,
            passwordEmptyError: false,
            passwordInvalidError: false
        });
    }

    handlePassword2Change(evt) {
        const val = evt.target.value;

        const newState = {
            password2: evt.target.value
        };

        if (val.length > 0 && this.state.password !== val) {
            newState.password2NotMatchingError = true;
        } else {
            newState.password2NotMatchingError = false;
        }

        this.setState(newState);
    }

    validateValues() {
        const newState = {};
        if (this.state.email.length === 0) {
            newState.emailEmptyError = true;
            // eslint-disable-next-line
        } else if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email)) {
            newState.emailInvalidError = true;
        }

        if (this.state.password.length === 0) {
            newState.passwordEmptyError = true;
        } else if (this.state.password.length < 6) {
            newState.passwordInvalidError = true;
        } else if (this.state.password2.length === 0 || this.state.password !== this.state.password2) {
            newState.password2NotMatchingError = true;
        }

        if (this.state.emailEmptyError || newState.emailEmptyError
            || this.state.emailInvalidError || newState.emailInvalidError
            || this.state.passwordEmptyError || newState.passwordEmptyError
            || this.state.passwordInvalidError || newState.passwordInvalidError
            || this.state.password2NotMatchingError || newState.password2NotMatchingError
        ) {

            this.setState(newState);
            return false;
        } else
            return true;
    }

    render() {
        const emailClasses = ClassNames('user', {
            'error': this.state.emailEmptyError || this.state.emailTakenError || this.state.passwordInvalidError
        });

        const passwordClasses = ClassNames('pass', {
            'error': this.state.passwordEmptyError || this.state.passwordInvalidError
        });

        const password2Classes = ClassNames('pass', {
            'error': this.state.password2NotMatchingError
        });

        return (
            <section className="signinupwrap">
                <div className="container">

                    <div className="signinupform">
                        <div className="block top">
                            <h1>sign up today</h1>
                        </div>
                        <SignUpProgress currentNumber={1} onPageChange={this.props.onPageChange}/>
                        <div className="block inputs">
                            <h2>Personal Info</h2>
                            <input className={emailClasses} type="text" value={this.state.email}
                                   placeholder="Enter Your Email Address"
                                   onChange={this.handleEmailChange.bind(this)}/>

                            {this.state.emailEmptyError && <p className="error">Please enter your email address.</p>}
                            {this.state.emailTakenError && <p className="error">This email address is already registered to an account.</p>}
                            {this.state.emailInvalidError && <p className="error">Email address is not valid. Please follow this example: <a
                                href="mailto:info@statroute.com">info@statroute.com</a></p>}

                            <input className={passwordClasses} type="password" value={this.state.password}
                                   placeholder="Password"
                                   onChange={this.handlePasswordChange.bind(this)}/>

                            {this.state.passwordEmptyError && <p className="error">Please enter your password.</p>}
                            {this.state.passwordInvalidError && <p className="error">Password must contain at least six characters with one unique
                                character.</p>}

                            <input className={password2Classes} type="password" value={this.state.password2}
                                   placeholder="Password Again"
                                   onChange={this.handlePassword2Change.bind(this)}/>

                            {this.state.password2NotMatchingError && <p className="error">Password does not match. Please enter the same password as above.</p>}

                            <button type="button" className="buttonarrow" onClick={() => {
                                if (this.validateValues())
                                    this.props.onPageChange(2, {
                                        email: this.state.email,
                                        password: this.state.password
                                    });
                            }}>Next
                            </button>
                        </div>
                    </div>

                </div>
            </section>
        );
    }
}

SignUpPageOne.propTypes = {
    onPageChange: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
    error: PropTypes.object,
};

export default SignUpPageOne;