import React from 'react';
import Popup from 'react-popup';
import PropTypes from 'prop-types';
import {Elements} from 'react-stripe-elements';

import SignUpProgress from '../SignUpProgress';

import {plans} from '../../../../utils/PaymentsUtils';

import './SignUpPageThree.css';
import PaymentForm from './PaymentForm';

class SignUpPageThree extends React.Component {

    handleSubmit(evt) {
        evt.preventDefault();

        this.paymentForm.getWrappedInstance().proceed().then((result) => {
            this.props.onPageChange(4, {
                token: result.source.id,
                three_d_secure: result.three_d_secure
            });
        }, (error) => {
            Popup.alert(error);
            // payment error
        });
    }

    render() {
        return (
            <section className="signinupwrap Comp-SignUpPageThree">
                <div className="container">
                    <Elements>
                        <div className="signinupform">
                            <div className="block top">
                                <h1>sign up today</h1>
                            </div>
                            <SignUpProgress currentNumber={3} onPageChange={this.props.onPageChange}/>

                            <div className="block inputs">
                                <div className="regdata payment">
                                    <h2>Amount</h2>
                                    <div className="item">
                                        <span className="left">Amount (USD $):</span>
                                        <div className="right price">
                                            <input type="text" disabled
                                                   value={plans[this.props.values.plan][(this.props.values.promocode) ? 'promoPrice' : 'price'] / 100}/>
                                        </div>
                                    </div>
                                </div>

                                <PaymentForm error={this.props.error} ref={(paymentForm) => this.paymentForm = paymentForm}/>

                                <button type="button" className="buttonarrow"
                                        onClick={this.handleSubmit.bind(this)}>Next</button>
                            </div>
                        </div>
                    </Elements>
                </div>
            </section>
        );
    }
}

SignUpPageThree.propTypes = {
    onPageChange: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
    error: PropTypes.object,
};

export default SignUpPageThree;
