import React from 'react';
import PropTypes from 'prop-types';
import Promise from 'promise';

import {
    injectStripe,
    CardNumberElement,
    CardCVCElement,
    CardExpiryElement,
    PostalCodeElement
} from 'react-stripe-elements';


import cardsImage from '../../../../resources/basic/images/cards.png';

class PaymentForm extends React.Component {

    constructor() {
        super();

        this.state = {
            cardErrorMessage: null,
            cvcErrorMessage: null,
            expirationErrorMessage: null,
            zipErrorMessage: null
        };

        this.proceed = this.proceed.bind(this);
    }

    componentDidMount() {
        this._processError(this.props.error);
    }

    componentDidUpdate(oldProps) {
        if (oldProps.error !== this.props.error) this._processError(this.props.error);
    }

    _processError(error) {
        if (error === null) return;

        let field;
        switch (error.code) {
            case 'incomplete_number':
            case 'invalid_number':
            case 'incorrect_number':
            case 'expired_card':
            case 'card_declined':
                field = 'cardErrorMessage';
                break;
            case 'incomplete_cvc':
            case 'incorrect_cvc':
                field = 'cvcErrorMessage';
                break;
            case 'incomplete_expiry':
            case 'invalid_expiry_month':
            case 'invalid_expiry_year':
            case 'invalid_expiry_year_past':
                field = 'expirationErrorMessage';
                break;
            case 'incomplete_zip':
            case 'incorrect_zip':
                field = 'zipErrorMessage';
                break;
            default:
                field = 'zipErrorMessage';
                break;
        }

        console.log(error);

        const newState = {};
        newState[field] = error.message;
        this.setState(newState);

    }

    proceed() {
        return new Promise((resolve, reject) => {
            this.setState({
                cardErrorMessage: null,
                cvcErrorMessage: null,
                expirationErrorMessage: null,
                zipErrorMessage: null
            }, () => {
                this.props.stripe.createSource().then((result) => {
                    if (result.error) {
                        this._processError(result.error);
                        reject();
                        return;
                    }

                    resolve({
                        source: result.source,
                        three_d_secure: !(result.source.card.three_d_secure === 'not_supported')
                    });
                });
            });
        });
    }

    render() {
        const styles = {
            base: {
                color: '#636363',
                fontSize: '15px',
                lineHeight: '48px',
                fontSmoothing: 'antialiased',
            },
        };

        return (
            <div className="Comp-PaymentForm">
                {false && <div className="regdata payment">
                    <h2>billing information</h2>
                    <div className="item">
                        <span className="left">First Name:</span>
                        <div className="right"><input type="text" placeholder="Bob"/></div>
                    </div>
                    <div className="item">
                        <span className="left">Last Name:</span>
                        <div className="right"><input type="text" placeholder="Cravens"/></div>
                    </div>
                    <div className="item">
                        <span className="left">Email Address:</span>
                        <div className="right"><input type="text" placeholder="bob.cravens@gmail.com"/>
                        </div>
                    </div>
                </div>}

                <div className="regdata payment last-child">
                    <h2>payment information</h2>
                    <img src={cardsImage} alt="cards.png, 19kB" title="cards"/>
                    <div className="item">
                        <span className="left">Card Number:</span>
                        <div className="right">
                            <CardNumberElement className="element-input card" style={styles}/>
                        </div>
                        {this.state.cardErrorMessage !== null && <p className="error">{this.state.cardErrorMessage}</p>}
                    </div>
                    <div className="item">
                        <span className="left">CVC:</span>
                        <div className="right">
                            <CardCVCElement className="element-input cvc" style={styles}/>
                        </div>
                        {this.state.cvcErrorMessage !== null && <p className="error">{this.state.cvcErrorMessage}</p>}
                    </div>
                    <div className="item">
                        <span className="left">Expiration:</span>
                        <div className="right">
                            <CardExpiryElement className="element-input expiration" style={styles}/>
                        </div>
                        {this.state.expirationErrorMessage !== null &&
                        <p className="error">{this.state.expirationErrorMessage}</p>}
                    </div>
                    <div className="item">
                        <div className="left">ZIP:</div>
                        <div className="right">
                            <PostalCodeElement className="element-input zip" style={styles}/>
                        </div>
                        {this.state.zipErrorMessage !== null && <p className="error">{this.state.zipErrorMessage}</p>}
                    </div>
                </div>
            </div>
        );
    }
}

PaymentForm.propTypes = {
    error: PropTypes.object
};

export default injectStripe(PaymentForm, {withRef: true});
