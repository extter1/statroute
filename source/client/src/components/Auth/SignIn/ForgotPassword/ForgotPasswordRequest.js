import React from 'react';
import ClassNames from 'classnames';
import PropTypes from 'prop-types';

class ForgotPasswordRequest extends React.Component {

    constructor() {
        super();

        this.state = {
            error: false,
            message: null,
            value: ''
        };
    }

    handleSubmit(evt) {
        evt.preventDefault();

        this.props.onUserForgotPasswordRequest(this.state.value).then(() => {
            this.setState({
                error: false,
                message: 'Recovery email was successfully send',
                value: ''
            });
        }, () => {
            this.setState({
                error: true,
                message: 'Associated account was not found'
            });
        });
    }

    handleInputChange(evt) {
        this.setState({
            value: evt.target.value
        });
    }

    render() {
        return (
            <div className="Comp-ForgotPasswordRequest">
                <h2>Send password recovery email</h2>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <input className={ClassNames('user', {error: this.state.error})} type="text"
                           placeholder="Email or Username" value={this.state.value}
                           onChange={this.handleInputChange.bind(this)}
                    />
                    {this.state.message !== null && <p className={ClassNames({
                        error: this.state.error,
                        success: !this.state.error
                    })}>{this.state.message}</p>}
                    <button type="submit">send email</button>
                </form>
            </div>
        );
    }
}

ForgotPasswordRequest.propTypes = {
    onUserForgotPasswordRequest: PropTypes.func.isRequired
};

export default ForgotPasswordRequest;
