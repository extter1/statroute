import React from 'react';
import ClassNames from 'classnames';
import {withRouter, Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import axios from 'axios';

class ForgotPasswordRequest extends React.Component {

    constructor() {
        super();

        this.state = {
            validHash: null,
            error: false,
            message: null,
            password: '',
            password2: '',
            passwordError: false
        };
    }

    componentDidMount() {
        axios.get('/api/auth/isForgotPasswordHashValid', {
            params: {
                hash: this.props.hash
            }
        }).then(() => {
            this.setState({
                validHash: true
            });
        }, () => {
            this.setState({
                validHash: false
            });
        });
    }

    _validateValues() {
        const newState = {
            message: ''
        };

        let error = false;
        if (this.state.password.length === 0) {
            newState.message = 'Please enter your password.';
            error = true;
        } else if (this.state.passwordError || this.state.password !== this.state.password2) {
            newState.message = 'Please enter your password again.';
            newState.passwordError = true;
            error = true;
        }
        newState.error = error;

        this.setState(newState);

        return error;
    }

    handleSubmit(evt) {
        evt.preventDefault();
        if (this._validateValues()) return;

        this.props.onUserForgotPasswordResetPassword(this.props.hash, this.state.password2).then(() => {
            this.setState({
                error: false,
                message: 'New password was successfully set',
            });
            setTimeout(() => {
                this.props.history.push('/sign-in');
            }, 2000);
        }, () => {
            this.setState({
                error: true,
                message: 'Associated account was not found'
            });
        });
    }

    handlePasswordChange(evt) {
        this.setState({
            password: evt.target.value
        });
    }

    handlePassword2Change(evt) {
        const val = evt.target.value;

        const newState = {
            password2: evt.target.value
        };

        newState.passwordError = (val.length > 0 && this.state.password !== val);

        this.setState(newState);
    }

    render() {

        const passwordClasses = ClassNames('pass', {
            'error': this.state.passwordError
        });

        return (
            <div className="Comp-ForgotPasswordRequest">
                <h2>Set new account password</h2>
                {this.state.validHash === true && <form onSubmit={this.handleSubmit.bind(this)}>
                    <input className="pass" type="password" value={this.state.password}
                           placeholder="Password"
                           onChange={this.handlePasswordChange.bind(this)}/>
                    <input className={passwordClasses} type="password" value={this.state.password2}
                           placeholder="Password Again"
                           onChange={this.handlePassword2Change.bind(this)}/>
                    {this.state.passwordError && <p className={ClassNames({
                        error: this.state.passwordError
                    })}></p>}

                    {this.state.message !== null && <p className={ClassNames({
                        error: this.state.error,
                        success: !this.state.error
                    })}>{this.state.message}</p>}
                    <button type="submit">Set password
                    </button>
                </form>}
                {this.state.validHash === false && <p className="error">
                    Your password reset recovery link is not valid, please <Link to="/forgot-password">try to request
                    new one</Link>.
                </p>}
            </div>
        );
    }
}

ForgotPasswordRequest.propTypes = {
    hash: PropTypes.string.isRequired,
    onUserForgotPasswordResetPassword: PropTypes.func.isRequired
};

export default withRouter(ForgotPasswordRequest);
