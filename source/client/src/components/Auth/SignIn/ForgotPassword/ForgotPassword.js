import React from 'react';
import DocumentTitle from 'react-document-title';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import queryString from 'querystring';
import ForgotPasswordRequest from './ForgotPasswordRequest';
import ForgotPasswordReset from './ForgotPasswordReset';

class ForgotPassword extends React.Component {

    render() {
        const params = queryString.parse(this.props.location.search.replace('?', ''));
        const isRequest = !(params.hash);
        return (
            <DocumentTitle title="Forgot Password - StatRoute">
                <div className="Comp-ForgotPassword">
                    <section className="signinupwrap">
                        <div className="container">

                            <div className="signinupform">
                                <div className="block top">
                                    <h1>Forgot Password</h1>
                                </div>
                                <div className="block inputs">
                                    {isRequest && <ForgotPasswordRequest
                                        onUserForgotPasswordRequest={this.props.onUserForgotPasswordRequest}/>}
                                    {!isRequest && <ForgotPasswordReset
                                        hash={params.hash}
                                        onUserForgotPasswordResetPassword={this.props.onUserForgotPasswordResetPassword}/>}
                                </div>
                                <div className="block bottom">
                                    <Link to="/sign-in" className="noacc">Sign In to your account</Link>
                                </div>
                            </div>

                        </div>
                    </section>
                </div>
            </DocumentTitle>
        );
    }
}

ForgotPassword.propTypes = {
    onUserForgotPasswordRequest: PropTypes.func.isRequired,
    onUserForgotPasswordResetPassword: PropTypes.func.isRequired,
};

export default ForgotPassword;
