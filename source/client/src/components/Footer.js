import React from 'react';
import {Link} from 'react-router-dom';

import './footer.css';

import { Glyphicon,Grid,Col,Row } from 'react-bootstrap';
import footerLogo from '../resources/basic/images/footer-logo.png';
import social from '../resources/basic/images/social.png';
import facebook from '../resources/basic/images/socials/facebook.png';
import twitter from '../resources/basic/images/socials/twitter.png';
import youtube from '../resources/basic/images/socials/youtube.png';
import linkedin from '../resources/basic/images/socials/linkedin.png';
import instagram from '../resources/basic/images/socials/instagram.png';

const Footer = (props) => {
    return (
        <footer>
            <div className="container">
  <Grid className="full-width">


    <Row className="show-grid">
    <Col lg={4} md={4}>

<h5 className="text-left pb10 color-white">Quick Links</h5>
<ul>

<li className='footer-li color-white'><Link to="/about-us">About Us</Link></li>
<li className='footer-li color-white'><Link to="/contact-us">Contact Us</Link></li></ul>
    </Col>

    </Row>
  <Row className="show-grid pt25">
    <Col lg={4} md={3}>


    </Col>

        <Col lg={4} md={5} sm={7} xs={12}>

    <p> <a >Privacy Policy</a>
    <Link to="/terms-condition">Terms Of Use</Link>
     <Link to="/sitemap">Sitemap</Link>
            </p>
    </Col>

        <Col lg={4} md={4} sm={5} xs={12}>

<img src={social} className="social-img"/>
    </Col>
    </Row>

    </Grid>
<p className="rights"> &copy; Copyright 2018   |  STATROUTE | All Rights Reserved.</p>

  {/*              <div className="footer-socials">
                    <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/StatRouteInc/"><img
                        src={facebook} alt="Facebook" title=""/></a>
                    <a target="_blank" rel="noopener noreferrer" href="https://twitter.com/Stat_Route"><img
                        src={twitter} alt="Twitter" title=""/></a>
                    <a target="_blank" rel="noopener noreferrer"
                       href="https://www.youtube.com/channel/UCQ5NhuUHWygyBOz6QTgcIiA"><img src={youtube} alt="Youtube"
                                                                                            title=""/></a>
                    <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/24601099/"><img
                        src={linkedin} alt="LinkedIn" title=""/></a>
                    <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/statroute/"><img
                        src={instagram} alt="Instagram" title=""/></a>
                </div>*/}

            </div>
        </footer>
    );
};


export default Footer;