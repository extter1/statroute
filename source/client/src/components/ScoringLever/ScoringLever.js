import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import PropTypes from 'prop-types';
import './ScoringLever.css';
import axios from 'axios';
import logo from '../../resources/basic/images/footer-logo.png';
import {
  Glyphicon,
  Grid,
  Col,
  Row,
  DropdownButton,
  MenuItem
} from 'react-bootstrap';
const jQuery = window.$;
export default class ScoringLever extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,

      rushYardsVal: '',
      receivingYards: '',
      passYardsVal: '',
      qbInterVal: '',
      pTouchdownsVal: '',
      receptionVal: '',

      rYards: false,
      rushYards: false,
      passYards: false,
      qbInter: false,
      ruYards: false,
      reception: false,
      league: 'Default',
      newLeague: false,
      getItems: [],
      chars_left: 50,
      showList: false,
      error:false
    };
  }
  initialState = () => {
    this.setState({
      rYards: false,
      rushYards: false,
      passYards: false,
      qbInter: false,
      ruYards: false,
      reception: false
    });
  }
  onOpenModal = () => {
    this.setState({open: true});
  };

  onCloseModal = () => {
this.setState({error: false});
    this.setState({open: false});
  };
  openDropDown = (evt) => {
    //jQuery(evt.target.nextElementSibling.children[0]).css('display','block');
    this.initialState();
    const stateP = evt
      .target
      .getAttribute('data-label');
    const opened = [stateP];
    console.log('heea');
    window.title = this.state[stateP];
    this.setState({
      [stateP]: !this.state[stateP]
    });
  };

  updateVals = (evt) => {
    console.log(evt.target.innerHTML);
    const updateValue = evt
      .target
      .getAttribute('data-key');
    this.setState({[updateValue]: evt.target.innerHTML});
    this.initialState();
  };
  showListFun = () => {
    this.setState({
      showList: !this.state.showList
    });
  };
  addLeague = () => {
    console.log('here');
    this.setState({
      rYards: false,
      rushYards: false,
      passYards: false,
      qbInter: false,
      ruYards: false,
      reception: false,

      rushYardsVal: '10',
      receivingYards: '15',
      passYardsVal: '20',
      qbInterVal: '-2',
      pTouchdownsVal: '4',
      receptionVal: '0',
      open: true,
      league: '',
      newLeague: true
    });
  };
  handleLeague(evt) {
    const val = evt
      .target
      .value
      .replace(/\s+/, '');
    if (val.length > 50) 
      return;
    this.setState({
      league: val,
      chars_left: 50 - val.length
    });
  };

    selectedLeague = () => {
    var spans = window.$('.label-sco');
    jQuery(spans).removeClass('animated-label');
    var spans = window.$('.edit-icons');
    jQuery(spans).addClass('hide');
    this.showListFun();
    this.props.onScoringLeversSave(this.state.league);

  };
    editLeague = () => {
    const season = this.state.league;
    var spans = window.$('.label-sco');
    jQuery(spans).removeClass('animated-label');
    var spans = window.$('.edit-icons');
    jQuery(spans).addClass('hide');
    const items = this.state.getItems;
    const that = this;
    this.setState({
      chars_left: 50 - season.length,
      newLeague:false
    });
    {
      Object
        .keys(items)
        .map((league) => {
          var scoringLevers = items[league].scoringLevers;
          if (items[league].leaguename == season) {
            Object
              .keys(scoringLevers)
              .map((key, values) => {
                const param = scoringLevers[values].paramname;
                const scoringVal = scoringLevers[values];
                if (param == 'rush yards') {
                  that.setState({rushYardsVal: scoringVal.assnvalue, assosiaterushYard: scoringVal.associationid});
                }
                if (param == 'receiving yards') {
                  that.setState({receivingYards: scoringVal.assnvalue, assosiatereceivingYard: scoringVal.associationid});
                }
                if (param == 'pass yards') {
                  that.setState({passYardsVal: scoringVal.assnvalue, assosiatepassYard: scoringVal.associationid});
                }
                if (param == 'passing touchdowns') {
                  that.setState({pTouchdownsVal: scoringVal.assnvalue, assosiatepTouchYard: scoringVal.associationid});
                }
                if (param == 'receptions') {
                  that.setState({receptionVal: scoringVal.assnvalue, assosiatereception: scoringVal.associationid});
                }
                if (param == 'qb Interceptions') {
                  that.setState({qbInterVal: scoringVal.assnvalue, assosiateqb: scoringVal.associationid});
                  // if (assosiateId == undefined) {   that     ._saveUser()     .then(() => {
                  // resolve();     that.initialValues();     }); }else{   that.setState({open:
                  // true}); }
                }

              })
          }
        })
    }
    this.setState({open: true});
  }
  handleListClick(season) {
    var spans = window.$('.label-sco');
    jQuery(spans).removeClass('animated-label');
    var spans = window.$('.edit-icons');
    jQuery(spans).addClass('hide');
    var spans = window.$('.scoring_'+season);
    jQuery(spans).addClass('animated-label');
    var spans = window.$('.scoringspan_'+season);
    jQuery(spans).removeClass('hide');
    // const items = this.state.getItems;
    // const that = this;
    this.setState({league: season});
    // this.setState({
    //   chars_left: 50 - season.length,
    //   newLeague:false
    // });
    // {
    //   Object
    //     .keys(items)
    //     .map((league) => {
    //       var scoringLevers = items[league].scoringLevers;
    //       if (items[league].leaguename == season) {
    //         Object
    //           .keys(scoringLevers)
    //           .map((key, values) => {
    //             const param = scoringLevers[values].paramname;
    //             const scoringVal = scoringLevers[values];
    //             if (param == 'rush yards') {
    //               that.setState({rushYardsVal: scoringVal.assnvalue, assosiaterushYard: scoringVal.associationid});
    //             }
    //             if (param == 'receiving yards') {
    //               that.setState({receivingYards: scoringVal.assnvalue, assosiatereceivingYard: scoringVal.associationid});
    //             }
    //             if (param == 'pass yards') {
    //               that.setState({passYardsVal: scoringVal.assnvalue, assosiatepassYard: scoringVal.associationid});
    //             }
    //             if (param == 'passing touchdowns') {
    //               that.setState({pTouchdownsVal: scoringVal.assnvalue, assosiatepTouchYard: scoringVal.associationid});
    //             }
    //             if (param == 'receptions') {
    //               that.setState({receptionVal: scoringVal.assnvalue, assosiatereception: scoringVal.associationid});
    //             }
    //             if (param == 'qb Interceptions') {
    //               that.setState({qbInterVal: scoringVal.assnvalue, assosiateqb: scoringVal.associationid});
    //               // if (assosiateId == undefined) {   that     ._saveUser()     .then(() => {
    //               // resolve();     that.initialValues();     }); }else{   that.setState({open:
    //               // true}); }
    //             }

    //           })
    //       }
    //     })
    // }
    // this.setState({open: true});
  }
  initialValues = () => {
    this.setState({updatedLeague: []});
    return new Promise((resolve, reject) => {
      axios
        .get('/api/ip/scoringLevers')
        .then((result) => {
          if (result.data.status == 'success') {
            const dataVal = result.data.data;
            this.setState({getItems: dataVal});
            // console.log(dataVal);
            const that = this;
            var leagueArray = [];
            var usersWithName = Object
              .keys(dataVal)
              .map(function (key) {
                const user = dataVal[key];
                const profileId = user.profileid;
                const assosiateId = user.associationid;

                                  that.setState((oldState) => {     
                                            
                                leagueArray.push(user.leaguename);
                return {         leagueNames: leagueArray     }; });

                console.log(that.state.leagueNames);
              });
          }
        })
        .catch(error => {
          console.log(error);
        });
    })
  };

  _saveUser() {
    const values = {
      "league": {
        "leaguename": this.state.league,
        "scoringLevers": [
          {
            "assnvalue": 10,
            "profileid": 1
          }, {
            "assnvalue": 20,
            "profileid": 2
          }, {
            "assnvalue": 25,
            "profileid": 3
          }, {
            "assnvalue": 6,
            "profileid": 4
          }, {
            "assnvalue": 0.5,
            "profileid": 5
          }, {
            "assnvalue": -2,
            "profileid": 6
          }
        ]
      }
    };
    const that = this;
    return axios
      .post('/api/ip/scoringLevers', values)
      .then((res) => {
        this
          .props
          .onScoringLeversSave(this.state.league);
        that.initialValues();
        that.setState({open: false});
      }, () => {})
  };

  updateValues = () => {
      if(this.state.leagueNames.includes(this.state.league)){
 this.setState({error: true});
      }
      else{
        
        this.setState({error: false});
    if (!this.state.newLeague) {
      const values = {
        "league": {
          "leaguename": this.state.league,
          "scoringLevers": [
            {
              "assnvalue": this.state.rushYardsVal,
              "associationid": this.state.assosiaterushYard
            }, {
              "assnvalue": this.state.receivingYards,
              "associationid": this.state.assosiatereceivingYard
            }, {
              "assnvalue": this.state.passYardsVal,
              "associationid": this.state.assosiatepassYard
            }, {
              "assnvalue": this.state.pTouchdownsVal,
              "associationid": this.state.assosiatepTouchYard
            }, {
              "assnvalue": this.state.receptionVal,
              "associationid": this.state.assosiatereception
            }, {
              "assnvalue": this.state.qbInterVal,
              "associationid": this.state.assosiateqb
            }
          ]
        }
      }
      const that = this;
      return axios
        .put('/api/ip/scoringLevers', values)
        .then((res) => {
          this
            .props
            .onScoringLeversSave(this.state.league);
          that.initialValues();
          that.setState({open: false});
        }, () => {})
    } else {
        this._saveUser();
    }
      }
  };
  componentDidMount() {
    this.initialValues();
  }
  render() {
    const updateValFun = this.updateVals;
    const {open} = this.state;
    function NumberList(props) {
      const numbers = props.numbers;
      const classObj = props.classObj;
      const dataVal = props.dataVal;
      const listItems = numbers.map((number) => <li key={number.toString()} onClick={updateValFun} data-key={dataVal}>
        {number}
      </li>);
      return (
        <ul className={classObj}>
          {listItems}
        </ul>
      );
    }; 
    const rushYards = [10, 15, 20];
    const receivingYards = [10, 15, 20];
    const passYards = [20, 25, 30];
    const passingTouch = [4, 5, 6];
    const receptions = [0, 0.5, 1];
    const qbInter = [-3, -2, -1];
    const items = this.state.getItems;
    return (
      <div>
        <div className="scoring-btn" onClick={this.showListFun}>Scoring Levers</div>

        {/* <button className="new-btn" onClick={this.initialValues}><Glyphicon glyph="plus" className="glyph-position color-white"/></button> */}
        <div
          className={"scoring-list " + (this.state.showList
          ? 'show'
          : 'hidden')}>
          {Object
            .keys(items)
            .map((league) => {
              console.log(this.state.league === items[league].leaguename);
              return (
                <div key={league}>
                  <input
                    checked={this.state.league === items[league].leaguename}
                    onChange={() => this.handleListClick(items[league].leaguename)}
                    id={'season_' + league}
                    value={league}
                    type="checkbox"/>
                  <label htmlFor={'season_' + league} className={'label-sco scoring_' + items[league].leaguename}>{items[league].leaguename}</label>
                  <span className={'hide '+'edit-icons '+ 'scoringspan_' + items[league].leaguename}>
<span onClick={this.selectedLeague}> <Glyphicon glyph="ok" className="glyph-position color-white"/> </span>
<span onClick={this.editLeague}> <Glyphicon glyph="pencil" className="glyph-position color-white"/> </span>
<span> <Glyphicon glyph="remove" className="glyph-position color-white"/> </span>
</span>
                </div>

              );
            })}
          <div onClick={this.addLeague}>
            <input
              checked={this.state.league === ''}
              onChange={() => this.addLeague()}
              type="checkbox"/>
            <label><Glyphicon glyph="plus" className="glyph-position color-white"/></label>

          </div>
        </div>
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
          modal: "sco-modlwidth"
        }}
          little>
          <div className="">
            <Grid className="full-width">
              <Row className="show-grid">
                <div>
                  <h3 className="scoring-title">
                    Scoring Levers
                  </h3>
                </div>
              </Row>

              <Row className="sc-p">
                <Col lg={3} sm={4} className=" pr0">
                  <div className="color-white">Rush Yards</div>
                  <div className="sc-dropdown" onClick={this.openDropDown} data-label='rushYards'>
                    {this.state.rushYardsVal}
                    <Glyphicon
                      glyph="menu-down"
                      className="sc-dropdown-glyph"
                      data-label='rushYards'/>
                  </div>
                  <div>
                    <NumberList
                      numbers={rushYards}
                      dataVal='rushYardsVal'
                      classObj=
                      {'sc-dropdwn '+((this.state.rushYards === true) ?'show':'hide')}/>

                  </div>
                </Col>

                <Col lg={1} sm={2} className="sc-label pl0">YDS/Point
                </Col>
                <Col lg={3} sm={4} className="pr0">
                  <div className="color-white">Recieving Yards</div>
                  <div className="sc-dropdown" onClick={this.openDropDown} data-label='rYards'>
                    {this.state.receivingYards}
                    <Glyphicon glyph="menu-down" className='sc-dropdown-glyph' data-label='rYards'/>
                  </div>

                  <div>
                    <NumberList
                      numbers={receivingYards}
                      dataVal='receivingYards'
                      classObj=
                      {'sc-dropdwn '+((this.state.rYards === true) ?'show':'hide')}/>

                  </div>

                </Col>

                <Col lg={1} sm={2} className="sc-label pl0">YDS/Point
                </Col>
                <Col lg={3} sm={4} smHidden className="pr0">
                  <div className="color-white">Pass Yards</div>
                  <div className="sc-dropdown" onClick={this.openDropDown} data-label='passYards'>
                    {this.state.passYardsVal}
                    <Glyphicon
                      glyph="menu-down"
                      className="sc-dropdown-glyph"
                      data-label='passYards'/>
                  </div>
                  <div>
                    <NumberList
                      numbers={passYards}
                      dataVal='passYardsVal'
                      classObj=
                      {'sc-dropdwn '+((this.state.passYards === true) ?'show':'hide')}/>

                  </div>
                </Col>

                <Col lg={1} sm={2} smHidden className="sc-label pl0">YDS/Point
                </Col>
              </Row>

              <Row className="sc-p">
              <Col lg={3} sm={4} lgHidden className="pr0">
                  <div className="color-white">Pass Yards</div>
                  <div className="sc-dropdown" onClick={this.openDropDown} data-label='passYards'>
                    {this.state.passYardsVal}
                    <Glyphicon
                      glyph="menu-down"
                      className="sc-dropdown-glyph"
                      data-label='passYards'/>
                  </div>
                  <div>
                    <NumberList
                      numbers={passYards}
                      dataVal='passYardsVal'
                      classObj=
                      {'sc-dropdwn '+((this.state.passYards === true) ?'show':'hide')}/>

                  </div>
                </Col>

                <Col lg={1} sm={2} lgHidden className="sc-label pl0">YDS/Point
                </Col>
                <Col lg={3} sm={4} className=" pr0">
                  <div className="color-white">P-Touchdowns</div>
                  <div className="sc-dropdown" onClick={this.openDropDown} data-label='ruYards'>
                    {this.state.pTouchdownsVal}
                    <Glyphicon
                      glyph="menu-down"
                      className="sc-dropdown-glyph"
                      data-label='ruYards'/>
                  </div>
                  <div>
                    <NumberList
                      numbers={passingTouch}
                      dataVal='pTouchdownsVal'
                      classObj=
                      {'sc-dropdwn '+((this.state.ruYards === true) ?'show':'hide')}/>

                  </div>
                </Col>

                <Col lg={1} sm={2} className="sc-label pl0">Points
                </Col>
                <Col lg={3} sm={4} className="pr0">
                  <div className="color-white">Receptions</div>
                  <div className="sc-dropdown" onClick={this.openDropDown} data-label='reception'>
                    {this.state.receptionVal}
                    <Glyphicon
                      glyph="menu-down"
                      className="sc-dropdown-glyph"
                      data-label='reception'/>
                  </div>
                  <div>
                    <NumberList
                      numbers={receptions}
                      dataVal='receptionVal'
                      classObj=
                      {'sc-dropdwn '+((this.state.reception === true) ?'show':'hide')}/>

                  </div>
                </Col>

                <Col lg={1} sm={2} className="sc-label pl0">PPR
                </Col>
                <Col lg={3} sm={4} className="pr0">
                  <div className="color-white">QB Interceptions</div>
                  <div className="sc-dropdown" onClick={this.openDropDown} data-label='qbInter'>
                    {this.state.qbInterVal}
                    <Glyphicon
                      glyph="menu-down"
                      className="sc-dropdown-glyph"
                      data-label='qbInter'/>
                  </div>
                  <div>
                    <NumberList
                      numbers={qbInter}
                      dataVal='qbInterVal'
                      classObj=
                      {'sc-dropdwn '+((this.state.qbInter === true) ?'show':'hide')}/>

                  </div>
                </Col>

                <Col lg={1} sm={2} className="sc-label pl0">Points
                </Col>
              </Row>

              <Row className="scoring-text">
                <div className="scoring-txttitle">Name of League
                  <span className="max-text"> 
                    {this.state.chars_left}
                    {' '+'char remaining'}</span>
                </div>
               {this.state.error && <span className="error-text">League name already exists!</span>}
                <input
                  type="text"
                  className="scoring-input"
                  value={this.state.league}
                  onChange={this
                  .handleLeague
                  .bind(this)}/>
              </Row>

              <Row className="scoring-text save-div">
                <button className="save-btn" onClick={this.updateValues}>
                  SAVE SCORING INPUTS
                </button>
              </Row>

            </Grid>

          </div>

        </Modal>
      </div>
    );
  }
}
// } SignUpModal.propTypes = {   onUserSubscribe: PropTypes.func.isRequired, };


// WEBPACK FOOTER //
// src/components/ScoringLever/ScoringLever.js