import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import WeatherUtils from '../../../utils/WeatherUtils';
import Utils from '../../../utils/Utils';

import windDirection from '../../../resources/basic/images/wind-direction.png';

import './MatchStadiumStats.css';

const stadiums = Utils.importAll(require.context('../../../resources/basic/images/stadiums', false, /\.(png|jpe?g|svg)$/));

class StadiumValues extends React.Component {

    constructor() {
        super();

        this.state = {
            location: ''
        };
    }

    componentDidMount() {
        this._loadLocationByZip();
    }

    componentDidUpdate(oldProps) {
        if(oldProps.stadium.zip !== this.props.stadium.zip) this._loadLocationByZip();
    }

    _loadLocationByZip() {
        if(!this.props.stadium.zip) return;
        axios.get('http://maps.googleapis.com/maps/api/geocode/json?address='+this.props.stadium.zip+'&sensor=true').then((res) => {
            const address = res.data.results[0].address_components;
            this.setState({
                location: address[1].long_name + ', ' + address[3].short_name
            });
        }, () => {
           this.setState({
               location: this.props.stadium.zip
           });
        });
    }

    render() {
        const windDeg = WeatherUtils.getWindDegrees(this.props.stadium.winddirection);
        const stadiumDeg = this.props.stadium.stadiumorientation + 90;

        return (
            <div className="Comp-StadiumValues">
                <h2>{this.props.stadium.stadiumname}</h2>
                <span className="place">{this.state.location}</span>
                <p className="place_info">
                    ELEV. <strong>{this.props.stadium.elevation}</strong> ft
                    <span>
                    <strong>{Math.abs(this.props.stadium.long)}°</strong>&nbsp;
                        {(this.props.stadium.long > 0) ? 'N' : 'S'},&nbsp;
                        <strong>{Math.abs(this.props.stadium.long)}°</strong>&nbsp;
                        {(this.props.stadium.long > 0) ? 'E' : 'W'}
                </span>
                </p>

                <div className="sto">
                    <div className="weather_wind">
                    <span>
                        <strong>WIND:</strong> {this.props.stadium.windspeed} mph
                            <img alt="wind"
                                 style={{
                                     transform: 'rotate(' + windDeg + 'deg)',
                                     'msTransform': 'rotate(' + windDeg + 'deg)',
                                     'WebkitTransform': 'rotate(' + windDeg + 'deg)',
                                     'MozTransform': 'rotate(' + windDeg + 'deg)'
                                 }}
                                 src={windDirection}
                                 title={this.props.stadium.winddirection}/>
                    </span>
                        <p>
                            From the {WeatherUtils.translateDirection(this.props.stadium.winddirection)}<br/>
                            up to {this.props.stadium.maxwindspeed} mph
                        </p>
                    </div>
                </div>

                <div className="stadion">
                    <img alt="stadion" style={{
                        maxWidth: '100%',
                        transform: 'rotate(' + stadiumDeg + 'deg)',
                        'msTransform': 'rotate(' + stadiumDeg + 'deg)',
                        'WebkitTransform': 'rotate(' + stadiumDeg + 'deg)',
                        'MozTransform': 'rotate(' + stadiumDeg + 'deg)'
                    }} src={stadiums[this.props.stadium.stadiumgraphic]} title={this.props.stadium.stadiumgraphic}/>
                </div>

                {false && <span className="uploaded">Uploaded 13 minutes ago</span>}
            </div>
        );
    }
};

StadiumValues.propTypes = {
    stadium: PropTypes.object.isRequired
};


class MatchStadiumStats extends React.Component {

    constructor() {
        super();

        this.state = {
            stadium: null
        };
    }

    componentDidMount() {
        this._loadGameStadium();
    }

    componentDidUpdate(oldProps) {
        if (oldProps.weatherKey !== this.props.weatherKey) this._loadGameStadium();
    }

    _loadGameStadium() {
        if (this.props.weatherKey === null) return;

        axios.get('/api/weather/gameStadium', {
            params: {
                weatherKey: this.props.weatherKey
            }
        }).then((res) => {
            this.setState({
                stadium: res.data.data.stadium
            });
        }, () => {
            this.setState({
                stadium: null
            });
        });
    }


    render() {
        return (
            <div className="Comp-MatchStadiumStats weather-graphs_right weather-padd">

                {this.state.stadium === null
                && <h2>Loading...</h2>}

                {this.state.stadium !== null
                && <StadiumValues stadium={this.state.stadium}/>}
            </div>
        );
    }
}

MatchStadiumStats.propTypes = {
    weatherKey: PropTypes.string,
};

export default MatchStadiumStats;
