import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import teams from '../../../../utils/parameters/teams';

import WeatherStatIPPlayers from './WeatherStatIPPlayers';
import WeatherStatIPTeamDefensiveStats from './WeatherStatIPTeamDefensiveStats';


class WeatherStatIPTeamHalf extends React.Component {

    constructor() {
        super();

        this.state = {
            data: null
        };
    }

    componentDidMount() {
        this._loadTeamData();
    }

    componentDidUpdate(oldProps) {
        if (oldProps.weatherKey !== this.props.weatherKey
            || oldProps.team !== this.props.team) {
            this._loadTeamData();
        }
    }

    _loadTeamData() {
        if (!this.props.weatherKey || !this.props.team) return;

        axios.get('/api/weather/teamStats', {
            params: {
                team: this.props.team,
                weatherKey: this.props.weatherKey
            }
        }).then((res) => {
            this.setState({
                data: res.data.data
            });
        }, () => {
            this.setState({
                data: null
            });
        });
    }


    render() {
        return (
            <div className="Comp-WeatherStatIPTeamHalf">
                {this.state.data && <div className=" weather-graphs_half">
                    <div className="temp-statip-data_half-title weather-padd">
                        {teams[this.props.team].fullName} <span className="gp">GP: {this.state.data.team.gpsumofgamesplayed}</span> <span
                        className="pf">PF: <strong>{this.state.data.team.pfsumofpointsforthatoffense}</strong></span> <span
                        className="pa">PA: <strong>{this.state.data.team.pasumofthepointsgivenupbythatoffense}</strong></span>
                    </div>

                    <div className="linewrap weather-padd">
                        <WeatherStatIPPlayers position='QB' players={this.state.data.players.QB}/>

                        <WeatherStatIPPlayers position='RB' players={this.state.data.players.RB}/>
                    </div>

                    <div className="linewrap weather-padd">
                        <WeatherStatIPPlayers position='WR1' players={this.state.data.players.WR1}/>

                        <WeatherStatIPPlayers position='WR2' players={this.state.data.players.WR2}/>
                    </div>

                    <div className="linewrap weather-padd">
                        <WeatherStatIPPlayers position='TE' players={this.state.data.players.TE}/>

                        <WeatherStatIPPlayers position='K' players={this.state.data.players.K}/>
                    </div>

                    <WeatherStatIPTeamDefensiveStats team={teams[this.props.team].fullName} teamData={this.state.data.team}/>

                </div>}
            </div>
        );
    }
}

WeatherStatIPTeamHalf.propTypes = {
    weatherKey: PropTypes.string.isRequired,
    team: PropTypes.string.isRequired
};

export default WeatherStatIPTeamHalf;
