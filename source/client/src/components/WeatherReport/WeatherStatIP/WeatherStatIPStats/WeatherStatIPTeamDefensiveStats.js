import React from 'react';
import PropTypes from 'prop-types';

class WeatherStatIPTeamDefensiveStats extends React.Component {

    render() {
        return (
            <div className="Comp-WeatherStatIPTeamDefensiveStats bottomdata weather-padd">
                <div className="twocoldata threecoldata">
                    <div className="dataheader">
                        {this.props.team.replace(/.*?(\w+)$/, '$1')} Defense
                        <span>GP: {this.props.teamData.gpsumofgamesplayed}</span>
                    </div>
                    <div className="sto">
                        <ul>
                            <li>
                                <span className="dleft">Fant. Pts/Gm:</span>
                                <span className="dright">{this.props.teamData.fantasypointspergame}</span>
                            </li>
                            <li>
                                <span className="dleft">Fant. Floor:</span>
                                <span className="dright">{this.props.teamData.fantasyfloor}</span>
                            </li>
                            <li>
                                <span className="dleft">Fant. Ceiling:</span>
                                <span className="dright">{this.props.teamData.fantasyceiling}</span>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <span className="dleft">Yards/Gm:</span>
                                <span className="dright">{this.props.teamData.yardsallowed}</span>
                            </li>
                            <li>
                                <span className="dleft">Rush/Gm:</span>
                                <span className="dright">{this.props.teamData.rushyardsallowed}</span>
                            </li>
                            <li>
                                <span className="dleft">Pass/Gm:</span>
                                <span className="dright">{this.props.teamData.passyardsallowed}</span>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <span className="dleft">TDs:</span>
                                <span className="dright">{this.props.teamData.defensivetds}</span>
                            </li>
                            <li>
                                <span className="dleft">INT:</span>
                                <span className="dright">{this.props.teamData.interceptions}</span>
                            </li>
                            <li>
                                <span className="dleft">Sacks:</span>
                                <span className="dright">{this.props.teamData.sacks}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

WeatherStatIPTeamDefensiveStats.propTypes = {
    teamData: PropTypes.object.isRequired,
    team: PropTypes.string.isRequired
};

export default WeatherStatIPTeamDefensiveStats;
