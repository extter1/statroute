import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';

import './WeatherStatIPPlayers.css';


const values = {
    'QB': [
        {field: 'fantasypointspergame', label: 'Fant. Pts/Gm'},
        {field: 'fantasyfloor', label: 'Fant. Floor'},
        {field: 'fantasyceiling', label: 'Fant. Ceiling'},
        {field: 'scrimmageyards', label: 'Scrim. yards'},
        {field: 'touchdowns', label: 'TDs'},
        {field: 'interceptions', label: 'INT'}
    ],
    'RB': [
        {field: 'fantasypointspergame', label: 'Fant. Pts/Gm'},
        {field: 'fantasyfloor', label: 'Fant. Floor'},
        {field: 'fantasyceiling', label: 'Fant. Ceiling'},
        {field: 'scrimmageyards', label: 'Scrim. yards'},
        {field: 'touchdowns', label: 'TDs'},
        {field: 'fumbleslost', label: 'Fumb. lost'},
    ],
    'WR1': [
        {field: 'fantasypointspergame', label: 'Fant. Pts/Gm'},
        {field: 'fantasyfloor', label: 'Fant. Floor'},
        {field: 'fantasyceiling', label: 'Fant. Ceiling'},
        {field: 'scrimmageyards', label: 'Scrim. yards'},
        {field: 'touchdowns', label: 'TDs'},
        {field: 'fumbleslost', label: 'Fumb. lost'},
    ],
    'WR2': [
        {field: 'fantasypointspergame', label: 'Fant. Pts/Gm'},
        {field: 'fantasyfloor', label: 'Fant. Floor'},
        {field: 'fantasyceiling', label: 'Fant. Ceiling'},
        {field: 'scrimmageyards', label: 'Scrim. yards'},
        {field: 'touchdowns', label: 'TDs'},
        {field: 'fumbleslost', label: 'Fumb. lost'},
    ],
    'TE': [
        {field: 'fantasypointspergame', label: 'Fant. Pts/Gm'},
        {field: 'fantasyfloor', label: 'Fant. Floor'},
        {field: 'fantasyceiling', label: 'Fant. Ceiling'},
        {field: 'scrimmageyards', label: 'Scrim. yards'},
        {field: 'touchdowns', label: 'TDs'},
        {field: 'fumbleslost', label: 'Fumb. lost'},
    ],
    'K': [
        {field: 'fantasypointspergame', label: 'Fant. Pts/Gm'},
        {field: 'fantasyfloor', label: 'Fant. Floor'},
        {field: 'fantasyceiling', label: 'Fant. Ceiling'},
        {field: 'fgattempts', label: 'FG att.'},
        {field: 'fgmade', label: 'FG made'},
        {field: 'long', label: 'Long'}
    ],
};

class WeatherStatIPPlayers extends React.Component {

    constructor() {
        super();

        this.state = {
            player: null
        };
    }

    componentDidMount() {
        this.setState({
            player: this.props.players[0]
        });
    }

    componentDidUpdate(oldProps) {
        if(oldProps.players !== this.props.players) {
            this.setState({
                player: this.props.players[0]
            });
        }
    }

    render() {
        if (this.state.player === null) return null;

        const players = this.props.players.map((player) => {
            return {value: player.playername, label: player.playername, player: player};
        });

        return (
            <div className="Comp-WeatherStatIPPlayers twocoldata">
                <div className="dataheader">
                    <div className="position">{this.props.position.replace(/([^0-9]+).*/, '$1')}</div>
                    <Select searchable={false} clearable={false} placeholder="Player" options={players}
                            className="react-select"
                            value={this.state.player && this.state.player.playername} onChange={(val) => {
                        this.setState({
                            player: val.player
                        });
                    }}
                    />
                    <span className="gp">GP: {this.state.player && this.state.player.totalgames}</span>
                </div>
                <div className="sto">
                    <ul>
                        {this.state.player && values[this.props.position].map((val, index) => {
                            if (index >= 3) return null;
                            return (
                                <li key={index}>
                                    <span className="dleft">{val.label}:</span>
                                    <span className="dright">{this.state.player[val.field]}</span>
                                </li>
                            );
                        })}
                    </ul>
                    <ul>
                        {this.state.player && values[this.props.position].map((val, index) => {
                            if (index < 3) return null;
                            return (
                                <li key={index}>
                                    <span className="dleft">{val.label}:</span>
                                    <span className="dright">{this.state.player[val.field]}</span>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}


WeatherStatIPPlayers.propTypes = {
    position: PropTypes.string.isRequired,
    players: PropTypes.array.isRequired,
};

export default WeatherStatIPPlayers;
