import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';

import WeatherStatIPTeamHalf from './WeatherStatIPTeamHalf';

import './WeatherStatIPStats.css';

class WeatherStatIPStats extends React.Component {


    render() {
        return (
            <div className={ClassNames('Comp-WeatherStatIPStats', 'temp-statip-data', {
                visible: this.props.visible
            })}>
                <WeatherStatIPTeamHalf weatherKey={this.props.weatherKey} team={this.props.awayTeam}/>
                <WeatherStatIPTeamHalf weatherKey={this.props.weatherKey} team={this.props.homeTeam}/>
            </div>
        );
    }
}

WeatherStatIPStats.propTypes = {
    visible: PropTypes.bool.isRequired,
    weatherKey: PropTypes.string.isRequired,
    homeTeam: PropTypes.string.isRequired,
    awayTeam: PropTypes.string.isRequired,
};

export default WeatherStatIPStats;
