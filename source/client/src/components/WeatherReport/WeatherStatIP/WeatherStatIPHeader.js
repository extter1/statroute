import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import WeatherUtils from '../../../utils/WeatherUtils';

import './WeatherStatIPHeader.css';

class WeatherStatIPHeader extends React.Component {


    constructor() {
        super();

        this.state = {
            data: null
        };
    }

    componentDidMount() {
        this._loadRecords();
    }

    componentDidUpdate(oldProps) {
        if (oldProps.weatherKey !== this.props.weatherKey) this._loadRecords();
    }

    _loadRecords() {
        if (this.props.weatherKey === null) return;

        axios.get('/api/weather/gameGraphData', {
            params: {
                weatherKey: this.props.weatherKey
            }
        }).then((res) => {
            this.setState({
                data: res.data.data.records[0]
            });
        }, () => {
            this.setState({
                data: null
            });
        });
    }

    render() {
        let temp = [];
        if(this.state.data) {
            if(this.state.data.tempcode1) {
                temp.push(WeatherUtils.translateTempCode(this.state.data.tempcode1));
            }
            if(this.state.data.tempcode2) {
                temp.push(WeatherUtils.translateTempCode(this.state.data.tempcode2));
            }
        }

        return (
            <div className="Comp-WeatherStatIPHeader temp-statip-header weather-padd">
                {this.state.data &&
                <h2>At {this.state.data.hometeam.replace(/.*?(\w+)$/, '$1')}, {temp.length > 0 ? temp.join(' & ') + ' weather,' : ''} {this.state.data.wconditions}, previous 10 years</h2>}
            </div>
        );
    }
}

WeatherStatIPHeader.propTypes = {
    weatherKey: PropTypes.string.isRequired,
};

export default WeatherStatIPHeader;