import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';

import WeatherStatIPButton from './WeatherStatIPButton';
import WeatherStatIPHeader from './WeatherStatIPHeader';
import WeatherStatIPStats from './WeatherStatIPStats/WeatherStatIPStats';


class WeatherStatIP extends React.Component {

    constructor() {
        super();

        this.state = {
            open: false
        };
    }

    render() {
        return (
            <div className="Comp-WeatherStatIP weather-report martop20">

                {this.props.weatherKey &&
                <WeatherStatIPHeader homeTeam={this.props.homeTeam} weatherKey={this.props.weatherKey}/>}

                {this.props.weatherKey &&
                <WeatherStatIPStats visible={this.state.open} weatherKey={this.props.weatherKey}
                                    homeTeam={this.props.homeTeam} awayTeam={this.props.awayTeam}/>}

                <WeatherStatIPButton
                    className={ClassNames({
                        clicked: this.state.open
                    })}
                    onClick={() => {
                        this.setState((oldState) => {
                            return {
                                open: !oldState.open
                            };
                        });
                    }}/>
            </div>
        );
    }
}

WeatherStatIP.propTypes = {
    weatherKey: PropTypes.string,
    homeTeam: PropTypes.string,
    awayTeam: PropTypes.string,
};

export default WeatherStatIP;
