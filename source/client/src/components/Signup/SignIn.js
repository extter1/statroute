import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import PropTypes from 'prop-types';
import './SignIn.css';
import axios from 'axios';
import logo from '../../resources/basic/images/footer-logo.png';
import { Glyphicon,Grid,Col,Row } from 'react-bootstrap';
const jQuery = window.$;
export default class SignUpModal extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
        firstname: '',
        lastname: '',
        email:'',
        device:'',
        ip:'',
        location:'',
        inputStay:'',
        error: false,
        open: false,
        signupsuccess:false,
        signupfail:false,
        errorfname : false,
        errorlname:false,
        erroremail : false,
        erroremailpattern : false
    };
}
handleBlur(event)  {
  const val = event.target.value;
  console.log(event.target.value);
  if(val != ''){
    const input = jQuery(event.target);
    input.addClass('has-val');
  }
}

handleFnameChange(evt) {
  const input = jQuery(jQuery(evt.target)[0].nextSibling);
  input.removeClass('top-push');
  const val = evt.target.value.replace(/\s+/, '');
  if (val.length > 25) return;
  this.setState({
      firstname: val,
      error: false
  });
}

handleLnameChange(evt) {
  const input = jQuery(jQuery(evt.target)[0].nextSibling);
  input.removeClass('top-push');
  const val = evt.target.value.replace(/\s+/, '');
  if (val.length > 25) return;
  this.setState({
      lastname: val,
      error: false
  });
}
handleEmailChange(evt) {
  const input = jQuery(jQuery(evt.target)[0].nextSibling);
  input.removeClass('top-push');
  const val = evt.target.value;
  this.setState({
      email: val,
      error: false
  });
}
handleSubscribe(values) {
  console.log('success',values);
   return new Promise((resolve, reject) => {
       axios.post('/api/auth/subscribe', values)
       
       .then((result) => {
           if(result.data.status == 'success'){
           console.log(result);
            this.setState({signupsuccess:true });
           }
       })
        .catch(error => {
            console.log(error);
            this.setState({signupsuccess:false });
                        this.setState({signupfail:true });
               setTimeout(function() {   this.setState({signupfail:false }); }.bind(this), 3000);
                         this.setState ({
        firstname: '',
        lastname: '',
        email:'',
        device:'',
        ip:'',
        location:'',
        inputStay:''
                         });
 });
   })
}
validateValues() {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (this.state.firstname.length === 0 || this.state.firstname > 25){
    this.setState({
      errorfname: true
  }); return false;
  } 
  else{
    this.setState({
      errorfname: false
  })
  }
  if(this.state.lastname.length === 0 || this.state.lastname > 25) {
    this.setState({
      errorlname: true
  }); return false;
  }
  else{
    this.setState({
      errorlname: false
  })
  }
  if(this.state.email.length === 0) {
    this.setState({
      erroremail: true
  }); return false;
  }
  else{
    this.setState({
      erroremail: false
  })
  }
  if (re.test(String(this.state.email).toLowerCase()) != true) {
    this.setState({
      erroremailpattern: true
  }); return false;
  }
  else{
    this.setState({
      erroremailpattern: false
  })
  }

  return true;
}

handleSubmit(evt) {
  if(evt) evt.preventDefault();
  if (this.validateValues()) {
      this.handleSubscribe ({
          firstname: this.state.firstname,
          lastname: this.state.lastname,
          email: this.state.email,
          ip:this.props.signupinfo.ip,
          device:this.props.signupinfo.device,
          location:this.props.signupinfo.location
      }).then(() => {
          // no need to do anything router will automatically redirect
      }, () => {
          this.setState({
              error: true
          })
      })
  } else {
      this.setState({
          error: true
      })
  }
}
  onOpenModal = () => {
          this.state = {
        firstname: '',
        lastname: '',
        email:'',
        device:'',
        ip:'',
        location:'',
        inputStay:''
    };
    this.setState({ open: true });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };
 
  render() {
    const { open } = this.state;
     const signupsuccess = this.state.signupsuccess;
     const signupfail = this.state.signupfail;
        const message = signupsuccess ? (
       <div className = "sign-sucess">Thank You For Signing Up.</div>
    ) : (
   <div></div>
    );
    const message1 = signupfail ? (
     <div className = "sign-fail">Sign Up Failed</div>
    ) : (
      <div></div>
    );
    return (
      <div>
        <button onClick={this.onOpenModal} className="cta "><span className="color-white">Get your free trial Today</span></button>  
        <Modal open={open} onClose={this.onCloseModal} classNames={{modal:"width-modal"}} little>
        <div className="">
                   <Grid className="full-width">
                 {message}
                  {message1}
  <Row className="show-grid">
  <Col lg={6} sm={6} md={6} className="sign-bg hidden-xs">
<img src={logo} className="sign-logo"/>

<h2 className="sign-wel">Welcome to the new standard for stats delivery.</h2>
    </Col>

    <Col lg={6} sm={6} md={6} className="sign-bgothr">
    <div className="limiter">
			<div className="container-login100">
				<div className="wrap-login100 p-t-45 p-b-20">
					<form className="login100-form validate-form">
						<span className="login100-form-title">
							Get Started
						</span>
            
						<div className="wrap-input100 validate-input m-t-35 m-b-30" data-validate = "Enter first Name">
							<input className={'input100'} type="serach" name="fname" onChange={this.handleFnameChange.bind(this)} value={this.state.firstname} onBlur={this.handleBlur}/>
							<span className="focus-input100" data-placeholder="First Name *">
              <span className={'errortext' + ((this.state.errorfname) ? ' show' : '')}>First name is a required field</span>
              </span>
						</div>
            <div className="wrap-input100 validate-input  m-b-30" data-validate = "Enter last Name">
							<input className="input100" type="search" name="lname" onChange={this.handleLnameChange.bind(this)} value={this.state.lastname} onBlur={this.handleBlur}/>
							<span className="focus-input100" data-placeholder="Last Name *">
              <span className={'errortext' + ((this.state.errorlname) ? ' show' : '')}>Last name is a required field</span></span>
						</div>
						<div className="wrap-input100 validate-input m-b-45" data-validate="Enter email">
							<input className="input100" type="search" name="email" value={this.state.email} onBlur={this.handleBlur} onChange={this.handleEmailChange.bind(this)}/>
							<span className="focus-input100" data-placeholder="Email Address *">
              <span className={'errortext' + ((this.state.erroremail) ? ' show' : '')}>Email ID is a required field</span> <span className={'errortext' + ((this.state.erroremailpattern) ? ' show' : '')}>Email format is invalid</span></span>
              
						</div>

						<div className="container-login100-form-btn">
							<button className="login100-form-btn" onClick={this.handleSubmit.bind(this)}>
								SIGN UP
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
</Col>
    </Row>
    </Grid>

</div>
 
        </Modal>
      </div>
    );
  }}
// }
// SignUpModal.propTypes = {
//   onUserSubscribe: PropTypes.func.isRequired,
// };