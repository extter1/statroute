import React from 'react';
import Popup from 'react-popup';
import PropTypes from 'prop-types';

import './PopupMessage.css';

class PopupMessage extends React.Component {

    render() {
        return (
            <div className="Comp-PopupMessage">
                <p className="h1">{this.props.title}</p>
                <div className="clearfix"/>
                <div className="block">
                    <div className="content">
                        {this.props.children}
                    </div>
                    <button onClick={() => Popup.close()} type="button" className="buttonarrow">Next</button>
                    <div className="clearfix"/>
                </div>
            </div>
        );
    }
}

PopupMessage.propTypes = {
    title: PropTypes.string.isRequired,
};

export default PopupMessage;
