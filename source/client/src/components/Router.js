import React from 'react';
import {Route, Redirect, Switch, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';
import AboutUs from './HomeNav/AboutUs';
import ContactUs from './HomeNav/ContactUs';
import IP from './IP/IP';
import WeatherReport from './WeatherReport/WeatherReport';
import DepthChart from './DepthChart/DepthChart';

import Header from './Header/Header';
import NewHeader from './Header/NewHeader';
import Error from './Error/Error';
import LandingPage from './LandingPage/LandingPage';
import NewLandingPage from './LandingPage/NewLandingPage';
import SignUp from './Auth/SignUp/SignUp';
import SignIn from './Auth/SignIn/SignIn';
import ForgotPassword from './Auth/SignIn/ForgotPassword/ForgotPassword';
import LogOut from './Auth/LogOut';
import Pages from './Pages/Pages';
import Footer from './Footer';
import Termsandcondition from './Termsandcondition';
import Sitemap from './Sitemap';
import ReactGA from 'react-ga';
import AccountRouter from './Account/AccountRouter';
import Popups from '../Popups';

ReactGA.initialize('UA-105614587-1');


class Router extends React.Component {


    sendPageView(location) {
        window.popesClose();
        ReactGA.set({page: location.pathname});
        ReactGA.pageview(location.pathname);
    }

    componentDidMount() {
        this.sendPageView(this.props.history.location);
        this.unlisten = this.props.history.listen(this.sendPageView);
    }

    componentWillUnmount() {
        this.unlisten();
    }

    render() {
        return (
            <DocumentTitle title="StatRoute">
                <div>
                    
                    <Switch>
                        {this.props.user.loggedIn && <Redirect from='/sign-up' to='/landing'/>}
                        {this.props.user.loggedIn && <Redirect exact from='/sign-in' to='/landing'/>}
                        {!this.props.user.loggedIn && <Redirect exact from='/log-out' to='/landing'/>}

                        <Route exact from='/' render={(props) => (
                            <div>
                    <NewHeader user={this.props.user} />
                            <NewLandingPage {...props} user={this.props.user} signupinfo={this.props.signupinfo}/>
                            </div>
                        )}/>
                           <Route exact from='/landing' render={(props) => (
                               <div>
                                   <Header user={this.props.user}/>
                            <LandingPage {...props} user={this.props.user}/>
                            </div>
                        )}/>
                        <Route exact from='/sign-in'
                               render={(props) => (
                                   <div>
                                       <Header user={this.props.user}/>
                                   <SignIn {...props} onUserSignIn={this.props.onUserSignIn}/>
                                   </div>
                                   )
                               }/>
                        <Route exact from='/log-out'
                               render={(props) => (
                                   <div>
                                       <Header user={this.props.user}/>
                                   <LogOut {...props} onUserLogOut={this.props.onUserLogOut}/></div>
                               )}/>
                        <Route from='/sign-up'
                               render={(props) => (
                                   <div>
                                       <Header user={this.props.user}/>
                                   <SignUp {...props} onUserSignUp={this.props.onUserSignUp}/>
                                   </div>
                               )}/>
                        <Route from='/forgot-password'
                               render={(props) => (
                                   <div>
                                       <Header user={this.props.user}/>
                                   <ForgotPassword {...props}
                                                   onUserForgotPasswordRequest={this.props.onUserForgotPasswordRequest}
                                                   onUserForgotPasswordResetPassword={this.props.onUserForgotPasswordResetPassword}
                                   />
                                   </div>
                               )}/>
      <Route exact from='/about-us'
                               render={(props) => (
                                   <div>
                                       <NewHeader user={this.props.user}/>
                                       <AboutUs/>
                                       </div>
                               )}/>
                                <Route exact from='/contact-us'
                               render={(props) => (
                                   <div>
                                       <NewHeader user={this.props.user}/>
                                       <ContactUs/>
                                       </div>
                               )}/>
   <Route from='/terms-condition'
                               render={(props) => (
                                   <div>
                                       <NewHeader user={this.props.user}/>
                                   <Termsandcondition />
                                   </div>
                               )}/>
                                 <Route from='/sitemap'
                               render={(props) => (
                                   <div>
                                       <NewHeader user={this.props.user}/>
                                   <Sitemap />
                                   </div>
                               )}/>
                        {(this.props.user.loggedIn)
                        && <Route exact path='/stats-ip'
                                  render={(props) => (
                                      <div>
                                          <Header user={this.props.user}/>
                                      <IP {...props} user={this.props.user}/>
                                      </div>
                                  )}/>}

                        {(this.props.user.loggedIn)
                        && <Route exact path='/weather-report'
                                  render={(props) => (
                                      <div>
                                          <Header user={this.props.user}/>
                                      <WeatherReport {...props} user={this.props.user}/>
                                      </div>
                                  )}/>}
                        {(this.props.user.loggedIn)
                        && <Route exact path='/depth-chart'
                                  render={(props) => (
                                      <div>
                                          <Header user={this.props.user}/>
                                      <DepthChart {...props} user={this.props.user}/>
                                      </div>
                                  )}/>}

                        {(this.props.user.loggedIn)
                        && <Route path='/account'
                                  render={(props) => (
                                      <div>
                                          <Header user={this.props.user}/>
                                      <AccountRouter {...props} user={this.props.user}
                                                     onForceUserLoad={this.props.forceLoadUser}
                                      />
                                      </div>
                                  )}/>}

                        <Route exact path='/pages/:slug'
                               render={(props) => (
                                   <div>
                                       <Header user={this.props.user}/>
                                   <Pages {...props} user={this.props.user}/>
                                   </div>
                               )}/>

                        <Route component={Error}/>
                    </Switch>
                    <Footer/>
                    <Popups/>
                </div>
            </DocumentTitle>
        );
    }
}

Router.propTypes = {
    user: PropTypes.object.isRequired,
    forceLoadUser: PropTypes.func.isRequired,
    onUserSignIn: PropTypes.func.isRequired,
    onUserSignUp: PropTypes.func.isRequired,
    onUserLogOut: PropTypes.func.isRequired,
    onUserForgotPasswordRequest: PropTypes.func.isRequired,
    onUserForgotPasswordResetPassword: PropTypes.func.isRequired,
    router: PropTypes.object
};

export default withRouter(Router);

