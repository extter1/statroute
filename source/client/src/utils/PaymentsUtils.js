const plans = {
    'monthly': {
        paid: 'Per Month',
        price: 1499,
        promoPrice: 799,
        isBig: false,
        couponCode: 'PROMO_MONTHLY',
        interval: 30
    },
    'yearly': {
        paid: 'Per Year',
        price: 4999,
        promoPrice: 2499,
        isBig: true,
        couponCode: 'PROMO_YEARLY',
        interval: 365
    },
    'lifetime': {
        paid: 'Once-only',
        price: 19999,
        promoPrice: 9999,
        isBig: false
    }
};

const keys = {
    PUBLIC: process.env.STRIPE_PUBLIC || 'pk_test_DqOvEqQ6lALmGEIXPvaTRGM4',
    SECRET: process.env.STRIPE_SECRET
};

module.exports = {
    plans: plans,
    keys: keys
};