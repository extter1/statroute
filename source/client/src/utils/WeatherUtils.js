const directions = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW', 'N'];

module.exports = {
    getWindDegrees: function (direction) {
        return (directions.indexOf(direction.toUpperCase()) * 22.5) % 360;
    },
    translateTempCode: function (tempCode) {
        switch (tempCode) {
            case 'J':
                return 'Very Cold';
            case 'K':
                return 'Cold';
            case 'L':
                return 'Fair';
            case 'M':
                return 'Warm';
            case 'N':
                return 'Hot';
            case 'O':
                return 'Very Hot';

            case 'S':
                return 'Clear';
            case 'T':
                return 'Overcast';
            case 'U':
                return 'Rain';
            case 'V':
                return 'Heavy Rain';
            case 'W':
                return 'Snow';
            case 'Y':
                return 'Heavy Snow';
            default:
                return 'Unknown';
        }
    },
    translateDirection: function (direction) {
        switch (direction) {
            case 'N':
                return 'North';
            case 'NNE':
                return 'North-northeast';
            case 'NE':
                return 'Northeast';
            case 'ENE':
                return 'East-northeast';
            case 'E':
                return 'East';
            case 'ESE':
                return 'East-southeast';
            case 'SE':
                return 'Southeast';
            case 'SSE':
                return 'South-southeast';
            case 'S':
                return 'South';
            case 'SSW':
                return 'South-southwest';
            case 'SW':
                return 'Southwest';
            case 'WSW':
                return 'West-southwest';
            case 'W':
                return 'West';
            case 'WNW':
                return 'West-northwest';
            case 'NW':
                return 'Northwest';
            case 'NNW':
                return 'North-northwest';
            default:
                return 'Unknown';
        }
    }
};
