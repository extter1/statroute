module.exports = {
    SEASON:{
   'THIS_SEASON': 'This season',
   '1_SEASON': 'Past Season',
   '3_SEASONS': 'Past 3 Seasons',
   '5_SEASONS': 'Past 5 Seasons',
   '10_SEASONS': 'Past 10 Seasons'
    },
    THI_SEASON: {
        'ALL_WEEKS': 'All Weeks',
        '2_WEEKS': 'Past 2 Weeks',
        '4_WEEKS': 'Past 4 Weeks',
        '8_WEEKS': 'Past 8 Weeks',
        '12_WEEKS': 'Past 12 Weeks',
        },
        OTHR_SEASON: {
            'ALL_WEEKS': 'All Weeks',
            'FIRST_HALF': 'First Half',
            'SECOND_HALF': 'Second Half',
            'FANASY_PLAYOFF': 'Fantasy Playoffs',
            'NFL_PLAYOFF': 'NFL Playoffs',
            },
};