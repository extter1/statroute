module.exports = {
    WEEKS: {
        'ALL': 'All weeks',
        'WEEKS_1_6': 'Weeks 1-6',
        'WEEKS_7_13': 'Weeks 7-13',
        'WEEKS_14_16': 'Weeks 14-16',
        'PLAYOFFS': 'NFL Playoffs'
    },

    QUARTERS: {
        'ALL': 'All Quarters',
        '1_QUATER': '1st Quarter',
        '2_QUATER': '2nd Quarter',
        '3_QUATER': '3rd Quarter',
        '4_QUATER': '4rd Quarter',
        'OT': 'Overtime'
    },
    QUATER:{
        '1_QUATER':'1st Quater',
        '2_QUATER':'2nd Quater'
    },
    HALFS:{
        '1_HALF':'1st Half',
        '2_HALF':'2nd Half'
    },
    FIELDS:{
        'HOME':'Home',
        'AWAY':'Away',
        'INDOOR':'Indoor',
        'OUTDOOR':'Outdoor',
        'GRASS':'Grass',
        'TUFF':'Tuff'
    },
    SCENARIO:{
        'PRIMETIME':'Prime Time',
        'REDZONE':'Red Zone'
    }
};