module.exports = {
    ARI:
        {
            fullName: 'Arizona Cardinals',
            team: 'nfcwest',
            index: 12,
            HC: 'Bruce Arians',
            OC: 'Harold Goodwin',
            DC: 'James Bettcher',
            color: '97233F'
        },
    ATL:
        {
            fullName: 'Atlanta Falcons',
            team: 'nfcsouth',
            index: 8,
            HC: 'Dan Quinn',
            OC: 'Steve Sarkisian',
            DC: 'Marquand Manuel',
            color: 'A6192E'
        },
        DET:
        {
            fullName: 'Detroit Lions',
            team: 'nfcnorth',
            index: 16,
            HC: 'Matt Patricia',
            OC: 'Jim Bob Cooter',
            DC: 'Paul Pasqualoni',
            color: 'A6192E'
        },
    BAL:
        {
            fullName: 'Baltimore Ravens',
            team: 'afcnorth',
            index: 4,
            HC: 'John Harbaugh',
            OC: 'Marty Mornhinweg',
            DC: 'Dean Pees',
            color: '241773'
        },
    BUF:
        {
            fullName: 'Buffalo Bills',
            team: 'afceast',
            index: 0,
            HC: 'Sean McDermott',
            OC: 'Rick Dennison',
            DC: 'Leslie Frazier',
            color: '00338D'
        },
    CAR:
        {
            fullName: 'Carolina Panthers',
            team: 'nfcsouth',
            index: 9,
            HC: 'Ron Rivera',
            OC: 'Mike Shula',
            DC: 'Steve Wilks',
            color: '0085CA'
        },
    CHI:
        {
            fullName: 'Chicago Bears',
            team: 'nfcnorth',
            index: 4,
            HC: 'John Fox',
            OC: 'Dowell Loggains',
            DC: 'Vic Fangio',
            color: '051C2C'
        },
    CIN:
        {
            fullName: 'Cincinnati Bengals',
            team: 'afcnorth',
            index: 5,
            HC: 'Marvin Lewis',
            OC: 'Ken Zampese',
            DC: 'Paul Guenther',
            color: 'FC4C02'
        },
    CLE:
        {
            fullName: 'Cleveland Browns',
            team: 'afcnorth',
            index: 6,
            HC: 'Hue Jackson',
            OC: 'Hue Jackson',
            DC: 'Gregg Williams',
            color: '382F2D'
        },
    DAL:
        {
            fullName: 'Dallas Cowboys',
            team: 'nfceast',
            index: 0,
            HC: 'Jason Garrett',
            OC: 'Scott Linehan',
            DC: 'Rod Marinelli',
            color: '003594'
        },
    DEN:
        {
            fullName: 'Denver Broncos',
            team: 'afcwest',
            index: 12,
            HC: 'Vance Joseph',
            OC: 'Mike McCoy',
            DC: 'Joe Woods',
            color: 'FC4C02'
        },
   
    JAX:
        {
            fullName: 'Jacksonville Jaguars',
            team: 'afcsouth',
            index: 10,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
    KAN:
        {
            fullName: 'Kansas City Chiefs',
            team: 'afcwest',
            index: 13,
            HC: 'Andy Reid',
            OC: 'Matt Nagy',
            DC: 'Bob Sutton',
            color: 'C8102E'
        },
    LAC:
        {
            fullName: 'Los Angeles Chargers',
            team: 'afcwest',
            index: 14,
            HC: 'Anthony Lynn',
            OC: 'Ken Whisenhunt',
            DC: 'Gus Bradley',
            color: '0C2340'
        },
    SDG:
        {
            fullName: 'Los Angeles Chargers',
            team: true,
            index: 14,
            HC: 'Anthony Lynn',
            OC: 'Ken Whisenhunt',
            DC: 'Gus Bradley',
            color: '0C2340'
        },
    LAR:
        {
            fullName: 'Los Angeles Rams',
            team: 'nfcwest',
            index: 13,
            HC: 'Sean McVay',
            OC: 'Matt LaFleur',
            DC: 'Wade Phillips',
            color: '002244'
        },
    MIA:
        {
            fullName: 'Miami Dolphins',
            team: 'afceast',
            index: 1,
            HC: 'Adam Gase',
            OC: 'Clyde Christensen',
            DC: 'Matt Burke',
            color: '008E97'
        },
    MIN:
        {
            fullName: 'Minnesota Vikings',
            team: 'nfcnorth',
            index: 7,
            HC: 'Mike Zimmer',
            OC: 'Pat Shurmur',
            DC: 'George Edwards',
            color: '512D6D'
        },
    NEW:
        {
            fullName: 'New England Patriots',
            team: 'afceast',
            index: 2,
            HC: 'Bill Belichick',
            OC: 'Josh McDaniels',
            DC: 'Matt Patricia',
            color: '0C2340'
        },
    NOR:
        {
            fullName: 'New Orleans Saints',
            team: 'nfcsouth',
            index: 10,
            HC: 'Sean Payton',
            OC: 'Pete Carmichael, Jr.',
            DC: 'Dennis Allen',
            color: 'A28D5B'
        },
    NYG:
        {
            fullName: 'New York Giants',
            team: 'nfceast',
            index: 1,
            HC: 'Ben McAdoo',
            OC: 'Mike Sullivan',
            DC: 'Steve Spagnuolo',
            color: '001E62'
        },
    NYJ:
        {
            fullName: 'New York Jets',
            team: 'afceast',
            index: 3,
            HC: 'Todd Bowles',
            OC: 'John Morton',
            DC: 'Kacy Rodgers',
            color: '0C371D'
        },
    OAK:
        {
            fullName: 'Oakland Raiders',
            team: 'afcwest',
            index: 15,
            HC: 'Jack Del Rio',
            OC: 'Todd Downing',
            DC: 'Ken Norton, Jr.',
            color: '101820'
        },
    PHI:
        {
            fullName: 'Philadelphia Eagles',
            team: 'nfceast',
            index: 2,
            HC: 'Doug Pederson',
            OC: 'Frank Reich',
            DC: 'Jim Schwartz',
            color: '004851'
        },
    PIT:
        {
            fullName: 'Pittsburgh Steelers',
            team: 'afcnorth',
            index: 7,
            HC: 'Mike Tomlin',
            OC: 'Todd Haley',
            DC: 'Keith Butler',
            color: 'FFB81C'
        },
    SFO:
        {
            fullName: 'San Francisco 49ers',
            team: 'nfcwest',
            index: 14,
            HC: 'Kyle Shanahan',
            OC: 'Kyle Shanahan',
            DC: 'Robert Saleh',
            color: '9B2743'
        },
    SEA:
        {
            fullName: 'Seattle Seahawks',
            team: 'nfcwest',
            index: 15,
            HC: 'Pete Carroll',
            OC: 'Darrell Bevell',
            DC: 'Kris Richard',
            color: '001433'
        },
    TAM:
        {
            fullName: 'Tampa Bay Buccaneers',
            team: 'nfcsouth',
            index: 11,
            HC: 'Dirk Koetter',
            OC: 'Todd Monken',
            DC: 'Mike Smith',
            color: 'C8102E'
        },
    TEN:
        {
            fullName: 'Tennessee Titans',
            team: 'afcsouth',
            index: 11,
            HC: 'Mike Mularkey',
            OC: 'Terry Robiskie',
            DC: 'Dick LeBeau',
            color: '0C2340'
        },
    WAS:
        {
            fullName: 'Washington Redskins',
            team: 'nfceast',
            index: 3,
            HC: 'Jay Gruden',
            OC: 'Matt Cavanaugh',
            DC: 'Greg Manusky',
            color: '862633'
        },
        HOU:
        {
            fullName: 'Houston Texans',
            team: 'afcsouth',
            index: 8,
            HC: 'Bill O\'Brien',
            OC: 'Bill O\'Brien',
            DC: 'Mike Vrabel',
            color: '091F2C'
        },
    IND:
        {
            fullName: 'Indianapolis Colts',
            team: 'afcsouth',
            index: 9,
            HC: 'Chuck Pagano',
            OC: 'Rob Chudzinski',
            DC: 'Ted Monachino',
            color: '001489'
        },
    JAX:
        {
            fullName: 'Jacksonville Jaguars',
            team: 'afcsouth',
            index: 10,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
        GNB:
        {
            fullName: 'Green Bay Packers',
            team: 'nfcnorth',
            index: 100,
            HC: 'Mike McCarthy',
            OC: 'Joe Philbi Mike',
            DC: 'Pettine',
            color: 'D49F12'
        },
        afceast:
        {
            fullName: 'AFC EAST',
            team: 'none',
            index: 100,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        }, 		 

        afcnorth:
        {
            fullName: 'AFC North',
            team: 'none',
            index: 101,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
        afcwest:
        {
            fullName: 'AFC West',
            team: 'none',
            index: 101,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
        afcsouth:
        {
            fullName: 'AFC South',
            team: 'none',
            index: 101,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
        nfceast:
        {
            fullName: 'NFC East',
            team: 'none',
            index: 101,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
        nfcnorth:
        {
            fullName: 'NFC North',
            team: 'none',
            index: 101,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
        nfcwest:
        {
            fullName: 'NFC West',
            team: 'none',
            index: 101,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
        nfcsouth:
        {
            fullName: 'NFC South',
            team: 'none',
            index: 101,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
        afc:
        {
            fullName: 'AFC',
            team: 'none',
            index: 101,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
        nfc:
        {
            fullName: 'NFC',
            team: 'none',
            index: 101,
            HC: 'Doug Marrone',
            OC: 'Nathaniel Hackett',
            DC: 'Todd Wash',
            color: 'D49F12'
        },
};
