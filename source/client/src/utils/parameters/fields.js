module.exports = {
   'ALL': 'All Field Splits',
   'HOME': 'Home',
   'AWAY': 'Away',
   'INDOOR': 'Indoor',
   'OUTDOOR': 'Outdoor',
   'TURF': 'Turf',
   'GRASS': 'Grass',
   'PRIMETIME': 'Primetime',
   'REDZONE': 'Redzone'
};