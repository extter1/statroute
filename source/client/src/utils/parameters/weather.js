module.exports = {
    TEMPERATURES: {
        'ALL': 'All Temperatures',
        'COLD': 'Cold',
        'FAIR': 'Fair',
        'WARM': 'Warm',
        'HOT': 'Hot'

    },
    CONDITIONS: {
        'ALL': 'All Conditions',
        'CLEAR': 'Clear',
        'WET': 'Wet/Rain',
        'WINDY': 'Windy',
        'SNOW': 'Snow'
    }
};