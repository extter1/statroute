module.exports = {
  nflpcompare : [
                    {
                        "title": "Rank",
                        "field": "rownum"
                    },
                    {
                        "title": "Player",
                        "field": "player"
                    },
                    {
                        "title": "Position",
                        "field": "positions"
                    },
                    {
                        "title": "Team",
                        "field": "teams"
                    },
                    {
                        "title": "Age",
                        "field": "age"
                    },
                    {
                        "title": "Bye Week",
                        "field": "bye"
                    },
                    {
                        "title": "Games",
                        "field": "gamesplayed"
                    },
                    {
                        "title": "Fantasy PPG",
                        "field": "fantasyppg"
                    },
                    {
                        "title": "Fantasy PTS",
                        "field": "ffp"
                    },
                    {
                        "title": "Ceiling",
                        "field": "pceiling"
                    },
                    {
                        "title": "Floor",
                        "field": "pfloor"
                    },
                    {
                        "title": "TD's",
                        "field": "tds"
                    },
                    {
                        "title": "Pass Yards",
                        "field": "pyd"
                    },
                    {
                        "title": "Rush YDS/GM",
                        "field": "ruyd"
                    },
                    {
                        "title": "REC. Yards",
                        "field": "reyd"
                    },
                    {
                        "title": "Receptions",
                        "field": "receptions"
                    },
                    {
                        "title": "Actions",
                        "field": "actions"
                    },
                    {
                        "title": "Pass Attempts",
                        "field": "passingattempts"
                    },
                    {
                        "title": "Pass Intercept",
                        "field": "totalinterceptions"
                    },
                    {
                        "title": "Rush Attempts",
                        "field": "reydperattempt"
                    },
                    {
                        "title": "Pass Receptions",
                        "field": "fumbleslost"
                    },
                    {
                        "title": "Re Targets",
                        "field": "fumbleslost"
                    },
                    {
                        "title":"Field Attempts",
                        "field":"fieldgoalmade"
                    },
                    {
                        "title":"30+",
                        "field":"fgthirtyplus"
                    },
                    {
                        "title":"40+",
                        "field":"fgfortyplus"
                    },
                    ,
                    {
                        "title":"50+",
                        "field":"fgfiftyplus"
                    }
                ],
nfltcompare:[{
        "title": "Rank",
        "field": "rank"
      },
      {
        "title": "Team",
        "field": "team"
      },
      {
        "title": "Games",
        "field": "gamesplayed"
      },
      {
        "title": "Fant PPG",
        "field": "dffpg"
      },
      {
        "title": "Points",
        "field": "points"
      },
      {
        "title": "Points/Gm",
        "field": "dppg"
      },
      {
        "title": "Defensive Plays",
        "field": "dplays"
      },
      {
        "title": "Total Yards Allowed",
        "field": "yardsallowed"
      },
      {
        "title": "Pass Yards Allowed",
        "field": "pyardsallowed"
      },
      {
        "title": "Rush Yards Allowed",
        "field": "ryardsallowed"
      },
      {
        "title": "Defensive TD's",
        "field": "deftds"
      },
      {
        "title": "Interceptions",
        "field": "intercepts"
      },
      {
        "title": "Sacks",
        "field": "sacks"
      },
      {
        "title": "Safeties",
        "field": "safeties"
      }]
        
        
};
