import React, {Component} from 'react';
import Popup from 'react-popup';
import Promise from 'promise';
import {BrowserRouter} from 'react-router-dom';
import axios from 'axios';
import {StripeProvider} from 'react-stripe-elements';

import Router from './components/Router';
import IframePopup from './components/Account/BillingInformation/IframePopup';

import {keys} from './utils/PaymentsUtils';

import './App.css';
import PopupMessage from './components/PopupMessage';

class App extends Component {

    constructor() {
        super();

        this.state = {
            user: {
                loggedIn: false,
                premium: false,
            },
            loading: true,
            signupinfo:{
                ip:'',
                location:'',
                device:''
            }
        };
        
        
        this._loadUser();

        this.singingIn = false;
        this.singingUp = false;
    }

    componentDidMount() {
        this.getIP();
        console.log('mount');
        this.interval = setInterval(this._loadUser.bind(this), 3 * 60 * 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    handleSignUp(values) {
        if(this.singingUp) return Promise.reject();
        this.singingUp = true;

        const success = (
            <PopupMessage title={'Sign up was successful'}>
                <h2>You can now continue to sign in and start using our service.</h2>
            </PopupMessage>
        );

        const error = (
            <PopupMessage title={'Error occurred'}>
                <h2>Error occurred while creating your account. Please try again later.</h2>
            </PopupMessage>
        );

        const paymentError = (
            <PopupMessage title={'Error occurred'}>
                <h2>Error occurred while authorizing your card. Please try again later.</h2>
            </PopupMessage>
        );

        return new Promise((resolve, reject) => {
            axios.post('/api/auth/signUp', values).then((result) => {
                this.singingUp = false;
                if (result.data.error) {
                    reject(result.data.error);
                } else if (result.data.data.three_d_secure) {
                    const three_d_secure = result.data.data.three_d_secure;

                    Popup.plugins().component(
                        <IframePopup src={three_d_secure.redirect.url} title="3D Secure validation"
                                     onClose={(result) => {
                                         Popup.close();
                                         // TODO: show Popup / success message
                                         if(!result) Popup.plugins().component(paymentError);
                                         Popup.plugins().component(success);
                                         resolve();
                                     }}/>, false, false);
                } else {
                    // TODO: show Popup / success message
                    Popup.plugins().component(success);
                    resolve();
                }
            }, () => {
                this.singingUp = false;
                Popup.plugins().component(error);
                reject();
            });
        });
    }

    handleSignIn(values) {
        if(this.singingIn) return Promise.reject();
        this.singingIn = true;

        return new Promise((resolve, reject) => {
            this.singingIn = true;
            axios.post('/api/auth/signIn', values).then(() => {
                this.singingIn = false;
                setTimeout(function() {  this._loadUser().then(() => resolve()); }.bind(this), 2000);
               
            }, () => {
                this.singingIn = false;
                reject();
            });
        });
    }

    handleLogOut() {
        return new Promise((resolve, reject) => {
            axios.delete('/api/auth/signIn').then(() => {
                this._setUser();
                resolve();
            }, () => {
                this._setUser();
                reject();
            });
        });
    }

    handleForgotPasswordRequest(emailOrUsername) {
        return new Promise((resolve, reject) => {
            axios.post('/api/auth/forgotPasswordRequest', {
                emailorusername: emailOrUsername
            }).then(() => {
                resolve();
            }).catch(() => {
                reject();
            });
        });
    }

    handleForgotPasswordResetPassword(hash, newPassword) {
        return new Promise((resolve, reject) => {
            axios.post('/api/auth/forgotPasswordReset', {
                hash: hash,
                password: newPassword
            }).then(() => {
                resolve();
            }, () => {
                reject();
            });
        });
    }

    forceLoadUser() {
        this._loadUser();
    }
        getIP() {
            console.log('enteres');
        return axios.get('http://freegeoip.net/json/').then((res) => {
        console.log(res);
         this._setIP(res.data);
        }, () => {
        });
    }

    _loadUser() {
        return axios.get('/api/auth/user').then((res) => {
            if (res.data.status) {
                this._setUser(res.data.data);
            } else {
                this._setUser();
            }
        }, () => {
            this._setUser();
        });
    }
_setIP(data){
var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
if(isMobile){
     const device = 'Mobile';
     const signupinfo = {
                ip:data.ip,
                location:data.country_name +' , '+ data.region_name,
                device: device
            };

            this.setState({  signupinfo:signupinfo})
}
else{
const device = 'desktop';
const signupinfo = {
                ip:data.ip,
                location:data.country_name +' , '+ data.region_name,
                device: device
            };

            this.setState({  signupinfo:signupinfo})
}

           
}
    _setUser(userData) {
        if (!userData) {
            const user = {
                loggedIn: false,
                premium: false
            };

            this.setState({
                loading: false,
                user: user
            });
        } else {
            const premium = userData.accounttype !== 1;
            const user = {
                loggedIn: true,
                premium: premium,
                data: userData
            };

            this.setState({
                loading: false,
                user: user
            });
        }
    }

    render() {
        return (
            <StripeProvider apiKey={keys.PUBLIC}>
                <BrowserRouter>
                    {!this.state.loading && <Router user={this.state.user} signupinfo={this.state.signupinfo}
                                                    forceLoadUser={this.forceLoadUser.bind(this)}
                                                    onUserSignUp={this.handleSignUp.bind(this)}
                                                    onUserSignIn={this.handleSignIn.bind(this)}
                                                    onUserLogOut={this.handleLogOut.bind(this)}
                                                    onUserForgotPasswordRequest={this.handleForgotPasswordRequest.bind(this)}
                                                    onUserForgotPasswordResetPassword={this.handleForgotPasswordResetPassword.bind(this)}
                    />}
                </BrowserRouter>
            </StripeProvider>
        );
    }
}

export default App;
